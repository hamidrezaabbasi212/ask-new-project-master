<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdvanceLaw extends Model
{
    protected $fillable = ['link','title'];
}
