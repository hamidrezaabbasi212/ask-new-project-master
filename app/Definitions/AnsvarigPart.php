<?php

namespace App\Definitions;

use Illuminate\Database\Eloquent\Model;

class AnsvarigPart extends Model
{
    protected $fillable = ['title'];



    public function scopeSearchAnsvarig($query , $keywords)
    {
        $query->where('title' , 'LIKE' , '%' . $keywords . '%');
        return $query;
    }
}
