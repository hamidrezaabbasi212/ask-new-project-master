<?php

namespace App\Definitions;

use Illuminate\Database\Eloquent\Model;

class TemplateCategory extends Model
{
    protected $guarded = [''];
}
