<?php

namespace App\Http\Controllers\Admin;

use App\AdvanceLaw;
use App\AdvanceLawDesign;
use App\Definitions\AnsvarigPart;
use App\Definitions\Kontrollskede;
use App\Definitions\KraverPlatsbesok;
use App\Definitions\PBLCategory;
use App\Definitions\ProduceratIntyg;
use App\Definitions\Risk;
use App\Definitions\Verifieringsmetod;
use App\design;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AmaDesignController extends Controller
{
    public function search()
    {
        $keywords = request('keywords');
        $Laws = DB::table('advance_laws')
            ->whereRaw('title like  "%'.$keywords.'%" or link like "%'.$keywords.'%"')
            ->get();


        $count = $Laws->count();

        $output = '';

        if (count($Laws) > 0)
        {
            $output = '<ul id="myid" style="height: 200px">';
            foreach ($Laws as $k => $law)
            {
                $output .= '<li style="display:inline-block" class="liclick2" onClick="appendConfirmationAmaLaw(\'' .$law->link. '\');select2(this)" value="'.$law->id.'"><i class="fa fa-arrow-right" aria-hidden="true"  data-value="'.$law->id.'"></i>'.$law->title.'</a></li> - <a target="_blank" href="'.$law->link.'">öppen länk</a></br>';
            }
            $output .= '</ul>';
        }
        else
        {
            $output .= '<li class="list-group-item">'.'No results'.'</li>';
        }
        return response()->json(['output' => $output , 'count' => $count]);
    }

    public function getLaw($id)
    {
        $law = AdvanceLaw::whereId($id)->first();
        return response()->json($law);
    }

    public function store(Request $request)
    {
        $amaDesign = AdvanceLawDesign::where('design_id' , $request->controllPoint)->get();
        if(count($amaDesign) < 6)
        {
            $advanceDesign = new AdvanceLawDesign();
            $advanceDesign->project_id = $request->project_id;
            $advanceDesign->design_id = $request->controllPoint;
            $advanceDesign->templateInformation = $request->amaLink;
            $advanceDesign->save();
            return response()->json('success' , 200);
        }
        else
        {
            return response()->json('error' , 200);
        }

    }

    public function getTemplate($id)
    {
        $design = design::where('project_id',$id)->get();
        $AnsvarigPart = '';
        foreach ($design as $row => $template)
        {
            $templateInformation [] = unserialize($template->templateInformation);
            $templateInformation [$row]['id'] = $template->id;

        }
        $output = '<option value="0">Välj kontrollpunkt/egenkontroll</option>';
        foreach ($templateInformation as $k => $row)
        {
            $pblcategory = PBLCategory::where('id',$row['PBLKategori'])->pluck('title')->first();
            if(array_key_exists('AnsvarigPart' ,$row))
            {
                $AnsvarigPart = AnsvarigPart::where('id',$row['AnsvarigPart'])->pluck('title')->first();
            }

            $Kontrollskede = Kontrollskede::where('id',$row['Kontrollskede'])->pluck('title')->first();
            $Risk = Risk::where('id',$row['Risk'])->pluck('title')->first();
            if($Risk == '')
            {
                $Risk = $row['riskText'];
            }

            $ProduceratIntyg = ProduceratIntyg::where('id',$row['ProduceratIntyg'])->pluck('title')->first();
            if($ProduceratIntyg == '')
            {
                $ProduceratIntyg = $row['ProduceratIntygText'];
            }

            $Verifieringsmetod = Verifieringsmetod::where('id',$row['Verifieringsmetod'])->pluck('title')->first();
            if($Verifieringsmetod == '')
            {
                $Verifieringsmetod = $row['VerifieringsmetodText'];
            }

            $KraverPlatsbesok = KraverPlatsbesok::where('id',$row['KraverPlatsbesok'])->pluck('title')->first();
            if($KraverPlatsbesok == '')
            {
                $KraverPlatsbesok = $row['KraverPlatsbesokText'];
            }


            $output .= '<option value="'.$row['id'].'">'.$row['BSABAMA'].' - '.$pblcategory.'
            - ' .$AnsvarigPart.' - '.$Kontrollskede.' - '.$Risk.'-'.$ProduceratIntyg.' - '.$Verifieringsmetod.' - '.$KraverPlatsbesok.'</option>';


        }
        return response()->json($output);
    }

    public function getDesignTemplate($id)
    {
        $advance = AdvanceLawDesign::where('design_id',$id)->pluck('templateInformation')->first();
        return response()->json($advance);
    }

    public function update($id,Request $request)
    {
        $advanceDesign = AdvanceLawDesign::where('design_id',$id)->first();
        $advanceDesign->templateInformation = $request->LawAmaText;
        $advanceDesign->save();
        return response()->json('success' , 200);
    }

    public function delete($id)
    {
        $delete = AdvanceLawDesign::where('id',$id)->delete();
        if ($delete == 1) {
            $success = true;
            $message = "design deleted successfully";
        } else {
            $success = true;
            $message = "design not found";
        }
        return response()->json([
            'success' => $success,
            'message' => $message,
        ]);
    }
}
