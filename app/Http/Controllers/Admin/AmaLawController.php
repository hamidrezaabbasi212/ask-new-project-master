<?php

namespace App\Http\Controllers\Admin;

use App\AdvanceLaw;
use App\AdvanceLawDesign;
use App\Http\Controllers\Controller;
use App\Imports\AdvanceLawImport;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\DataTables\DataTables;

class AmaLawController extends Controller
{

    public function index()
    {
        $amalaws = AdvanceLaw::paginate(20);
        return view('Admin.amaLaw.index',compact('amalaws'));
    }

    public function create()
    {
        return view('Admin.amaLaw.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'amalawexecel'  => 'required|mimes:xls,xlsx'
        ]);
        if ($request->file('amalawexecel')) {
            Excel::import(new AdvanceLawImport(),request()->file('amalawexecel'));
            return back();
        }
//        $amadesign = new AdvanceLaw();
//        $amadesign->Hanvisning = $request->title;
//        $amadesign->Text = $request->Text;
//        $amadesign->save();
//        Session::flash('success','Location Data Entered succesfully');
//        return redirect()->route('amaLaw.index');
    }

    public function edit($id)
    {
        $amadesign = AdvanceLaw::find($id);
        return view('Admin.amaLaw.edit',compact('amadesign'));
    }

    public function update($id,Request $request)
    {
        $advanceLaw = AdvanceLaw::where('id',$id)->first();
        $advanceLaw->Hanvisning = $request->title;
        $advanceLaw->Text = $request->Text;
        $advanceLaw->save();
        Session::flash('successUpdate','Location Data Entered succesfully');
        return redirect()->route('amaLaw.index');
    }

    public function delete(Request $request)
    {
        $advanceLaw = AdvanceLaw::where('id', $request->id)->delete();
        if($advanceLaw == 1)
        {
            return response()->json(['success'=>1],200);
        }
    }

    public function agentControllAmaLaw($id)
    {
         $amaLaw = AdvanceLawDesign::where('design_id',$id)->get();

        return DataTables::of($amaLaw)
            ->addColumn('link', function($data){
                return '<a target="_blank" href="'.$data->templateInformation.'">'.$data->templateInformation.'</a>';
            })
            ->addColumn('action',function ($data){
                $action = '<button type="button" onclick="deleteConfirmationAmaLaw('.$data->id.')"><i class="fa fa-times" aria-hidden="true"></i></button>';
                return $action;
            })
            ->rawColumns(['link','action'])

            ->make(true);
    }

}
