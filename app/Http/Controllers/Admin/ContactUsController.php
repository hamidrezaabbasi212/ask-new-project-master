<?php

namespace App\Http\Controllers\Admin;

use App\Contact;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ContactUsController extends Controller
{
    public function store(Request $request)
    {
        $contact = Contact::take(1)->first();
        if(!$contact)
        {
            Contact::create([
                'address' => $request->contactAddress,
                'phone' => $request->contactPhone,
                'post' => $request->contactPost,
                'contactUsTime' => $request->contactUsTime,
            ]);
        }
        else
        {
            $contact->address = $request->contactAddress;
            $contact->phone = $request->contactPhone;
            $contact->post = $request->contactPost;
            $contact->contactUsTime = $request->contactUsTime;
            $contact->save();
        }
    }

    public function getContactInformation()
    {
        $contact = Contact::take(1)->first();

        return response()->json($contact);
    }
}
