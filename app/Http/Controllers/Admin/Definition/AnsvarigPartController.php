<?php

namespace App\Http\Controllers\Admin\Definition;

use App\Definitions\AnsvarigPart;
use App\Http\Controllers\Controller;
use App\Imports\AnsvarigPartImport;
use App\Imports\LawsImport;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class AnsvarigPartController extends Controller
{
    public function index()
    {
        return view('Admin.definitions.ansvarigPart.index');
    }


    public function getData()
    {
        $ansvarigPart = AnsvarigPart::latest()->get();
        return response()->json($ansvarigPart);
    }


    public function store(Request $request)
    {
        $ansvarigPart = new AnsvarigPart();
        $ansvarigPart->title = $request->title;
        $ansvarigPart->save();
    }


    public function update($id,Request $request)
    {
        $ansvarigPart =  AnsvarigPart::find($request->id);
        $ansvarigPart->title = $request->title;
        $ansvarigPart->save();
    }


    public function delete($id,Request $request)
    {
        $ansvarigPart =  AnsvarigPart::find($request->id);
        $ansvarigPart->delete();
    }


    public function import(Request $request)
    {
        $this->validate($request, [
            'ansvarigPart'  => 'required|mimes:xls,xlsx'
        ]);
        $path = $request->file('ansvarigPart')->getRealPath();


        if ($request->file('ansvarigPart')) {
            Excel::import(new AnsvarigPartImport(),request()->file('ansvarigPart'));
            return back();
        }
    }


}
