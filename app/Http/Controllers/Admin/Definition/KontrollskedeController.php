<?php

namespace App\Http\Controllers\Admin\Definition;

use App\Definitions\Kontrollskede;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class KontrollskedeController extends Controller
{
    public function index()
    {
        return view('Admin.definitions.kontrollskede.index');
    }


    public function getData()
    {
        $kontrollskedes = Kontrollskede::latest()->get();
        return response()->json($kontrollskedes);
    }


    public function store(Request $request)
    {
        $kontrollskedes = new Kontrollskede();
        $kontrollskedes->title = $request->title;
        $kontrollskedes->save();
    }


    public function update($id,Request $request)
    {
        $kontrollskedes =  Kontrollskede::find($request->id);
        $kontrollskedes->title = $request->title;
        $kontrollskedes->save();
    }


    public function delete($id,Request $request)
    {
        $kontrollskedes =  Kontrollskede::find($request->id);
        $kontrollskedes->delete();
    }

}
