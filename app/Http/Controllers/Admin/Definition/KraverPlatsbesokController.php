<?php

namespace App\Http\Controllers\Admin\Definition;

use App\Definitions\KraverPlatsbesok;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class KraverPlatsbesokController extends Controller
{
    public function index()
    {
        return view('Admin.definitions.kraverplatsbesok.index');
    }


    public function getData()
    {
        $kraverplatsbesok = KraverPlatsbesok::latest()->get();
        return response()->json($kraverplatsbesok);
    }


    public function store(Request $request)
    {
        $kraverplatsbesok = new KraverPlatsbesok();
        $kraverplatsbesok->title = $request->title;
        $kraverplatsbesok->save();
    }


    public function update($id,Request $request)
    {
        $kraverplatsbesok =  KraverPlatsbesok::find($request->id);
        $kraverplatsbesok->title = $request->title;
        $kraverplatsbesok->save();
    }


    public function delete($id,Request $request)
    {
        $kraverplatsbesok =  KraverPlatsbesok::find($request->id);
        $kraverplatsbesok->delete();
    }


}
