<?php

namespace App\Http\Controllers\Admin\Definition;

use App\Definitions\PBLCategory;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class PBLCategoryController extends Controller
{
    public function index()
    {

        return view('Admin.definitions.pblcategory.index');
    }

    public function getData()
    {
        $pblcategorys = PBLCategory::latest()->get();
        return response()->json($pblcategorys);
    }

    public function store(Request $request)
    {
        $pblcategory = new PBLCategory();
        $pblcategory->title = $request->title;
        $pblcategory->save();
    }



    public function update($id,Request $request)
    {
        $pblcategory =  PBLCategory::find($request->id);
        $pblcategory->title = $request->title;
        $pblcategory->save();
    }


    public function delete($id,Request $request)
    {
        $pblcategory =  PBLCategory::find($request->id);
        $pblcategory->delete();
    }
}
