<?php

namespace App\Http\Controllers\Admin\Definition;

use App\Definitions\ProduceratIntyg;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ProduceratIntygController extends Controller
{
    public function index()
    {
        return view('Admin.definitions.produceratIntygs.index');
    }


    public function getData()
    {
        $produceratIntygs = ProduceratIntyg::latest()->get();
        return response()->json($produceratIntygs);
    }


    public function store(Request $request)
    {
        $produceratIntygs = new ProduceratIntyg();
        $produceratIntygs->title = $request->title;
        $produceratIntygs->save();
    }


    public function update($id,Request $request)
    {
        $produceratIntygs =  ProduceratIntyg::find($request->id);
        $produceratIntygs->title = $request->title;
        $produceratIntygs->save();
    }


    public function delete($id,Request $request)
    {
        $produceratIntygs =  ProduceratIntyg::find($request->id);
        $produceratIntygs->delete();
    }

}
