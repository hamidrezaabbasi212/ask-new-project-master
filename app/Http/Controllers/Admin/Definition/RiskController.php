<?php

namespace App\Http\Controllers\Admin\Definition;

use App\Definitions\Risk;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class RiskController extends Controller
{
    public function index()
    {
        return view('Admin.definitions.risk.index');
    }


    public function getData()
    {
        $risks = Risk::latest()->get();
        return response()->json($risks);
    }


    public function store(Request $request)
    {
        $risks = new Risk();
        $risks->title = $request->title;
        $risks->save();
    }


    public function update($id,Request $request)
    {
        $risks =  Risk::find($request->id);
        $risks->title = $request->title;
        $risks->save();
    }


    public function delete($id,Request $request)
    {
        $risks =  Risk::find($request->id);
        $risks->delete();
    }

}
