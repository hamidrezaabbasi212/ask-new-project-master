<?php

namespace App\Http\Controllers\Admin\Definition;

use App\Definitions\TemplateCategory;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;

class TemplateCategoryController extends Controller
{
    public function index()
    {
        $categories = TemplateCategory::paginate(10);
        return view('Admin.definitions.TemplateCategory.index',compact('categories'));
    }

    public function getData(Request $request)
    {
        if($request->ajax())
        {
            $categories = TemplateCategory::paginate(2);
            return view('Admin.definitions.TemplateCategory.loadData', compact('categories'))->render();
        }
    }


    public function store(Request $request)
    {
        $avatar = '';
        $templateCategory = new TemplateCategory();
        if ($request->logo)
        {
            $avatar = $this->uploadImages($request->logo);
        }

        $templateCategory->title = $request->title;
        $templateCategory->logo = $avatar;
        $templateCategory->save();
    }


    public function update($id,Request $request)
    {
        $templateCategory =  TemplateCategory::find($request->id);
        $avatar = $templateCategory->logo;
        if ($request->hasFile('logo'))
        {
            $avatar = $this->uploadImages($request->logo);
        }
        $templateCategory->logo = $avatar;
        $templateCategory->title = $request->titleedit;
        $templateCategory->save();
    }


    public function delete($id,Request $request)
    {
        $delete =  TemplateCategory::where('id', $id)->delete();
        if ($delete == 1) {
            $success = true;
            $message = "TemplateCategory deleted successfully";
        } else {
            $success = true;
            $message = "TemplateCategory not found";
        }
        return response()->json([
            'success' => $success,
            'message' => $message,
        ]);
    }


    public function uploadImages($file)
    {
        $current_timestamp = Carbon::now()->timestamp;
        $imagePath = "/upload/";
        $filename = $current_timestamp . $file->getClientOriginalName();
        $file = $file->move(public_path($imagePath) , $filename);
        return $imagePath.$filename;
    }
}
