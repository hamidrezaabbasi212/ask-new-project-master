<?php

namespace App\Http\Controllers\Admin\Definition;

use App\Definitions\Verifieringsmetod;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class VerifieringsmetodController extends Controller
{
    public function index()
    {
        return view('Admin.definitions.verifieringsmetod.index');
    }


    public function getData()
    {
        $verifieringsmetods = Verifieringsmetod::latest()->get();
        return response()->json($verifieringsmetods);
    }


    public function store(Request $request)
    {
        $verifieringsmetods = new Verifieringsmetod();
        $verifieringsmetods->title = $request->title;
        $verifieringsmetods->save();
    }


    public function update($id,Request $request)
    {
        $verifieringsmetods =  Verifieringsmetod::find($request->id);
        $verifieringsmetods->title = $request->title;
        $verifieringsmetods->save();
    }


    public function delete($id,Request $request)
    {
        $verifieringsmetods =  Verifieringsmetod::find($request->id);
        $verifieringsmetods->delete();
    }
}
