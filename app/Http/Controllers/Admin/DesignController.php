<?php

namespace App\Http\Controllers\Admin;

use App\AdvanceLawDesign;
use App\Definitions\AnsvarigPart;
use App\Definitions\Kontrollskede;
use App\Definitions\KraverPlatsbesok;
use App\Definitions\PBLCategory;
use App\Definitions\ProduceratIntyg;
use App\Definitions\Risk;
use App\Definitions\Verifieringsmetod;
use App\design;
use App\Http\Controllers\Controller;
use App\Project\Organization;
use App\Project\Project;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Psy\Util\Str;
use Yajra\DataTables\DataTables;

class DesignController extends Controller
{
    public function store(Request $request)
    {

        $now = Carbon::now();
        $number_rand = rand(1000,999999);
        $guid_code = $now->format('Ymd').$number_rand;

        $design = new design();
        $data = $request->all();
        $data['KontrollplanPBL'] = '';
        $data['KontrollplanAvtal'] = '';
        if($request->importSelect == 'KontrollplanPBL')
        {
            $data['KontrollplanPBL'] = 'KontrollplanPBL';
        }
        elseif ($request->importSelect == 'KontrollplanAvtal')
        {
            $data['KontrollplanAvtal'] = 'KontrollplanAvtal';
        }

        $data['image'] = $request->hasFile('image') ? $this->uploadImages($request->file('image')) : '';
        $design->user_id = auth()->user()->id;
        $design->parent_id = auth()->user()->parent_id;
        $design->robrik_id = $request->robrik_id;
        $design->ka_id = $request->ka_id;
        $design->project_id = $data['project_id'];
        unset($data['project_id']);
        unset($data['_token']);
        $design->templateInformation =serialize($data);
        $design->guid = $guid_code;
        $array = [];
        $history = ['status'=> 'skapad' , 'time' => Carbon::now()->format('Y-m-d H:i')];
        array_push($array,$history);
        $design->history = serialize($array);
        $design->save();
        return response()->json('success' , 200);
    }


    public function getTemplate($id)
    {
        $design = design::where('project_id',$id)->get();
        $designid = $design->pluck('id');
        foreach ($design as $row => $template)
        {
            $templateInformation [] = unserialize($template->templateInformation);
            $templateInformation [$row]['id'] = $template->id;

        }
        $output = '';
        $AnsvarigPart = '';
        foreach ($templateInformation as $k => $row)
        {
            $pblcategory = PBLCategory::where('id',$row['PBLKategori'])->pluck('title')->first();

            if(array_key_exists('AnsvarigPart' ,$row))
            {
                $AnsvarigPart = AnsvarigPart::where('id',$row['AnsvarigPart'])->pluck('title')->first();
            }

            $Kontrollskede = Kontrollskede::where('id',$row['Kontrollskede'])->pluck('title')->first();
            $Risk = Risk::where('id',$row['Risk'])->pluck('title')->first();

            if($Kontrollskede == '')
            {
                $Kontrollskede = $row['kontrollskedeText'];
            }


            if($Risk == '')
            {
                $Risk = $row['riskText'];
            }
            $ProduceratIntyg = ProduceratIntyg::where('id',$row['ProduceratIntyg'])->pluck('title')->first();
            if($ProduceratIntyg == '')
            {
                $ProduceratIntyg = $row['ProduceratIntygText'];
            }
            $Verifieringsmetod = Verifieringsmetod::where('id',$row['Verifieringsmetod'])->pluck('title')->first();
            if($Verifieringsmetod == '')
            {
                $Verifieringsmetod = $row['VerifieringsmetodText'];
            }
            $KraverPlatsbesok = KraverPlatsbesok::where('id',$row['KraverPlatsbesok'])->pluck('title')->first();
            if($KraverPlatsbesok == '')
            {
                $KraverPlatsbesok = $row['KraverPlatsbesokText'];
            }

            $output .= '<li id="controlplan'.$row['id'].'" class="liclick" onClick="getTemplateid('.$row['id'].');select(this)"><i class="fa fa-arrow-right" aria-hidden="true"></i>'.$row['BSABAMA'].' - '.$pblcategory.'
            - ' .$AnsvarigPart.' - '.$Kontrollskede.' - '.$Risk.'-'.$ProduceratIntyg.' - '.$Verifieringsmetod.' - '.$KraverPlatsbesok.'</li>';
        }
        return response()->json(['output' =>$output,'design' => $designid]);
    }


    public function getTemp($id)
    {
        $design = design::whereId($id)->first();
        $template = unserialize($design->templateInformation);
        $template['id'] = $design->id;
        $template['robrik_id'] = $design->robrik_id;
        $template['ka_id'] = $design->ka_id;
        return response()->json($template);
    }


    public function update($id,Request $request)
    {
        $importSelect = $request->importSelect;
        $design = design::whereId($id)->first();
        $design->robrik_id = $request->robrik_id;
        $design->ka_id = $request->ka_id;
        $designItem = unserialize($design->templateInformation);
        $data = $request->all();
        unset($data['importSelect']);
        unset($data['_token']);
        $designItem['BSABAMA'] = $data['BSABAMA'];
        $designItem['Kontrollskede'] = isset($data['Kontrollskede']) ? $data['Kontrollskede'] : '';

        $designItem['LawText'] = $data['LawText'];
        $designItem['PBLKategori'] = isset($data['PBLKategori']) ? $data['PBLKategori'] : '';

        $designItem['AnsvarigPart'] = isset($data['AnsvarigPart']) ? $data['AnsvarigPart'] : '';
        $designItem['AnsvarigPartText'] = isset($data['AnsvarigPartText']) ? $data['AnsvarigPartText'] : '';

        $designItem['Risk'] = isset($data['Risk']) ? $data['Risk'] : '';
        $designItem['riskText'] = $data['riskText'];
        $designItem['riskDescription'] = $data['riskDescription'];

        $designItem['ProduceratIntyg'] = isset($data['ProduceratIntyg']) ? $data['ProduceratIntyg'] : '';
        $designItem['ProduceratIntygText'] = $data['ProduceratIntygText'];
        $designItem['ProduceratIntygDescription'] = $data['ProduceratIntygDescription'];

        $designItem['Verifieringsmetod'] = isset($data['Verifieringsmetod']) ? $data['Verifieringsmetod'] : '';
        $designItem['VerifieringsmetodText'] = $data['VerifieringsmetodText'];
        $designItem['VerifieringsmetodDescription'] = $data['VerifieringsmetodDescription'];

        $designItem['KraverPlatsbesok'] = isset($data['KraverPlatsbesok']) ? $data['KraverPlatsbesok'] : '';
        $designItem['KraverPlatsbesokText'] = $data['KraverPlatsbesokText'];
        $designItem['KraverPlatsbesokDescription'] = $data['KraverPlatsbesokDescription'];

        if(array_key_exists('Funktionskrav',$designItem))
        {
            $designItem['Funktionskrav'] = $data['Funktionskrav'];
        }

        if(isset($importSelect) and $importSelect !=null)
        {
            if($importSelect == 'KontrollplanPBL')
            {
                $designItem['KontrollplanPBL'] = 'KontrollplanPBL';
                $designItem['KontrollplanAvtal'] = '';
            }
            else
            {
                $designItem['KontrollplanAvtal'] = 'KontrollplanAvtal';
                $designItem['KontrollplanPBL'] = '';
            }
        }
        else
        {
            $designItem['KontrollplanAvtal'] = '';
            $designItem['KontrollplanPBL'] = '';
        }

        try {
            $designItem['image'] = $request->hasFile('image') ? $this->uploadImages($request->file('image')) : $designItem['image'];
        }
        catch (\Exception $e)
        {
            $designItem['image'] = $request->hasFile('image') ? $this->uploadImages($request->file('image')) : $designItem['image'];
        }

        $design->templateInformation =serialize($designItem);
        $design->save();
        return unserialize($design->templateInformation);
    }


    public function delete($id)
    {
        $delete =  design::where('id', $id)->delete();
        if ($delete == 1) {
            $success = true;
            $message = "design deleted successfully";
        } else {
            $success = true;
            $message = "design not found";
        }
        return response()->json([
            'success' => $success,
            'message' => $message,
        ]);
    }


    public function getTemplateKontrollPlan($id)
    {
        $design = design::where('project_id',$id)->get();
        $AnsvarigPart = '';
        foreach ($design as $row => $template)
        {
            $templateInformation [] = unserialize($template->templateInformation);
            $pblcategory = PBLCategory::where('id',$templateInformation[$row]['PBLKategori'])->pluck('title')->first();
            if(array_key_exists('AnsvarigPart' , $templateInformation[$row]))
            {
                $AnsvarigPart = AnsvarigPart::where('id',$templateInformation[$row]['AnsvarigPart'])->pluck('title')->first();
            }

            $status = $template->status;

            $Kontrollskede = Kontrollskede::where('id',$templateInformation[$row]['Kontrollskede'])->pluck('title')->first();
            if($Kontrollskede == '')
            {
                $Kontrollskede = $templateInformation[$row]['kontrollskedeText'];
            }
            $Risk = Risk::where('id',$templateInformation[$row]['Risk'])->pluck('title')->first();
            if($Risk == '')
            {
                $Risk = $templateInformation[$row]['riskText'];
            }
            else
            {
                $riskDescription = $templateInformation[$row]['riskDescription'] ? $templateInformation[$row]['riskDescription'] : '';
                $Risk = $Risk ? $Risk . '-' . $riskDescription : $Risk;
            }

            $ProduceratIntyg = ProduceratIntyg::where('id',$templateInformation[$row]['ProduceratIntyg'])->pluck('title')->first();
            if($ProduceratIntyg == '')
            {
                $ProduceratIntyg = $templateInformation[$row]['ProduceratIntygText'];
            }
            else
            {
                $ProduceratIntygDescription = $templateInformation[$row]['ProduceratIntygDescription'] ? $templateInformation[$row]['ProduceratIntygDescription'] : '';
                $ProduceratIntyg = $ProduceratIntyg ? $ProduceratIntyg . '-' . $ProduceratIntygDescription : $ProduceratIntyg;
            }

            $Verifieringsmetod = Verifieringsmetod::where('id',$templateInformation[$row]['Verifieringsmetod'])->pluck('title')->first();
            if($Verifieringsmetod == '')
            {
                $Verifieringsmetod = $templateInformation[$row]['VerifieringsmetodText'];
            }
            else
            {
                $VerifieringsmetodDescription = $templateInformation[$row]['VerifieringsmetodDescription'] ? $templateInformation[$row]['VerifieringsmetodDescription'] : '';
                $Verifieringsmetod = $Verifieringsmetod ? $Verifieringsmetod . '-' . $VerifieringsmetodDescription : $Verifieringsmetod;
            }

            $KraverPlatsbesok = KraverPlatsbesok::where('id',$templateInformation[$row]['KraverPlatsbesok'])->pluck('title')->first();
            if($KraverPlatsbesok == '')
            {
                $KraverPlatsbesok = $templateInformation[$row]['KraverPlatsbesokText'];
            }
            else
            {
                $KraverPlatsbesokDescription = $templateInformation[$row]['KraverPlatsbesokDescription'] ? $templateInformation[$row]['KraverPlatsbesokDescription'] : '';
                $KraverPlatsbesok = $KraverPlatsbesokDescription ? $KraverPlatsbesok . '-' . $KraverPlatsbesokDescription : $KraverPlatsbesokDescription;
            }

            $templateInformation [$row]['id'] = $template->id;
            $templateInformation [$row]['assign_user'] = $template->assign_user;

            $templateInformation [$row]['PBLKategori'] =$pblcategory;
            $templateInformation [$row]['AnsvarigPart'] = $AnsvarigPart;
            $templateInformation [$row]['Kontrollskede'] = $Kontrollskede;
            $templateInformation [$row]['Risk'] = $Risk;
            $templateInformation [$row]['ProduceratIntyg'] = $ProduceratIntyg;
            $templateInformation [$row]['Verifieringsmetod'] = $Verifieringsmetod;
            $templateInformation [$row]['KraverPlatsbesok'] = $KraverPlatsbesok;
            $templateInformation [$row]['status'] = $status;
            $templateInformation [$row]['history'] = unserialize($template->history);
            $statusTitle = '';
            if($status == 0)
            {
                $statusTitle = 'Ej tilldelad';
//              $statusTitle = 'Not assigned';
            }
            elseif ($status == 1)
            {
                $statusTitle = 'Tilldelad';
                $statusTitle = 'Assigned';
            }
            elseif ($status == 2)
            {
                $statusTitle = 'Mottaget';
//              $statusTitle = 'Received';
            }
            elseif ($status == 3)
            {
                $statusTitle = 'Godkänt innehåll';
//              $statusTitle = 'Approved Content';
            }
            elseif ($status == 4)
            {
                $statusTitle = 'Funktion uppfylld';
//            $statusTitle = 'Function fulfilled';
            }
            elseif ($status == 5)
            {
                $statusTitle = 'Admin Vidimera';
//                $statusTitle = 'Admin Vidimera';
            }
            elseif ($status == 6)
            {
                $statusTitle = 'KA Utlåtande';
//              $statusTitle = 'KA Statement';
            }
            elseif ($status == 7)
            {
                $statusTitle = 'Byggherren fastställer';
//              $statusTitle = 'The builder states';
            }
            elseif ($status == 8)
            {
                $statusTitle = 'Slutbesked ges';
//              $statusTitle = 'Final message given';
            }
            elseif ($status == 9)
            {
                $statusTitle = 'Egenkontroll Underkänt';
//              $statusTitle = 'Self Check Fail';
            }
            elseif ($status == 10)
            {
                $statusTitle = 'Avvisa';
//              $statusTitle = 'Reject';
            }
            $templateInformation [$row]['statusTitle'] = $statusTitle;

        }
        return DataTables::of($templateInformation)
            ->addColumn('BSABAMA', function($data){
                return '<div data-toggle="modal" title="'.$data['BSABAMA'].'" data-target="#new1384">'.\Illuminate\Support\Str::limit($data['BSABAMA'],30).'</div>';
            })
            ->addColumn('PBLKategori', function($data){
                return '<div data-toggle="modal" title="'.$data['PBLKategori'].'" data-target="#new1384">'.$data['PBLKategori'].'</div>';
            })
            ->addColumn('AnsvarigPart', function($data){
                return '<div data-toggle="modal" title="'.$data['AnsvarigPart'].'" data-target="#new1384">'.$data['AnsvarigPart'].'</div>';
            })
            ->addColumn('Kontrollskede', function($data){
                return '<div data-toggle="modal" title="'.$data['Kontrollskede'].'" data-target="#new1384">'.$data['Kontrollskede'].'</div>';
            })
            ->addColumn('Risk', function($data){
                return '<div data-toggle="modal" title="'.$data['Risk'].'" data-target="#new1384">'.$data['Risk'].'</div>';
            })
            ->addColumn('Verifieringsmetod', function($data){
                return '<div data-toggle="modal" title="'.$data['Verifieringsmetod'].'" data-target="#new1384">'.$data['Verifieringsmetod'].'</div>';
            })
            ->addColumn('ProduceratIntyg', function($data){
                return '<div data-toggle="modal" title="'.$data['ProduceratIntyg'].'" data-target="#new1384">'.$data['ProduceratIntyg'].'</div>';
            })
            ->addColumn('Verifieringsmetod', function($data){
                return '<div data-toggle="modal" title="'.$data['Verifieringsmetod'].'" data-target="#new1384">'.$data['Verifieringsmetod'].'</div>';
            })
            ->addColumn('KraverPlatsbesok', function($data){
                return '<div data-toggle="modal" title="'.$data['KraverPlatsbesok'].'" data-target="#new1384">'.$data['KraverPlatsbesok'].'</div>';
            })
            ->addColumn('Status', function($data){
                return $data['statusTitle'];
            })
            ->addColumn('assign_user', function($data){
                $image = '';
                $user = '';
                $avatar = '';
                $name = '';
                $organization = '';
                $organizationInfo = '';
                if($data['assign_user'] !='')
                {
                    $user =  \App\User::whereId($data['assign_user'])->first();
                    if($user)
                    {
                        $avatar = $user->avatar;
                        $name = $user->name;
                        $organization =\App\User::whereId($user->parent_organization_app)->first();
                        $organizationInfo = $organization->organizationName;
                    }
                    if($avatar !=null)
                    {
                        $image = '<a href="#" data-toggle="modal" data-target-src="'.$avatar.'" data-target="#showIMAGE"><img alt="Avatar" class="img-circle table-avatar" width="30" height="30" src="'.$avatar.'" data-toggle="tooltip" data-placement="top" title="'.$name.' From as '.$organizationInfo.'"></a>';
                    }
                    else
                    {
                        $image = 'Inga avatarer';
                    }

                }
                else
                {
                    $image = 'Ingen tilldelad användare';
                }
                return $image;
            })
            ->addColumn('LawText', function($data){
                return '<div class="limit-td2" data-toggle="modal" title="'.strip_tags($data['LawText']).'" data-target="#new1384">'.\Illuminate\Support\Str::limit(strip_tags($data['LawText']),30).'</div>';
            })
            ->addColumn('action',function ($data){
                $action = '<button data-toggle="modal" data-target="#asignUser" data-target-id="'.$data['id'].'" title=""><i class="fa fa-user" aria-hidden="true"></i></button>';
                $action .= '<button onClick="getTemplateid('.$data['id'].'),activaTab(\'userform\');select3('.$data['id'].')" title=""><i class="fa fa-edit" aria-hidden="true"></i></button>';
                $action .= '<button onclick="deleteConfirmation('.$data['id'].')" title=""><i class="fa fa-times" aria-hidden="true"></i></button>';
                $action .= '<a target="blank" href="'.route('design.KontrollPlan.print' ,['id' => $data['id']]).'" class="fa fa-print" aria-hidden="true"></a>';
                $action .= '<button data-toggle="modal" data-target="#attachmentModal" data-target-id="'.$data['id'].'" title=""><i class="fa fa-link" aria-hidden="true"></i></button>';
                $action .= '<button data-toggle="modal" data-target="#changeStatusModal" data-target-id="'.$data['id'].'" title="status"><i class="fa fa-flag" aria-hidden="true"></i></button>';
                if($data['history'] !=null)
                {
                    $action .= '<button data-toggle="modal" data-target="#showHisttoryModal" data-target-id="'.$data['id'].'" title=""><i class="fa fa-history" aria-hidden="true"></i></button>';
                }
                $attachment = AdvanceLawDesign::where('design_id' , $data['id'])->first();
                if($attachment)
                {
                    $action .= '<a target="blank" href="'.route('design.KontrollPlan.printAttachment' ,['id' => $data['id']]).'" class="fa fa-paperclip" aria-hidden="true"></a>';
                }
                return $action;
            })
            ->rawColumns(['BSABAMA','PBLKategori','AnsvarigPart','Kontrollskede','Risk','ProduceratIntyg',
                'Verifieringsmetod','KraverPlatsbesok','assign_user','LawText','action'])
            ->make(true);

    }

    public function printKontrollPlan($id)
    {
        $design = design::find($id);
        $controllPlan = unserialize($design->templateInformation);
        $project = Project::whereId($design->project_id)->pluck('projectInfo')->first();
        $project = unserialize($project);
//        return $project;
        $controllPlan['guid']  = $design['guid'];
        $controllPlan['AnsvarigPart']  = AnsvarigPart::where('id',$controllPlan['AnsvarigPart'])->pluck('title')->first();
        $controllPlan['PBLKategori'] = PBLCategory::where('id',$controllPlan['PBLKategori'])->pluck('title')->first();
        $controllPlan['ProduceratIntyg'] = ProduceratIntyg::where('id',$controllPlan['ProduceratIntyg'])->pluck('title')->first();
        $controllPlan['Verifieringsmetod'] = Verifieringsmetod::where('id',$controllPlan['Verifieringsmetod'])->pluck('title')->first();
        $controllPlan['Kontrollskede'] = Kontrollskede::where('id',$controllPlan['Kontrollskede'])->pluck('title')->first();
        $controllPlan['Risk'] = Risk::where('id',$controllPlan['Risk'])->pluck('title')->first();
        $controllPlan['KraverPlatsbesok'] = KraverPlatsbesok::where('id',$controllPlan['KraverPlatsbesok'])->pluck('title')->first();
        $controllPlan['LawText'] = $controllPlan['LawText'];
        $controllPlan['image'] = $controllPlan['image'];
        $controllPlan['status'] = $design->status;
        $user = User::where('id' , $design->assign_user)->first(['name']);
        if($user)
        {
            $controllPlan['user'] = $user;
        }
        else
        {
            $controllPlan['user'] = '';
        }
        if($design->assign_user !=null)
        {
            $controllPlan['user'] = User::where('id' , $design->assign_user)->pluck('name')->first();
        }
        $data['tstatus'] = '';
        $data['tdate'] = '';
        $data['mstatus'] = '';
        $data['mdate'] = '';
        $data['gstatus'] = '';
        $data['gdate'] = '';
        $data['kstatus'] = '';
        $data['kdate'] = '';
        $data['fstatus'] = '';
        $data['fdate'] = '';
        $data['estatus'] = '';
        $data['edate'] = '';
        $history = unserialize($design->history);
        if($history)
        {
            foreach ($history as $row)
            {
                if($row['status'] == 'Tilldelad')
                {
                    $controllPlan['statusDate'] = $row['time'];
                    $data['tstatus'] = 'Tilldelad';
                    $data['tdate'] = $row['time'];
                }
                elseif ($row['status'] == 'Mottaget')
                {
                    $controllPlan['statusDate'] = $row['time'];
                    $data['mstatus'] = 'Mottaget';
                    $data['mdate'] = $row['time'];
                }
                elseif ($row['status'] == 'Godkänt innehåll')
                {
                    $controllPlan['statusDate'] = $row['time'];
                    $data['gstatus'] = 'Godkänt innehåll';
                    $data['gdate'] =  $row['time'];
                }
                elseif ($row['status'] == 'KA Utlåtande')
                {
                    $controllPlan['statusDate'] = $row['time'];
                    $data['kstatus'] = 'KA Utlåtande';
                    $data['kdate'] = $row['time'];
                }
                elseif ($row['status'] == 'Funktion uppfylld')
                {
                    $controllPlan['statusDate'] = $row['time'];
                    $data['fstatus'] = 'Funktion uppfylld';
                    $data['fdate'] = $row['time'];
                }
                elseif ($row['status'] == 'Egenkontroll Underkänt')
                {
                    $controllPlan['statusDate'] = $row['time'];
                    $data['estatus'] = 'Egenkontroll Underkänt';
                    $data['edate'] = $row['time'];
                }
            }
        }


        $advance_law = AdvanceLawDesign::where('design_id' , $design->id)->get();
        return view('Admin.project.print',compact(['controllPlan' , 'project' , 'advance_law','data']));
    }

    public function backControllPlan(Request $request)
    {
        $project_id = $request->project_id;
        $tabindex = $request->tabindex;
        $projectName = Project::where('id',$project_id)->pluck('projectInfo')->first();
        $projectName = unserialize($projectName);
        $projectName = $projectName['Projektnamn'];
        return view('Admin.project.controllePlan.index',compact(['project_id','tabindex','projectName']));
    }

    public function printAllKontrollPlan($id)
    {
        $printOption =  request('printOption');
        $designsTemplate = [];
        if($printOption == 1)
        {
            $projectInformation = Project::whereId($id)->first();
            $projectInfo = unserialize($projectInformation->projectInfo);
            $projectInformations = $projectInfo;
            $designs = design::where('project_id',$id)->get();
            foreach ($designs as $row => $design)
            {
                $designsTemplate[$row]= unserialize($design->templateInformation);
                $designsTemplate[$row]['design_id'] = $design->id;
                $designsTemplate[$row]['organization_id'] = $design->organization_id;
                $designsTemplate[$row]['assign_user'] = $design->assign_user;
                $designsTemplate[$row]['guid'] = $design->guid;
                $designsTemplate[$row]['Projektnamn'] = $projectInfo['Projektnamn'];
                $designsTemplate[$row]['PBLKategori'] = PBLCategory::where('id', $designsTemplate[$row]['PBLKategori'])->pluck('title')->first();
                $designsTemplate[$row]['AnsvarigPart'] = AnsvarigPart::where('id', $designsTemplate[$row]['AnsvarigPart'])->pluck('title')->first();
                $designsTemplate[$row]['Kontrollskede'] = Kontrollskede::where('id', $designsTemplate[$row]['Kontrollskede'])->pluck('title')->first();
                $designsTemplate[$row]['Risk'] = Risk::where('id', $designsTemplate[$row]['Risk'])->pluck('title')->first();
                $designsTemplate[$row]['ProduceratIntyg'] = ProduceratIntyg::where('id', $designsTemplate[$row]['ProduceratIntyg'])->pluck('title')->first();
                $designsTemplate[$row]['Verifieringsmetod'] = Verifieringsmetod::where('id', $designsTemplate[$row]['Verifieringsmetod'])->pluck('title')->first();
                $designsTemplate[$row]['KraverPlatsbesok'] = KraverPlatsbesok::where('id', $designsTemplate[$row]['KraverPlatsbesok'])->pluck('title')->first();

            }
            $kaInforation = User::where('id',$projectInformation->proManager)->first();
            return view('Admin.project.print.printAll',compact(['projectInfo','designsTemplate','kaInforation','projectInformations']));
        }
        else
        {
            $projectInformation = Project::whereId($id)->first();
            $projectInfo = unserialize($projectInformation->projectInfo);
            $projectInfo['project_id'] = $projectInformation->id;
            $projectInformations = $projectInfo;
            $designs = design::where('project_id',$id)->get();
            foreach ($designs as $row => $design)
            {
                $designsTemplate[$row]= unserialize($design->templateInformation);
                $designsTemplate[$row]['design_id'] = $design->id;
                $designsTemplate[$row]['organization_id'] = $design->organization_id;
                $designsTemplate[$row]['assign_user'] = $design->assign_user;
                $designsTemplate[$row]['guid'] = $design->guid;
                $designsTemplate[$row]['PBLKategori'] = PBLCategory::where('id', $designsTemplate[$row]['PBLKategori'])->pluck('title')->first();
                $designsTemplate[$row]['AnsvarigPart'] = AnsvarigPart::where('id', $designsTemplate[$row]['AnsvarigPart'])->pluck('title')->first();
                $designsTemplate[$row]['Kontrollskede'] = Kontrollskede::where('id', $designsTemplate[$row]['Kontrollskede'])->pluck('title')->first();
                $designsTemplate[$row]['Risk'] = Risk::where('id', $designsTemplate[$row]['Risk'])->pluck('title')->first();
                $designsTemplate[$row]['ProduceratIntyg'] = ProduceratIntyg::where('id', $designsTemplate[$row]['ProduceratIntyg'])->pluck('title')->first();
                $designsTemplate[$row]['Verifieringsmetod'] = Verifieringsmetod::where('id', $designsTemplate[$row]['Verifieringsmetod'])->pluck('title')->first();
                $designsTemplate[$row]['KraverPlatsbesok'] = KraverPlatsbesok::where('id', $designsTemplate[$row]['KraverPlatsbesok'])->pluck('title')->first();
            }
            $kaInforation = User::where('id',$projectInformation->proManager)->first();
            return view('Admin.project.print.printkontrollplan',compact(['projectInfo','designsTemplate','kaInforation','projectInformations']));
        }


    }

    public function printAttachmentKontrollPlan($id)
    {
        $design = design::find($id);
        $controllPlan = unserialize($design->templateInformation);
        $project = Project::whereId($design->project_id)->pluck('projectInfo')->first();
        $project = unserialize($project);
//        return $project;
        $controllPlan['guid']  = $design['guid'];
        $controllPlan['AnsvarigPart']  = AnsvarigPart::where('id',$controllPlan['AnsvarigPart'])->pluck('title')->first();
        $controllPlan['PBLKategori'] = PBLCategory::where('id',$controllPlan['PBLKategori'])->pluck('title')->first();
        $controllPlan['ProduceratIntyg'] = ProduceratIntyg::where('id',$controllPlan['ProduceratIntyg'])->pluck('title')->first();
        $controllPlan['Verifieringsmetod'] = Verifieringsmetod::where('id',$controllPlan['Verifieringsmetod'])->pluck('title')->first();
        $controllPlan['Kontrollskede'] = Kontrollskede::where('id',$controllPlan['Kontrollskede'])->pluck('title')->first();
        $controllPlan['Risk'] = Risk::where('id',$controllPlan['Risk'])->pluck('title')->first();
        $controllPlan['KraverPlatsbesok'] = KraverPlatsbesok::where('id',$controllPlan['KraverPlatsbesok'])->pluck('title')->first();
        $controllPlan['LawText'] = $controllPlan['LawText'];
        $controllPlan['image'] = $controllPlan['image'];
        $controllPlan['status'] = $design->status;

        $advance_law = AdvanceLawDesign::where('design_id' , $design->id)->get();
        return view('Admin.project.print.printAttachment',compact(['controllPlan','advance_law' , 'project']));
    }

    public function getUserOrganization($id)
    {
        $users = User::where('parent_organization_app',$id)->where('is_app',1)->get();

        $output = '<option value="">Selected User</option>';
        foreach ($users as $user)
        {
            $output .= '<option value="'.$user['id'].'">'.$user['name'].'</option>';
        }
        return response()->json($output);
    }


    public function assignUser($id,Request $request)
    {
        $design = design::find($id);
        $design->organization_id = $request->organization_id;
        $design->assign_user = $request->user_id;
        $design->status = 1;
        $design->seen = 0;
        $array = [];
        if($design->history !=null)
        {
            $array = unserialize($design->history);
        }
        $history = ['status'=> 'Tilldelad' , 'time' => Carbon::now()->format('Y-m-d H:i')];
        array_push($array,$history);
        $design->history = serialize($array);
        $design->save();
        $user = User::find($request->user_id);
        $title = 'Ny aktivitet';
        $body = 'Du har tilldelats en ny aktivitet.';
        pushNotification($user->firebaseToken,$title,$body);
    }


    //todo
    public function searchControllePlan(Request $request)
    {
        $keywords = request('keyword');
        $designs = DB::table('designs')
            ->whereRaw('templateInformation like  "%'.$keywords.'%"')
            ->get();
        $designs =  $designs->where('project_id',$request->project_id);
        return $designs;
    }


    public function uploadAttachmentFileUser(Request $request)
    {
        $files = $request->attachmentFileToUser;
        $fileUpload = [];
        $design = design::where('id',$request->design_idForFile)->first();
        $fileOld = [];
        if($design->attachmentFileUser !=null)
        {
            $fileOld = unserialize($design->attachmentFileUser);
        }

        foreach ($files as $key => $file)
        {
            $fileUpload[$key]['fileId'] =  rand(1,9999);
            $fileUpload[$key]['title'] = $request->title;
            $fileUpload[$key]['files'] = $this->uploadImages($file);
            array_push($fileOld,$fileUpload[$key]);
        }

        $design->attachmentFileUser = serialize($fileOld);
        $design->save();
        return response()->json('success',200);
    }


    public function getAttachment(Request $request)
    {
        $design = design::where('id',$request->id)->first();
        if($design !=null)
        {
            $files = unserialize($design->attachmentFileUser);

        }
        $output = '';
        $row = 1;
        foreach($files as $file)
        {
            $output .= '<tr><td>'.$row.'</td><td>'.$file['title'].'</td>
            <td><button style="border: none; background-color: transparent" data-toggle="modal" data-target="#FileAttachmentModalEdit" data-target-id="'.$file['fileId'].'" data-target-designid="'.$design->id.'" data-target-title="'.$file['title'].'" data-target-fileid="'.asset($file['files']).'" title="edit"><i class="fa fa-edit" aria-hidden="true"></i></button>
            <button style="border: none; background-color: transparent" onclick="ConfirmDeleteFileAttachRow('.$design->id.','.$file['fileId'].')" title=""><i class="fa fa-times" aria-hidden="true"></i></button>
            <a href="'.asset($file['files']).'" class="fa fa-eye"  target="_blank" aria-hidden="true"></a></td></tr>';
            $row = $row+1;
        }

        return response()->json(['output' => $output,'success' => 'true']);
    }


    public function deleteFileAttachment($id,Request $request)
    {
        $design = design::where('id',$request->design_id)->first();
        $fileAttachment = unserialize($design->attachmentFileUser);
        foreach ($fileAttachment as $key => $file)
        {
            if ($fileAttachment[$key]['fileId'] == $id)
            {
                unset($fileAttachment[$key]);
            }
        }
        $design->attachmentFileUser = serialize($fileAttachment);
        $design->save();
        return response()->json(['success' => true]);
    }


    public function uploadAttachmentFileUserEdit(Request $request)
    {
        $design = design::where('id',$request->design_idForFile)->first();
        $fileOld = unserialize($design->attachmentFileUser);
        foreach ($fileOld as $key =>$files)
        {
            if ($fileOld[$key]['fileId'] == $request->filesIdUploadEdit)
            {
                $fileOld[$key]['title'] = $request->title;
                if($request->hasFile('file'))
                {
                    $fileOld[$key]['files'] = $this->uploadImages($request->file);
                }
            }
        }
        $design->attachmentFileUser = serialize($fileOld);
        $design->save();
        return response()->json('success',200);
    }


    public function getHistory(Request $request)
    {
        $design = design::where('id',$request->id)->first();
        if($design !=null)
        {
            $historys = unserialize($design->history);

        }
        $output = '';
        $row = 1;
        foreach($historys as $history)
        {
            $description = array_key_exists('description',$history) ? $history['description'] : '';
            $descriptionDisplay = !$description !='' ? 'none' : 'block';
            $file = array_key_exists('file',$history) ? $history['file'] : '';
            $fileDisplay = !$file !='' ? 'none' : 'block';
            $image = array_key_exists('image',$history) ? $history['image'] : '';
            $imageDisplay = !$image !='' ? 'none' : 'block';
            $output .= '<tr><td>'.$row.'</td><td>'.$history['status'].'</td><td>'.$history['time'].'</td>
            <td>
            <button style="border: none; background-color: transparent; display:'.$descriptionDisplay.'" data-toggle="modal" data-target="#showDescriptionHistoryModal" data-target-description="'.$description.'" title="description"><i class="fa fa-file" aria-hidden="true"></i></button>
            <a href="'.asset($file).'" class="fa fa-eye" style="display:'.$fileDisplay.'"  target="_blank" aria-hidden="true"></a>
            <a href="'.asset($image).'" class="fa fa-paperclip" style="display:'.$imageDisplay.'"  target="_blank" aria-hidden="true"></a>
            </td></tr>'
            ;
            $row = $row+1;
        }

        return response()->json(['output' => $output,'success' => 'true']);
    }

    public function changeStatus(Request $request)
    {
        $design = design::where('id',$request->designidByStatus)->first();
        $History = [];
        if($design->history)
        {
            $History = unserialize($design->history);
        }
        $file = $request->hasFile('file') ? $this->uploadImages($request->file) : '';
        $status = $request->status;
        $statusTitle = '';
        if($status == 0)
        {
            $statusTitle = 'Ej tilldelad';
//              $statusTitle = 'Not assigned';
        }
        elseif ($status == 1)
        {
            $statusTitle = 'Tilldelad';
//            $statusTitle = 'Assigned';
        }
        elseif ($status == 2)
        {
            $statusTitle = 'Mottaget';
//              $statusTitle = 'Received';
        }
        elseif ($status == 3)
        {
            $statusTitle = 'Godkänt innehåll';
//              $statusTitle = 'Approved Content';
        }
        elseif ($status == 4)
        {
            $statusTitle = 'Funktion uppfylld';
//            $statusTitle = 'Function fulfilled';
        }
        elseif ($status == 5)
        {
            $statusTitle = 'Admin Vidimera';
//                $statusTitle = 'Admin Vidimera';
        }
        elseif ($status == 6)
        {
            $statusTitle = 'KA Utlåtande';
//              $statusTitle = 'KA Statement';
        }
        elseif ($status == 7)
        {
            $statusTitle = 'Byggherren fastställer';
//              $statusTitle = 'The builder states';
        }
        elseif ($status == 8)
        {
            $statusTitle = 'Slutbesked ges';
//              $statusTitle = 'Final message given';
        }
        elseif ($status == 9)
        {
            $statusTitle = 'Egenkontroll Underkänt';
//              $statusTitle = 'Self Check Fail';
        }
        elseif ($status == 10)
        {
            $statusTitle = 'Avvisa';
//              $statusTitle = 'Reject';
        }

        if($design->confirmationHistory !='')
        {
            $History = unserialize($design->history);
        }
        if($request->status !=null)
        {
            $design->status = $request->status;
            $data  = [];
            $data['status'] = $statusTitle;
            $data['time'] = now()->format('Y-m-d H:i:s');
            $data['file'] = $file;
            $data['description'] = $request->description;
            $data['image'] = '';
            $data['video'] = '';
            array_push($History,$data);
            $design->history = serialize($History);
            $design->status = $request->status;
            $design->save();
            return response()->json('success');
        }
        else
        {
            return response()->json('validationError');
        }
    }

    public function uploadImages($file)
    {
        $current_timestamp = Carbon::now()->timestamp . rand(1,999999);
        $imagePath = "/upload/";
        $filename = $current_timestamp . $file->getClientOriginalName();
        $file = $file->move(public_path($imagePath) , $filename);
        return $imagePath.$filename;
    }

    function KontrollplanAvtal($id)
    {
        $design = design::where('project_id',$id)->get();
        $arrrays = [];
        $AnsvarigPart = '';
        foreach ($design as $row => $template)
        {
            $templateInformation = [];
            $templateInformation = unserialize($template->templateInformation);
            if($templateInformation['KontrollplanAvtal'] == 'KontrollplanAvtal')
            {
                $pblcategory = PBLCategory::where('id',$templateInformation['PBLKategori'])->pluck('title')->first();
                try {
                    if(array_key_exists ( 'AnsvarigPart' , $templateInformation[$row] ))
                    {
                        $AnsvarigPart = AnsvarigPart::where('id',$templateInformation[$row]['AnsvarigPart'])->pluck('title')->first();
                    }

                } catch (\Exception $e) {

                    $AnsvarigPart = '';
                }

                $status = $template->status;

                $Kontrollskede = Kontrollskede::where('id',$templateInformation['Kontrollskede'])->pluck('title')->first();
                if($Kontrollskede == '')
                {
                    $Kontrollskede = $templateInformation['kontrollskedeText'];
                }
                $Risk = Risk::where('id',$templateInformation['Risk'])->pluck('title')->first();
                if($Risk == '')
                {
                    $Risk = $templateInformation['riskText'];
                }
                else
                {
                    $riskDescription = $templateInformation['riskDescription'] ? $templateInformation['riskDescription'] : '';
                    $Risk = $Risk ? $Risk . '-' . $riskDescription : $Risk;
                }

                $ProduceratIntyg = ProduceratIntyg::where('id',$templateInformation['ProduceratIntyg'])->pluck('title')->first();
                if($ProduceratIntyg == '')
                {
                    $ProduceratIntyg = $templateInformation['ProduceratIntygText'];
                }
                else
                {
                    $ProduceratIntygDescription = $templateInformation['ProduceratIntygDescription'] ? $templateInformation['ProduceratIntygDescription'] : '';
                    $ProduceratIntyg = $ProduceratIntyg ? $ProduceratIntyg . '-' . $ProduceratIntygDescription : $ProduceratIntyg;
                }

                $Verifieringsmetod = Verifieringsmetod::where('id',$templateInformation['Verifieringsmetod'])->pluck('title')->first();
                if($Verifieringsmetod == '')
                {
                    $Verifieringsmetod = $templateInformation['VerifieringsmetodText'];
                }
                else
                {
                    $VerifieringsmetodDescription = $templateInformation['VerifieringsmetodDescription'] ? $templateInformation['VerifieringsmetodDescription'] : '';
                    $Verifieringsmetod = $Verifieringsmetod ? $Verifieringsmetod . '-' . $VerifieringsmetodDescription : $Verifieringsmetod;
                }

                $KraverPlatsbesok = KraverPlatsbesok::where('id',$templateInformation['KraverPlatsbesok'])->pluck('title')->first();
                if($KraverPlatsbesok == '')
                {
                    $KraverPlatsbesok = $templateInformation['KraverPlatsbesokText'];
                }
                else
                {
                    $KraverPlatsbesokDescription = $templateInformation['KraverPlatsbesokDescription'] ? $templateInformation['KraverPlatsbesokDescription'] : '';
                    $KraverPlatsbesok = $KraverPlatsbesokDescription ? $KraverPlatsbesok . '-' . $KraverPlatsbesokDescription : $KraverPlatsbesokDescription;
                }

                $templateInformation['id'] = $template->id;
                $templateInformation['assign_user'] = $template->assign_user;

                $templateInformation['PBLKategori'] =$pblcategory;
                $templateInformation['AnsvarigPart'] = $AnsvarigPart;
                $templateInformation['Kontrollskede'] = $Kontrollskede;
                $templateInformation['Risk'] = $Risk;
                $templateInformation['ProduceratIntyg'] = $ProduceratIntyg;
                $templateInformation['Verifieringsmetod'] = $Verifieringsmetod;
                $templateInformation['KraverPlatsbesok'] = $KraverPlatsbesok;
                $templateInformation['status'] = $status;
                $templateInformation['history'] = unserialize($template->history);
                $statusTitle = '';
                if($status == 0)
                {
                    $statusTitle = 'Ej tilldelad';
//              $statusTitle = 'Not assigned';
                }
                elseif ($status == 1)
                {
                    $statusTitle = 'Tilldelad';
                    $statusTitle = 'Assigned';
                }
                elseif ($status == 2)
                {
                    $statusTitle = 'Mottaget';
//              $statusTitle = 'Received';
                }
                elseif ($status == 3)
                {
                    $statusTitle = 'Godkänt innehåll';
//              $statusTitle = 'Approved Content';
                }
                elseif ($status == 4)
                {
                    $statusTitle = 'Funktion uppfylld';
//            $statusTitle = 'Function fulfilled';
                }
                elseif ($status == 5)
                {
                    $statusTitle = 'Admin Vidimera';
//                $statusTitle = 'Admin Vidimera';
                }
                elseif ($status == 6)
                {
                    $statusTitle = 'KA Utlåtande';
//              $statusTitle = 'KA Statement';
                }
                elseif ($status == 7)
                {
                    $statusTitle = 'Byggherren fastställer';
//              $statusTitle = 'The builder states';
                }
                elseif ($status == 8)
                {
                    $statusTitle = 'Slutbesked ges';
//              $statusTitle = 'Final message given';
                }
                elseif ($status == 9)
                {
                    $statusTitle = 'Egenkontroll Underkänt';
//              $statusTitle = 'Self Check Fail';
                }
                elseif ($status == 10)
                {
                    $statusTitle = 'Avvisa';
//              $statusTitle = 'Reject';
                }
                $templateInformation['statusTitle'] = $statusTitle;
                array_push($arrrays , $templateInformation);
            }

        }


        return DataTables::of($arrrays)
            ->addColumn('BSABAMA', function($data){
                return '<div  data-toggle="modal" title="'.$data['BSABAMA'].'" data-target="#new1384">'.\Illuminate\Support\Str::limit($data['BSABAMA'],30).'</div>';
            })
            ->addColumn('PBLKategori', function($data){
                return '<div data-toggle="modal" title="'.$data['PBLKategori'].'" data-target="#new1384">'.$data['PBLKategori'].'</div>';
            })
            ->addColumn('AnsvarigPart', function($data){
                return '<div data-toggle="modal" title="'.$data['AnsvarigPart'].'" data-target="#new1384">'.$data['AnsvarigPart'].'</div>';
            })
            ->addColumn('Kontrollskede', function($data){
                return '<div data-toggle="modal" title="'.$data['Kontrollskede'].'" data-target="#new1384">'.$data['Kontrollskede'].'</div>';
            })
            ->addColumn('Risk', function($data){
                return '<div data-toggle="modal" title="'.$data['Risk'].'" data-target="#new1384">'.$data['Risk'].'</div>';
            })
            ->addColumn('Verifieringsmetod', function($data){
                return '<div data-toggle="modal" title="'.$data['Verifieringsmetod'].'" data-target="#new1384">'.$data['Verifieringsmetod'].'</div>';
            })
            ->addColumn('ProduceratIntyg', function($data){
                return '<div data-toggle="modal" title="'.$data['ProduceratIntyg'].'" data-target="#new1384">'.$data['ProduceratIntyg'].'</div>';
            })
            ->addColumn('Verifieringsmetod', function($data){
                return '<div data-toggle="modal" title="'.$data['Verifieringsmetod'].'" data-target="#new1384">'.$data['Verifieringsmetod'].'</div>';
            })
            ->addColumn('KraverPlatsbesok', function($data){
                return '<div data-toggle="modal" title="'.$data['KraverPlatsbesok'].'" data-target="#new1384">'.$data['KraverPlatsbesok'].'</div>';
            })
            ->addColumn('Status', function($data){
                return $data['statusTitle'];
            })
            ->addColumn('assign_user', function($data){
                $image = '';
                if($data['assign_user'] !='')
                {
                    $user =  \App\User::whereId($data['assign_user'])->first();
                    $avatar = $user->avatar;
                    $name = $user->name;
                    $organization =\App\User::whereId($user->parent_organization_app)->first();
                    $organizationInfo = $organization->organizationName;
                    $image = '<a href="#" data-toggle="modal" data-target-src="'.$avatar.'" data-target="#showIMAGE"><img alt="Avatar" class="img-circle table-avatar" width="30" height="30" src="'.$avatar.'" data-toggle="tooltip" data-placement="top" title="'.$name.' From as '.$organizationInfo.'"></a>';
                }
                else
                {
                    $image = 'Ingen tilldelad användare';
                }
                return $image;
            })
            ->addColumn('LawText', function($data){
                return '<div class="limit-td2" data-toggle="modal" title="'.strip_tags($data['LawText']).'" data-target="#new1384">'.\Illuminate\Support\Str::limit(strip_tags($data['LawText']),30).'</div>';
            })
            ->addColumn('action',function ($data){
                $action = '<button data-toggle="modal" data-target="#asignUser" data-target-id="'.$data['id'].'" title=""><i class="fa fa-user" aria-hidden="true"></i></button>';
                $action .= '<button onClick="getTemplateid('.$data['id'].'),activaTab(\'userform\');select3('.$data['id'].')" title=""><i class="fa fa-edit" aria-hidden="true"></i></button>';
                $action .= '<button onclick="deleteConfirmation('.$data['id'].')" title=""><i class="fa fa-times" aria-hidden="true"></i></button>';
                $action .= '<a target="blank" href="'.route('design.KontrollPlan.print' ,['id' => $data['id']]).'" class="fa fa-print" aria-hidden="true"></a>';
                $action .= '<button data-toggle="modal" data-target="#attachmentModal" data-target-id="'.$data['id'].'" title=""><i class="fa fa-link" aria-hidden="true"></i></button>';
                $action .= '<button data-toggle="modal" data-target="#changeStatusModal" data-target-id="'.$data['id'].'" title="status"><i class="fa fa-flag" aria-hidden="true"></i></button>';
                if($data['history'] !=null)
                {
                    $action .= '<button data-toggle="modal" data-target="#showHisttoryModal" data-target-id="'.$data['id'].'" title=""><i class="fa fa-history" aria-hidden="true"></i></button>';
                }
                $attachment = AdvanceLawDesign::where('design_id' , $data['id'])->first();
                if($attachment)
                {
                    $action .= '<a target="blank" href="'.route('design.KontrollPlan.printAttachment' ,['id' => $data['id']]).'" class="fa fa-paperclip" aria-hidden="true"></a>';
                }
                return $action;
            })
            ->rawColumns(['BSABAMA','PBLKategori','AnsvarigPart','Kontrollskede','Risk','ProduceratIntyg',
                'Verifieringsmetod','KraverPlatsbesok','assign_user','LawText','action'])
            ->make(true);
    }

    function KontrollplanPBL($id)
    {
        $design = design::where('project_id',$id)->get();
        $arrrays = [];
        $AnsvarigPart = '';
        foreach ($design as $row => $template)
        {
            $templateInformation = [];
            $templateInformation = unserialize($template->templateInformation);
            if($templateInformation['KontrollplanPBL'] == 'KontrollplanPBL')
            {
                $pblcategory = PBLCategory::where('id',$templateInformation['PBLKategori'])->pluck('title')->first();
                try {
                    if(array_key_exists ( 'AnsvarigPart' , $templateInformation[$row] ))
                    {
                        $AnsvarigPart = AnsvarigPart::where('id',$templateInformation[$row]['AnsvarigPart'])->pluck('title')->first();
                    }

                } catch (\Exception $e) {

                    $AnsvarigPart = '';
                }

                $status = $template->status;

                $Kontrollskede = Kontrollskede::where('id',$templateInformation['Kontrollskede'])->pluck('title')->first();
                if($Kontrollskede == '')
                {
                    $Kontrollskede = $templateInformation['kontrollskedeText'];
                }
                $Risk = Risk::where('id',$templateInformation['Risk'])->pluck('title')->first();
                if($Risk == '')
                {
                    $Risk = $templateInformation['riskText'];
                }
                else
                {
                    $riskDescription = $templateInformation['riskDescription'] ? $templateInformation['riskDescription'] : '';
                    $Risk = $Risk ? $Risk . '-' . $riskDescription : $Risk;
                }

                $ProduceratIntyg = ProduceratIntyg::where('id',$templateInformation['ProduceratIntyg'])->pluck('title')->first();
                if($ProduceratIntyg == '')
                {
                    $ProduceratIntyg = $templateInformation['ProduceratIntygText'];
                }
                else
                {
                    $ProduceratIntygDescription = $templateInformation['ProduceratIntygDescription'] ? $templateInformation['ProduceratIntygDescription'] : '';
                    $ProduceratIntyg = $ProduceratIntyg ? $ProduceratIntyg . '-' . $ProduceratIntygDescription : $ProduceratIntyg;
                }

                $Verifieringsmetod = Verifieringsmetod::where('id',$templateInformation['Verifieringsmetod'])->pluck('title')->first();
                if($Verifieringsmetod == '')
                {
                    $Verifieringsmetod = $templateInformation['VerifieringsmetodText'];
                }
                else
                {
                    $VerifieringsmetodDescription = $templateInformation['VerifieringsmetodDescription'] ? $templateInformation['VerifieringsmetodDescription'] : '';
                    $Verifieringsmetod = $Verifieringsmetod ? $Verifieringsmetod . '-' . $VerifieringsmetodDescription : $Verifieringsmetod;
                }

                $KraverPlatsbesok = KraverPlatsbesok::where('id',$templateInformation['KraverPlatsbesok'])->pluck('title')->first();
                if($KraverPlatsbesok == '')
                {
                    $KraverPlatsbesok = $templateInformation['KraverPlatsbesokText'];
                }
                else
                {
                    $KraverPlatsbesokDescription = $templateInformation['KraverPlatsbesokDescription'] ? $templateInformation['KraverPlatsbesokDescription'] : '';
                    $KraverPlatsbesok = $KraverPlatsbesokDescription ? $KraverPlatsbesok . '-' . $KraverPlatsbesokDescription : $KraverPlatsbesokDescription;
                }

                $templateInformation['id'] = $template->id;
                $templateInformation['assign_user'] = $template->assign_user;

                $templateInformation['PBLKategori'] =$pblcategory;
                $templateInformation['AnsvarigPart'] = $AnsvarigPart;
                $templateInformation['Kontrollskede'] = $Kontrollskede;
                $templateInformation['Risk'] = $Risk;
                $templateInformation['ProduceratIntyg'] = $ProduceratIntyg;
                $templateInformation['Verifieringsmetod'] = $Verifieringsmetod;
                $templateInformation['KraverPlatsbesok'] = $KraverPlatsbesok;
                $templateInformation['status'] = $status;
                $templateInformation['history'] = unserialize($template->history);
                $statusTitle = '';
                if($status == 0)
                {
                    $statusTitle = 'Ej tilldelad';
//              $statusTitle = 'Not assigned';
                }
                elseif ($status == 1)
                {
                    $statusTitle = 'Tilldelad';
                    $statusTitle = 'Assigned';
                }
                elseif ($status == 2)
                {
                    $statusTitle = 'Mottaget';
//              $statusTitle = 'Received';
                }
                elseif ($status == 3)
                {
                    $statusTitle = 'Godkänt innehåll';
//              $statusTitle = 'Approved Content';
                }
                elseif ($status == 4)
                {
                    $statusTitle = 'Funktion uppfylld';
//            $statusTitle = 'Function fulfilled';
                }
                elseif ($status == 5)
                {
                    $statusTitle = 'Admin Vidimera';
//                $statusTitle = 'Admin Vidimera';
                }
                elseif ($status == 6)
                {
                    $statusTitle = 'KA Utlåtande';
//              $statusTitle = 'KA Statement';
                }
                elseif ($status == 7)
                {
                    $statusTitle = 'Byggherren fastställer';
//              $statusTitle = 'The builder states';
                }
                elseif ($status == 8)
                {
                    $statusTitle = 'Slutbesked ges';
//              $statusTitle = 'Final message given';
                }
                elseif ($status == 9)
                {
                    $statusTitle = 'Egenkontroll Underkänt';
//              $statusTitle = 'Self Check Fail';
                }
                elseif ($status == 10)
                {
                    $statusTitle = 'Avvisa';
//              $statusTitle = 'Reject';
                }
                $templateInformation['statusTitle'] = $statusTitle;
                array_push($arrrays , $templateInformation);
            }

        }


        return DataTables::of($arrrays)
            ->addColumn('BSABAMA', function($data){
                return '<div data-toggle="modal" title="'.$data['BSABAMA'].'" data-target="#new1384">'.\Illuminate\Support\Str::limit($data['BSABAMA'],30).'</div>';
            })
            ->addColumn('PBLKategori', function($data){
                return '<div data-toggle="modal" title="'.$data['PBLKategori'].'" data-target="#new1384">'.$data['PBLKategori'].'</div>';
            })
            ->addColumn('AnsvarigPart', function($data){
                return '<div data-toggle="modal" title="'.$data['AnsvarigPart'].'" data-target="#new1384">'.$data['AnsvarigPart'].'</div>';
            })
            ->addColumn('Kontrollskede', function($data){
                return '<div data-toggle="modal" title="'.$data['Kontrollskede'].'" data-target="#new1384">'.$data['Kontrollskede'].'</div>';
            })
            ->addColumn('Risk', function($data){
                return '<div data-toggle="modal" title="'.$data['Risk'].'" data-target="#new1384">'.$data['Risk'].'</div>';
            })
            ->addColumn('Verifieringsmetod', function($data){
                return '<div data-toggle="modal" title="'.$data['Verifieringsmetod'].'" data-target="#new1384">'.$data['Verifieringsmetod'].'</div>';
            })
            ->addColumn('ProduceratIntyg', function($data){
                return '<div data-toggle="modal" title="'.$data['ProduceratIntyg'].'" data-target="#new1384">'.$data['ProduceratIntyg'].'</div>';
            })
            ->addColumn('Verifieringsmetod', function($data){
                return '<div data-toggle="modal" title="'.$data['Verifieringsmetod'].'" data-target="#new1384">'.$data['Verifieringsmetod'].'</div>';
            })
            ->addColumn('KraverPlatsbesok', function($data){
                return '<div data-toggle="modal" title="'.$data['KraverPlatsbesok'].'" data-target="#new1384">'.$data['KraverPlatsbesok'].'</div>';
            })
            ->addColumn('Status', function($data){
                return $data['statusTitle'];
            })
            ->addColumn('assign_user', function($data){
                $image = '';
                if($data['assign_user'] !='')
                {
                    $user =  \App\User::whereId($data['assign_user'])->first();
                    $avatar = $user->avatar;
                    $name = $user->name;
                    $organization =\App\User::whereId($user->parent_organization_app)->first();
                    $organizationInfo = $organization->organizationName;
                    $image = '<a href="#" data-toggle="modal" data-target-src="'.$avatar.'" data-target="#showIMAGE"><img alt="Avatar" class="img-circle table-avatar" width="30" height="30" src="'.$avatar.'" data-toggle="tooltip" data-placement="top" title="'.$name.' From as '.$organizationInfo.'"></a>';
                }
                else
                {
                    $image = 'Ingen tilldelad användare';
                }
                return $image;
            })
            ->addColumn('LawText', function($data){
                return '<div class="limit-td2" data-toggle="modal" title="'.strip_tags($data['LawText']).'" data-target="#new1384">'.\Illuminate\Support\Str::limit(strip_tags($data['LawText']),30).'</div>';
            })
            ->addColumn('action',function ($data){
                $action = '<button data-toggle="modal" data-target="#asignUser" data-target-id="'.$data['id'].'" title=""><i class="fa fa-user" aria-hidden="true"></i></button>';
                $action .= '<button onClick="getTemplateid('.$data['id'].'),activaTab(\'userform\');select3('.$data['id'].')" title=""><i class="fa fa-edit" aria-hidden="true"></i></button>';
                $action .= '<button onclick="deleteConfirmation('.$data['id'].')" title=""><i class="fa fa-times" aria-hidden="true"></i></button>';
                $action .= '<a target="blank" href="'.route('design.KontrollPlan.print' ,['id' => $data['id']]).'" class="fa fa-print" aria-hidden="true"></a>';
                $action .= '<button data-toggle="modal" data-target="#attachmentModal" data-target-id="'.$data['id'].'" title=""><i class="fa fa-link" aria-hidden="true"></i></button>';
                $action .= '<button data-toggle="modal" data-target="#changeStatusModal" data-target-id="'.$data['id'].'" title="status"><i class="fa fa-flag" aria-hidden="true"></i></button>';
                if($data['history'] !=null)
                {
                    $action .= '<button data-toggle="modal" data-target="#showHisttoryModal" data-target-id="'.$data['id'].'" title=""><i class="fa fa-history" aria-hidden="true"></i></button>';
                }
                $attachment = AdvanceLawDesign::where('design_id' , $data['id'])->first();
                if($attachment)
                {
                    $action .= '<a target="blank" href="'.route('design.KontrollPlan.printAttachment' ,['id' => $data['id']]).'" class="fa fa-paperclip" aria-hidden="true"></a>';
                }
                return $action;
            })
            ->rawColumns(['BSABAMA','PBLKategori','AnsvarigPart','Kontrollskede','Risk','ProduceratIntyg',
                'Verifieringsmetod','KraverPlatsbesok','assign_user','LawText','action'])
            ->make(true);
    }

    function refresh($id)
    {
        $design = design::where('project_id',$id)->get();
        $AnsvarigPart = '';
        foreach ($design as $row => $template)
        {
            $templateInformation [] = unserialize($template->templateInformation);
            $pblcategory = PBLCategory::where('id',$templateInformation[$row]['PBLKategori'])->pluck('title')->first();
            if(array_key_exists('AnsvarigPart' , $templateInformation[$row]))
            {
                $AnsvarigPart = AnsvarigPart::where('id',$templateInformation[$row]['AnsvarigPart'])->pluck('title')->first();
            }


            $status = $template->status;

            $Kontrollskede = Kontrollskede::where('id',$templateInformation[$row]['Kontrollskede'])->pluck('title')->first();
            if($Kontrollskede == '')
            {
                $Kontrollskede = $templateInformation[$row]['kontrollskedeText'];
            }
            $Risk = Risk::where('id',$templateInformation[$row]['Risk'])->pluck('title')->first();
            if($Risk == '')
            {
                $Risk = $templateInformation[$row]['riskText'];
            }
            else
            {
                $riskDescription = $templateInformation[$row]['riskDescription'] ? $templateInformation[$row]['riskDescription'] : '';
                $Risk = $Risk ? $Risk . '-' . $riskDescription : $Risk;
            }

            $ProduceratIntyg = ProduceratIntyg::where('id',$templateInformation[$row]['ProduceratIntyg'])->pluck('title')->first();
            if($ProduceratIntyg == '')
            {
                $ProduceratIntyg = $templateInformation[$row]['ProduceratIntygText'];
            }
            else
            {
                $ProduceratIntygDescription = $templateInformation[$row]['ProduceratIntygDescription'] ? $templateInformation[$row]['ProduceratIntygDescription'] : '';
                $ProduceratIntyg = $ProduceratIntyg ? $ProduceratIntyg . '-' . $ProduceratIntygDescription : $ProduceratIntyg;
            }

            $Verifieringsmetod = Verifieringsmetod::where('id',$templateInformation[$row]['Verifieringsmetod'])->pluck('title')->first();
            if($Verifieringsmetod == '')
            {
                $Verifieringsmetod = $templateInformation[$row]['VerifieringsmetodText'];
            }
            else
            {
                $VerifieringsmetodDescription = $templateInformation[$row]['VerifieringsmetodDescription'] ? $templateInformation[$row]['VerifieringsmetodDescription'] : '';
                $Verifieringsmetod = $Verifieringsmetod ? $Verifieringsmetod . '-' . $VerifieringsmetodDescription : $Verifieringsmetod;
            }

            $KraverPlatsbesok = KraverPlatsbesok::where('id',$templateInformation[$row]['KraverPlatsbesok'])->pluck('title')->first();
            if($KraverPlatsbesok == '')
            {
                $KraverPlatsbesok = $templateInformation[$row]['KraverPlatsbesokText'];
            }
            else
            {
                $KraverPlatsbesokDescription = $templateInformation[$row]['KraverPlatsbesokDescription'] ? $templateInformation[$row]['KraverPlatsbesokDescription'] : '';
                $KraverPlatsbesok = $KraverPlatsbesokDescription ? $KraverPlatsbesok . '-' . $KraverPlatsbesokDescription : $KraverPlatsbesokDescription;
            }

            $templateInformation [$row]['id'] = $template->id;
            $templateInformation [$row]['assign_user'] = $template->assign_user;

            $templateInformation [$row]['PBLKategori'] =$pblcategory;
            $templateInformation [$row]['AnsvarigPart'] = $AnsvarigPart;
            $templateInformation [$row]['Kontrollskede'] = $Kontrollskede;
            $templateInformation [$row]['Risk'] = $Risk;
            $templateInformation [$row]['ProduceratIntyg'] = $ProduceratIntyg;
            $templateInformation [$row]['Verifieringsmetod'] = $Verifieringsmetod;
            $templateInformation [$row]['KraverPlatsbesok'] = $KraverPlatsbesok;
            $templateInformation [$row]['status'] = $status;
            $templateInformation [$row]['history'] = unserialize($template->history);
            $statusTitle = '';
            if($status == 0)
            {
                $statusTitle = 'Ej tilldelad';
//              $statusTitle = 'Not assigned';
            }
            elseif ($status == 1)
            {
                $statusTitle = 'Tilldelad';
                $statusTitle = 'Assigned';
            }
            elseif ($status == 2)
            {
                $statusTitle = 'Mottaget';
//              $statusTitle = 'Received';
            }
            elseif ($status == 3)
            {
                $statusTitle = 'Godkänt innehåll';
//              $statusTitle = 'Approved Content';
            }
            elseif ($status == 4)
            {
                $statusTitle = 'Funktion uppfylld';
//            $statusTitle = 'Function fulfilled';
            }
            elseif ($status == 5)
            {
                $statusTitle = 'Admin Vidimera';
//                $statusTitle = 'Admin Vidimera';
            }
            elseif ($status == 6)
            {
                $statusTitle = 'KA Utlåtande';
//              $statusTitle = 'KA Statement';
            }
            elseif ($status == 7)
            {
                $statusTitle = 'Byggherren fastställer';
//              $statusTitle = 'The builder states';
            }
            elseif ($status == 8)
            {
                $statusTitle = 'Slutbesked ges';
//              $statusTitle = 'Final message given';
            }
            elseif ($status == 9)
            {
                $statusTitle = 'Egenkontroll Underkänt';
//              $statusTitle = 'Self Check Fail';
            }
            elseif ($status == 10)
            {
                $statusTitle = 'Avvisa';
//              $statusTitle = 'Reject';
            }
            $templateInformation [$row]['statusTitle'] = $statusTitle;

        }
        return DataTables::of($templateInformation)
            ->addColumn('BSABAMA', function($data){
                return '<div data-toggle="modal" title="'.$data['BSABAMA'].'" data-target="#new1384">'.\Illuminate\Support\Str::limit($data['BSABAMA'],30).'</div>';
            })
            ->addColumn('PBLKategori', function($data){
                return '<div data-toggle="modal" title="'.$data['PBLKategori'].'" data-target="#new1384">'.$data['PBLKategori'].'</div>';
            })
            ->addColumn('AnsvarigPart', function($data){
                return '<div data-toggle="modal" title="'.$data['AnsvarigPart'].'" data-target="#new1384">'.$data['AnsvarigPart'].'</div>';
            })
            ->addColumn('Kontrollskede', function($data){
                return '<div data-toggle="modal" title="'.$data['Kontrollskede'].'" data-target="#new1384">'.$data['Kontrollskede'].'</div>';
            })
            ->addColumn('Risk', function($data){
                return '<div data-toggle="modal" title="'.$data['Risk'].'" data-target="#new1384">'.$data['Risk'].'</div>';
            })
            ->addColumn('Verifieringsmetod', function($data){
                return '<div data-toggle="modal" title="'.$data['Verifieringsmetod'].'" data-target="#new1384">'.$data['Verifieringsmetod'].'</div>';
            })
            ->addColumn('ProduceratIntyg', function($data){
                return '<div data-toggle="modal" title="'.$data['ProduceratIntyg'].'" data-target="#new1384">'.$data['ProduceratIntyg'].'</div>';
            })
            ->addColumn('Verifieringsmetod', function($data){
                return '<div data-toggle="modal" title="'.$data['Verifieringsmetod'].'" data-target="#new1384">'.$data['Verifieringsmetod'].'</div>';
            })
            ->addColumn('KraverPlatsbesok', function($data){
                return '<div data-toggle="modal" title="'.$data['KraverPlatsbesok'].'" data-target="#new1384">'.$data['KraverPlatsbesok'].'</div>';
            })
            ->addColumn('Status', function($data){
                return $data['statusTitle'];
            })
            ->addColumn('assign_user', function($data){
                $image = '';
                if($data['assign_user'] !='')
                {
                    $user =  \App\User::whereId($data['assign_user'])->first();
                    $avatar = $user->avatar;
                    $name = $user->name;
                    $organization =\App\User::whereId($user->parent_organization_app)->first();
                    $organizationInfo = $organization->organizationName;
                    $image = '<a href="#" data-toggle="modal" data-target-src="'.$avatar.'" data-target="#showIMAGE"><img alt="Avatar" class="img-circle table-avatar" width="30" height="30" src="'.$avatar.'" data-toggle="tooltip" data-placement="top" title="'.$name.' From as '.$organizationInfo.'"></a>';
                }
                else
                {
                    $image = 'Ingen tilldelad användare';
                }
                return $image;
            })
            ->addColumn('LawText', function($data){
                return '<div class="limit-td2" data-toggle="modal" title="'.strip_tags($data['LawText']).'" data-target="#new1384">'.\Illuminate\Support\Str::limit(strip_tags($data['LawText']),30).'</div>';
            })
            ->addColumn('action',function ($data){
                $action = '<button data-toggle="modal" data-target="#asignUser" data-target-id="'.$data['id'].'" title=""><i class="fa fa-user" aria-hidden="true"></i></button>';
                $action .= '<button onClick="getTemplateid('.$data['id'].'),activaTab(\'userform\');select3('.$data['id'].')" title=""><i class="fa fa-edit" aria-hidden="true"></i></button>';
                $action .= '<button onclick="deleteConfirmation('.$data['id'].')" title=""><i class="fa fa-times" aria-hidden="true"></i></button>';
                $action .= '<a target="blank" href="'.route('design.KontrollPlan.print' ,['id' => $data['id']]).'" class="fa fa-print" aria-hidden="true"></a>';
                $action .= '<button data-toggle="modal" data-target="#attachmentModal" data-target-id="'.$data['id'].'" title=""><i class="fa fa-link" aria-hidden="true"></i></button>';
                $action .= '<button data-toggle="modal" data-target="#changeStatusModal" data-target-id="'.$data['id'].'" title="status"><i class="fa fa-flag" aria-hidden="true"></i></button>';
                if($data['history'] !=null)
                {
                    $action .= '<button data-toggle="modal" data-target="#showHisttoryModal" data-target-id="'.$data['id'].'" title=""><i class="fa fa-history" aria-hidden="true"></i></button>';
                }
                $attachment = AdvanceLawDesign::where('design_id' , $data['id'])->first();
                if($attachment)
                {
                    $action .= '<a target="blank" href="'.route('design.KontrollPlan.printAttachment' ,['id' => $data['id']]).'" class="fa fa-paperclip" aria-hidden="true"></a>';
                }
                return $action;
            })
            ->rawColumns(['BSABAMA','PBLKategori','AnsvarigPart','Kontrollskede','Risk','ProduceratIntyg',
                'Verifieringsmetod','KraverPlatsbesok','assign_user','LawText','action'])
            ->make(true);

    }


}

