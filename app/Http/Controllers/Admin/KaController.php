<?php

namespace App\Http\Controllers\Admin;

use App\AdvanceLawDesign;
use App\Definitions\AnsvarigPart;
use App\Definitions\Kontrollskede;
use App\Definitions\KraverPlatsbesok;
use App\Definitions\PBLCategory;
use App\Definitions\ProduceratIntyg;
use App\Definitions\Risk;
use App\Definitions\Verifieringsmetod;
use App\design;
use App\Http\Controllers\Controller;
use App\KA;
use App\Law;
use App\Robrik;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class KaController extends Controller
{
    public function storeTitle(Request $request)
    {
        $robrik = Robrik::firstOrNew(['title' =>  $request->title]);
        $robrik->save();
        return response()->json(['message' => 'success' , 'data' => $robrik] , 200);
    }

    public function kaSearch()
    {
        $keywords = request('keywords');
        $Laws = DB::table('laws')
            ->whereRaw('Hanvisning like  "%'.$keywords.'%" or Text like "%'.$keywords.'%"');
            $Laws = $Laws->where('category_id',1)->where('category_id' , 6)->get();



        $output = '';
        if (count($Laws)>0) {

            $output = '<ul id="myid" style="height: 200px">';
            foreach ($Laws as $k => $law)
            {
                $output .= '<li class="liclick2" onClick="getLawKa(this.value);getBesabKa(this.value);select2(this)" value="'.$law->id.'"><i class="fa fa-arrow-right" aria-hidden="true" data-value="'.$law->id.'"></i>'.$law->Hanvisning.'</a></li>';
            }

            $output .= '</ul>';
        }

        else {

            $output .= '<li class="list-group-item">'.'No results'.'</li>';
        }

        return response()->json(['output' => $output]);

    }


    public function searchAll(Request $request)
    {

        $keywords = request('keywords');
        $Laws = DB::table('laws')
            ->whereRaw('Hanvisning like  "%'.$keywords.'%" or Text like "%'.$keywords.'%"')->get();

        $counts = DB::table('laws')
            ->whereRaw('Hanvisning like  "%'.$keywords.'%" or Text like "%'.$keywords.'%"')
            ->select('category_id', \DB::raw('count(*) as count'))
            ->groupBy('category_id')
            ->get();

        $output = '';
        if (count($Laws)>0)
        {
            $output = '<ul id="myid" style="height: 200px">';
            foreach ($Laws as $k => $law)
            {
                $output .= '<li class="liclick2" onClick="getLawKa(this.value);select2(this)" value="'.$law->id.'"><i class="fa fa-arrow-right" aria-hidden="true" data-value="'.$law->id.'"></i>'.$law->Hanvisning.'</a></li>';
            }
            $output .= '</ul>';



        }
        else
        {
            $output .= '<li class="list-group-item">'.'No results'.'</li>';
        }

        return response()->json([$output,$counts,$Laws]);
    }


    public function design(Request $request)
    {
        $now = Carbon::now();
        $number_rand = rand(1000,999999);
        $guid_code = $now->format('Ymd').$number_rand;

        $design = new KA();
        $data = $request->all();
        $data['KontrollplanPBL'] = '';
        $data['KontrollplanAvtal'] = '';
        if($request->importSelect == 'KontrollplanPBL')
        {
            $data['KontrollplanPBL'] = 'KontrollplanPBL';
        }
        elseif ($request->importSelect == 'KontrollplanAvtal')
        {
            $data['KontrollplanAvtal'] = 'KontrollplanAvtal';
        }

        $data['image'] = $request->hasFile('image') ? $this->uploadImages($request->file('image')) : '';
        $design->user_id = auth()->user()->id;
        $design->robrik_id = $data['robrik_id'];
        $design->project_id = $data['project_id'];
        unset($data['project_id']);
        unset($data['_token']);
        $design->templateInformation =serialize($data);
        $design->save();
        $id = $design->id;

        $guid_code = $now->format('Ymd').$number_rand;

        $ansvarigs = explode(',' , $data['AnsvarigPart']);

        foreach ($ansvarigs as $key => $ansvarig)
        {
            //make design ka
            $design = [];
            $params = [
                'BSABAMA' => $data['BSABAMA'],
                'PBLKategori' => $data['PBLKategori'],
                'AnsvarigPart' => $ansvarig,
                'AnsvarigPartText' => AnsvarigPart::where('id' , $ansvarig)->pluck('title')->first(),

                'Kontrollskede' => $data['Kontrollskede'],
                'kontrollskedeText' => $data['kontrollskedeText'],
                'Risk' => $data['Risk'],
                'riskText' => $data['riskText'],
                'riskDescription' => $data['riskDescription'],
                'ProduceratIntyg' => $data['ProduceratIntyg'],
                'ProduceratIntygText' => $data['ProduceratIntygText'],
                'ProduceratIntygDescription' => $data['ProduceratIntygDescription'],
                'Verifieringsmetod' => $data['Verifieringsmetod'],
                'VerifieringsmetodText' => $data['VerifieringsmetodText'],
                'VerifieringsmetodDescription' => $data['VerifieringsmetodDescription'],
                'KraverPlatsbesok' => $data['KraverPlatsbesok'],
                'KraverPlatsbesokText' => $data['KraverPlatsbesokText'],
                'KraverPlatsbesokDescription' => $data['KraverPlatsbesokDescription'],
                'importSelect' => $data['importSelect'],
                'LawText' => $data['LawText'],
                'image' => $data['image'],
                'Funktionskrav' => $data['Funktionskrav'],
                'KontrollplanPBL' => $data['KontrollplanPBL'],
                'KontrollplanAvtal' => $data['KontrollplanAvtal'],
            ];

            $array  = [
                'project_id' => $request->project_id,
                'user_id' => auth()->user()->id,
                'parent_id' => auth()->user()->parent_id,
                'robrik_id' => $data['robrik_id'],
                'ka_id' => $id,
                'templateInformation' => serialize($params),
                'seen' => 0,
                'guid' => $guid_code,
                'status' => 0,
            ];

            design::create($array);
        }

        return response()->json('success' , 200);
    }

    public function getTemplate($id)
    {
        $design = KA::where('project_id',$id)->get();
        $designid = $design->pluck('id');
        $templateInformation = [];
        foreach ($design as $row => $template)
        {
            $templateInformation [] = unserialize($template->templateInformation);
            $templateInformation [$row]['id'] = $template->id;

        }
        $output = '';
        $AnsvarigPart = '';
        foreach ($templateInformation as $k => $row)
        {
            $pblcategory = PBLCategory::where('id',$row['PBLKategori'])->pluck('title')->first();

            if(array_key_exists('AnsvarigPart' ,$row))
            {
                $AnsvarigPart = AnsvarigPart::where('id',$row['AnsvarigPart'])->pluck('title')->first();
            }

            $Kontrollskede = Kontrollskede::where('id',$row['Kontrollskede'])->pluck('title')->first();
            $Risk = Risk::where('id',$row['Risk'])->pluck('title')->first();

            if($Kontrollskede == '')
            {
                $Kontrollskede = $row['kontrollskedeText'];
            }


            if($Risk == '')
            {
                $Risk = $row['riskText'];
            }
            $ProduceratIntyg = ProduceratIntyg::where('id',$row['ProduceratIntyg'])->pluck('title')->first();
            if($ProduceratIntyg == '')
            {
                $ProduceratIntyg = $row['ProduceratIntygText'];
            }
            $Verifieringsmetod = Verifieringsmetod::where('id',$row['Verifieringsmetod'])->pluck('title')->first();
            if($Verifieringsmetod == '')
            {
                $Verifieringsmetod = $row['VerifieringsmetodText'];
            }
            $KraverPlatsbesok = KraverPlatsbesok::where('id',$row['KraverPlatsbesok'])->pluck('title')->first();
            if($KraverPlatsbesok == '')
            {
                $KraverPlatsbesok = $row['KraverPlatsbesokText'];
            }

            $output .= '<li id="controlplan'.$row['id'].'" class="liclick" onClick="getTemplateKaid('.$row['id'].');select(this)"><i class="fa fa-arrow-right" aria-hidden="true"></i>'.$row['BSABAMA'].' - '.$pblcategory.'
            - ' .$AnsvarigPart.' - '.$Kontrollskede.' - '.$Risk.'-'.$ProduceratIntyg.' - '.$Verifieringsmetod.' - '.$KraverPlatsbesok.'</li>';
        }
        return response()->json(['output' =>$output,'design' => $designid]);
    }

    public function getLaw($id)
    {
        $law = Law::whereId($id)->first();
//        $lawText = $law->pluck('Text');
        return response()->json($law);
    }

    public function getBesabLaw($id)
    {
        $law = Law::where('id' , $id)->first();
        return response()->json($law);
    }


    public function getRobrikLaw(Request $request)
    {
        $lawKa = KA::where('project_id' , $request->project_id)->where('robrik_id' , $request->robrik_id)->get();
        $params = [];
        foreach ($lawKa as $key => $row)
        {
            $law = unserialize($row->templateInformation);
            $params[$key]['id'] = $row['id'];
            $params[$key]['title'] = $law['BSABAMA'];
        }

        return response()->json(['message' => 'success' , 'data' => $params] , 200);
    }


    public function getTemp($id)
    {
        $design = KA::whereId($id)->first();
        $template = unserialize($design->templateInformation);
        $template['id'] = $design->id;
        $ansvarigSelected = $template['AnsvarigPart'];
        $ansvarigSelected = explode(',',$ansvarigSelected);
        $ansvarigPart = AnsvarigPart::whereIn('id' , $ansvarigSelected)->get();
        return response()->json(['message' => 'success' , 'data' => $template ,'ansvarigSelected' => $ansvarigSelected , 'ansvarigPart' => $ansvarigPart] , 200);
    }


    public function getAllTemplate($project_id)
    {
        $ka = KA::where('project_id' , $project_id)->get();
        $kaIds = $ka->pluck('id');
//        $designs = design::whereIn('ka_id' ,$kaIds )->get();
        $content = '';
        foreach ($ka as $key => $row)
        {
            $controllpointTitle = unserialize($row->templateInformation)['BSABAMA'];
            $robrikTitle = Robrik::where('id' , $row->robrik_id)->pluck('title')->first();
            $designs = design::where('ka_id' , $row->id)->where('robrik_id' , $row->robrik_id)->get();
            $content .='<tr style="background: aquamarine;"><td>'.$key.'</td><td colspan="12"  style="text-align: left">'.$robrikTitle.'</td></tr>
            <tr style="background: cadetblue;"><td colspan="13" style="text-align: left">'.$controllpointTitle.'</td></tr>';

            foreach ($designs as $key2 => $row2)
            {
                $idNumber = 0;
                $temp = unserialize($row->templateInformation);
                $PBLKategori = $temp['PBLKategori'] !=null && $temp['PBLKategori'] !=0 ? PBLCategory::where('id' , $temp['PBLKategori'])->pluck('title')->first() : '-';
                $AnsvarigPart = $temp['AnsvarigPart'] !=null && $temp['AnsvarigPart'] !=0 ? AnsvarigPart::where('id' , $temp['AnsvarigPart'])->pluck('title')->first() : '-';
                $Kontrollskede = $temp['Kontrollskede'] !=null && $temp['Kontrollskede'] !=0 ? Kontrollskede::where('id' , $temp['Kontrollskede'])->pluck('title')->first() : '-';
                $Risk = $temp['Risk'] !=null && $temp['Risk'] !=0 ? Risk::where('id' , $temp['Risk'])->pluck('title')->first() : '-';
                $ProduceratIntyg = $temp['ProduceratIntyg'] !=null && $temp['ProduceratIntyg'] !=0 ? ProduceratIntyg::where('id' , $temp['ProduceratIntyg'])->pluck('title')->first() : '-';
                $Verifieringsmetod = $temp['Verifieringsmetod'] !=null && $temp['Verifieringsmetod'] !=0 ? Verifieringsmetod::where('id' , $temp['Verifieringsmetod'])->pluck('title')->first() : '-';
                $KraverPlatsbesok = $temp['KraverPlatsbesok'] !=null && $temp['KraverPlatsbesok'] !=0 ? KraverPlatsbesok::where('id' , $temp['KraverPlatsbesok'])->pluck('title')->first() : '-';
                $Funktionskrav = $temp['Funktionskrav'];
                $KontrollplanAvtal = $temp['KontrollplanAvtal'];
                $status = $row2->status;
                $statusTitle = '';
                if($status == 0)
                {
                    $statusTitle = 'Ej tilldelad';
                }
                elseif ($status == 1)
                {
                    $statusTitle = 'Tilldelad';
//            $statusTitle = 'Assigned';
                }
                elseif ($status == 2)
                {
                    $statusTitle = 'Mottaget';
//              $statusTitle = 'Received';
                }
                elseif ($status == 3)
                {
                    $statusTitle = 'Godkänt innehåll';
//              $statusTitle = 'Approved Content';
                }
                elseif ($status == 4)
                {
                    $statusTitle = 'Funktion uppfylld';
//            $statusTitle = 'Function fulfilled';
                }
                elseif ($status == 5)
                {
                    $statusTitle = 'Admin Vidimera';
//                $statusTitle = 'Admin Vidimera';
                }
                elseif ($status == 6)
                {
                    $statusTitle = 'KA Utlåtande';
//              $statusTitle = 'KA Statement';
                }
                elseif ($status == 7)
                {
                    $statusTitle = 'Byggherren fastställer';
//              $statusTitle = 'The builder states';
                }
                elseif ($status == 8)
                {
                    $statusTitle = 'Slutbesked ges';
//              $statusTitle = 'Final message given';
                }
                elseif ($status == 9)
                {
                    $statusTitle = 'Egenkontroll Underkänt';
//              $statusTitle = 'Self Check Fail';
                }
                elseif ($status == 10)
                {
                    $statusTitle = 'Avvisa';
//              $statusTitle = 'Reject';
                }

                $image = '';
                $user = '';
                $avatar = '';
                $name = '';
                $organization = '';
                $organizationInfo = '';
                if($row2->assign_user !='')
                {
                    $user =  \App\User::whereId($data['assign_user'])->first();
                    if($user)
                    {
                        $avatar = $user->avatar;
                        $name = $user->name;
                        $organization =\App\User::whereId($user->parent_organization_app)->first();
                        $organizationInfo = $organization->organizationName;
                    }
                    if($avatar !=null)
                    {
                        $image = '<a href="#" data-toggle="modal" data-target-src="'.$avatar.'" data-target="#showIMAGE"><img alt="Avatar" class="img-circle table-avatar" width="30" height="30" src="'.$avatar.'" data-toggle="tooltip" data-placement="top" title="'.$name.' From as '.$organizationInfo.'"></a>';
                    }
                    else
                    {
                        $image = 'Inga avatarer';
                    }

                }
                else
                {
                    $image = 'Ingen tilldelad användare';
                }


                $action = '<button data-toggle="modal" data-target="#asignUser" data-target-id="'.$row2->id.'" title=""><i class="fa fa-user" aria-hidden="true"></i></button>';
                $action .= '<button onClick="getTemplateid('.$row2->id.'),activaTab(\'userform\');select3('.$row2->id.')" title=""><i class="fa fa-edit" aria-hidden="true"></i></button>';
                $action .= '<button onclick="deleteConfirmation('.$row2->id.')" title=""><i class="fa fa-times" aria-hidden="true"></i></button>';
                $action .= '<a target="blank" href="'.route('design.KontrollPlan.print' ,['id' => $row2->id]).'" class="fa fa-print" aria-hidden="true"></a>';
                $action .= '<button data-toggle="modal" data-target="#attachmentModal" data-target-id="'.$row2->id.'" title=""><i class="fa fa-link" aria-hidden="true"></i></button>';
                $action .= '<button data-toggle="modal" data-target="#changeStatusModal" data-target-id="'.$row2->id.'" title="status"><i class="fa fa-flag" aria-hidden="true"></i></button>';
                if($row->history !=null)
                {
                    $action .= '<button data-toggle="modal" data-target="#showHisttoryModal" data-target-id="'.$row2->id.'" title=""><i class="fa fa-history" aria-hidden="true"></i></button>';
                }
                $attachment = AdvanceLawDesign::where('design_id' , $row2->id)->first();
                if($attachment)
                {
                    $action .= '<a target="blank" href="'.route('design.KontrollPlan.printAttachment' ,['id' => $row2->id]).'" class="fa fa-paperclip" aria-hidden="true"></a>';
                }


                $content .='<tr>
                                <td>'.$key . $key2.'</td>
                                <td>'.$temp['BSABAMA'].'</td>
                                <td>'.$PBLKategori.'</td>
                                <td>'.$AnsvarigPart.'</td>
                                <td>'.$Kontrollskede.'</td>
                                <td>'.$Risk.'</td>
                                <td>'.$ProduceratIntyg.'</td>
                                <td>'.$Verifieringsmetod.'</td>
                                <td>'.$KraverPlatsbesok.'</td>
                                <td>'.$Funktionskrav.'</td>
                                <td>'.$statusTitle.'</td>
                                <td>'.$image.'</td>
                                <td>'.$action.'</td>
                            </tr>';
            }


        }


        return response()->json(['message' => 'success' , 'data' => $content] , 200);
    }


    public function update($id,Request $request)
    {
        $importSelect = $request->importSelect;
        $design = Ka::whereId($id)->first();
        $design->robrik_id = $request->robrik_id;
        $designItem = unserialize($design->templateInformation);
        $data = $request->all();
        unset($data['importSelect']);
        unset($data['_token']);
        $designItem['BSABAMA'] = $data['BSABAMA'];
        $designItem['Kontrollskede'] = isset($data['Kontrollskede']) ? $data['Kontrollskede'] : '';

        $designItem['LawText'] = $data['LawText'];
        $designItem['PBLKategori'] = isset($data['PBLKategori']) ? $data['PBLKategori'] : '';


        $AnsvarigPart = explode(',' , $request->AnsvarigPart);

        $designItem['AnsvarigPart'] = isset($data['AnsvarigPart']) ? $data['AnsvarigPart'] : '';
        $designItem['AnsvarigPartText'] = isset($data['AnsvarigPartText']) ? $data['AnsvarigPartText'] : '';

        $designItem['Risk'] = isset($data['Risk']) ? $data['Risk'] : '';
        $designItem['riskText'] = $data['riskText'];
        $designItem['riskDescription'] = $data['riskDescription'];

        $designItem['ProduceratIntyg'] = isset($data['ProduceratIntyg']) ? $data['ProduceratIntyg'] : '';
        $designItem['ProduceratIntygText'] = $data['ProduceratIntygText'];
        $designItem['ProduceratIntygDescription'] = $data['ProduceratIntygDescription'];

        $designItem['Verifieringsmetod'] = isset($data['Verifieringsmetod']) ? $data['Verifieringsmetod'] : '';
        $designItem['VerifieringsmetodText'] = $data['VerifieringsmetodText'];
        $designItem['VerifieringsmetodDescription'] = $data['VerifieringsmetodDescription'];

        $designItem['KraverPlatsbesok'] = isset($data['KraverPlatsbesok']) ? $data['KraverPlatsbesok'] : '';
        $designItem['KraverPlatsbesokText'] = $data['KraverPlatsbesokText'];
        $designItem['KraverPlatsbesokDescription'] = $data['KraverPlatsbesokDescription'];

        $designItem['Funktionskrav'] = $data['Funktionskrav'];
        $designItem['EndastSignering'] = $data['EndastSignering'];


        if(isset($importSelect) and $importSelect !=null)
        {
            if($importSelect == 'KontrollplanPBL')
            {
                $designItem['KontrollplanPBL'] = 'KontrollplanPBL';
                $designItem['KontrollplanAvtal'] = '';
            }
            else
            {
                $designItem['KontrollplanAvtal'] = 'KontrollplanAvtal';
                $designItem['KontrollplanPBL'] = '';
            }
        }
        else
        {
            $designItem['KontrollplanAvtal'] = '';
            $designItem['KontrollplanPBL'] = '';
        }

        try {
            $designItem['image'] = $request->hasFile('image') ? $this->uploadImages($request->file('image')) : $designItem['image'];
        }
        catch (\Exception $e)
        {
            $designItem['image'] = $request->hasFile('image') ? $this->uploadImages($request->file('image')) : $designItem['image'];
        }

        $design->templateInformation =serialize($designItem);
        $design->save();

        //update ek
        $designs = design::where('ka_id' , $id)->get();

        foreach ($designs as $key => $row) {

            if ($row->templateInformation != null)
            {
                $templateInformation = unserialize($row->templateInformation);
                $templateInformation['robrik_id'] = $data['robrik_id'];
                $templateInformation['BSABAMA'] = $data['BSABAMA'];
                $templateInformation['PBLKategori'] = $data['PBLKategori'];
                $templateInformation['AnsvarigPart'] = $AnsvarigPart[$key];
                $templateInformation['AnsvarigPartText'] =  AnsvarigPart::where('id' , $AnsvarigPart[$key])->pluck('title')->first();
                $templateInformation['AnsvarigPart'] = $AnsvarigPart[$key];
                $templateInformation['Kontrollskede'] = $data['Kontrollskede'];
                $templateInformation['kontrollskedeText'] = $data['kontrollskedeText'];
                $templateInformation['Risk'] = $data['Risk'];
                $templateInformation['riskText'] = $data['riskText'];
                $templateInformation['riskDescription'] = $data['riskDescription'];
                $templateInformation['ProduceratIntyg'] = $data['ProduceratIntyg'];
                $templateInformation['ProduceratIntygText'] = $data['ProduceratIntygText'];
                $templateInformation['ProduceratIntygDescription'] = $data['ProduceratIntygDescription'];
                $templateInformation['Verifieringsmetod'] = $data['Verifieringsmetod'];
                $templateInformation['VerifieringsmetodText'] = $data['VerifieringsmetodText'];
                $templateInformation['VerifieringsmetodDescription'] = $data['VerifieringsmetodDescription'];
                $templateInformation['KraverPlatsbesok'] = $data['KraverPlatsbesok'];
                $templateInformation['KraverPlatsbesokText'] = $data['KraverPlatsbesokText'];
                $templateInformation['KraverPlatsbesokDescription'] = $data['KraverPlatsbesokDescription'];
                $templateInformation['LawText'] = $data['LawText'];
                $templateInformation['image'] = $data['image'];
                $templateInformation['Funktionskrav'] = $data['Funktionskrav'];
                $templateInformation['EndastSignering'] = $data['EndastSignering'];

                $des = design::where('id' , $row->id)->first();
                $des->robrik_id = $request->robrik_id;
                $des->templateInformation = serialize($templateInformation);
                $des->save();
            }
        }
        return response()->json(['message' => 'success' , 'data' => $design->project_id] , 200);
//        return unserialize($design->templateInformation);
    }


    public function searchCategory()
    {
        $keywords = request('keywords') . "";
        $category_id = request('id');
        $Laws = DB::table('laws')
            ->whereRaw('Hanvisning like  "%'.$keywords.'%" or Text like "%'.$keywords.'%"')->get();
        $Laws = $Laws->where('category_id',$category_id);

        $output = '';
        if (count($Laws)>0) {

            $output = '<ul id="myid" style="height: 200px">';
            foreach ($Laws as $k => $law)
            {
                $output .= '<li class="liclick2" onClick="getLawKa(this.value);getBesabKa(this.value);select2(this)" value="'.$law->id.'"><i class="fa fa-arrow-right" aria-hidden="true" data-value="'.$law->id.'"></i>'.$law->Hanvisning.'</a></li>';
            }

            $output .= '</ul>';
        }

        else {

            $output .= '<li class="list-group-item">'.'No results'.'</li>';
        }

        return response()->json([$output]);
    }


    public function delete($id)
    {
        $delete =  Ka::where('id', $id)->delete();
        if ($delete == 1) {
            $success = true;
            $message = "design deleted successfully";
        } else {
            $success = true;
            $message = "design not found";
        }
        return response()->json([
            'success' => $success,
            'message' => $message,
        ]);
    }


    public function uploadImages($file)
    {
        $current_timestamp = Carbon::now()->timestamp . rand(1,999999);
        $imagePath = "/upload/";
        $filename = $current_timestamp . $file->getClientOriginalName();
        $file = $file->move(public_path($imagePath) , $filename);
        return $imagePath.$filename;
    }
}
