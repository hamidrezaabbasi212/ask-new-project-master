<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Imports\LawsImport;
use App\Law;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class LawController extends Controller
{
    public function index()
    {
        return view('Admin.Law.index');
    }

    public function create()
    {
        return view('Admin.Law.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'lawFile'  => 'required|mimes:xls,xlsx'
        ]);
        $path = $request->file('lawFile')->getRealPath();


        if ($request->file('lawFile')) {
            Excel::import(new LawsImport($request->category_id), request()->file('lawFile'));
            return back();
        }

        return $request->all();
    }


    public function getLaw($id)
    {
        $law = Law::whereId($id)->first();
//        $lawText = $law->pluck('Text');
        return response()->json($law);
    }



    public function getBesabLaw($id)
    {
        $law = Law::where(['id' => $id , 'category_id' => 6])->first();
        return response()->json($law);
    }


}
