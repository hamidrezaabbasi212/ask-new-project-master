<?php

namespace App\Http\Controllers\Admin;

use App\Definitions\AnsvarigPart;
use App\Http\Controllers\Controller;
use App\OrganizationUsers;
use App\Project\Organization;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class OrganizationController extends Controller
{
    public function index()
    {
        $organizations = User::where('is_organization',1)->where('parent_id',auth()->user()->id)->paginate(10);
        return view('Admin.Organization.index',compact('organizations'));
    }

    public function store(Request $request)
    {
        $organization = new Organization();
        $organization->name = $request->name;
        $organization->description = $request->description;
        $organization->address = $request->address;
        $organization->contact = $request->contact;
        $organization->save();
        Session::flash('successAddOrganization','Location Data Entered succesfully');
        return redirect(route('Organization.index'));
    }

    public function edit($id)
    {
        return $id;
    }

    public function update($id,Request $request)
    {
        $organization = Organization::find($id);
        $organization->name = $request->organizationNameEdit;
        $organization->description = $request->organizationDescriptionEdit;
        $organization->address = $request->organizationAddressEdit;
        $organization->contact = $request->organizationContact;
        $organization->save();
        return response()->json('success',200);
    }

    public function delete($id)
    {
        $organization =  Organization::find($id);
        $organization->organizationUsers()->delete();
        $delete =  $organization->delete();
        if ($delete == 1) {
            $success = true;
            $message = "Organization deleted successfully";
        } else {
            $success = false;
            $message = "Organization not found";
        }
        return response()->json([
            'success' => $success,
            'message' => $message,
        ]);
    }

    public function users($id)
    {
        return view('Admin.Organization.users',compact('id'));
    }

    public function getUser()
    {
        $users = User::where('parent_organization_app',request()->organizationid)->get();
        $output = '';
        foreach ($users as $row => $user)
        {
            $output .= '<tr><td>'.$user->name.'</td><td>'.$user->phone.'</td><td>'.$user->email.'</td>
            <td><ul class="list-inline"><li class="list-inline-item">
            <img alt="Avatar" style="display: initial; border-radius: 50px; width:2.5rem" class="table-avatar" src="'.asset($user['avatar']).'"></li></ul></td>
            <td><a href="#" data-toggle="modal" data-target="#EditUser" data-target-id="'.$user['id'].'" title="Edit"><i class="fa fa-edit" aria-hidden="true"></i></a></td>
            <td><button type="button" onclick="deleteConfirmation('.$user['id'].')" class="fa fa-trash-alt sa-remove"></button></td></tr>';
        }
        return response()->json($output);
    }

    public function getInfoUser()
    {
        $user = User::where('id',request()->user_id)->first();
        return response()->json($user);
    }

    public function getExperts()
    {
        $organizationUsers = User::where('id',request()->user_id)->first();
        $experts = unserialize($organizationUsers->experts);
        $output = '';
        foreach ($experts as $row =>$expert)
        {
            $ansvaringPart  = AnsvarigPart::where('id',$expert)->first();
            $output .= '<tr><td>'.$ansvaringPart['title'].'</td><td><button type="button" onclick="deleteConfirmationExpert('.$row.')" title=""><i class="fa fa-times" aria-hidden="true"></i></button></td></tr>';
        }

        return response()->json($output);

    }

    public function deleteExperts()
    {
        $organizationUsers = User::where('id',request()->userid)->first();
        $experts = unserialize($organizationUsers->experts);
        unset($experts[request()->id]);
        $organizationUsers->experts = serialize($experts);
        $organizationUsers->save();
        return response()->json('success',200);
    }

    public function createUser($id,Request $request)
    {
        $user = new User();
        $avatar = '';
        if (request()->hasFile('avatar')) {
            $avatar = $this->uploadImages($request->file('avatar'));
        }
        $user->name = $request->name;
        $user->phone = $request->phone;
        $user->email = $request->email;
        $user->avatar = $avatar;
        $user->password = $request->phone;
        $user->is_app = 1;
        $user->parent_organization_app = $id;
        $user->parent_id = auth()->user()->id;
        $user->experts = serialize($request->AnsvarigPartUser);
        $user->legitimationCard = $request->legitimationCard;
        $user->save();
        Session::flash('successaddusers','Location Data Entered succesfully');
        return back();
    }


    public function userUpdate($id,Request $request)
    {
        $user = User::find($id);
        $image = $user->avatar;
        if (request()->hasFile('avatar')) {
            $image = $this->uploadImages($request->file('avatar'));
        }
        $user->name = $request->name;
        $user->phone = $request->phone;
        $user->Email = $request->email;
        $user->avatar = $image;
        $user->legitimationCard = $request->legitimationCard;
        $user->password = bcrypt($request->phone);
        $experts = unserialize($user->experts);
        if($request->AnsvarigPartUserEdit !=null)
        {
            foreach ($request->AnsvarigPartUserEdit as $row)
            {
                array_push($experts,$row);
            }

            $user->experts = serialize($experts);
        }

        $user->update();
        return response()->json('success',200);

    }

    public function deleteUser($id,Request $request)
    {
        $user = User::find($id);
        $user->delete();
        $success = true;
        $message = "User success delete";
        return response()->json([
            'success' => $success,
            'message' => $message,
        ]);

    }


    public function uploadImages($file)
    {
        $current_timestamp = Carbon::now()->timestamp;
        $imagePath = "/upload/";
        $filename =rand(100,99999). $current_timestamp . $file->getClientOriginalName();
        $file = $file->move(public_path($imagePath) , $filename);
        return $imagePath.$filename;
    }
}
