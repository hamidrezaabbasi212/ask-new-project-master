<?php

namespace App\Http\Controllers\Admin;

use App\design;
use App\Http\Controllers\Controller;
use App\Law;
use App\Project\Project;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;


class PanelController extends Controller
{
    public function index()
    {
        //TODO
//        $collection = Project::groupBy('step')
//            ->selectRaw('count(*) as total, step')
//            ->get();
//        return $collection->max('total');
//
//        $result = Project::select(DB::raw('MAX(step) AS MaxStepTitle'))->take(2)->get();
//        return $result;

//
//        $user = User::find(1);
//            $title = 'تست';
//            $body = 'متن تست';
//            pushNotification($user->firebaseToken,$title,$body);


        $projects = Project::latest()->take(6)->get();
        return view('Admin.panel',compact('projects'));
    }

    public function profile()
    {
        $account = auth()->user();
        return view('Admin.profile',compact('account'));
    }

    public function setting()
    {
        return view('admin.setting');
    }
    public function contact()
    {
        return view('contact');
    }
}
