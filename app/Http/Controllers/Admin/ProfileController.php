<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;

class ProfileController extends Controller
{
    public function update($id,Request $request)
    {
        $avatar = '';
        $user = User::find($id);
        if (request()->hasFile('avatar')) {
            $avatar = $this->uploadImages($request->file('avatar'));
            $user->avatar = $avatar;
        }

        if(! is_null($request->password)) {
            $request->validate([
                'password' => ['required', 'string', 'min:8', 'confirmed'],
            ]);
            $user->password  = $request->password;
        }

        $user->name = $request->name;
        $user->email = $request->email;
        $user->phone = $request->phone;

        $user->save();
        Session::flash('success','Location Data Entered succesfully');
        return redirect(route('Profile'));

    }

    public function uploadImages($file)
    {
        $current_timestamp = Carbon::now()->timestamp;
        $imagePath = "/upload/";
        $filename = $current_timestamp . $file->getClientOriginalName();
        $file = $file->move(public_path($imagePath) , $filename);
        return $imagePath.$filename;
    }
}
