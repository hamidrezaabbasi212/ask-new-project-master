<?php

namespace App\Http\Controllers\Admin\Project;

use App\Http\Controllers\Controller;
use App\Imports\AdvanceLawImport;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class AdvanceLawController extends Controller
{
    public function import()
    {
        return view('Admin.project.advance.import');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'lawFile'  => 'required|mimes:xls,xlsx'
        ]);
        if ($request->file('lawFile')) {
            Excel::import(new AdvanceLawImport(),request()->file('lawFile'));
            return back();
        }

        return $request->all();
    }


}
