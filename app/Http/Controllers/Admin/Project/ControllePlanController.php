<?php

namespace App\Http\Controllers\Admin\Project;

use App\Definitions\AnsvarigPart;
use App\Definitions\PBLCategory;
use App\Http\Controllers\Controller;
use App\Law;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DataTables;

class ControllePlanController extends Controller
{
    public function index()
    {
        return view('Admin.project.controllePlan.index');
    }

    public function getallLaws()
    {
        $AllLaws = Law::all('id','Hanvisning','Text');
        return DataTables::of($AllLaws)
            ->addIndexColumn()
            ->addColumn('action', function($row){

                $btn = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Edit" class="edit btn btn-primary btn-sm editProduct">Edit</a>';

                $btn = $btn.' <a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Delete" class="btn btn-danger btn-sm deleteProduct">Delete</a>';

                return $btn;
            })
            ->rawColumns(['action'])
            ->make(true);
    }


    public function search(Request $request)
    {

        $keywords = request('keywords');
        $Laws = DB::table('laws')
            ->whereRaw('Hanvisning like  "%'.$keywords.'%" or Text like "%'.$keywords.'%"')->get();

        $counts = DB::table('laws')
            ->whereRaw('Hanvisning like  "%'.$keywords.'%" or Text like "%'.$keywords.'%"')
            ->select('category_id', \DB::raw('count(*) as count'))
            ->groupBy('category_id')
            ->get();

        $output = '';
        if (count($Laws)>0)
        {
            $output = '<ul id="myid" style="height: 200px">';
            foreach ($Laws as $k => $law)
            {
                $output .= '<li class="liclick2" onClick="getLaw(this.value);select2(this)" value="'.$law->id.'"><i class="fa fa-arrow-right" aria-hidden="true" data-value="'.$law->id.'"></i>'.$law->Hanvisning.'</a></li>';
            }
            $output .= '</ul>';



        }
        else
        {
            $output .= '<li class="list-group-item">'.'No results'.'</li>';
        }

        return response()->json([$output,$counts,$Laws]);

    }


    public function searchBesab()
    {
        $keywords = request('keywords');
        $Laws = DB::table('laws')
            ->whereRaw('Hanvisning like  "%'.$keywords.'%" or Text like "%'.$keywords.'%"')
            ->get();
        $Laws =  $Laws->where('category_id',6);

        $counts = DB::table('laws')
            ->whereRaw('Hanvisning like  "%'.$keywords.'%" or Text like "%'.$keywords.'%"')
            ->select('category_id', \DB::raw('count(*) as count'))
            ->groupBy('category_id')
            ->get()->where('category_id',6)->pluck('count')->first();

        $output = '';
        if (count($Laws)>0)
        {
            $output = '<ul id="myid" style="height: 200px">';
            foreach ($Laws as $k => $law)
            {
                $output .= '<li class="liclick2" onClick="getLaw(this.value);getBesab(this.value);select2(this)" value="'.$law->id.'"><i class="fa fa-arrow-right" aria-hidden="true" data-value="'.$law->id.'"></i>'.$law->Hanvisning.'</a></li>';
            }
            $output .= '</ul>';

        }
        else
        {
            $output .= '<li class="list-group-item">'.'No results'.'</li>';
        }


        return response()->json([$output,$counts]);
    }


    public function searchAnsvarig(Request $request)
    {
        $keywords = request('keywords');
        $Ansvarig = AnsvarigPart::searchAnsvarig($keywords)->latest('id')->get();
        $output = '';
        if (count($Ansvarig)>0) {
            foreach ($Ansvarig as $row)
            {
                $output .= '<option value="'.$row->id.'">'.$row->title.'</option>';
            }
        }
        else
        {
            $output .= '<option>'.'No results'.'</option>';
        }

        return response()->json($output);
    }

    public function searchAnsvarigUser(Request $request)
    {
        $keywords = request('keywords');
        $Ansvarig = AnsvarigPart::searchAnsvarig($keywords)->latest('id')->get();
        $output = '';
        if (count($Ansvarig)>0) {
            foreach ($Ansvarig as $row)
            {
                $output .= '<option value="'.$row->id.'">'.$row->title.'</option>';
            }
        }
        else
        {
            $output .= '<option>'.'No results'.'</option>';
        }

        return response()->json($output);
    }


    public function searchCategory()
    {
        $keywords = request('keywords');
        $category_id = request('id');
        $Laws = DB::table('laws')
            ->whereRaw('Hanvisning like  "%'.$keywords.'%" or Text like "%'.$keywords.'%"')
            ->get();
        $Laws =  $Laws->where('category_id',$category_id);
        $output = '';
        if (count($Laws)>0) {

            $output = '<ul id="myid" style="height: 200px">';
            foreach ($Laws as $k => $law)
            {
                $output .= '<li class="liclick2" onClick="getLaw(this.value);select2(this)" value="'.$law->id.'"><i class="fa fa-arrow-right" aria-hidden="true" data-value="'.$law->id.'"></i>'.$law->Hanvisning.'</a></li>';
            }

            $output .= '</ul>';
        }

        else {

            $output .= '<li class="list-group-item">'.'No results'.'</li>';
        }

        return response()->json([$output]);
    }

}
