<?php

namespace App\Http\Controllers\Admin\Project;

use App\AdvanceLawDesign;
use App\Building;
use App\design;
use App\Http\Controllers\Controller;
use App\LawImport;
use App\OrganizationUsers;
use App\Project\Organization;
use App\Template;
use App\TemplateInformation;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Project\Project;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Response;

use Illuminate\Validation\Rule;
use mysql_xdevapi\Exception;
use Yajra\DataTables\DataTables;
use function GuzzleHttp\Promise\queue;

class ProjectController extends Controller
{
    public function index()
    {

        $user = auth()->user();
        $projects = Project::query();
//        $projects = Project::with('designs')->search($request->keyword)->paginate(10);
        if($user->is_supperuser == 1 or $user->is_municipality == 1)
        {
            $projects->with('designs')->where('projectInfo' , 'LIKE' , '%' . request('keyword') . '%');
        }
        elseif ($user->is_supervisingCompany == 1)
        {
            $projects->with('designs')->where('projectInfo' , 'LIKE' , '%' . request('keyword') . '%')->where('proManager',$user->id);
        }
        elseif ($user->is_owner == 1)
        {
            $projects->with('designs')->where('projectInfo' , 'LIKE' , '%' . request('keyword') . '%')->where('owner_id',$user->id);
        }
        elseif ($user->is_organization == 1)
        {
            //LAST TODO
        }
        $projects = $projects->latest()->paginate(20);



//        $projects = Project::with('designs')->latest()->paginate(10);
        return view('Admin.project.index',compact('projects'));
    }

    public function search(Request $request)
    {
        $user = auth()->user();
        $projects = Project::query();
//        $projects = Project::with('designs')->search($request->keyword)->paginate(10);
        if($user->is_supperuser == 1 or $user->is_municipality == 1)
        {
            $projects->with('designs')->where('projectInfo' , 'LIKE' , '%' . $request->keyword . '%');
        }
        elseif ($user->is_supervisingCompany == 1)
        {
            $projects->with('designs')->where('projectInfo' , 'LIKE' , '%' . $request->keyword . '%')->where('proManager',$user->id);
        }
        elseif ($user->is_owner == 1)
        {
            $projects->with('designs')->where('projectInfo' , 'LIKE' , '%' . $request->keyword . '%')->where('owner_id',$user->id);
        }
        elseif ($user->is_organization == 1)
        {
            //LAST TODO
        }
        $projects = $projects->latest()->paginate(20);
        return view('Admin.project.index',compact('projects'));
    }

    public function create()
    {
        return view('Admin.project.create');
    }

    public function createMall()
    {
        return view('Admin.project.mall.templateType');
    }

    public function uploadifc()
    {
        return view('Admin.project.tekniskBeskrivning.uploadifc');
    }

    public function owntemplate()
    {
        $user_id = auth()->user()->id;
        $templates = Template::where('user_id',$user_id)->where('is_supper_user',0)->get();
        return view('Admin.project.egnaMallar.owntemplate',compact('templates'));
    }

    public function createProject()
    {
        return view('Admin.project.infoProject');
    }

    public function store(Request $request)
    {

        $userAuth = auth()->user();
        $project  = new Project();
        $project->location = $request->location;
        $validator =  $this->validate($request,[
//            'Projektnamn' => 'required',
//            'Entreprenadform' => 'required',
//            'BNÄrendenr' => 'required',
//            'Entreprenadform' => 'required',
//            'Byggherre' => 'required',
//            'Strukturbeskrivning' => 'required',
//            'Byggplatsensgatuadress' => 'required',
//            'Byggnadsbeskrivning' => 'required',
//            'Bygglov' => 'required',
//            //Step 2
//            'Teknisktsamråd' => 'required',
//            'Startbesked' => 'required',
//            'Slutsamråd' => 'required',
//            'Interimistisktslutbesked' => 'required',
//            'Slutbesked' => 'required',
//            'Projektstartslut' => 'required',
//            'Kontrollansvarig' => 'required',
//            'KAFöretag' => 'required',
//            'KAAdress' => 'required',
//            'KATelefon' => 'required',
//            'KAMail' => 'required',
//            //Step 3
//            'Brandskyddsbeskrivning' => 'required',
//            'Projektbeskrivning' => 'required',
//            'Geotekniskundersökning' => 'required',
//            'Fuktsäkerhetsprojektering' => 'required',
//            'Energiberäkning' => 'required',
//            'APDplan' => 'required',
//            'Sprängplan' => 'required',
//            'Tillgänglighetsbeskrivning' => 'required',
//            'Konstruktionsdokumentation' => 'required',
//            'Konstruktionsritningar' => 'required',
//            'VVSritningar' => 'required',
//            'VAplan' => 'required',
//            'Bullerutredning' => 'required',
//            'Dagsljusberäkning' => 'required',
//            'Marksaneringsplan' => 'required',
//            'Dagvattenutredning' => 'required',
//            'OVK&Luftflödesprotokoll' => 'required',
//            'Intygtäthetsprovning' => 'required',
//            'IntygMiljö&Hälsoskyddsnämnden' => 'required',
//            'KAAdress' => 'required',
        ]);

        if($userAuth->is_owner == 1)
        {
            $project->owner_id = $userAuth->id;

            if($request->typeCreateInfoKa == 1 and $request->userIdKa !=0)
            {
                $project->proManager = $request->userIdKa;

                if(isset($request->platName) and $request->platName !=null)
                {
//                        $validator =  $this->validate($request,[
//                            'platName' => 'required',
//                            'platEmail' => 'required',
//                            'platPhone' => 'required',
//                        ]);

                    $userPlat = new User();
                    $userPlat->name = $request->platName;
                    $userPlat->email = $request->platEmail;
                    $userPlat->phone = $request->platPhone;
                    $userPlat->parent_id = $request->userIdKa;
                    $userPlat->is_supervisingEngineer = 1;
                    $userPlat->password = $request->phone;
                    $userPlat->save();
                    $project->engineer_id = $userPlat->id;
                }
            }
            elseif(isset($request->Kontrollansvarig))
            {
//                $validator =  $this->validate($request,[
//                    'Kontrollansvarig' => 'required',
//                    'email' => 'required|unique:users',
//                    'KATelefon' => 'required',
//                    'KACertNr' => 'required',
//                    'KAAdress' => 'required',
//                    'KAFöretag' => 'required',
//                ]);

                $user = new User();
                $user->name = $request->Kontrollansvarig;
                $user->companyName = $request->KAFöretag;
                $user->email = $request->email;
                $user->phone = $request->KATelefon;
                $user->supervisingCompany_parent_owner = $userAuth->id;
                $user->is_supervisingCompany = 1;
                $user->password = $request->phone;
                $user->supervisingCompanyAddress = $request->KAAdress;
                $user->supervisingCompanyCertNr = $request->KACertNr;
                $user->save();
                $project->proManager = $user->id;

                if($user)
                {
                    if(isset($request->platName) and $request->platName !=null)
                    {
                        $validator =  $this->validate($request,[
                            'platName' => 'required',
                            'platEmail' => 'required',
                            'platPhone' => 'required',
                        ]);

                        $userPlat = new User();
                        $userPlat->name = $request->platName;
                        $userPlat->email = $request->platEmail;
                        $userPlat->phone = $request->platPhone;
                        $userPlat->parent_id = $user->id;
                        $userPlat->is_supervisingEngineer = 1;
                        $userPlat->password = $request->phone;
                        $userPlat->save();
                        $project->engineer_id = $userPlat->id;
                    }
                }
            }
        }

        else
        {
            $project->proManager = $userAuth->id;

            if(isset($request->platName) and $request->platName !=null)
            {
//                $validator =  $this->validate($request,[
//                    'platName' => 'required',
//                    'platEmail' => 'required',
//                    'platPhone' => 'required',
//                ]);

                $userPlat = new User();
                $userPlat->name = $request->platName;
                $userPlat->email = $request->platEmail;
                $userPlat->phone = $request->platPhone;
                $userPlat->parent_id = $userAuth->id;
                $userPlat->is_supervisingEngineer = 1;
                $userPlat->password = $request->phone;
                $userPlat->save();
                $project->engineer_id = $userPlat->id;
            }

            if($request->typeCreateInfoUser == 1 and $request->userId !=0)
            {
                $project->owner_id = $request->userId;
            }

            elseif(isset($request->name))
            {
//                $validator =  $this->validate($request,[
//                    'name' => 'required',
//                    'email' => 'required,unique:users',
//                    'tel' => 'required',
//                ]);
                $user = new User();
                $user->name = $request->name;
                $user->email = $request->email;
                $user->phone = $request->tel;
                $user->parent_id = auth()->user()->id;
                $user->is_owner = 1;
                $user->password = $request->phone;
                $user->ownerAddress = $request->address;
                if(isset($request->avatar))
                {
                    $user->avatar = $this->uploadImages($request->avatar);
                }
                $user->save();
                $project->owner_id = $user->id;
            }

        }



        $data = $request->all();
        $data['Bygglovbeviljat'] = 0;
        $data['Slutsamråd'] = 0;
        $data['Interimistiskslutbesked'] = 0;
        $data['projectImage'] = '';

        unset($data['_token']);
        unset($data['typeCreateInfoUser']);
        unset($data['name']);
        unset($data['email']);
        unset($data['tel']);
        unset($data['userId']);
        unset($data['location']);
        unset($data['typeCreateInfoUser']);
        unset($data['address']);

        unset($data['typeCreateInfoKa']);
        unset($data['userIdKa']);
        unset($data['Kontrollansvarig']);
        unset($data['KAFöretag']);
        unset($data['KATelefon']);
        unset($data['KAMail']);
        unset($data['KACertNr']);
        unset($data['KAAdress']);

        unset($data['typeCreateInfoPlat']);
        unset($data['platName']);
        unset($data['platEmail']);
        unset($data['platPhone']);

        unset($data['checkImport']);

        $uploadKey= $request->file();
        $uploadKeyy =  array_keys($_FILES);
        if(isset($request->avatar) or isset($_FILES['avatar']))
        {
            unset($data['avatar']);
            array_splice($uploadKeyy, 0, 1);
        }

        foreach ($uploadKeyy as $row => $file)
        {
            if($file !="projectImage")
            {
                if($request->hasFile($file))
                {
                    $files = $request[$file];
                    foreach ($files as $row2 => $fi)
                    {
                        $image[$row2] = $this->uploadImages($fi);
                    }
                    $data[$file] = $image;
                }
            }
            else
            {
                if(isset($request->projectImage))
                {
                    $data['projectImage'] =  $this->uploadImages($request->projectImage);
                }
            }
        }
        $count = count($uploadKeyy)-1;
        for($i = 0; $i<=$count;$i++)
        {
            if(!array_key_exists($uploadKeyy[$i],$data))
            {
                $data[$uploadKeyy[$i]] = array();
            }

        }

        if (isset($request->Bygglovbeviljat))
        {
            $data['Bygglovbeviljat'] = 1;
        }
        if (isset($request->Slutsamråd))
        {
            $data['Slutsamråd'] = 1;
        }
        if (isset($request->Interimistiskslutbesked))
        {
            $data['Interimistiskslutbesked'] = 1;
        }
        $project->projectInfo = serialize($data);
        $project->proAdmin = 1;
        $project->save();
        $project_id = $project->id;

        if(isset($request->checkImport) and $request->checkImport !=null)
        {
            if(isset($request->laws) and $request->laws !=null)
            {
                $laws = unserialize($request->laws);
                foreach ($laws as $key => $law)
                {
                    $templateInformation = [];
                    $title = $law['code'] . ' ' .$law['title'];
                    $templateInformation['BSABAMA'] = $title;
                    $templateInformation['PBLKategori'] = '';
                    $templateInformation['AnsvarigPart'] = '';
                    $templateInformation['AnsvarigPartText'] = '';
                    $templateInformation['Kontrollskede'] = '';
                    $templateInformation['kontrollskedeText'] = '';
                    $templateInformation['Risk'] = '';
                    $templateInformation['riskText'] = '';
                    $templateInformation['riskDescription'] = '';
                    $templateInformation['ProduceratIntyg'] = '';
                    $templateInformation['ProduceratIntygText'] = '';
                    $templateInformation['ProduceratIntygDescription'] = '';
                    $templateInformation['Verifieringsmetod'] = '';
                    $templateInformation['VerifieringsmetodText'] = '';
                    $templateInformation['VerifieringsmetodDescription'] = '';
                    $templateInformation['KraverPlatsbesok'] = '';
                    $templateInformation['KraverPlatsbesokText'] = '';
                    $templateInformation['KraverPlatsbesokDescription'] = '';
                    $templateInformation['KontrollplanPBL'] = $law['check1'];
                    $templateInformation['KontrollplanAvtal'] = $law['check2'];
                    $templateInformation['KraverPlatsbesokDescription'] = '';
                    $templateInformation['Funktionskrav'] = '';
                    $templateInformation['statusTitle'] = '';
                    $templateInformation['image'] = '';
                    $templateInformation['LawText'] = '<p><b>'.$title.'</b></p><p>'.$law['text'].'</p>';

                    $now = Carbon::now();
                    $number_rand = rand(1000,999999);
                    $guid_code = $now->format('Ymd').$number_rand;

                    $array = [];
                    $history = ['status'=> 'skapad' , 'time' => Carbon::now()->format('Y-m-d H:i')];
                    array_push($array,$history);
                    design::create([
                        'project_id' => $project_id,
                        'user_id' => auth()->user()->id,
                        'parent_id' => auth()->user()->parent_id,
                        'templateInformation' => serialize($templateInformation),
                        'guid' => $guid_code,
                        'history' => serialize($array)
                    ]);
                }

            }
        }


        Session::flash('successAddProject','Location Data Entered succesfully');
        $tabindex = '';
        $projectName  = $data['Projektnamn'];
        return view('Admin.project.controllePlan.index',compact(['project_id','tabindex','projectName']));

    }

    public function edit($id)
    {
        $p = Project::find($id);
        $project = unserialize($p->projectInfo);
        $project['id'] = $p->id;
        $project['location'] = $p->location;
        $project['owner_id'] = $p->owner_id;
        $project['proAdmin'] = $p->proAdmin;
        if($project['owner_id'] !=null or $project['owner_id'] !=0)
        {
            $user = User::where('id',$project['owner_id'])->first();
            $project['userId'] = $user->id;
            $project['userName'] = $user->name;
            $project['userEmail'] = $user->email;
            $project['userTelephone'] = $user->phone;
            $project['avatar'] = $user->avatar;
            $project['userAddress'] = $user->ownerAddress;
        }

        if($project['proAdmin'] !=null or $project['proAdmin'] !=0)
        {
            $userKa = User::where('id',$project['proAdmin'])->first();
            $project['Kontrollansvarig'] = $userKa->name;
            $project['KAFöretag'] = $userKa->companyName;
            $project['KATelefon'] = $userKa->phone;
            $project['KAMail'] = $userKa->email;
            $project['KACertNr'] = $userKa->supervisingCompanyCertNr;
            $project['KAAdress'] = $userKa->supervisingCompanyAddress;
        }

        return view('Admin.project.edit',compact('project'));
    }

    public function controllPlan($id)
    {
        $project_id = $id;
        $project = Project::where('id',$id)->pluck('projectInfo')->first();
        $projectInfo = unserialize($project);
        $projectName = $projectInfo['Projektnamn'];
        $tabindex = '';
        return view('Admin.project.controllePlan.index',compact(['project_id','tabindex','projectName']));
    }

    public function getFile()
    {
        $data =  request()->all();

        $project = Project::whereId($data['projectid'])->pluck('projectInfo')->first();
        $project = unserialize($project);
        return $project[$data['key']];
    }

    public function removeFile()
    {
        $data = request()->all();
        $project = Project::whereId($data['projectid'])->first();
        $projectFile = unserialize($project->projectInfo);
        $file = $projectFile[$data['key']];
        unset($file[$data['index']]);
        $projectFile[$data['key']] = $file;
        $project->projectInfo = serialize($projectFile);
        $project->save();
        return response()->json('success',200);
    }

    public function templatecategory($id)
    {
        $templates = Template::where('templateCategory',$id)->where('is_supper_user',1)->get();
        return view('Admin.project.templateList',compact('templates'));
    }

    public function template($id)
    {
        return view('Admin.project.infoProjectTemplate',compact('id'));
    }


    public function saveBaseProject($id,Request $request)
    {

        $project  = new Project();
        $userAuth = auth()->user();
        $project->location = $request->location;
        $validator =  $this->validate($request,[
            'Projektnamn' => 'required',
//            'Entreprenadform' => 'required',
//            'BNÄrendenr' => 'required',
//            'Entreprenadform' => 'required',
//            'Byggherre' => 'required',
//            'Strukturbeskrivning' => 'required',
//            'Byggplatsensgatuadress' => 'required',
//            'Byggnadsbeskrivning' => 'required',
//            'Bygglov' => 'required',
//            //Step 2
//            'Teknisktsamråd' => 'required',
//            'Startbesked' => 'required',
//            'Slutsamråd' => 'required',
//            'Interimistisktslutbesked' => 'required',
//            'Slutbesked' => 'required',
//            'Projektstartslut' => 'required',
//            'Kontrollansvarig' => 'required',
//            'KAFöretag' => 'required',
//            'KAAdress' => 'required',
//            'KATelefon' => 'required',
//            'KAMail' => 'required',
//            //Step 3
//            'Brandskyddsbeskrivning' => 'required',
//            'Projektbeskrivning' => 'required',
//            'Geotekniskundersökning' => 'required',
//            'Fuktsäkerhetsprojektering' => 'required',
//            'Energiberäkning' => 'required',
//            'APDplan' => 'required',
//            'Sprängplan' => 'required',
//            'Tillgänglighetsbeskrivning' => 'required',
//            'Konstruktionsdokumentation' => 'required',
//            'Konstruktionsritningar' => 'required',
//            'VVSritningar' => 'required',
//            'VAplan' => 'required',
//            'Bullerutredning' => 'required',
//            'Dagsljusberäkning' => 'required',
//            'Marksaneringsplan' => 'required',
//            'Dagvattenutredning' => 'required',
//            'OVK&Luftflödesprotokoll' => 'required',
//            'Intygtäthetsprovning' => 'required',
//            'IntygMiljö&Hälsoskyddsnämnden' => 'required',
//            'KAAdress' => 'required',

        ]);

        if($userAuth->is_owner == 1)
        {
            $project->owner_id = $userAuth->id;
            if($request->typeCreateInfoKa == 1 and $request->userIdKa !=0)
            {
                $project->proManager = $request->userIdKa;
            }
            elseif(isset($request->Kontrollansvarig))
            {
                $validator =  $this->validate($request,[
                    'Kontrollansvarig' => 'required',
                    'email' => 'required|unique:users',
                    'KATelefon' => 'required',
                    'KACertNr' => 'required',
                    'KAAdress' => 'required',
                    'KAFöretag' => 'required',
                ]);

                $user = new User();
                $user->name = $request->Kontrollansvarig;
                $user->companyName = $request->KAFöretag;
                $user->email = $request->email;
                $user->phone = $request->KATelefon;
                $user->supervisingCompany_parent_owner = $userAuth->id;
                $user->is_supervisingCompany = 1;
                $user->password = $request->phone;
                $user->supervisingCompanyAddress = $request->KAAdress;
                $user->supervisingCompanyCertNr = $request->KACertNr;
                $user->save();
                $project->proManager = $user->id;
            }
        }
        else
        {
            $project->proManager = $userAuth->id;

            if(isset($request->platName) and $request->platName !=null)
            {
                $validator =  $this->validate($request,[
                    'platName' => 'required',
                    'platEmail' => 'required',
                    'platPhone' => 'required',
                ]);

                $userPlat = new User();
                $userPlat->name = $request->platName;
                $userPlat->email = $request->platEmail;
                $userPlat->phone = $request->platPhone;
                $userPlat->parent_id = $userAuth->id;
                $userPlat->is_supervisingEngineer = 1;
                $userPlat->password = $request->phone;
                $userPlat->save();
                $project->engineer_id = $userPlat->id;
            }

            if(isset($request->typeCreateInfoUser) and $request->typeCreateInfoUser == 1 and  $request->userId !=0)
            {
                $project->owner_id = $request->userId;
            }
            elseif(isset($request->name))
            {
                $validator =  $this->validate($request,[
                    'name' => 'required',
                    'email' => 'required,unique:users',
                    'tel' => 'required',
                ]);

                $user = new User();
                $user->name = $request->name;
                $user->email = $request->email;
                $user->phone = $request->tel;
                $user->parent_id = auth()->user()->id;
                $user->is_owner = 1;
                $user->password = $request->phone;
                $user->ownerAddress = $request->address;
                if(isset($request->avatar))
                {
                    $user->avatar = $this->uploadImages($request->avatar);
                }
                $user->save();
                $project->owner_id = $user->id;
            }
        }



        $templateCategory = Template::where('id' , $id)->first();

        $temp =  $templateCategory->templetInformations;
        foreach ($temp as $k => $row)
        {
            $tempInfo []= $row->templateInformation;
        }

        $count = count($tempInfo);

        $project->location = $request->location;
        $data = $request->all();
        $data['Bygglovbeviljat'] = 0;
        $data['Slutsamråd'] = 0;
        $data['Interimistiskslutbesked'] = 0;

        $data['projectImage'] = '';
        unset($data['_token']);
        unset($data['typeCreateInfoUser']);
        unset($data['name']);
        unset($data['email']);
        unset($data['tel']);
        unset($data['userId']);
        unset($data['location']);
        unset($data['typeCreateInfoUser']);
        unset($data['address']);

        unset($data['typeCreateInfoKa']);
        unset($data['userIdKa']);
        unset($data['Kontrollansvarig']);
        unset($data['KAFöretag']);
        unset($data['KATelefon']);
        unset($data['KAMail']);
        unset($data['KACertNr']);
        unset($data['KAAdress']);

        unset($data['typeCreateInfoPlat']);
        unset($data['platName']);
        unset($data['platEmail']);
        unset($data['platPhone']);

        $uploadKey= $request->file();
        $uploadKeyy =  array_keys($_FILES);
        if(isset($request->avatar) or isset($_FILES['avatar']))
        {
            unset($data['avatar']);
            array_splice($uploadKeyy, 0, 1);
        }

        foreach ($uploadKeyy as $row => $file)
        {
            if($file !="projectImage")
            {
                if($request->hasFile($file))
                {
                    $files = $request[$file];
                    foreach ($files as $row2 => $fi)
                    {
                        $image[$row2] = $this->uploadImages($fi);
                    }
                    $data[$file] = $image;
                }
            }
            else
            {
                if(isset($request->projectImage))
                {
                    $data['projectImage'] =  $this->uploadImages($request->projectImage);
                }
            }


        }
        $count = count($uploadKeyy)-1;
        for($i = 0; $i<=$count;$i++)
        {
            if(!array_key_exists($uploadKeyy[$i],$data))
            {
                $data[$uploadKeyy[$i]] = array();
            }
        }
        if (isset($request->Bygglovbeviljat))
        {
            $data['Bygglovbeviljat'] = 1;
        }
        if (isset($request->Slutsamråd))
        {
            $data['Slutsamråd'] = 1;
        }
        if (isset($request->Interimistiskslutbesked))
        {
            $data['Interimistiskslutbesked'] = 1;
        }

        $project->projectInfo = serialize($data);
        $project->proAdmin = 1;
        $project->proManager= auth()->user()->id;
        $project->save();

        $project_id = $project->id;

        foreach ($tempInfo as $k => $row)
        {
            $arraySave []= array(
                'project_id' => $project_id,
                'user_id' => auth()->user()->id,
                'parent_id' => auth()->user()->parent_id,
                'templateInformation' => $tempInfo[$k]
            );
        }

        $save = design::insert($arraySave);
        Session::flash('successBaseProject','Location Data Entered succesfully');
        $tabindex = '';
        $projectName = $data['Projektnamn'];
        return view('Admin.project.controllePlan.index',compact(['project_id','tabindex','projectName']));
    }


    public function update($id,Request $request)
    {
        $projects = Project::find($id);
        $project = unserialize($projects->projectInfo);
        $projects->location = $request->location;
        $projectName = $project['Projektnamn'];
        if(isset($request->typeCreateInfoUser) and $request->typeCreateInfoUser == 1 and $request->userId !=0)
        {
            $projects->owner_id = $request->userId;
        }

        elseif(isset($request->name) and !isset($request->userIdEdit))
        {

            $validator =  $this->validate($request,[
                'name' => 'required',
                'email' => 'required|email|unique:users',
                'tel' => 'required',
            ]);
            $user = new User();
            $user->name = $request->name;
            $user->email = $request->email;
            $user->phone = $request->tel;
            $user->parent_id = auth()->user()->id;
            $user->is_owner = 1;
            $user->password = $request->phone;
            $user->ownerAddress = $request->address;
            if(isset($request->avatar))
            {
                $user->avatar = $this->uploadImages($request->avatar);
            }
            $user->save();
            $projects->owner_id = $user->id;
        }

        elseif(isset($request->name) and  isset($request->userIdEdit))
        {

            $user = User::where('id',$request->userIdEdit)->first();
//            return $user;
            if($user)
            {

                $validator =  $this->validate($request,[
                    'name' => 'required',
//                    'email' => 'unique:users,email,' , Rule::unique('users')->ignore($user->id),
                    'tel' => 'required',
                ]);

                $user->name = $request->name;
//                $user->email = $request->email;
                $user->phone = $request->tel;
                $user->password = $request->phone;
                $user->ownerAddress = $request->address;
                if(isset($request->avatar))
                {
                    $user->avatar = $this->uploadImages($request->avatar);
                }
                $user->save();
            }
        }


        $data = $request->all();
        $uploadKeyy =  array_keys($_FILES);
        unset($data['_token']);
        unset($data['location']);
        unset($data['typeCreateInfoUser']);
        unset($data['name']);
        unset($data['email']);
        unset($data['tel']);
        unset($data['userId']);
        unset($data['location']);
        unset($data['typeCreateInfoUser']);
        unset($data['address']);
        unset($data['userIdEdit']);

        unset($data['Kontrollansvarig']);
        unset($data['KAFöretag']);
        unset($data['KATelefon']);
        unset($data['email']);
        unset($data['KACertNr']);

        $arrayNotExists = [];
        foreach ($uploadKeyy as $row => $file)
        {
            if($request->hasFile($file))
            {
                $files = $request[$file];

                foreach ($files as $row2 => $fi)
                {
                    if(array_key_exists($file,$project))
                    {
                        array_push($project[$file],$this->uploadImages($fi));
                    }
                    else
                    {
                        array_push($arrayNotExists,$file);
                    }
                }
            }
        }

        $arrayNotExists = array_unique($arrayNotExists);
//        return $arrayNotExists;
        foreach ($arrayNotExists as $key => $row)
        {
            $project[$row] = [];
        }


        foreach ($uploadKeyy as $uploadKey)
        {
            if(in_array($uploadKey,$arrayNotExists))
            {

                if($request->hasFile($file))
                {
                    $files = $request[$file];
                    foreach ($files as $row2 => $fi)
                    {
                        // TODO LAST CHECK
                        array_push($project[$file],$this->uploadImages($fi));
                    }
                }
            }
        }
        $project['Projektnamn'] = $request->Projektnamn;
        $project['Fastighetsadress'] = $request->Fastighetsadress;
        $project['Fastighetsbeteckning'] = $request->Fastighetsbeteckning;
        $project['BNDiarienummer'] = $request->BNDiarienummer;
        $project['Byggherre'] = $request->Byggherre;
        $project['Bygglovbeviljat'] = $request->Bygglovbeviljat;
        $project['Startbesked'] = $request->Startbesked;
        $project['Slutsamråd'] = $request->Slutsamråd;
        $project['Slutbesked'] = $request->Slutbesked;
        $project['Interimistiskslutbesked'] = $request->Interimistiskslutbesked;
        $project['Hustyp'] = $request->Hustyp;
        $project['Grundkonstruktion'] = $request->Grundkonstruktion;
        $project['Värmesystem'] = $request->Värmesystem;
        $project['Ventilationssystem'] = $request->Ventilationssystem;
        $project['Teknisktsamråddatum'] = $request->Teknisktsamråddatum;
        $project['Entreprenadform'] = $request->Entreprenadform;
        $project['Övriginformation'] = $request->Övriginformation;
        $project['byggnad'] = $request->byggnad;
        if(isset($request->projectImage))
        {
            $project['projectImage'] = $this->uploadImages($request->projectImage);
        }

        if (isset($request->Bygglovbeviljat))
        {
            $project['Bygglovbeviljat'] = 1;
        }
        if (isset($request->Slutsamråd))
        {
            $project['Slutsamråd'] = 1;
        }
        if(isset($request->Interimistiskslutbesked))
        {
            $project['Interimistiskslutbesked'] = 1;
        }

        $projects->projectInfo = serialize($project);
        $projects->save();
        $project_id = $projects->id;
        $tabindex = '';
        Session::flash('successEditProject','Location Data Entered succesfully');
        return view('Admin.project.controllePlan.index',compact(['project_id','tabindex','projectName']));
    }


    public function delete(Request $request)
    {
        $project =  Project::find($request->id);
        $project->designs()->delete();
        $delete =  $project->delete();
        if ($delete == 1) {
            $success = true;
            $message = "Organization deleted successfully";
        } else {
            $success = false;
            $message = "Organization not found";
        }
        return response()->json([
            'success' => $success,
            'message' => $message,
        ]);
    }


    public function getInformation($id)
    {
        $params = array(['id' => 1 ,'title' => 'Fjärrvärme'],['id' => 2 ,'title' => 'El (direktverkande och elpanna) 1,0'],
            ['id' => 3 ,'title' => 'El, frånluftsvärmepump'] , ['id' => 4 ,'title' => 'El, El, uteluft-vattenvärmepump'] ,
            ['id' => 5 ,'title' => 'El, markvärmepump (berg, mark, sjö)'],['id' => 6 ,'title' => 'Biobränslepanna (pellets, ved, flis mm)'],
            ['id' => 7 ,'title' => 'Olja']);

        $p = Project::find($id);
        $project = unserialize($p->projectInfo);
        $project['id'] = $p->id;
        $byggnad = Building::where('id' , $project['byggnad'])->first(['typeCode' , 'description']);
        foreach ($params as $param)
        {
            if($param['id'] == $project['Värmesystem']);
            {
                $project['Värmesystem'] = $param['title'];
            }
        }
        if($byggnad)
        {
            $project['byggnad'] = $byggnad->typeCode .'-' . $byggnad->description;
        }

        if($project['Entreprenadform'] == 1)
        {
            $project['Entreprenadform'] = 'Totalentreprenad';
        }
        elseif($project['Entreprenadform'] == 2)
        {
            $project['Entreprenadform'] = 'Utförandeentreprenad';
        }
        elseif ($project['Entreprenadform'] == 3)
        {
            $project['Entreprenadform'] = 'Egen regi';
        }
        else
        {
            $project['Entreprenadform'] = '-';
        }

        return response()->json($project);
    }


    public function step(Request $request)
    {
        $project = Project::find($request->project_id);
        $project->step = $request->step;
        $project->save();
    }


    public function getStep(Request $request)
    {
        $project = Project::whereId($request->project_id)->pluck('step')->first();
        return $project;
    }


    public function uploadImages($file)
    {
        $current_timestamp = Carbon::now()->timestamp . rand(1,9999);
        $imagePath = "/upload/";
        $filename = $current_timestamp . $file->getClientOriginalName();
        $file = $file->move(public_path($imagePath) , $filename);
        return $imagePath.$filename;
    }


    //organizations
    public function getOrganizations()
    {
        $organizations =User::where('is_organization',1)->where('parent_id',auth()->user()->id)->get();
        return DataTables::of($organizations)
            ->addColumn('Name', function($data){
                return $data['organizationName'];
            })
            ->addColumn('Description', function($data){
                return $data['organizationDescription'];
            })
            ->addColumn('Address', function($data){
                return $data['email'];
            })
            ->addColumn('Contact', function($data){
                return $data['organizationContact'];
            })
            ->addColumn('Logo', function($data){
                $image = '';
                if($data['avatar'] !='')
                {
                    $avatar = $data['avatar'];
                    $image = '<a href="#" data-toggle="modal" data-target-src="'.$data['avatar'].'" data-target="#showIMAGE"><img alt="Avatar" class="img-circle table-avatar" width="30" height="30" src="'.$data['avatar'].'" data-toggle="tooltip" data-placement="top" title="'.$data['organizationName'].'">';
                }
                else
                {
                    $image = 'Not Logo';
                }
                return $image;
            })

            ->addColumn('Action',function ($data){
                $action = '<a href="#" class="mr-3" data-toggle="modal" data-target="#editorganization" data-target-id="'.$data['id'].'" data-target-name="'.$data['organizationName'].'" data-target-description="'.$data['organizationDescription'].'" data-target-address="'.$data['organizationAddress'].'" data-target-contact="'.$data['organizationContact'].'"  data-target-mobile="'.$data['mobile'].'" data-target-email="'.$data['email'].'" title="Edit" ><i class="fa fa-edit" aria-hidden="true"></i></a>';
                $action .= '<a href="#" class="mr-3" onclick="deleteConfirmationOrganization('.$data['id'].')" title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>';
                $action .= '<a href="'.route('Organization.users' , ['id' =>$data['id']]).'" class="mr-3" title="branch"><i class="fa fa-users" aria-hidden="true"></i></a>';
                $action .= '<a href="#" class="mr-3" data-toggle="modal" data-target="#CreatUser" data-target-organizationid="'.$data['id'].'"><i class="fa fa-user-plus" aria-hidden="true"></i></a>';
                return $action;
            })
            ->rawColumns(['Name','Description','Address','Contact','Logo','Action'])
            ->make(true);

    }


    public function getOrga()
    {
        $organizations = User::where('parent_id',auth()->user()->id)->where('is_organization',1)->get();
        $output = '<option value="">Selected Organization</option>';
        foreach ($organizations as $row => $value)
        {
            $output .= '<option value="'.$value->id.'">'.$value->name.'</option>';
        }
        return response()->json($output);
    }


    public function addOrganizations(Request $request)
    {
        $user = new User();
        $user->parent_id = auth()->user()->id;
        $user->name = $request->organizationName;
        $user->organizationName = $request->organizationName;
        $user->organizationDescription = $request->organizationDescription;
//        $user->organizationAddress = $request->organizationAddress;
        $user->organizationContact = $request->organizationContact;
        $user->is_organization = 1;
        $user->email = $request->organizationMailadress;
        $user->password = 123456789;
        $user->phone = $request->organizationContact;
        $user->mobile = $request->organizationMobiltel;
        $logo = '';
        if (request()->hasFile('organizationLogo')) {
            $logo = $this->uploadImages($request->file('organizationLogo'));
        }
        $user->avatar = $logo;
        $user->save();
        $role = [7];
        $user->roles()->sync($role);
        return response()->json('success',200);
    }


    public function addUser($id,Request $request)
    {
        $avatar = '';
        if (request()->hasFile('avatarUser')) {
            $avatar = $this->uploadImages($request->file('avatarUser'));
        }
        $users = new User();
        $users->name = $request->nameUser;
        $users->phone = $request->phoneUser;
        $users->email = $request->emailUser;
        $users->avatar = $avatar;
        $users->password = $request->phoneUser;
        $users->is_app = 1;
        $users->parent_organization_app = $id;
        $users->parent_id = auth()->user()->id;
        $users->experts = serialize($request->AnsvarigPartUser);
        $users->legitimationCard = $request->legitimationCard;
        $users->save();
        $role = [6];
        $users->roles()->sync($role);
        return response()->json('success',200);
    }


    public function deleteOrganization($id,Request $request)
    {
        $user =  User::find($id);
        $delete =  $user->delete();
        $ids= [$id];
        $parentUser = User::whereIn('parent_organization_app',$ids)->delete();
        if ($delete == 1) {
            $success = true;
            $message = "Organization deleted successfully";
        } else {
            $success = false;
            $message = "Organization not found";
        }
        return response()->json([
            'success' => $success,
            'message' => $message,
        ]);
    }


    public function updateOrganization($id,Request $request)
    {
        $user =User::find($id);
        $user->parent_id = auth()->user()->id;
        $user->name = $request->organizationNameEdit;
        $user->organizationName = $request->organizationNameEdit;
        $user->organizationDescription = $request->organizationDescriptionEdit;
//        $user->organizationAddress = $request->organizationAddressEdit;
        $user->organizationContact = $request->organizationContact;
        $user->is_organization = 1;
        $user->phone = $request->organizationContact;
        $user->mobile = $request->organizationMobiltelEdit;
        $user->email = $request->organizationMailadressEdit;
        if (request()->hasFile('organizationLogo')) {
            $user->avatar = $this->uploadImages($request->file('organizationLogo'));
        }
        $user->save();
        $role = [7];
        $user->roles()->sync($role);
        return response()->json('success',200);
    }


    public function ownerDelete(Request $request)
    {
        $project = Project::where('id',$request->id)->first();
        $project->owner_id = 0;
        $project->save();

        return response()->json([
            'success' => true,
            'message' => 'ok',
        ]);
    }


    public function kaConfirm(Request $request)
    {
        $project = Project::where('id',$request->projectId)->first();
        $confirmationHistory = [];
        if($project->confirmationHistory !='a:{}')
        {
            $confirmationHistory = unserialize($project->confirmationHistory);
        }
        if($request->koConfirmSelected !=null)
        {
            $project->supervisingConfirmation = $request->koConfirmSelected;
            $data  = [];
            $data['id'] = rand(1,99999);
            $data['date'] = now()->format('Y-m-d H:i:s');
            $data['user'] = auth()->user()->id;
            $data['role'] = 'Kontrollansvarig';
            if($request->supervisingConfirmation == 1)
            {
                $data['status'] = 'Tar ansvar';
            }
            else
            {
                $data['status'] = 'Underkänner kontrollplanen';
            }
            $data['description'] = $request->koDescription;
            array_push($confirmationHistory,$data);
            $project->confirmationHistory = serialize($confirmationHistory);
            $project->save();
            return response()->json('success');
        }
        else
        {
            return response()->json('validationError');
        }
    }


    public function bigherrConfirm(Request $request)
    {
        $project = Project::where('id',$request->projectId)->first();
        $confirmationHistory = [];
        if($project->confirmationHistory !='a:{}')
        {
            $confirmationHistory = unserialize($project->confirmationHistory);
        }
        if($request->ByggherrenConfirmSelected !=null)
        {
            $project->ownerConfirmation = $request->ByggherrenConfirmSelected;
            $data  = [];
            $data['id'] = rand(1,99999);
            $data['date'] = now()->format('Y-m-d H:i:s');
            $data['user'] = auth()->user()->id;
            $data['role'] = 'Byggherren';
            if($request->ByggherrenConfirmSelected == 1)
            {
                $data['status'] = 'Tar ansvar';
            }
            else
            {
                $data['status'] = 'Underkänner kontrollplanen';
            }
            $data['description'] = $request->ByggherrenDescription;
            array_push($confirmationHistory,$data);
            $project->confirmationHistory = serialize($confirmationHistory);
            $project->save();
            return response()->json('success');
        }
        else
        {
            return response()->json('validationError');
        }
    }


    public function bignadConfirm(Request $request)
    {
        $project = Project::where('id',$request->projectId)->first();
        $confirmationHistory = [];
        if($project->confirmationHistory !='a:{}')
        {
            $confirmationHistory = unserialize($project->confirmationHistory);
        }
        if($request->ByggnadsnConfirmSelected !=null)
        {
            $project->municipalityConfirmation = $request->ByggnadsnConfirmSelected;
            $data  = [];
            $data['id'] = rand(1,99999);
            $data['date'] = now()->format('Y-m-d H:i:s');
            $data['user'] = auth()->user()->id;
            $data['role'] = 'Byggnadsnämnden';
            if($request->ByggnadsnConfirmSelected == 1)
            {
                $data['status'] = 'Tar ansvar';
            }
            else
            {
                $data['status'] = 'Underkänner kontrollplanen';
            }
            $data['description'] = $request->ByggherrenDescription;
            array_push($confirmationHistory,$data);
            $project->confirmationHistory = serialize($confirmationHistory);
            $project->save();
            return response()->json('success');
        }
        else
        {
            return response()->json('validationError');
        }
    }


    public function supervisingEngineerConfirm(Request $request)
    {
        $project = Project::where('id',$request->projectId)->first();
        $confirmationHistory = [];
        if($project->confirmationHistory !='a:{}')
        {
            $confirmationHistory = unserialize($project->confirmationHistory);
        }
        $project->supervisingEngineerConfirmation = 1;
        $data  = [];
        $data['id'] = rand(1,99999);
        $data['date'] = now()->format('Y-m-d H:i:s');
        $data['user'] = auth()->user()->id;
        $data['role'] = 'Platsledning';
        $data['status'] = 'Tar ansvar';
        $data['description'] = "";
        array_push($confirmationHistory,$data);
        $project->confirmationHistory = serialize($confirmationHistory);
        $project->save();
        return response()->json('success');

    }


    public function showHistory(Request $request)
    {
        $project = Project::where('id',$request->projectId)->first();
        $history = [];
        if($project->confirmationHistory !='a:{}')
        {
            $history = unserialize($project->confirmationHistory);
        }
        if(count($history) !=0)
        {
            foreach ($history as $key => $row)
            {
                $user = User::where('id',$row['user'])->first();
                $name = '';
                if($user->is_supervisingCompany == 1)
                {
                    $name =  $user->companyName;
                }
                else
                {
                    $name =  $user->name;
                }
                $history[$key]['user'] = $name;
            }
            return $history;
        }
        else
        {
            return response()->json('NotingHistory');
        }

    }



    public function searchInMap(Request $request){

        $lat = $request['lat'] ? $request['lat'] : '32.65';
        $lng = $request['lng'] ? $request['lng'] : '51.70';
        $title = $request['title'] ? $request['title'] : 'اصفهان';

        $response = Http::withHeaders([
            'Api-Key' => 'service.UQJB8BAh8sPvDPmC9xsPvsCtFncQntMijvTs6tJf'
        ])->get('https://photon.komoot.io/api/?q='.$title);
        $response = isset($response['features']) ? $response['features'] : [];
        $params = [];
        foreach ($response as $key=>$value){

            $params[$key]['lat'] = $value['geometry']['coordinates'][1];
            $params[$key]['lng'] = $value['geometry']['coordinates'][0];
            $params[$key]['title'] = $value['properties']['name'];

        }

        return $params;
    }



}
