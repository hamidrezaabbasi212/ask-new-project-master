<?php

namespace App\Http\Controllers\Admin;

use App\Definitions\AnsvarigPart;
use App\Definitions\Kontrollskede;
use App\Definitions\KraverPlatsbesok;
use App\Definitions\PBLCategory;
use App\Definitions\ProduceratIntyg;
use App\Definitions\Risk;
use App\Definitions\Verifieringsmetod;
use App\Definitions\TemplateCategory;
use App\design;
use App\Http\Controllers\Controller;
use App\Template;
use App\TemplateInformation;
use App\User;
use Illuminate\Http\Request;

class TemplateController extends Controller
{
    public function store(Request $request)
    {

        $count = count($request->designs);
        $template = new Template();
        $template->templateName = $request->templateName;
        $template->templateCategory = $request->templateCategory;
        $template->user_id = auth()->user()->id;
        $template->parent_id = auth()->user()->parent_id;
        $template->is_supper_user = $request->is_supper_user;
        $template->save();
        $template_id = $template->id;
        for ($i = 0; $i < $count; $i++) {
            $tempInfo = design::where('id', $request->designs[$i])->pluck('templateInformation')->first();
            $data [] =
                array('templateInformation' => $tempInfo,
                    'template_id' => $template_id);
        }

        TemplateInformation::insert($data);
    }

    public function addTemplate(Request $request)
    {
        $template = new Template();
        $template->templateName = $request->templateName;
        $template->templateCategory = $request->templateCategory;
        $template->user_id = auth()->user()->id;
        $template->parent_id = auth()->user()->parent_id;
        $template->is_supper_user = 1;
        $template->save();

        return response()->json('success', 200);
    }

    public function addDesignPage($id)
    {
        return redirect(route('template.addDesign', ['id' => $id]));
    }

    public function addDesign(Request $request)
    {
        return view('Admin.project.template.addDesign');
    }

    public function saveDesign(Request $request)
    {
//        return $request->all();
        $templateInformation = new TemplateInformation();
        $data = $request->all();
        $templateInformation->template_id = $request->id; //TODO Last Set template_id
        unset($data['_token']);
        unset($data['id']);
        $templateInformation->templateInformation = serialize($data);
        $templateInformation->save();
        return response()->json('success', 200);


    }

    public function getTemplate($id)
    {
        //TODO Get Base Project_id
        $design = TemplateInformation::where('template_id', $id)->get();
        $designid = $design->pluck('id');

        $templateInformation;
        foreach ($design as $row => $template) {
            $templateInformation [] = unserialize($template->templateInformation);
            $templateInformation [$row]['id'] = $template->id;
        }

        $output = '';
        foreach ($templateInformation as $k => $row) {
            $pblcategory = PBLCategory::where('id',$row['PBLKategori'])->pluck('title')->first();
            $AnsvarigPart = AnsvarigPart::where('id',$row['AnsvarigPart'])->pluck('title')->first();
            $Kontrollskede = Kontrollskede::where('id',$row['Kontrollskede'])->pluck('title')->first();
            $Risk = Risk::where('id',$row['Risk'])->pluck('title')->first();

            if($Kontrollskede == '')
            {
                $Kontrollskede = $row['kontrollskedeText'];
            }

            if($Risk == '')
            {
                $Risk = $row['riskText'];
            }
            $ProduceratIntyg = ProduceratIntyg::where('id',$row['ProduceratIntyg'])->pluck('title')->first();
            if($ProduceratIntyg == '')
            {
                $ProduceratIntyg = $row['ProduceratIntygText'];
            }
            $Verifieringsmetod = Verifieringsmetod::where('id',$row['Verifieringsmetod'])->pluck('title')->first();
            if($Verifieringsmetod == '')
            {
                $Verifieringsmetod = $row['VerifieringsmetodText'];
            }
            $KraverPlatsbesok = KraverPlatsbesok::where('id',$row['KraverPlatsbesok'])->pluck('title')->first();
            if($KraverPlatsbesok == '')
            {
                $KraverPlatsbesok = $row['KraverPlatsbesokText'];
            }

            $output .= '<li id="controlplan'.$row['id'].'" class="liclick" onClick="getTemplateid('.$row['id'].');select(this)"><i class="fa fa-arrow-right" aria-hidden="true"></i>'.$row['BSABAMA'].' - '.$pblcategory.'
            - ' .$AnsvarigPart.' - '.$Kontrollskede.' - '.$Risk.'-'.$ProduceratIntyg.' - '.$Verifieringsmetod.' - '.$KraverPlatsbesok.'</li>';
        }
        return response()->json(['output' => $output, 'design' => $designid]);
    }

    public function getTemp($id)
    {
        $design = TemplateInformation::whereId($id)->first();
        $template = unserialize($design->templateInformation);
        $template['id'] = $design->id;
        return response()->json($template);
    }

    public function designUpdate($id,Request $request)
    {

        $design = TemplateInformation::whereId($id)->first();
        $data = $request->all();
        $designItem = unserialize($design->templateInformation);
        $data = $request->all();
        unset($data['_token']);
        $designItem['BSABAMA'] = $data['BSABAMA'];
        $designItem['Kontrollskede'] = $data['Kontrollskede'];

        $designItem['LawText'] = $data['LawText'];
        $designItem['PBLKategori'] = $data['PBLKategori'];

        $designItem['Risk'] = $data['Risk'];
        $designItem['riskText'] = $data['riskText'];
        $designItem['riskDescription'] = $data['riskDescription'];

        $designItem['ProduceratIntyg'] = $data['ProduceratIntyg'];
        $designItem['ProduceratIntygText'] = $data['ProduceratIntygText'];
        $designItem['ProduceratIntygDescription'] = $data['ProduceratIntygDescription'];

        $designItem['Verifieringsmetod'] = $data['Verifieringsmetod'];
        $designItem['VerifieringsmetodText'] = $data['VerifieringsmetodText'];
        $designItem['VerifieringsmetodDescription'] = $data['VerifieringsmetodDescription'];

        $designItem['KraverPlatsbesok'] = $data['KraverPlatsbesok'];
        $designItem['KraverPlatsbesokText'] = $data['KraverPlatsbesokText'];
        $designItem['KraverPlatsbesokDescription'] = $data['KraverPlatsbesokDescription'];
        $design->templateInformation =serialize($designItem);
        $design->save();
        return response()->json('success' , 200);

    }

    public function index()
    {
        $templates = Template::paginate(10);
        foreach ($templates as $row =>$template)
        {
            $templates[$row]['user_id'] =  User::whereId($template['user_id'] )->pluck('name')->first();
            $templates[$row]['templateCategoryid'] =  TemplateCategory::whereId($template['templateCategory'])->pluck('id')->first();
            $templates[$row]['templateCategory'] =  TemplateCategory::whereId($template['templateCategory'])->pluck('title')->first();

        }
        return view('Admin.project.template.index',compact('templates'));
    }

    public function getData(Request $request)
    {
        if($request->ajax())
        {
        $templates = Template::where('is_supper_user',1)->paginate(10);
        foreach ($templates as $row =>$template)
        {
            $templates[$row]['user_id'] =  User::whereId($template['user_id'] )->pluck('name')->first();
            $templates[$row]['templateCategoryid'] =  TemplateCategory::whereId($template['templateCategory'])->pluck('id')->first();
            $templates[$row]['templateCategory'] =  TemplateCategory::whereId($template['templateCategory'])->pluck('title')->first();

        }

            return view('Admin.project.template.loadDataTemplate',compact('templates'));
        }


    }

    public function edit($id)
    {
        return $id;
    }

    public function update($id,Request $request)
    {
        $template = Template::whereId($id)->first();
        $template->templateName = $request->templateName;
        $template->templateCategory = $request->templateCategory;
        $template->user_id = auth()->user()->id;
        $template->parent_id = auth()->user()->parent_id;
        $template->save();
        return response()->json('success' , 200);
    }

    public function delete($id)
    {
        $delete =  Template::where('id', $id)->delete();
        $template =  TemplateInformation::where('template_id', $id)->delete();
        if ($delete == 1) {
            $success = true;
            $message = "Template deleted successfully";
        } else {
            $success = true;
            $message = "Template not found";
        }
        return response()->json([
            'success' => $success,
            'message' => $message,
        ]);
    }

    public function designDelete($id)
    {
        $delete =  TemplateInformation::where('id', $id)->delete();
        if ($delete == 1) {
            $success = true;
            $message = "design deleted successfully";
        } else {
            $success = true;
            $message = "design not found";
        }
        return response()->json([
            'success' => $success,
            'message' => $message,
        ]);
    }


}
