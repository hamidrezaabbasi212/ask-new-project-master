<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Ticket;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class TicketController extends Controller
{
    public function index()
    {
        $user = auth()->user();
        $tickets = Ticket::where('projectEngineer_id',$user->id)->where('parent_id',0)->orderBy('id','desc')->paginate(20);
        return view('Admin.ticket.all',compact('tickets'));
    }

    public function getTicket($id)
    {
        $tickets = Ticket::where('parent_id',$id)->orWhere('id',$id)->orderBy('id','desc')->get();
        $appUser_id = $tickets[0]['appUser_id'];
        return view('Admin.ticket.chat',compact(['tickets','id','appUser_id']));
    }

    public function sendTicket(Request $request)
    {
        $ticket = new Ticket();
        $ticket->parent_id = $request->chet_id;
        $ticket->projectEngineer_id = auth()->user()->id;
        $ticket->appUser_id = $request->appUser_id;
        $ticket->subject = '';
        $ticket->project_id = 0;
        $ticket->design_id = 0;
        $ticket->type = 1;
        if($request->hasFile('attachment'))
        {
            $ticket->attachment = $this->uploadImages($request->attachment);
        }
        else
        {
            $ticket->attachment = '';
        }
        $ticket->description = $request->message;
        $ticket->save();
        $title = 'Ny Biljett';
        $body = 'du fick ett meddelande.';
        $userfirebaseToken = User::where('id',$request->appUser_id)->pluck('firebaseToken')->first();
        pushNotification($userfirebaseToken,$title,$body);
        Session::flash('success','Location Data Entered succesfully');
        return back();
    }

    public function delete(Request $request)
    {
        $ids = [];
        array_push($ids,$request->id);

        $ticket = Ticket::whereIn('parent_id', $ids)->orWhereIn('id',$ids)->delete();
       if($ticket == 1)
       {
           response()->json(['success'=>1],200);
       }
    }

    public function create()
    {
        return view('Admin.ticket.newTicket');
    }

    public function store(Request $request)
    {
        $ticket = new Ticket();
        $ticket->projectEngineer_id = auth()->user()->id;
        $ticket->appUser_id = $request->appUser_id;
        $ticket->subject = $request->subject;
        $ticket->description = $request->description;
        if($request->hasFile('attachment'))
        {
            $ticket->attachment = $this->uploadImages($request->attachment);
        }
        else
        {
            $ticket->attachment  = '';
        }
        $ticket->project_id = $request->project_id;
        $ticket->design_id = $request->design_id;
        $ticket->type = 1;
        $ticket->status = 0;
        $ticket->save();
        $title = 'Ny Biljett';
        $body = 'du fick ett meddelande.';
        $userfirebaseToken = User::where('id',$request->appUser_id)->pluck('firebaseToken')->first();
        pushNotification($userfirebaseToken,$title,$body);
        Session::flash('success','Location Data Entered succesfully');
        return redirect()->route('supportTicket.all');
    }

    public function uploadImages($file)
    {
        $current_timestamp = Carbon::now()->timestamp;
        $imagePath = "/upload/";
        $filename = $current_timestamp . $file->getClientOriginalName();
        $file = $file->move(public_path($imagePath) , $filename);
        return $imagePath.$filename;
    }
}
