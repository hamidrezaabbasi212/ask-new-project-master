<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Role;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Validation\Rule;

class UserController extends Controller
{
    public function index()
    {
        //TODO CHECK ROWS TABLE
        $accounts = User::orderBy('id','DESC')->paginate(10);
        return view('Admin.User.index',compact('accounts'));
    }

    public function create()
    {
        $user = auth()->user();
        $param = [];
        $roles = Role::all();
        if($user->is_supperuser == 1)
        {
            $param = [1,2,4,5];
        }
        elseif($user->is_supervisingCompany == 1)
        {
            $param = [3,4,5];
        }
        elseif ($user->is_supervisingEngineer ==1)
        {
            $param = [6,7];
        }

        $roles = $roles->whereIn('id',$param);
        return view('Admin.User.create',compact('roles'));

    }

    public function store(Request $request)
    {
        $validator =  $this->validate($request,[
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'phone' => ['required', 'string'],
            'roles' => ['required'],
            'avatar' => ['required','mimes:jpeg,bmp,png','max:2048'],
        ]);
        if ($validator)
        {
            $avatar ='';
            if($request->hasFile('avatar'))
            {
                $avatar = $this->uploadImages($request->avatar);
            }
            $user = new User();
            $user->name = $request->name;
            $user->email = $request->email;
            $user->phone = $request->phone;
            if(auth()->user()->is_supperuser == 1)
            {
                $user->parent_id = 0;
            }
            else
            {
                $user->parent_id = auth()->user()->id;
           }

            $user->avatar = $avatar;
            $user->password = $request->phone;
            $user->save();
            if(count($request->roles) > 0)
            {
                $user->roles()->sync($request->roles);
            }

            Session::flash('success','Location Data Entered succesfully');
        }
        return redirect(route('account.index'));
    }

    public function edit($id)
    {
        $account = User::find($id);
        return view('Admin.User.edit',compact('account'));
    }

    public function update($id,Request $request)
    {
        $user = User::find($id);
        $data = $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', Rule::unique('users')->ignore($user->id)],
            'phone' => ['required', 'string'],
            'roles' => ['required'],
            'avatar' => ['mimes:jpeg,bmp,png','max:2048'],
        ]);

//        if(! is_null($request->password)) {
//            $request->validate([
//                'password' => ['required', 'string', 'min:8', 'confirmed'],
//            ]);
//
//            $user->password  = $request->password;
//        }

        if($request->hasFile('avatar'))
        {
            $user->avatar =  $this->uploadImages($request->avatar);
        }
        if(auth()->user()->is_supperuser == 1)
        {
            $user->parent_id = 0;
        }
        else
        {
            $user->parent_id = auth()->user()->id;
        }

        $user->name = $request->name;
        $user->email = $request->email;
        $user->phone = $request->phone;
        $user->save();
        if(count($request->roles) > 0)
        {
            $user->roles()->sync($request->roles);
        }
        $user->roles()->sync($request->roles);
        Session::flash('edit','Location Data Entered succesfully');
        return redirect(route('account.index'));
    }

    public function delete($id)
    {
        $delete =  User::where('id', $id)->delete();
        if ($delete == 1) {
            $success = true;
            $message = "User deleted successfully";
        } else {
            $success = true;
            $message = "User not found";
        }
        return response()->json([
            'success' => $success,
            'message' => $message,
        ]);
    }

    public function uploadImages($file)
    {
        $current_timestamp = Carbon::now()->timestamp;
        $imagePath = "/upload/";
        $filename = $current_timestamp . $file->getClientOriginalName();
        $file = $file->move(public_path($imagePath) , $filename);
        return $imagePath.$filename;
    }
}
