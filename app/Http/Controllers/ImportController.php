<?php

namespace App\Http\Controllers;

use App\Imports\ImportLaw;
use App\LawImport;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;


class ImportController extends Controller
{
    public function import(Request $request)
    {
        Excel::import(new ImportLaw(),request()->file('laws'));

        return response()->json(['message' => 'success'] , 200);

    }


    public function update(Request $request)
    {
        $data = $request->all();
        unset($data['_token']);
        $ids = [];
        foreach ($data as $key => $row) {
            $law = LawImport::where('id', $key)->first();
            $check1 = array_key_exists('check1', $row) ? 'KontrollplanPBL' : '';
            $check2 = array_key_exists('check2', $row) ? 'KontrollplanAvtal' : '';
            if ($check1 != '' || $check2 != '')
            {
                array_push($ids , $law->id);
                $law->check1 = $check1;
                $law->check2 = $check2;
                $law->save();
            }
        }
        LawImport::whereNotIn('id' , $ids)->delete();
        return response()->json(['message' => 'success'] , 200);
    }


    public function infoProject()
    {
        $checkImport = 1;
        $laws = LawImport::where('user_id' , auth()->user()->id)->get(['code' , 'title' , 'text' , 'check1' , 'check2']);
        $laws = $laws->toArray();
        $laws  = serialize($laws);
        LawImport::where('user_id' , auth()->user()->id)->delete();
        return view('Admin.project.infoProject', compact(['checkImport' , 'laws']));
    }


    public function laws()
    {
        $imports = LawImport::where('user_id' , auth()->user()->id)->get();
        return view('Admin.project.importList' , compact('imports'));
    }
}
