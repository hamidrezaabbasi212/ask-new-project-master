<?php

namespace App\Http\Controllers\api;

use App\design;
use App\Http\Controllers\Controller;
use App\Project\Project;
use Illuminate\Http\Request;

class DefinitionController extends Controller
{
    public function getProjectList()
    {
        $user = auth()->user();
        $designs = design::where('assign_user',$user->id)->get();
        $projectIds = [];
        foreach ($designs as $design)
        {
            array_push($projectIds,$design->project_id);
        }
        $projects = Project::whereIn('id',$projectIds)->get();
        foreach ($projects as $key => $project)
        {
            $infoProject = [];
            $infoProject = unserialize($projects[$key]['projectInfo']);
            $projects[$key]['name'] = $infoProject['Projektnamn'];
            unset($projects[$key]['proAdmin']);
            unset($projects[$key]['proManager']);
            unset($projects[$key]['engineer_id']);
            unset($projects[$key]['projectInfo']);
            unset($projects[$key]['location']);
            unset($projects[$key]['step']);
            unset($projects[$key]['created_at']);
            unset($projects[$key]['updated_at']);
        }
        return response()->json($projects);
    }


    public function getAgentControllersProject($id)
    {
        $user = auth()->user();
        $designs = design::where('project_id',$id)->where('assign_user',$user->id)->get(['id','templateInformation']);

        $designInformation = [];

        foreach ($designs as $key => $design)
        {
            $infoDesign = [];
            $infoDesign = unserialize($designs[$key]['templateInformation']);
            $designInformation[$key]['id'] = $design->id;
            $designInformation[$key]['agentName'] = $infoDesign['BSABAMA'];
        }

        return response()->json($designInformation);
    }


}
