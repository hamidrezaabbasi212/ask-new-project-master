<?php

namespace App\Http\Controllers\api;

use App\AdvanceLawDesign;
use App\Definitions\AnsvarigPart;
use App\Definitions\Kontrollskede;
use App\Definitions\KraverPlatsbesok;
use App\Definitions\PBLCategory;
use App\Definitions\ProduceratIntyg;
use App\Definitions\Risk;
use App\Definitions\Verifieringsmetod;
use App\design;
use App\Http\Controllers\Controller;
use App\Project\Project;
use App\Ticket;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProjectController extends Controller
{
    public function index()
    {
        $user = Auth::user();
        $design = design::where('assign_user',$user->id)->orderBy('id','desc')->get();
        $projectIds = [];
        foreach ($design as $key => $row)
        {
            $project = Project::where('id',$row['project_id'])->first();
            if($project)
            {
                array_push($projectIds,$project->id);
            }
        }
        $projectIds = array_unique($projectIds);
        $projects = Project::whereIn('id',$projectIds)->orderBy('id','desc')->get();
        foreach ($projects as $key => $project)
        {
            $projectInfo = unserialize($project->projectInfo);
            $projectInformation[$key]['id'] = $project->id;
            $projectInformation[$key]['projectName'] = $projectInfo['Projektnamn'];
            $projectInformation[$key]['phone'] = "";
            $projectInformation[$key]['email'] = "";
            if($project->proManager !=null and $project->proManager !=0)
            {
                $projectInformation[$key]['phone'] = User::where('id',$project->proManager)->pluck('phone')->first();
                $projectInformation[$key]['email'] = User::where('id',$project->proManager)->pluck('email')->first();
            }

            $location = explode(',',$project->location);
            $location = array_reverse($location);
            $lat = $location[0];
            $lng = $location[1];
            $loca  = $lat . ',' . $lng;
            $projectInformation[$key]['location'] = $loca;

        }

        return response()->json($projectInformation,200);
    }

    public function projectDetails($id)
    {
        $project  = Project::where('id',$id)->first();
        $projectInformation = unserialize($project->projectInfo);
        $projectInformation['location'] = $project->location;
        $projectInformation['created_at'] = $project->created_at;
        unset($projectInformation['Brandskyddsbeskrivning']);
        unset($projectInformation['Projektbeskrivning']);
        unset($projectInformation['Geotekniskundersökning']);
        unset($projectInformation['Fuktsäkerhetsprojektering']);
        unset($projectInformation['Energiberäkning']);
        unset($projectInformation['APDplan']);
        unset($projectInformation['Sprängplan']);
        unset($projectInformation['Tillgänglighetsbeskrivning']);
        unset($projectInformation['Konstruktionsdokumentation']);
        unset($projectInformation['Konstruktionsritningar']);
        unset($projectInformation['VVSritningar']);
        unset($projectInformation['VAplan']);
        unset($projectInformation['Bullerutredning']);
        unset($projectInformation['Dagsljusberäkning']);
        unset($projectInformation['Marksaneringsplan']);
        unset($projectInformation['Dagvattenutredning']);
        unset($projectInformation['OVK&Luftflödesprotokoll']);
        unset($projectInformation['Intygtäthetsprovning']);
        unset($projectInformation['IntygMiljö&Hälsoskyddsnämnden']);

        return response()->json($projectInformation,200);

    }

    public function agentControl($id)
    {
        $user = auth()->user();
        $designs = design::where('project_id',$id)->get();
        foreach ($designs as $key => $design)
        {
            $color = '';
            $templateInformation = unserialize($design['templateInformation']);
            $templateInformations[$key]['designId'] = $design->id;
            $templateInformations[$key]['BSABAMA'] = $templateInformation['BSABAMA'];
            $templateInformations[$key]['PBL'] = PBLCategory::where('id',$templateInformation['PBLKategori'])->pluck('title')->first();
            $templateInformations[$key]['AnsvarigPart'] = AnsvarigPart::where('id',$templateInformation['AnsvarigPart'])->pluck('title')->first();
            $status = $design->status;
            $statusTitle = '';
            if($status == 0)
            {
                $color = 'f7f7f7';
            }
            elseif ($status == 1)
            {
                $color = 'ccf1fd';
            }
            elseif ($status == 2)
            {
                $color = 'fafaad';
            }
            elseif ($status == 3)
            {
                $color = 'fae5c0';
            }
            elseif ($status == 4)
            {
                $color = 'd9fdd9';
            }
            elseif ($status == 5)
            {
                $color = 'c1f0c1';
            }
            elseif ($status == 6)
            {
                $color = 'fad8fa';
            }
            elseif ($status == 7)
            {
                $color = 'eebcee';
            }
            elseif ($status == 8)
            {
                $color = 'f2e599';
            }
            elseif ($status == 9)
            {
                $color = 'fddada';
            }
            elseif ($status == 10)
            {
                $color = 'f0b6b6';
            }

            $templateInformations[$key]['tdColor'] = $color;
        }
        return response()->json($templateInformations,200);
    }

    public function assignedAgentControl($id)
    {
        $user = auth()->user();
        $designs = design::where('project_id',$id)->where('assign_user',$user->id)->get();
        foreach ($designs as $key => $design)
        {
            $templateInformation = unserialize($design['templateInformation']);
            $templateInformations[$key]['designId'] = $design->id;
            $templateInformations[$key]['BSABAMA'] = $templateInformation['BSABAMA'];
            $templateInformations[$key]['PBL'] = PBLCategory::where('id',$templateInformation['PBLKategori'])->pluck('title')->first();
            $templateInformations[$key]['AnsvarigPart'] = AnsvarigPart::where('id',$templateInformation['AnsvarigPart'])->pluck('title')->first();
            $status = $design->status;
            $statusTitle = '';
            if($status == 0)
            {
                $color = 'f7f7f7';
            }
            elseif ($status == 1)
            {
                $color = 'ccf1fd';
            }
            elseif ($status == 2)
            {
                $color = 'fafaad';
            }
            elseif ($status == 3)
            {
                $color = 'fae5c0';
            }
            elseif ($status == 4)
            {
                $color = 'd9fdd9';
            }
            elseif ($status == 5)
            {
                $color = 'c1f0c1';
            }
            elseif ($status == 6)
            {
                $color = 'fad8fa';
            }
            elseif ($status == 7)
            {
                $color = 'eebcee';
            }
            elseif ($status == 8)
            {
                $color = 'f2e599';
            }
            elseif ($status == 9)
            {
                $color = 'fddada';
            }
            elseif ($status == 10)
            {
                $color = 'f0b6b6';
            }
            $templateInformations[$key]['tdColor'] = $color;
            $templateInformations[$key]['status'] = $design->status;
        }
        return response()->json($templateInformations,200);
    }

    public function agentControlDetails($id)
    {
        $user = Auth::user();
        $design = design::where('id',$id)->first();
        $design->seen = 1;
        $array = [];
        if($design->history !=null)
        {
            $array = unserialize($design->history);
        }
        if($design->status < 2)
        {
            $history = ['status'=> 'Mottaget' , 'time' => Carbon::now()->format('Y-m-d H:i') , 'file' => '' , 'description' => '' , 'image' => '' , 'video' => ''];
            array_push($array,$history);
            $design->history = serialize($array);
            $design->status = 2;
            $design->save();
        }
        $templateInformation = unserialize($design->templateInformation);
        $templateInformation['id'] = $design->id;
        $templateInformation['projectId'] = $design->project_id;
        $templateInformation['PBLKategori'] = PBLCategory::where('id',$templateInformation['PBLKategori'])->pluck('title')->first();
        $templateInformation['AnsvarigPart'] = AnsvarigPart::where('id',$templateInformation['AnsvarigPart'])->pluck('title')->first();
        $templateInformation['Kontrollskede'] = Kontrollskede::where('id',$templateInformation['Kontrollskede'])->pluck('title')->first();
        $templateInformation['Risk'] = Risk::where('id',$templateInformation['Risk'])->pluck('title')->first();
        $templateInformation['ProduceratIntyg'] = ProduceratIntyg::where('id',$templateInformation['ProduceratIntyg'])->pluck('title')->first();
        $templateInformation['Verifieringsmetod'] = Verifieringsmetod::where('id',$templateInformation['Verifieringsmetod'])->pluck('title')->first();
        $templateInformation['KraverPlatsbesok'] = KraverPlatsbesok::where('id',$templateInformation['KraverPlatsbesok'])->pluck('title')->first();
//        $templateInformation['LawText'] = strip_tags($templateInformation['LawText']);
        $templateInformation['guid'] = $design->guid;
        $templateInformation['operationAccess'] = $user->id ==  $design->assign_user ? 1 : 0;
        $templateInformation['status'] = $user->id ==  $design->assign_user ? 1 : 0;
        $templateInformation['verify'] = $design->verify;
        return response()->json($templateInformation,200);
    }

    public function agentControllerGoToVerify($id)
    {
        return response(['id'=>$id],200);
    }

    public function extraLaw($id)
    {
        $extraLaw = AdvanceLawDesign::where('design_id',72)->get();

        if($extraLaw)
        {
            foreach ($extraLaw as $key => $extra)
            {
                $extraLaw[$key]['title'] = 'test'.$key;
            }
            return response()->json($extraLaw,200);
        }

        return response()->json(['message' => 'error'], 401);
    }

    public function attachmentFiles($id)
    {
        $project = Project::where('id',$id)->first();

        $projectInformation = unserialize($project->projectInfo);

//        $param = ['Brandskyddsbeskrivning','Konstruktionsdokumentation','Projektbeskrivning','Geotekniskundersökning'
//            ,'Fuktsäkerhetsprojektering','Energiberäkning','APDplan','Sprängplan','Tillgänglighetsbeskrivning','Konstruktionsritningar',
//            'VVSritningar','VAplan','Bullerutredning','Dagsljusberäkning','Marksaneringsplan','Dagvattenutredning'
//            ,'OVK&Luftflödesprotokoll','Intygtäthetsprovning','IntygMiljö&Hälsoskyddsnämnden'];
        unset($projectInformation['Projektnamn']);
        unset($projectInformation['Fastighetsadress']);
        unset($projectInformation['Fastighetsbeteckning']);
        unset($projectInformation['BNDiarienummer']);
        unset($projectInformation['Byggherre']);
        unset($projectInformation['Bygglovbeviljat']);
        unset($projectInformation['Startbesked']);
        unset($projectInformation['Slutsamråd']);
        unset($projectInformation['Slutbesked']);
        unset($projectInformation['Interimistiskslutbesked']);
        unset($projectInformation['Hustyp']);
        unset($projectInformation['Grundkonstruktion']);
        unset($projectInformation['Värmesystem']);
        unset($projectInformation['Ventilationssystem']);
        unset($projectInformation['Teknisktsamråddatum']);
        unset($projectInformation['Entreprenadform']);
        unset($projectInformation['Övriginformation']);


        return response()->json($projectInformation);
    }

    public function agentHistory($id)
    {
        $design = design::where('id',$id)->first();
        $history = unserialize($design->history);
        $array = [];
        foreach ($history as $key => $value)
        {
            $array[$key]['status'] = $value['status'];
            $array[$key]['time'] = $value['time'];
            $array[$key]['description'] = isset($value['description']) ? $value['description'] : '';
            $array[$key]['file'] = isset($value['file']) ? $value['file'] : '';
            $array[$key]['image'] = isset($value['image']) ? $value['image'] : '';
            $array[$key]['video'] = isset($value['video']) ? $value['video'] : '';
        }
        return response()->json($array);
    }

    public function agentControllVerify(Request $request)
    {

        $design = design::where('id',$request->id)->first();
        $verifyInfo = [];
        $array = [];
        if($design->history !=null)
        {
            $array = unserialize($design->history);
        }
        if($request->approved == 1)
        {
            $design->status = 3;
            $design->verify = 1;
            $history = ['status'=> 'Godkänt innehåll' , 'time' => Carbon::now()->format('Y-m-d H:i') ,
                'file' => '',
                'description' => '' ,
                'image' => '',
                'video' => ''];
            array_push($array,$history);
            $design->history = serialize($array);
            $design->save();
            return response()->json(['message' => 'success'], 200);
        }
        elseif ($request->approved == 2)
        {
            $design->status = 4;
            $history = ['status'=> 'Funktion uppfylld' ,
                'time' => Carbon::now()->format('Y-m-d H:i') ,
                'file' => $request->hasFile('file') ? $this->uploadImages('file') : '',
                'description' => $request->description ,
                'image' => $request->hasFile('image') ? $this->uploadImages('image') : '',
                'video' => $request->hasFile('video') ? $this->uploadImages('video') : ''];
            array_push($array,$history);
            $design->history = serialize($array);
            $design->save();
            return response()->json(['message' => 'success'], 200);
        }
        else
        {
            $design->status = 9;
            $design->verify = 0;
//            $verifyInfo['verifyId'] = rand(1,99999);
//            $verifyInfo['description'] = $request->description;
//            $verifyInfo['attachment'] = '';
//            if($request->hasFile('attachment'))
//            {
//                $verifyInfo['attachment'] = $this->uploadImages($request->attachment);
//            }
//            $verifyInfo['date'] = Carbon::now()->format('Y-m-d H:i');
//            $design->verifyInfo = serialize($verifyInfo);
            $history = ['status'=> 'Egenkontroll Underkänt' ,'time' => Carbon::now()->format('Y-m-d H:i') ,
                'file' => $request->hasFile('file') ? $this->uploadImages($request->file) : '',
                'description' => $request->description ,
                'image' => $request->hasFile('image') ? $this->uploadImages($request->image) : '',
                'video' => $request->hasFile('video') ? $this->uploadImages($request->video) : ''];
            array_push($array,$history);
            $design->history = serialize($array);
            $design->save();
            return response()->json(['message' => 'success'], 200);
        }

    }


    public function agentControlAttachment($id)
    {
        $design = design::where('id',$id)->first();
        $array = [];
        $attachments = [];
        if($design->attachmentFileUser !=null)
        {
            $attachments = unserialize($design->attachmentFileUser);

            foreach ($attachments as $attachment)
            {
                array_push($array,$attachment);
            }
        }

        if ($attachments !=null)
        {
            return response()->json($array,'200');
        }
        return response()->json(['message' => 'error'], 401);
    }


    public function allTickets()
    {
        $user = auth()->user();
        $tickets = Ticket::where('appUser_id',$user->id)->where('parent_id',0)->orderBy('id','desc')->get(['id','subject','status','created_at','project_id','design_id']);
        foreach ($tickets as $key => $ticket)
        {
            $project = Project::where('id',$ticket->project_id)->pluck('projectInfo')->first();
            $projectInfo = unserialize($project);
            $tickets[$key]['project'] = $projectInfo['Projektnamn'];
            unset($tickets[$key]['project_id']);

            $design = design::where('id',$ticket->design_id)->pluck('templateInformation')->first();
            if($design)
            {
                $designInfo = unserialize($design);
                $tickets[$key]['agent'] = $designInfo['BSABAMA'];
                unset($tickets[$key]['design_id']);
            }
            else
            {
                $tickets[$key]['agent'] = '';
                unset($tickets[$key]['design_id']);
            }

        }
        if (count($tickets) > 0)
        {
            return response()->json($tickets,'200');
        }
        return response()->json(['message' => 'error'], 401);
    }

    public function createNewTicket(Request $request)
    {
        $ticket = new Ticket();
        $validator =  $this->validate($request,[
            'subject' => 'required',
            'project_id' => 'required',
        ]);
        $ticket->subject = $request->subject;
        $ticket->description = $request->description;
        if($request->hasFile('attachment'))
        {
            $ticket->attachment = $this->uploadImages($request->attachment);
        }
        $project = Project::where('id',$request->project_id)->first();
        $ticket->projectEngineer_id = $project->engineer_id;
        $ticket->project_id = $request->project_id;
        $ticket->design_id = $request->design_id;
        $ticket->appUser_id = auth()->user()->id;
        $ticket->save();
        return response()->json(['message' => 'success'], 200);
    }

    public function getParentTicket($id)
    {
        $tickets = Ticket::where('parent_id',$id)->orWhere('id',$id)->orderBy('id','desc')->get();

        return response()->json($tickets);
    }

    public function replayTicketById($id)
    {
        $ticket = Ticket::where('id',$id)->pluck('parent_id')->first();
        return response()->json(['parent_id' => $ticket],200);
    }

    public function replayTicket(Request $request)
    {
        $ticket = new Ticket();
        $Engineer_id = Ticket::where('id',$request->parent_id)->pluck('projectEngineer_id')->first();
        $ticket->parent_id = $request->parent_id;
        $ticket->appUser_id = auth()->user()->id;
        $ticket->projectEngineer_id = $Engineer_id;
        $ticket->description = $request->description;

        $ticket->subject = '';
        $ticket->project_id = 0;
        $ticket->design_id = 0;


        if($request->hasFile('attachment'))
        {
            $ticket->attachment = $this->uploadImages($request->attachment);
        }
        else
        {
            $ticket->attachment = '';
        }
        $ticket->save();
        return response()->json(['message' => 'success'], 200);
    }


    public function uploadImages($file)
    {
        $current_timestamp = Carbon::now()->timestamp .rand(1,99999);
        $imagePath = "/upload/";
        $filename = $current_timestamp . $file->getClientOriginalName();
        $file = $file->move(public_path($imagePath) , $filename);
        return $imagePath.$filename;
    }





}
