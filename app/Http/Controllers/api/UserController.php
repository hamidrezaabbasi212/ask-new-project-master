<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public $successStatus = 200;

    public function login()
    {
        if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){
            $user = Auth::user();
            $success['token'] =  $user->createToken('MyApp')->accessToken;
            $success['status'] =  $this->successStatus;
            $user->firebaseToken = request('firebaseToken');
            $user->save();
            return response()->json(['success' => $success]);
        }
        else
        {
            return response()->json(['error'=>'Unauthorised'], 401);
        }
    }

    public function details()
    {
        $user = Auth::user();
        $userInfo['email'] = $user->email;
        $userInfo['mobile'] = $user->phone;
        $userInfo['company'] = User::where('id',$user->parent_id)->pluck('organizationName')->first();
        return response()->json($userInfo, $this->successStatus);
    }

    public function resetPassword(Request $request)
    {
        $user = User::where('id',auth()->user()->id)->first();

        $validator =  $this->validate($request,[
            'password' => 'required|confirmed|min:8',
        ]);

        if($validator)
        {
            $user->password = $request->password;
            $user->save();
            return response()->json('success',200);
        }
        return response()->json($validator->errors()->first());


    }


}
