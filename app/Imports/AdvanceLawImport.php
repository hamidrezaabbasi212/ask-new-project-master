<?php

namespace App\Imports;

use App\AdvanceLaw;
use Maatwebsite\Excel\Concerns\ToModel;

class AdvanceLawImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new AdvanceLaw([
            'link' => $row[0],
            'title' => $row[1],
            'text' => $row[2]
        ]);
    }
}
