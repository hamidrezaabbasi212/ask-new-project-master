<?php

namespace App\Imports;

use App\Definitions\AnsvarigPart;
use Maatwebsite\Excel\Concerns\ToModel;

class AnsvarigPartImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new AnsvarigPart([
            'title' => $row[0],
        ]);
    }
}
