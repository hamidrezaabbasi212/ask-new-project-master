<?php

namespace App\Imports;

use App\LawImport;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\OnEachRow;
use Maatwebsite\Excel\Row;


class ImportLaw implements OnEachRow
{

    public function onRow(Row $row)
    {
        $rowIndex = $row->getIndex();
        $row      = $row->toArray();

        if($row[0] !=null)
        {
            LawImport::create([
               'code' => $row[0],
               'title' => $row[1],
                'user_id' => auth()->user()->id
            ]);

        }
        else
        {
            $law = LawImport::latest('id')->first();
            $law->text = $law->text ==null ? $row[1] : $law->text . ',' . $row[1];
            $law->save();
        }
    }

}
