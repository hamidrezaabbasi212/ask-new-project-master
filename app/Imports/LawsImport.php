<?php

namespace App\Imports;

use App\Law;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class LawsImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Law([
            'Huvudparagraf' => $row[0],
            'Stycke1' => $row[1],
            'Stycke2' => $row[2],
            'Stycke3' => $row[3],
            'Stycke4' => $row[4],
            'Stycke5' => $row[5],
            'Text' => $row[6],
            'Hanvisning' =>$row[7],
            'category_id' =>$row[8],
        ]);
    }
}
