<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KA extends Model
{
    protected $table = 'k_a';

    protected $fillable = ['project_id' ,'robrik_id', 'user_id' , 'parent_id' , 'templateInformation'];
}
