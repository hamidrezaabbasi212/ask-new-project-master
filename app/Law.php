<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;

class Law extends Model
{
//    use Searchable;
    protected $fillable = ['Huvudparagraf','Stycke1','Stycke2','Stycke3','Stycke4','Stycke5','Text','Hanvisning','category_id'];


    public function scopeSearch($query , $keywords)
    {
        $query->where('Huvudparagraf' , 'LIKE' , '%' . $keywords . '%')
            ->orWhere('Stycke1' , 'LIKE' , '%' . $keywords . '%')
            ->orWhere('Stycke2' , 'LIKE' , '%' . $keywords . '%')
            ->orWhere('Stycke3' , 'LIKE' , '%' . $keywords . '%')
            ->orWhere('Stycke4' , 'LIKE' , '%' . $keywords . '%')
            ->orWhere('Stycke5' , 'LIKE' , '%' . $keywords . '%')
            ->orWhere('Text' , 'LIKE' , '%' . $keywords . '%');
        return $query;
    }


    public function scopeSearchBesab($query,$keywords)
    {
        $query->where('Huvudparagraf' , 'LIKE' , '%' . $keywords . '%')
            ->orWhere('Stycke1' , 'LIKE' , '%' . $keywords . '%')
            ->orWhere('Stycke2' , 'LIKE' , '%' . $keywords . '%')
            ->orWhere('Stycke3' , 'LIKE' , '%' . $keywords . '%')
            ->orWhere('Stycke4' , 'LIKE' , '%' . $keywords . '%')
            ->orWhere('Stycke5' , 'LIKE' , '%' . $keywords . '%')
            ->orWhere('Text' , 'LIKE' , '%' . $keywords . '%')
            ->orWhere('category_id' , 'LIKE' , '%' . $keywords . '%');
        return $query;
    }


}
