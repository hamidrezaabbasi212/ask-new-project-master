<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LawImport extends Model
{
    protected $table = 'law_import';
    protected $fillable = ['code' , 'title'  , 'text' ,'user_id' ,'project_id' ,'check1' ,'check2'];
}
