<?php

namespace App;

use App\Project\Organization;
use Illuminate\Database\Eloquent\Model;

class OrganizationUsers extends Model
{
    protected $guarded = [''];



    public function organization()
    {
        return $this->belongsTo(Organization::class);
    }
}
