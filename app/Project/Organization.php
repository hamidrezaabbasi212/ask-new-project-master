<?php

namespace App\Project;

use App\OrganizationUsers;
use Illuminate\Database\Eloquent\Model;

class Organization extends Model
{
    protected $guarded = [''];



    public function organizationUsers()
    {
        return $this->hasMany(OrganizationUsers::class);
    }
}
