<?php

namespace App\Project;

use App\design;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
class Project extends Model
{
    protected $guarded = [''];


    public function scopeSearch($query , $keywords)
    {
        $query->where('projectInfo' , 'LIKE' , '%' . $keywords . '%');
        return $query;
    }



    public function designs()
    {
        return $this->hasMany(design::class);
    }


}
