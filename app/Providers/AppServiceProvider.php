<?php

namespace App\Providers;

use App\OrganizationUsers;
use App\Project\Organization;
use App\Project\Project;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('*', function($view)
        {
            $projectCount = Project::count();
            $projectCompleteCount = Project::where('step',7)->count();
            $usersCount = User::count();
            $organizationCount = 0;
            try {
                $organizationCount = User::where('is_organization',1)->where('parent_id',auth()->user()->id)->count();
            } catch (\Exception $exception)
            {

            }

            $view->with(['projectCount' => $projectCount , 'projectCompleteCount' => $projectCompleteCount,
                'organizationCount' => $organizationCount ,'usersCount' =>  $usersCount]);
        });
    }
}