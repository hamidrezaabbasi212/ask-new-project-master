<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Robrik extends Model
{
    protected $fillable = ['title'];
}
