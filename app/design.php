<?php

namespace App;

use App\Project\Project;
use Illuminate\Database\Eloquent\Model;

class design extends Model
{
    protected $guarded = [''];



    public function project()
    {
        return $this->belongsTo(Project::class);
    }
}
