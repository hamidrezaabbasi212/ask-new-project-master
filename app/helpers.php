<?php
namespace App;
use Illuminate\Http\Request;

class Helper {

    public function uploadImages($file)
    {
        $current_timestamp = Carbon::now()->timestamp;
        $imagePath = "/upload/";
        $filename = $current_timestamp . $file->getClientOriginalName();
        $file = $file->move(public_path($imagePath) , $filename);
        return $imagePath.$filename;
    }
}



