<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLawsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('laws', function (Blueprint $table) {
            $table->id();
            $table->longText('Huvudparagraf')->nullable();
            $table->longText('Stycke1')->nullable();
            $table->longText('Stycke2')->nullable();
            $table->longText('Stycke3')->nullable();
            $table->longText('Stycke4')->nullable();
            $table->longText('Stycke5')->nullable();
            $table->longText('Text')->nullable();
            $table->longText('Hanvisning')->nullable();
            $table->string('category_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('laws');
    }
}
