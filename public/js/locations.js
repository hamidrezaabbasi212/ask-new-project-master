$(document).ready(function() {
    let locat = $('#location').val();
    locat = locat.split(',');
    let mylat =  locat[0] ? locat[0] : '59.325264776484666';
    let mylng =  locat[1] ? locat[1] : '18.071823321193428';

    let map = L.map('map', {
        layers: [
            L.tileLayer('//{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
                'attribution': '<a href="/">Ask Projekt</a>'
            })
        ],
        center: [mylat, mylng],
        zoom: 16
    });


    let marker = L.marker([mylat, mylng], {draggable: 'true'}).addTo(map);

    map.addControl(new L.Control.Search({
        url: '/search-in-map?title={s}&lat=' + mylat + '&lng=' + mylng,
        propertyLoc: ['lat', 'lng'],
        marker: marker
    }));

    marker.on("drag", function (e) {
        let marker = e.target;
        let position = marker.getLatLng();
        $('#location').val(position.lat + ',' + position.lng);

        map.panTo(new L.LatLng(position.lat, position.lng));
    });

});
