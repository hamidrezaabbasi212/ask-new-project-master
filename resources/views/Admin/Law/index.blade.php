@extends('Admin.master')
@section('content-title', 'LAG')
@section('title', 'LAG')
@section('content')
    <div class="row mb-3">
        <div class="col">
            <a class="btn-createproject" href="{{ route('Law.create') }}">Nytt LAG</a>
        </div><!-- /.col -->
    </div>
    <form action="{{ route('Law.store') }}" method="post" id="Projectinfo" enctype="multipart/form-data">
        @csrf
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 box-organization">
            <div class="form-row">
                <div class="col">
                    <label for="inputName2" class="col-sm-2 col-form-label">Import Laws</label>
                    <div class="form-group">
                        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 col-upload-in-project col-upload-inproject">
                            <input type="file" name="lawFile" class="form-control btn-upload-in-project">
                            <i class="fa fa-upload icon-upload" style="left: 100px" aria-hidden="true"></i>
                            <span class="specialupload" style="left: 0">ladda upp</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="btn-in-forminfoproject">
                <button type="submit" class="next3 bg-success">upload</button>
            </div>
        </div>

    </div>
    </form>
    @endsection

@section('script')

    @endsection
