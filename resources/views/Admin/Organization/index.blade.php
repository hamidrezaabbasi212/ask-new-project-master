@extends('Admin.master')
@section('content-title', 'Organization')
@section('title', 'Organization')
@section('content')

    <div class="card">
        <div class="card-body">
                <div class="btnadd-organization">
                    <a href="" class="" data-toggle="modal" data-target="#neworganization">+ Add New Organization</a>

                    <div class="modal fade" id="neworganization" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                                </div>
                                <div class="modal-body">
                                    <div class="logomodal">
                                        <img src="{{ asset('dist/img/favicon.png') }}" alt="">
                                    </div>
                                    <div class="title-modal"><span>Add New Organization</span></div>
                                    <form action="{{ route('Organization.store') }}" method="post" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-left">
                                        @csrf
                                        <div class="col-sm-6 pull-left">
                                            <!-- text input -->
                                            <div class="form-group">
                                                <label>namn</label>
                                                <input type="text" name="name" required class="form-control" placeholder="Enter ...">
                                            </div>
                                        </div>
                                        <div class="col-sm-6 pull-left">
                                            <!-- text input -->
                                            <div class="form-group">
                                                <label>Beskrivning</label>
                                                <input type="text" name="description" required class="form-control" placeholder="Enter ...">
                                            </div>
                                        </div>
                                        <div class="col-sm-6 pull-left">
                                            <!-- text input -->
                                            <div class="form-group">
                                                <label>Adress</label>
                                                <input type="text" name="address" required class="form-control" placeholder="Enter ...">
                                            </div>
                                        </div>
                                        <div class="col-sm-6 pull-left">
                                            <!-- text input -->
                                            <div class="form-group">
                                                <label>Kontakt</label>
                                                <input type="text" name="contact" required class="form-control" placeholder="Enter ...">
                                            </div>
                                        </div>
                                        <div class="col-sm-12 pull-left">
                                            <button type="submit" class="btnmodal">Lägg till organisation</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="modal fade" id="editorganization" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                                </div>
                                <div class="modal-body">
                                    <div class="logomodal">
                                        <img src="{{ asset('dist/img/favicon.png') }}" alt="">
                                    </div>
                                    <div class="title-modal"><span>Edit Organization</span></div>
                                    <form id="editorganizationform" method="post" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-left">
                                        @csrf
                                        <div class="col-sm-6 pull-left">
                                            <!-- text input -->
                                            <div class="form-group">
                                                <label>namn</label>
                                                <input type="text" id="organizationNameEdit" name="organizationNameEdit" required class="form-control" placeholder="Enter ...">
                                            </div>
                                        </div>
                                        <div class="col-sm-6 pull-left">
                                            <!-- text input -->
                                            <div class="form-group">
                                                <label>Beskrivning</label>
                                                <input type="text" id="organizationDescriptionEdit" name="organizationDescriptionEdit" required class="form-control" placeholder="Enter ...">
                                            </div>
                                        </div>
                                        <div class="col-sm-6 pull-left">
                                            <!-- text input -->
                                            <div class="form-group">
                                                <label>Adress</label>
                                                <input type="text" id="organizationAddressEdit" name="organizationAddressEdit" required class="form-control" placeholder="Enter ...">
                                            </div>
                                        </div>
                                        <div class="col-sm-6 pull-left">
                                            <!-- text input -->
                                            <div class="form-group">
                                                <label>Kontakt</label>
                                                <input type="text" id="organizationContactEdit" name="organizationContact" required class="form-control" placeholder="Enter ...">
                                            </div>
                                        </div>
                                        <div class="col-sm-12 pull-left">
                                            <button type="submit" class="btnmodal">uppdatera organisationen</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>




                </div>

                <div class="table-responsive">
                    <table class="table tbl-organizatin">
                        <thead>
                        <tr>
                            <td>Namn</td>
                            <td>Beskrivning</td>
                            <td>Adress</td>
                            <td>Kontakt</td>
                            <td>Redigera</td>
                            <td>Radera</td>
                            <td>Medlems</td>
                            <td>Lägg till användare</td>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($organizations as $row => $organization)
                            <tr>
                                <td>{{ $organization->name }}</td>
                                <td>{{ $organization->description }}</td>
                                <td>{{ $organization->address }}</td>
                                <td>{{ $organization->contact }}</td>
                                <td><a href="#" data-toggle="modal" data-target="#editorganization" data-target-id="{{ $organization->id }}" data-target-name="{{ $organization->name }}" data-target-description="{{ $organization->description }}" data-target-address="{{ $organization->address }}" data-target-contact="{{ $organization->contact }}" title="Edit" ><i class="fa fa-edit" aria-hidden="true"></i></a></td>
                                <td><a href="#" onclick="deleteConfirmation({{ $organization->id }})" title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a></td>
                                <td><a href="{{ route('Organization.users' , ['id' =>$organization->id]) }}" title="branch"><i class="fa fa-users" aria-hidden="true"></i></a></td>
                                <td><a href="#" data-toggle="modal" data-target="#CreatUser" data-target-organizationid="{{ $organization->id }}"><i class="fa fa-user-plus" aria-hidden="true"></i></a></td>
                            </tr>
                            @endforeach

                        </tbody>
                    </table>
                </div>

            <div class="modal fade" id="CreatUser" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                        </div>
                        <div class="modal-body">
                            <div class="logomodal">
                                <img src="{{ asset('dist/img/favicon.png') }}" alt="">
                            </div>
                            <div class="title-modal"><span>Add New User</span></div>
                            <form id="createUserForm" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-left" method="post" action="" enctype="multipart/form-data">
                                @csrf
                                <div class="col-sm-6 pull-left">
                                    <!-- text input -->
                                    <div class="form-group">
                                        <label>Name</label>
                                        <input type="text" name="name" required class="form-control" placeholder="Enter ...">
                                    </div>
                                </div>
                                <div class="col-sm-6 pull-left">
                                    <!-- text input -->
                                    <div class="form-group">
                                        <label>Phone</label>
                                        <input type="text" name="phone" required class="form-control" placeholder="Enter ...">
                                    </div>
                                </div>
                                <div class="col-sm-12 pull-left">
                                    <div class="form-group">
                                        <label>Email</label>
                                        <input type="text"  name="email" required class="form-control" placeholder="Enter ...">
                                    </div>
                                </div>

                                <div class="col-sm-12 pull-left">
                                    <label>Upload Your Image</label>
                                    <input type="file" name="avatar" class="hideinput">
                                    <span class="special-upload">Upload</span>
                                    <span class="shownameupload">show File Name upload</span>
                                </div>
                                <div class="col-sm-12 pull-left">
                                    <button class="btnmodal btn-add-user">Add User</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            </div>
        </div>

    @endsection

@section('script')
    @if(Session::has('successAddOrganization'))
        <script>
            Swal.fire(
                'Bra jobbat !',
                'organisation registreringen slutförd',
                'success');
        </script>
    @endif

    @if(Session::has('successEditOrganization'))
        <script>
            Swal.fire(
                'Bra jobbat !',
                'organisation uppdatering slutförd',
                'success');
        </script>
    @endif

    <script type="text/javascript">


        function deleteConfirmation(id) {
            Swal.fire({
                type: 'warning',
                title: 'Är du säker?',
                text: "Du kommer inte att kunna återställa detta!",
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                cancelButtonText: 'annullera',
                confirmButtonText: 'Ja, radera det!'
            }).then(function (e) {
                if (e.value === true) {
                    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
                    $.ajax({
                        type: 'POST',
                        url: "/panel/organisation/delete/" + id,
                        data: {_token: '{!! csrf_token() !!}'},
                        dataType: 'JSON',
                        success: function (results) {
                            if (results.success === true) {
                                Swal.fire(
                                    'Raderade!',
                                    'Din fil har tagits bort.',
                                    'success'
                                );
                                location.reload();
                            } else {
                                Swal.fire({
                                    icon: 'fel',
                                    title: 'Hoppsan...',
                                    text: 'Något gick fel!',
                                })
                            }
                        }
                    });
                }
                else {
                    e.dismiss;
                }}, function (dismiss) {
                return false;
            })

        }

        var organizationid = 0;
        $(document).ready(function () {
            $("#edit").on("show.bs.modal", function (e) {
                var id = $(e.relatedTarget).data('target-id');
                alert(id);
            });

            $("#CreatUser").on("show.bs.modal", function (e) {
                var organizationid = $(e.relatedTarget).data('target-organizationid');
                $("#createUserForm").attr("action", "/panel/organisation/createUser/" + organizationid);
            });

            $("#editorganization").on("show.bs.modal", function (e) {
                organizationid = $(e.relatedTarget).data('target-id');
                var name = $(e.relatedTarget).data('target-name');
                var description = $(e.relatedTarget).data('target-description');
                var address = $(e.relatedTarget).data('target-address');
                var contact = $(e.relatedTarget).data('target-contact');

                $("#editorganizationform").attr("action", "/panel/organisation/" + organizationid);
                $('#organizationNameEdit').val(name);
                $('#organizationDescriptionEdit').val(description);
                $('#organizationAddressEdit').val(address);
                $('#organizationContactEdit').val(contact);
            });


        });

    </script>
    @endsection
