@extends('Admin.master')
@section('content-title', 'Medlemmar')
@section('title', 'Medlemmar')
@section('content')

    <div class="card">
        <div class="card-body">
            <div class="row mb-3">
                <div class="col">
                    <a href="#" class="btn-createproject ml-3" data-toggle="modal" data-target="#CreatUser" data-target-id="" title="Edit">Lägg till</a>
                    <a href="#" class="btn-createproject" onclick="window.history.go(-1);">kommer tillbaka</a>
                </div><!-- /.col -->
            </div>
            <div class="table-responsive">
                <table class="table tbl-organizatin">
                    <thead>
                    <tr>
                        <td>Namn</td>
                        <td>Telefon</td>
                        <td>E-post</td>
                        <td>Foto</td>
                        <td>Redigera</td>
                        <td>Radera</td>
                    </tr>
                    </thead>
                    <tbody id="tblgetuser">

                    </tbody>
                </table>
            </div>

            <div class="modal fade" id="CreatUser" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                        </div>
                        <div class="modal-body">
                            <div class="logomodal">
                                <img src="{{ asset('dist/img/favicon.png') }}" alt="">
                            </div>
                            <div class="title-modal"><span>Lägg till ny användare</span></div>
                            <form id="formAddUser" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-left" method="post" action="{{ route('Organization.createUser', $id) }}" enctype="multipart/form-data">
                                @csrf
                                <div class="col-sm-6 pull-left">
                                    <!-- text input -->
                                    <div class="form-group">
                                        <label>Name</label>
                                        <input type="text" name="name" required class="form-control" placeholder="Enter ...">
                                    </div>
                                </div>
                                <div class="col-sm-6 pull-left">
                                    <!-- text input -->
                                    <div class="form-group">
                                        <label>Phone</label>
                                        <input type="text" name="phone" required class="form-control" placeholder="Enter ...">
                                    </div>
                                </div>
                                <div class="col-sm-12 pull-left">
                                    <div class="form-group">
                                        <label>E-Post</label>
                                        <input type="text"  name="email" required class="form-control" placeholder="Enter ...">
                                    </div>
                                </div>
                                <div class="col-sm-12 pull-left">
                                    <div class="form-group">
                                        <label>Legitimation Card</label>
                                        <input type="text" id="legitimationCard"  name="legitimationCard" required class="form-control" placeholder="Enter ...">
                                    </div>
                                </div>

                                <div class="col-sm-12 pull-left">
                                    <div class="form-group">
                                        <label>Search Ansvarig part</label>
                                        <input type="text" id="searchAnsvarigUser" class="form-control mb-3" placeholder="Enter ...">
                                        <select class="form-control" name="AnsvarigPartUser[]" id="AnsvarigPartUser" multiple>
                                            <option value="0">Option 1</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-sm-12 pull-left">
                                    <label>Upload Your Image</label>
                                    <input type="file" name="avatar" id="avatar" onchange="fileSelect('avatar')" class="hideinput">
                                    <span class="special-upload">Upload</span>
                                    <span class="shownameupload">show File Name upload</span>
                                </div>
                                <div class="col-sm-12 pull-left">
                                    <button type="submit" class="btnmodal btn-add-user">Add User</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="EditUser" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                        </div>
                        <div class="modal-body">
                            <div class="logomodal">
                                <img src="{{ asset('dist/img/favicon.png') }}" alt="">
                            </div>
                            <div class="title-modal"><span>Redigera</span></div>
                            <form id="editUserForm" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-left" method="post" action="" enctype="multipart/form-data">
                                @csrf
                                <div class="col-sm-6 pull-left">
                                    <!-- text input -->
                                    <input type="hidden" name="organizationid" id="organizationid" required class="form-control" placeholder="Enter ...">
                                    <div class="form-group">
                                        <label>Name</label>
                                        <input type="text" name="name" id="name" required class="form-control" placeholder="Enter ...">
                                    </div>
                                </div>
                                <div class="col-sm-6 pull-left">
                                    <!-- text input -->
                                    <div class="form-group">
                                        <label>Phone</label>
                                        <input type="text" name="phone" id="phone" required class="form-control" placeholder="Enter ...">
                                    </div>
                                </div>
                                <div class="col-sm-12 pull-left">
                                    <div class="form-group">
                                        <label>E-Post</label>
                                        <input type="text"  name="email" id="email" required class="form-control" placeholder="Enter ...">
                                    </div>
                                </div>
                                <div class="col-sm-12 pull-left">
                                    <div class="form-group">
                                        <label>Legitimation Card</label>
                                        <input type="text" id="legitimationCardEdit"  name="legitimationCard" required class="form-control" placeholder="Enter ...">
                                    </div>
                                </div>
                                <div class="col-sm-12 pull-left">
                                    <div class="form-group text-center">
                                        <a id="expertbutton" class="button btn btn-info file-link" href="#" data-toggle="modal" data-target-id =""  data-target="#expertsModal">Specializations<i class="fa fa-eye ml-2" aria-hidden="true"></i></a>
                                    </div>
                                </div>
                                <div class="col-sm-12 pull-left">
                                    <div class="form-group">
                                        <label>Search Ansvarig</label>
                                        <input type="text" id="searchAnsvarigUserEdit" class="form-control mb-3" placeholder="Enter ...">
                                        <select class="form-control" name="AnsvarigPartUserEdit[]" id="AnsvarigPartUserEdit" multiple>
                                            <option value="0">Option 1</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-12 pull-left">
                                    <label>Upload Your Image</label>
                                    <input type="file" name="avatar" id="avatarEdit" onchange="fileSelect('avatarEdit')" class="hideinput">
                                    <span class="special-upload">Upload</span>
                                    <span class="shownameupload">show File Name upload</span>
                                </div>
                                <div class="col-sm-12 pull-left">
                                    <button type="submit" class="btnmodal btn-add-user">Redigera</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="expertsModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                        </div>
                        <div class="modal-body">
                            <div class="logomodal">
                                <img src="{{ asset('dist/img/favicon.png') }}" alt="">
                            </div>
                            <div class="title-modal"><span>Ansvarig</span></div>
                            <form class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-left" method="post" action="{{ route('Organization.createUser', $id) }}" enctype="multipart/form-data">
                                @csrf
                                <div class="table-responsive">
                                    <table id="tbl-organizatin" class="table tbl-organizatin">
                                        <thead>
                                        <tr>
                                            <td>Namn</td>
                                            <td>Lägg till användare</td>
                                        </tr>
                                        </thead>
                                        <tbody id="getexpert">

                                        </tbody>
                                    </table>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

@endsection

@section('script')
    @if(Session::has('successeditusers'))
        <script>
            Swal.fire(
                'Bra jobbat !',
                'användare redigera slutförd',
                'success');
        </script>
    @endif

    @if(Session::has('successaddusers'))
        <script>
            Swal.fire(
                'Bra jobbat !',
                'användare registreringen slutförd',
                'success');
        </script>
    @endif


    <script type="text/javascript">
        let keyCountUser = 0;
        let keyCountUserEdit = 0;
        let userid = '';
        let organizationid = '{{ $id }}';


        $('#searchAnsvarigUser').on('keyup',function () {
            let keyword=$(this).val();
            var KeyID = event.keyCode;

            if(KeyID == 8)
            {
                if(keyCountUser>0)
                {
                    keyCountUser--;
                }

            }
            else
            {
                keyCountUser++;
            }

            if(keyCountUser >= 3)
            {
                keyword=$(this).val();

                if(keyword == "")
                {
                    $('#AnsvarigPartUser').html("");
                }
                $.ajax({
                    type : 'get',
                    url : '{{ route('searchAnsvarigUser') }}',
                    data:{'keywords':keyword},
                    success:function(data){
                        $("#AnsvarigPartUser").html(data);
                    }
                });
                keyCountUser--;
            }
            else
            {
                $("#AnsvarigPartUser").html("");
            }


        });

        $('#searchAnsvarigUserEdit').on('keyup',function () {
            let keyword=$(this).val();
            var KeyID = event.keyCode;

            if(KeyID == 8)
            {
                if(keyCountUserEdit>0)
                {
                    keyCountUserEdit--;
                }

            }
            else
            {
                keyCountUserEdit++;
            }

            if(keyCountUserEdit >= 3)
            {
                keyword=$(this).val();

                if(keyword == "")
                {
                    $('#AnsvarigPartUserEdit').html("");
                }
                $.ajax({
                    type : 'get',
                    url : '{{ route('searchAnsvarigUser') }}',
                    data:{'keywords':keyword},
                    success:function(data){
                        $("#AnsvarigPartUserEdit").html(data);
                    }
                });
                keyCountUserEdit--;
            }
            else
            {
                $("#AnsvarigPartUserEdit").html("");
            }


        });

        $(document).ready(function () {
            $('#AnsvarigPartUser').select2({
                'placeholder' : 'selected options'
            });
            $('#AnsvarigPartUserEdit').select2({
                'placeholder' : 'selected options'
            });

            getusers(organizationid);

            $("#EditUser").on("show.bs.modal", function (e) {
                var id = $(e.relatedTarget).data('target-id');
                userid  = id;
                $.ajax({
                    type : 'get',
                    url : '{{ route('Organization.getInfoUser') }}',
                    data : {'user_id' : id},
                    success:function (data) {
                        $('#name').val(data['name']);
                        $('#phone').val(data['phone']);
                        $('#email').val(data['email']);
                        $('#organizationid').val(organizationid);
                        $('#expertbutton').attr('data-target-id',id);
                        $('#legitimationCardEdit').val(data['legitimationCard']);
                    }
                });



            });

            $("#editUserForm").submit(function (event) {
                event.preventDefault();
                var formData = new FormData(this);
                let token =  $('input[name="_token"]').val();
                formData.append('_token',token);
                $.ajax({
                    type: 'POST',
                    url: '/panel/organisation/user/' + userid,
                    data: formData,
                    processData: false,
                    contentType: false,
                    success:function (data) {
                        $('#EditUser').modal('hide');
                        getusers(organizationid);
                        Swal.fire(
                            'Bra jobbat !',
                            'användare redigera slutförd',
                            'success');

                    },
                    error:function (err) {
                        console.log(err);
                    }
                });
            })


            $("#expertsModal").on("show.bs.modal", function (e) {
                let id =  $(e.relatedTarget).data('target-id');
                userid = id;
                getexperts(userid)
            });

        });

        function getexperts(userid) {
            $.ajax({
                type : 'get',
                url : '{{ route('Organization.getExperts') }}',
                data : {'user_id' : userid},
                success:function (data) {
                    $('#getexpert').html("");
                    $('#getexpert').html(data);
                }
            });
        }

        function getusers(organizationid) {
            $.ajax({
                type : 'get',
                url : '{{ route('Organization.getUser') }}',
                data : {'organizationid' : organizationid},
                success:function (data) {
                    $('#tblgetuser').html("");
                    $('#tblgetuser').html(data);
                }
            });
        }

        function deleteConfirmation(id) {
            Swal.fire({
                type: 'warning',
                title: 'Är du säker?',
                text: "Du kommer inte att kunna återställa detta!",
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                cancelButtonText: 'annullera',
                confirmButtonText: 'Ja, radera det!'
            }).then(function (e) {
                if (e.value === true) {
                    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
                    $.ajax({
                        type: 'POST',
                        url: "/panel/organisation/deleteUser/" + id,
                        data: {_token: '{!! csrf_token() !!}'},
                        dataType: 'JSON',
                        success: function (results) {
                            getusers(organizationid);
                            Swal.fire(
                                'Bra jobbat !',
                                'användare radera slutförd',
                                'success');
                        }
                    });
                }
                else {
                    e.dismiss;
                }}, function (dismiss) {
                return false;
            })

        }

        function deleteConfirmationExpert(id) {
            Swal.fire({
                type: 'warning',
                title: 'Är du säker?',
                text: "Du kommer inte att kunna återställa detta!",
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                cancelButtonText: 'annullera',
                confirmButtonText: 'Ja, radera det!'
            }).then(function (e) {
                if (e.value === true) {
                    $.ajax({
                        type: 'get',
                        url: "{{ route('Organization.deleteExperts') }}",
                        data: {'id':id , 'userid':userid},
                        dataType: 'JSON',
                        success: function (results) {
                            getexperts(userid);
                            Swal.fire(
                                'Bra jobbat !',
                                'användare radera slutförd',
                                'success');
                        }
                    });
                }
                else {
                    e.dismiss;
                }}, function (dismiss) {
                return false;
            })

        }


        function fileSelect(id){
            var filename  = $('#' + id).val().match(/[^\\/]*$/)[0];
            $('.shownameupload').text("");
            $('.shownameupload').text(filename);
        }


    </script>
@endsection
