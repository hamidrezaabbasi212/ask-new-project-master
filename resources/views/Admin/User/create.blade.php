@extends('Admin.master')
@section('content-title', 'Skapa konto')
@section('title', 'Skapa konto')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    @include('Admin.section.errors')
                    <div class="">
                        <!-- /.tab-pane -->
                        <!-- /.tab-pane -->
                        <div class="" id="settings">
                            <form class="form-horizontal" action="{{ route('account.store') }}" method="post" enctype="multipart/form-data">
                                @csrf
                                <div class="form-group row">
                                    <label for="inputName" class="col-sm-2 col-form-label">namn</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="name" class="form-control" id="inputName" placeholder="namn">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputEmail" class="col-sm-2 col-form-label">E-post</label>
                                    <div class="col-sm-10">
                                        <input type="email" name="email" class="form-control" id="inputEmail" placeholder="E-post">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputName2" class="col-sm-2 col-form-label">Telefon</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="phone" class="form-control" id="inputName2" placeholder="Telefon">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="inputName2" class="col-sm-2 col-form-label">Konto Roll</label>
                                    <div class="col-sm-10">
                                        <select name="roles" class="form-control">
                                            <option value="0">Select an Option</option>
                                            @foreach($roles as $role)
                                                <option value="{{ $role->id }}">{{ $role->label }} - {{ $role->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="inputName2" class="col-sm-2 col-form-label">Avatar</label>
                                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 col-upload-in-project col-upload-inproject">
                                        <input type="file"  name="avatar" class="form-control btn-upload-in-project">
                                        <i class="fa fa-upload icon-upload" style="left: 100px" aria-hidden="true"></i>
                                        <span class="specialupload" style="left: 0">ladda upp</span>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="offset-sm-2 col-sm-10">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox"> Jag godkänner de <a href="#">allmänna villkoren</a>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="offset-sm-2 col-sm-10">
                                        <button type="submit" class="btn btn-danger btnprofile">Skicka in</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <!-- /.tab-pane -->
                    </div>
                    <!-- /.tab-content -->
                </div><!-- /.card-body -->
            </div>
            <!-- /.nav-tabs-custom -->
        </div>
    </div>
    @endsection
