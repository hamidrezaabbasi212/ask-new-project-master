@extends('Admin.master')
@section('content-title', 'Redigera Konto')
@section('title', 'Redigera Konto')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    @include('Admin.section.errors')
                    <div class="">
                        <!-- /.tab-pane -->
                        <!-- /.tab-pane -->
                        <div class="" id="settings">
                            <form class="form-horizontal" action="{{ route('account.update' , $account->id) }}" method="post" enctype="multipart/form-data">
                                @csrf
                                <div class="form-group row">
                                    <div class="col text-center">
                                        <label for="inputName">Avatar</label>
                                    </div>
                                </div>
                                <div class="form-group row" style="margin-top: -20px">
                                    <div class="col">
                                        <div class="col text-center"><img src="{{ asset($account->avatar) }}" width="150px" height="150px" class="img-circle elevation-2" alt="User Image"></div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputName" class="col-sm-2 col-form-label">Namn</label>
                                    <div class="col-sm-10">
                                        <input type="text" value="{{ $account->name }}" name="name" class="form-control" id="inputName" placeholder="namn">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputEmail" class="col-sm-2 col-form-label">E-post</label>
                                    <div class="col-sm-10">
                                        <input type="email" value="{{ $account->email }}" name="email" class="form-control" id="inputEmail" placeholder="E-post">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputName2" class="col-sm-2 col-form-label">Telefon</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="phone" value="{{ $account->phone }}" class="form-control" id="inputName2" placeholder="Telefon">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputName2" class="col-sm-2 col-form-label">Lösenord</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="password" value="" class="form-control" id="inputName2" placeholder="Lösenord">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputName2" class="col-sm-2 col-form-label">Lösenords bekräftelse</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="password_confirmation" value="" class="form-control" id="inputName2" placeholder="Lösenord Sbekräftelse">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="inputName2" class="col-sm-2 col-form-label">Roll</label>
                                    <div class="col-sm-10">
                                        <select class="form-control" name="roles" dataEntry="0">
                                            <option value="0">Välj en roll</option>
                                            @foreach(\App\Role::all() as $role)
                                                <option value="{{ $role->id }}" {{ in_array($role->id , $account->roles->pluck('id')->toArray()) ? 'selected' : '' }}>{{ $role->label }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="inputName2" class="col-sm-2 col-form-label">Avatar</label>
                                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 col-upload-in-project col-upload-inproject">
                                        <input type="file" name="avatar" class="form-control btn-upload-in-project">
                                        <i class="fa fa-upload icon-upload" style="left: 100px" aria-hidden="true"></i>
                                        <span class="specialupload" style="left: 0">ladda upp</span>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="offset-sm-2 col-sm-10">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox"> Jag godkänner de <a href="#">allmänna villkoren</a>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="offset-sm-2 col-sm-10">
                                        <button type="submit" class="btn btn-danger btnprofile">Redigera</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <!-- /.tab-pane -->
                    </div>
                    <!-- /.tab-content -->
                </div><!-- /.card-body -->
            </div>
            <!-- /.nav-tabs-custom -->
        </div>
    </div>
@endsection
