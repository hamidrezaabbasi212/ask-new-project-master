@extends('Admin.master')
@section('content-title', 'konton')
@section('title', 'konton')
@section('content')
    <div class="row mb-3">
        <div class="col">
            <a class="btn-createproject" href="{{ route('account.create') }}">Nytt Konto</a>
        </div><!-- /.col -->
    </div>

    <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 box-organization">
            <div class="table-responsive">
                <table class="table tbl-organizatin">
                    <thead>
                    <tr>
                        <td>#</td>
                        <td>namn</td>
                        <td>E-post</td>
                        <td>Telefon</td>
                        <td>Roll</td>
                        <td>Redigera</td>
                        <td>Radera</td>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($accounts as $k =>$account)
                    <tr>
                        <td>{{ $k+ 1}}</td>
                        <td>{{ $account->name }}</td>
                        <td>{{ $account->email }}</td>
                        <td>{{ $account->phone }}</td>
                        <td>@foreach($account->roles as $roles)
                                {{ $roles->label }}
                            @endforeach
                        </td>
                        <td><a href="{{ route('account.edit' ,['id' => $account->id]) }}" title="Edit"><i class="fa fa-edit" aria-hidden="true"></i></a></td>
                        <td><a href="#" title="Delete" class="delete-confirm" onclick="deleteConfirmation({{$account->id}})"><i class="fa fa-times" aria-hidden="true"></i></a></td>
                    </tr>
                        @endforeach
                    </tbody>
                </table>
                <div class="text-center" style="width:15%;  margin: auto; margin-top: 20px !important;">{!! $accounts->links() !!}</div>
            </div>
            </div>
    </div>

    @endsection

@section('script')
    <script type="text/javascript">
        function deleteConfirmation(id) {
            Swal.fire({
                type: 'warning',
                title: 'Är du säker?',
                text: "Du kommer inte att kunna återställa detta!",
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                cancelButtonText: 'annullera',
                confirmButtonText: 'Ja, radera det!'
            }).then(function (e) {
                if (e.value === true) {
                    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
                    $.ajax({
                        type: 'POST',
                        url: "/panel/account/delete/" + id,
                        data: {_token: '{!! csrf_token() !!}',},
                        dataType: 'JSON',
                        success: function (results) {
                            window.setTimeout(function(){window.location.reload()}, 1000);
                            if (results.success === true) {
                                Swal.fire(
                                    'Raderade!',
                                    'Din fil har tagits bort.',
                                    'success'
                                );
                            } else {
                                Swal.fire({
                                    icon: 'fel',
                                    title: 'Hoppsan...',
                                    text: 'Något gick fel!',
                                })
                            }
                        }
                    });
                }
                else {
                    e.dismiss;
                }}, function (dismiss) {
                return false;
            })

        }
    </script>
    @if(Session::has('success'))
        <script>
            Swal.fire(
                'Bra jobbat !',
                'Kontot registreringen slutförd',
                'success');
        </script>
    @endif

    @if(Session::has('edit'))
        <script>
            Swal.fire(
                'Bra jobbat !',
                'Kontoredigering gjort',
                'success');
        </script>
    @endif
    @endsection
