@extends('Admin.master')
@section('content-title', 'Skapa konto')
@section('title', 'Skapa konto')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    @include('Admin.section.errors')
                        <!-- /.tab-pane -->
                        <!-- /.tab-pane -->
                            <form class="form-horizontal" action="{{ route('amaLaw.store') }}" method="post" enctype="multipart/form-data">
                                @csrf
{{--                                <div class="form-group row">--}}
{{--                                    <label for="inputName" class="col-sm-1 col-form-label" style="padding:0;">Hänvisning</label>--}}
{{--                                    <div class="col-sm-10">--}}
{{--                                        <input type="text" name="title" class="form-control" id="inputName" placeholder="Hanvisning">--}}
{{--                                    </div>--}}
{{--                                </div>--}}

{{--                                <div class="form-group row">--}}
{{--                                    <label for="inputEmail" class="col-sm-1 col-form-label">Text</label>--}}
{{--                                    <div class="col-sm-10">--}}
{{--                                        <textarea name="Text" class="form-control" rows="10"></textarea>--}}
{{--                                    </div>--}}
{{--                                </div>--}}

                             <div class="form-group row">
                                 <input type="file" name="amalawexecel">
                             </div>

                                <div class="form-group row">
                                    <div class="offset-sm-2 col-sm-10">
                                        <button type="submit" class="btn btn-danger btnprofile">Skicka in</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <!-- /.tab-pane -->
                    </div>
                    <!-- /.tab-content -->
                </div><!-- /.card-body -->
            </div>
            <!-- /.nav-tabs-custom -->
@endsection

@section('script')
    <script type="text/javascript">
        CKEDITOR.replace('Text',{
            height: 500
        });
    </script>
@endsection
