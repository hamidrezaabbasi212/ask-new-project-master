@extends('Admin.master')
@section('content-title', 'Biljetter')
@section('title', 'Biljetter')
@section('content')
    <div class="row mb-3">
        <div class="col">
            <a  href="{{ route('amaLaw.create') }}" class="btn-createproject">Ny</a>
        </div><!-- /.col -->
    </div>
    <div class="card">
        <div class="card-body p-0">
            <div id="table_data">
                <table class="table table-striped text-center projects">
                    <thead>
                    <tr>
                        <th style="width: 1%">
                            #
                        </th>
                        <th style="width: 30%">
                            Hanvisning
                        </th>
                        <th style="width: 30%">
                            Operationen
                        </th>
                    </tr>
                    </thead>
                    <tbody id="table_data">
                    @foreach($amalaws as $key => $amalaw)
                        <tr>
                            <td>{{ ($amalaws->currentpage()-1) * $amalaws->perpage() +  $key + 1 }}</td>
                            <td>{{ $amalaw->Hanvisning }}</td>
                            <td class="project-actions">
                                <button type="button" onclick="deleteConfirmation({{ $amalaw->id }})" class="fa fa-trash-alt sa-remove"></button>
                                <a href="{{ route('amaLaw.edit',$amalaw->id) }}" type="button" class="fa fa-edit btn-save-template"></a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <div class="text-center" style="width:15%;  margin: auto; margin-top: 20px !important;">{!! $amalaws->links() !!}</div>
            </div>
        </div>
    </div>
@endsection
@section('script')


    @if(Session::has('success'))
        <script>
            Swal.fire({
                position: 'top-end',
                type: 'success',
                title: 'registrerade',
                showConfirmButton: false,
                timer: 1500
            });
        </script>
    @endif

    @if(Session::has('successUpdate'))
        <script>
            Swal.fire({
                position: 'top-end',
                type: 'success',
                title: 'Uppdaterad',
                showConfirmButton: false,
                timer: 1500
            });
        </script>
    @endif

    @if(Session::has('successUpdate'))
        <script>
            Swal.fire({
                position: 'top-end',
                type: 'success',
                title: 'Meddelande skickat',
                showConfirmButton: false,
                timer: 1500
            });
        </script>
    @endif

    <script type="text/javascript">
        function deleteConfirmation(id) {
            Swal.fire({
                type: 'warning',
                title: 'Är du säker?',
                text: "Raderade biljetter kan inte längre återvinnas!",
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                cancelButtonText: 'annullera',
                confirmButtonText: 'Ja, radera det!'
            }).then(function (e) {
                if (e.value === true) {
                    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
                    $.ajax({
                        type: 'POST',
                        url: "{{ route('amaLaw.delete') }}",
                        data: {_token: '{!! csrf_token() !!}',id: id},
                        dataType: 'JSON',
                        success: function (results) {
                            console.log(results);
                            if (results.success == 1) {
                                Swal.fire({
                                    position: 'top-end',
                                    type: 'success',
                                    title: 'raderade',
                                    showConfirmButton: false,
                                    timer: 1500
                                });
                                location.reload();
                            } else {
                                Swal.fire({
                                    icon: 'fel',
                                    title: 'Hoppsan...',
                                    text: 'Något gick fel!',
                                })
                            }
                        }
                    });
                }
                else {
                    e.dismiss;
                }}, function (dismiss) {
                return false;
            })

        }
    </script>
@endsection

