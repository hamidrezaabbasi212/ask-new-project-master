@extends('Admin.master')
@section('content-title', 'Mallar')
@section('title', 'Mallar')
@section('content')
    <div class="row mb-3">
        <div class="col">
            <a href="#" class="btn-createproject btnGreen" data-toggle="modal" data-target="#createPBLKategori">Skapa ny mall</a>
        </div><!-- /.col -->
    </div>
    <div class="card">
        <div class="card-body p-0">

            <div id="table_data">
                @include('Admin.definitions.TemplateCategory.loadData')
            </div>

            <div class="modal fade" id="createPBLKategori" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                        </div>
                        <div class="modal-body">
                            <div class="logomodal">
                                <img src="{{ asset('dist/img/favicon.png') }}" alt="">
                            </div>
                            <div class="title-modal"><span>Lägg till en ny mallkategori</span></div>
                            <form action="" id="createCategoryForm" method="post" enctype="multipart/form-data" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-left">
                                @csrf
                                <div class="col-sm-12 pull-left">
                                    <!-- text input -->
                                    <div class="form-group">
                                        <label>Titel</label>
                                        <input type="text" name="title" id="title" required class="form-control" placeholder="Skriv in ...">
                                    </div>
                                </div>
                                <div class="col-sm-12 pull-left">
                                    <label>Icon</label>
                                    <input type="file" id="logo" name="logo" class="hideinput">
                                    <span class="special-upload">Ladda upp</span>
                                    <span class="shownameupload">uppladdad fil</span>
                                </div>
                                <div class="col-sm-12 pull-left">
                                    <button type="submit" id="savePBLCategory" class="btnmodal">Lägg till</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="editPBLKategori" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                        </div>
                        <div class="modal-body">
                            <div class="logomodal">
                                <img src="{{ asset('dist/img/favicon.png') }}" alt="">
                            </div>
                            <div class="title-modal"><span>Edit TemplateCategory</span></div>
                            <form action="" id="editFormTemplate" method="post" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-left">
                                @csrf
                                <div class="col-sm-12 pull-left">
                                    <!-- text input -->
                                    <div class="form-group">
                                        <label>titel</label>
                                        <input type="text" name="titleedit" id="titleedit" required class="form-control" placeholder="Enter ...">
                                    </div>
                                </div>
                                <div class="col-sm-12 pull-left">
                                    <label>Icon</label>
                                    <input type="file" id="logo" name="logo" class="hideinput">
                                    <span class="special-upload">Upload</span>
                                    <span class="shownameupload">show File Name upload</span>
                                </div>
                                <div class="col-sm-12 pull-left">
                                    <button type="submit" id="editPBLCategory" class="btnmodal">Lägg till TemplateCategory</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <!-- /.card-body -->
    </div>
@endsection

@section('script')

    <script type="text/javascript">
        function getdata(page)
        {
            $.ajax({
                url:"/panel/definitions/templateCategory/getData?page="+page,
                success:function(data)
                {
                    $('#table_data').html(data);
                }
            });
        }
        var page;
        $(document).ready(function(){
            var title;
            var id;

            $(document).on('click', '.pagination a', function(event){
                event.preventDefault();
                page = $(this).attr('href').split('page=')[1];

                getdata(page);
            });


            $("#createCategoryForm").on('submit',(function(event) {
                event.preventDefault();
                var formData = new FormData(this);
                $.ajax({
                    type: 'POST',
                    url: '{{ route('templateCategory.store') }}',
                    data: formData,
                    enctype: 'multipart/form-data',
                    cache:false,
                    contentType: false,
                    processData: false,
                    success: function(data) {
                        console.log(data);
                        $('#createPBLKategori').modal('hide');
                        Swal.fire(
                            'Bra jobbat !',
                            'Ny mallkategori har skapats',
                            'success');
                        getdata(page);


                    },
                    error: function(err){
                        if (err.status == 422) { // when status code is 422, it's a validation issue
                            console.log(err.responseJSON);

                            errorsHtml = '<div class="alert alert-danger"><ul>';

                            $.each(err.responseJSON.errors, function (i, error) {
                                errorsHtml += '<li>' +error[0] + '</li>'; //showing only the first error.
                            });
                            errorsHtml += '</ul></di>';

                            $( '#errors' ).html( errorsHtml ); //appending to a <div id="form-errors"></div> inside form
                        }
                    },
                });
            }));


            $("#editPBLKategori").on("show.bs.modal", function (e) {
                id = $(e.relatedTarget).data('target-id');

                title = $(e.relatedTarget).data('target-title');
                $("#titleedit").val(title);
            });

            $("#createCategoryForm").on('submit',(function(event) {
                event.preventDefault();
                var formData = new FormData(this);
                $.ajax({
                    type: 'POST',
                    url: '{{ route('templateCategory.store') }}',
                    data: formData,
                    enctype: 'multipart/form-data',
                    cache:false,
                    contentType: false,
                    processData: false,
                    success: function(data) {
                        console.log(data);
                        $('#createPBLKategori').modal('hide');
                        Swal.fire(
                            'Bra jobbat !',
                            'Ny mallkategori har skapats',
                            'success');
                        getdata(page);


                    },
                    error: function(err){
                        if (err.status == 422) { // when status code is 422, it's a validation issue
                            console.log(err.responseJSON);

                            errorsHtml = '<div class="alert alert-danger"><ul>';

                            $.each(err.responseJSON.errors, function (i, error) {
                                errorsHtml += '<li>' +error[0] + '</li>'; //showing only the first error.
                            });
                            errorsHtml += '</ul></di>';

                            $( '#errors' ).html( errorsHtml ); //appending to a <div id="form-errors"></div> inside form
                        }
                    },
                });
            }));


            $("#editFormTemplate").on('submit',(function(event) {
                event.preventDefault();
                var formData = new FormData(this);
                $.ajax({
                    type: 'POST',
                    url: '/panel/definitions/templateCategory/' + id,
                    data: formData,
                    enctype: 'multipart/form-data',
                    cache:false,
                    contentType: false,
                    processData: false,
                    success: function(data) {
                        getdata(page);
                        $('#editPBLKategori').modal('hide');
                        Swal.fire(
                            'Bra jobbat !',
                            'Ny mallkategori har skapats',
                            'success');
                    },
                    error: function(err){
                        if (err.status == 422) { // when status code is 422, it's a validation issue
                            console.log(err.responseJSON);

                            errorsHtml = '<div class="alert alert-danger"><ul>';

                            $.each(err.responseJSON.errors, function (i, error) {
                                errorsHtml += '<li>' +error[0] + '</li>'; //showing only the first error.
                            });
                            errorsHtml += '</ul></di>';

                            $( '#errors' ).html( errorsHtml ); //appending to a <div id="form-errors"></div> inside form
                        }
                    },
                });
            }));


            // $("#editPBLCategory").click(function() {
            //     var titleedit =  $("#titleedit").val();
            //     var logo = $('#logo')[0];
            //     $.ajax({
            //         type: 'POST',
            //         url: '/panel/definitions/templateCategory/' + id,
            //         data: {'_token': $('input[name="_token"]').val(),'id': id,'title': titleedit,''},
            //         success: function(data) {
            //             console.log(data);
            //             $('#editPBLKategori').modal('hide');
            //             Swal.fire(
            //                 'Bra jobbat !',
            //                 'Ny mallkategori har skapats',
            //                 'success');
            //             getdata(page);
            //         },
            //         error: function(err){
            //             if (err.status == 422) { // when status code is 422, it's a validation issue
            //                 console.log(err.responseJSON);
            //
            //                 errorsHtml = '<div class="alert alert-danger"><ul>';
            //
            //                 $.each(err.responseJSON.errors, function (i, error) {
            //                     errorsHtml += '<li>' +error[0] + '</li>'; //showing only the first error.
            //                 });
            //                 errorsHtml += '</ul></di>';
            //
            //                 $( '#errors' ).html( errorsHtml ); //appending to a <div id="form-errors"></div> inside form
            //             }
            //         },
            //     });
            // });


        });

        function deleteConfirmation(id) {
            //TODO CHECK USERID REPEAT
            Swal.fire({
                type: 'warning',
                title: 'Är du säker?',
                text: "Du kommer inte att kunna återställa detta!",
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                cancelButtonText: 'annullera',
                confirmButtonText: 'Ja, radera det!'
            }).then(function (e) {
                if (e.value === true) {
                    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

                    $.ajax({
                        type: 'POST',
                        url: "/panel/definitions/templateCategory/delete/" + id,
                        data: {_token: '{!! csrf_token() !!}', id: id},
                        dataType: 'JSON',
                        success: function (results) {
                            Swal.fire(
                                'Bra jobbat !',
                                'TemplateCategory radera slutförd',
                                'success');
                            getdata(page);
                        }
                    });
                }
                else {
                    e.dismiss;
                }}, function (dismiss) {
                return false;
            })

        }

    </script>
@endsection
