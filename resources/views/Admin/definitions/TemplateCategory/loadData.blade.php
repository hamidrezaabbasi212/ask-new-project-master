<table class="table table-striped projects text-center">
    <thead>
    <tr>
        <th style="width: 1%">
            #
        </th>
        <th style="width: 20%">
            Titel
        </th>
        <th style="width: 20%">
           Ikon
        </th>
        <th style="text-align:center;width: 20%">
            Drift
        </th>
    </tr>
    </thead>
    <tbody>
    @foreach($categories as $key => $category)
        <tr>
            <td>{{ ($categories->currentpage()-1) * $categories->perpage() +  $key + 1 }}</td>
            <td>{{ $category->title }}</td>
            <td><img alt="Avatar" class="table-avatar" style="background-color: #0c5460" src="{{ asset($category->logo !=null ? $category->logo : '/dist/img/white-logo.png') }}" data-toggle="tooltip" data-placement="top"></td>
            <td class="project-actions">
                <a href="#" data-toggle="modal" data-target="#editPBLKategori" data-target-id="{{ $category->id }}" data-target-title="{{ $category->title }}" title=""><i class="fa fa-edit" aria-hidden="true"></i></a>
{{--                <button type="button" onclick="deleteConfirmation({{ $category->id }})" class="fa fa-trash-alt sa-remove"></button>--}}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
<div class="text-center" style="width:15%;  margin: auto; margin-top: 20px !important;">{!! $categories->links() !!}</div>
