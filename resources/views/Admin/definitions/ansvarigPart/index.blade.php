@extends('Admin.master')
@section('content-title', 'AnsvarigPart')
@section('title', 'AnsvarigPart')
@section('content')
    <div class="row mb-3">
        <div class="col">
            <a href="#" class="btn-createproject" data-toggle="modal" data-target="#createPBLKategori">Nytt AnsvarigPart</a>
            <a href="#" class="btn-createproject mr-2" data-toggle="modal" data-target="#importexecel">importera Excel</a>
        </div><!-- /.col -->
    </div>
    <div class="card">
        <div class="card-body p-0">
            <table class="table table-striped projects text-center">
                <thead>
                <tr>
                    <th style="width: 1%">
                        #
                    </th>
                    <th style="width: 20%">
                        Titel
                    </th>
                    <th style="text-align:center;width: 20%">
                        Operation
                    </th>
                </tr>
                </thead>
                <tbody>

                    <tr>

                    </tr>
                </tbody>

            </table>


            <div class="modal fade" id="createPBLKategori" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                        </div>
                        <div class="modal-body">
                            <div class="logomodal">
                                <img src="{{ asset('dist/img/favicon.png') }}" alt="">
                            </div>
                            <div class="title-modal"><span>Add New Risk</span></div>
                            <form action="" method="post" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-left">
                                <div class="col-sm-12 pull-left">
                                    <!-- text input -->
                                    <div class="form-group">
                                        <label>titel</label>
                                        <input type="text" name="title" id="title" required class="form-control" placeholder="Enter ...">
                                    </div>
                                </div>
                                <div class="col-sm-12 pull-left">
                                    <button type="button" id="savePBLCategory" class="btnmodal">Lägg till Risk</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="editPBLKategori" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                        </div>
                        <div class="modal-body">
                            <div class="logomodal">
                                <img src="{{ asset('dist/img/favicon.png') }}" alt="">
                            </div>
                            <div class="title-modal"><span>Edit AnsvarigPart</span></div>
                            <form action="" method="post" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-left">
                                @csrf
                                <div class="col-sm-12 pull-left">
                                    <!-- text input -->
                                    <div class="form-group">
                                        <label>titel</label>
                                        <input type="text" name="titleedit" id="titleedit" required class="form-control" placeholder="Enter ...">
                                    </div>
                                </div>
                                <div class="col-sm-12 pull-left">
                                    <button type="button" id="editPBLCategory" class="btnmodal">Lägg till Risk</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="importexecel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                        </div>
                        <form action="{{ route('ansvarigPart.import') }}" method="post" class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-left" enctype="multipart/form-data">
                        <div class="modal-body">
                            <div class="logomodal">
                                <img src="{{ asset('dist/img/favicon.png') }}" alt="">
                            </div>
                            <div class="title-modal"><span>Excel AnsvarigPart</span></div>
                                @csrf
                                <div class="col-sm-12 pull-left">
                                    <!-- text input -->
                                    <div class="form-group">
                                        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 col-upload-in-project col-upload-inproject">
                                            <input type="file" name="ansvarigPart" class="form-control btn-upload-in-project">
                                            <i class="fa fa-upload icon-upload" style="left: 100px" aria-hidden="true"></i>
                                            <span class="specialupload" style="left: 0">ladda upp</span>
                                        </div>
                                    </div>
                                </div>
                            <div class="form-group">
                                <div class="col-sm-12 pull-left">
                                    <button type="submit" class="btnmodal">Lägg till ladda upp</button>
                                </div>
                            </div>

                        </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>
        <!-- /.card-body -->
    </div>
@endsection

@section('script')

<script type="text/javascript">
    function getdata()
    {
        $.ajax({
            type : 'get',
            url : '{{ route('ansvarigPart.getData') }}',
            success:function(response){
                console.log(response);
                var res='';
                $.each (response, function (key, value) {
                    res +=
                        '<tr>'+
                        '<td>'+key+'</td>'+
                        '<td>'+value.title+'</td>'+
                        '<td class="project-actions">' +
                        '<a href="#" data-toggle="modal" data-target="#editPBLKategori" data-target-id="'+value.id+'" data-target-title="'+value.title+'" title=""><i class="fa fa-edit" aria-hidden="true"></i></a>' +
                        '<button type="button" onclick="deleteConfirmation('+value.id+')" class="fa fa-trash-alt sa-remove"></button></td>'+
                        '</tr>';
                });
                $('tbody').html(res);
            }
        });
    }
    $(document).ready(function(){
        var title;
        var id;
        getdata();


        $("#savePBLCategory").click(function() {

            $.ajax({
                type: 'POST',
                url: '{{ route('ansvarigPart.store') }}',
                data: {'_token': $('input[name="_token"]').val(),'title': $('#title').val(),},
                success: function(data) {
                    $('#createPBLKategori').modal('hide');
                    Swal.fire(
                        'Bra jobbat !',
                        'AnsvarigPart registreringen slutförd',
                        'success');
                    getdata();


                },
                error: function(err){
                    if (err.status == 422) { // when status code is 422, it's a validation issue
                        console.log(err.responseJSON);

                        errorsHtml = '<div class="alert alert-danger"><ul>';

                        $.each(err.responseJSON.errors, function (i, error) {
                            errorsHtml += '<li>' +error[0] + '</li>'; //showing only the first error.
                        });
                        errorsHtml += '</ul></di>';

                        $( '#errors' ).html( errorsHtml ); //appending to a <div id="form-errors"></div> inside form
                    }
                },
            });
        });


        $("#editPBLKategori").on("show.bs.modal", function (e) {
            id = $(e.relatedTarget).data('target-id');

             title = $(e.relatedTarget).data('target-title');
            $("#titleedit").val(title);
        });


        $("#editPBLCategory").click(function() {
            var titleedit =  $("#titleedit").val();
            $.ajax({
                type: 'POST',
                url: '/panel/definitions/ansvarigPart/' + id,
                data: {'_token': $('input[name="_token"]').val(),'id': id,'title': titleedit,},
                success: function(data) {
                    $('#editPBLKategori').modal('hide');
                    Swal.fire(
                        'Bra jobbat !',
                        'AnsvarigPart redigera slutförd',
                        'success');
                    getdata();


                },
                error: function(err){
                    if (err.status == 422) { // when status code is 422, it's a validation issue
                        console.log(err.responseJSON);

                        errorsHtml = '<div class="alert alert-danger"><ul>';

                        $.each(err.responseJSON.errors, function (i, error) {
                            errorsHtml += '<li>' +error[0] + '</li>'; //showing only the first error.
                        });
                        errorsHtml += '</ul></di>';

                        $( '#errors' ).html( errorsHtml ); //appending to a <div id="form-errors"></div> inside form
                    }
                },
            });
        });


    });

    function deleteConfirmation(id) {
        //TODO CHECK USERID REPEAT
        Swal.fire({
            type: 'warning',
            title: 'Är du säker?',
            text: "Du kommer inte att kunna återställa detta!",
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'annullera',
            confirmButtonText: 'Ja, radera det!'
        }).then(function (e) {
            if (e.value === true) {
                var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

                $.ajax({
                    type: 'POST',
                    url: "/panel/definitions/ansvarigPart/delete/" + id,
                    data: {_token: '{!! csrf_token() !!}', id: id},
                    dataType: 'JSON',
                    success: function (results) {
                       console.log(results);
                        Swal.fire(
                            'Bra jobbat !',
                            'Risk radera slutförd',
                            'success');
                        getdata();
                    }
                });
            }
            else {
                e.dismiss;
            }}, function (dismiss) {
            return false;
        })

    }

</script>
    @endsection
