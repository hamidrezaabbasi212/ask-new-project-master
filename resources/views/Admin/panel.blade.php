@extends('Admin.master')
@section('content-title', 'Panel')
@section('title', 'Panel')
@section('content')
    <div class="row mb-3">
        <div class="col">
            <a class="btn-createproject" href="{{ route('project.create') }}">Nytt Projekt</a>
        </div><!-- /.col -->
    </div>

    <div class="row">
        <div class="col-12 col-sm-6 col-md-3">
            <a href="{{ route('project.index') }}"><div class="info-box">
                    <span class="info-box-icon"><i class="fas fa-tasks"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Pågående Projekt</span>
                        <span class="info-box-number">{{ number_format($projectCount) }}</span>
                    </div>
                    <!-- /.info-box-content -->
                </div></a>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-12 col-sm-6 col-md-3">
            <a href="{{ route('project.index') }}"><div class="info-box">
                    <span class="info-box-icon"><i class="fas fa-thumbs-up"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Avslutade Projekt</span>
                        <span class="info-box-number">{{ number_format($projectCompleteCount) }}</span>
                    </div>
                    <!-- /.info-box-content -->
                </div></a>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->

        <!-- fix for small devices only -->
        <div class="clearfix hidden-md-up"></div>

        <div class="col-12 col-sm-6 col-md-3">
            <a href="{{ route('account.index') }}"><div class="info-box mb-3">
                    <span class="info-box-icon"><i class="fas fa-users"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text">Aktiva Användare</span>
                        <span class="info-box-number">{{ number_format($usersCount) }}</span>
                    </div>
                    <!-- /.info-box-content -->
                </div></a>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-12 col-sm-6 col-md-3">
            <a href="{{ route('Organization.index') }}"><div class="info-box mb-3">
                    <span class="info-box-icon"><i class="fas fa-building"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text">Aktiva Organisationer</span>
                        <span class="info-box-number">{{ number_format($organizationCount) }}</span>
                    </div>
                    <!-- /.info-box-content -->
                </div></a>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->
    </div>



    <div class="row">
        <div class="modal fade" id="contact" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    </div>
                    <div class="modal-body">
                        <div class="logomodal">
                            <img src="{{ asset('dist/img/favicon.png') }}" alt="">
                        </div>
                        <div class="title-modal"><span>Support</span></div>
                        <form id="createContact" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-left" method="post" action="" enctype="multipart/form-data">
                            @csrf
                            <div class="col-sm-6 pull-left">
                                <!-- text input -->
                                <div class="form-group">
                                    <label>Telefon</label>
                                    <input type="text" id="contactPhone" name="contactPhone" required class="form-control" placeholder="Enter ...">
                                </div>
                            </div>
                            <div class="col-sm-6 pull-left">
                                <!-- text input -->
                                <div class="form-group">
                                    <label>Email</label>
                                    <input type="text" id="contactPost" name="contactPost" required class="form-control" placeholder="Enter ...">
                                </div>
                            </div>
                            <div class="col-sm-12 pull-left">
                                <div class="form-group">
                                    <label>Öppettider</label>
                                    <input type="text" id="contactUsTime" name="contactUsTime" required class="form-control" placeholder="Enter ...">
                                </div>
                            </div>

                            <div class="col-sm-12 pull-left">
                                <div class="form-group">
                                    <label>Address</label>
                                    <textarea id="contactAddress" name="contactAddress" class="form-control"></textarea>
                                </div>
                            </div>
{{--                            <div class="col-sm-12 pull-left">--}}
{{--                                <button id="btn-contact" type="submit" class="btnmodal btn-add-user">Set Contact us</button>--}}
{{--                            </div>--}}
                        </form>
                    </div>
                </div>
            </div>
        </div>


        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding sec-projects">
            @foreach($projects as $project)
                @php $prjectInformation = unserialize($project->projectInfo)  @endphp
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 col-project">
                    <div class="col-md-12 box-project">
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 l-box-project">
                            <div class="name-project">
                                <h3><a style="color: #1b1e21" href="{{ route('project.controllPlan' , ['id'=>$project['id']]) }}">{{ $prjectInformation['Projektnamn'] !=null ? $prjectInformation['Projektnamn'] : 'inget namn' }}</a></h3>
                            </div>
                            <span class="span-boxproject">{{ step($project->step) }}</span>
                            <ul>
                                <li><i class="fa fa-check-square" aria-hidden="true"></i>{{ percent($project->step) }} Pågående egenkontroller</li>
                                <li><i class="fa fa-clock-o" aria-hidden="true"></i>15 Avslutade egenkontroller</li>
                                <li><i class="fa fa-info-circle" aria-hidden="true"></i>7 Ej godkända egenkontroller</li>
                            </ul>
                        </div><!--l-box-project-->
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 r-box-project">
                            <div class="img-box-project">
                                <img src="{{ asset($prjectInformation['projectImage'] ? $prjectInformation['projectImage'] : 'dist/img/project.jpg') }}" alt="">
                            </div>
                            {{--                            <div class="btn-more">--}}
                            {{--                                <a href="{{ route('project.controllPlan' , ['id'=>$project['id']]) }}"><i class="fa fa-ellipsis-h" aria-hidden="true"></i></a>--}}
                            {{--                            </div><!--btn-more-->--}}
                        </div><!--r-box-project-->
                    </div><!--box-project-->
                </div><!--col-project-->
            @endforeach
            @php
                function percent($value)
               {
                   switch ($value) {
                       case "1":
                           echo "15%";
                           break;
                       case "2":
                           echo "30%";
                           break;
                       case "3":
                           echo "45%";
                           break;
                       case "4":
                           echo "60%";
                           break;
                       case "5":
                           echo "75%";
                           break;
                       case "6":
                           echo "85%";
                           break;
                       case "7":
                           echo "100%";
                           break;

                       default:
                           echo "Your favorite color is neither red, blue, nor green!";
                   }
               }

           function step($value)
        {
                           switch ($value) {
                    case "1":
                        echo "Pågående projekt";
                        break;
                    case "2":
                        echo "Tekniskt samråd";
                        break;
                    case "3":
                        echo "Startbesked";
                        break;
                    case "4":
                        echo "Slutsamråd";
                        break;
                    case "5":
                        echo "Interimistiskt slutbesked";
                        break;
                    case "6":
                        echo "Slutbesked";
                        break;
                    case "7":
                        echo "Projekt slut";
                        break;

                    default:
                        echo "Your favorite color is neither red, blue, nor green!";
                }
        }
            @endphp
        </div><!--sec-projects-->
    </div>

    {{--    <div class="row">--}}
    {{--        <div class="col-md-12">--}}
    {{--            <div class="card">--}}
    {{--                <div class="card-header">--}}
    {{--                    <h5 class="card-title">Framsteg samtliga projekt</h5>--}}

    {{--                    <div class="card-tools">--}}
    {{--                        <button type="button" class="btn btn-tool" data-card-widget="collapse">--}}
    {{--                            <i class="fas fa-minus"></i>--}}
    {{--                        </button>--}}
    {{--                        <div class="btn-group">--}}
    {{--                            <button type="button" class="btn btn-tool dropdown-toggle" data-toggle="dropdown">--}}
    {{--                                <i class="fas fa-wrench"></i>--}}
    {{--                            </button>--}}
    {{--                            <div class="dropdown-menu dropdown-menu-right" role="menu">--}}
    {{--                                <a href="#" class="dropdown-item">Action</a>--}}
    {{--                                <a href="#" class="dropdown-item">Another action</a>--}}
    {{--                                <a href="#" class="dropdown-item">Something else here</a>--}}
    {{--                                <a class="dropdown-divider"></a>--}}
    {{--                                <a href="#" class="dropdown-item">Separated link</a>--}}
    {{--                            </div>--}}
    {{--                        </div>--}}
    {{--                        <button type="button" class="btn btn-tool" data-card-widget="remove">--}}
    {{--                            <i class="fas fa-times"></i>--}}
    {{--                        </button>--}}
    {{--                    </div>--}}
    {{--                </div>--}}
    {{--                <!-- /.card-header -->--}}
    {{--                <div class="card-body">--}}
    {{--                    <div class="row">--}}
    {{--                        <div class="col-md-8">--}}
    {{--                            <p class="text-center">--}}
    {{--                                <strong>Designad av ASKprojekt team</strong>--}}
    {{--                            </p>--}}

    {{--                            <div class="chart">--}}
    {{--                                <!-- Sales Chart Canvas -->--}}
    {{--                                <canvas id="salesChart" height="180" style="height: 180px;"></canvas>--}}
    {{--                            </div>--}}
    {{--                            <!-- /.chart-responsive -->--}}
    {{--                        </div>--}}
    {{--                        <!-- /.col -->--}}
    {{--                        <div class="col-md-4">--}}
    {{--                            <p class="text-center">--}}
    {{--                                <strong>Mål-Slutförande</strong>--}}
    {{--                            </p>--}}

    {{--                            <div class="progress-group">--}}
    {{--                                Totalt antal egenkontroller--}}
    {{--                                <span class="float-right"><b>160</b>/200</span>--}}
    {{--                                <div class="progress progress-sm">--}}
    {{--                                    <div class="progress-bar bg-primary" style="width: 80%"></div>--}}
    {{--                                </div>--}}
    {{--                            </div>--}}
    {{--                            <!-- /.progress-group -->--}}

    {{--                            <div class="progress-group">--}}
    {{--                               Totalt avslutade egenkontroller--}}
    {{--                                <span class="float-right"><b>310</b>/400</span>--}}
    {{--                                <div class="progress progress-sm">--}}
    {{--                                    <div class="progress-bar bg-success" style="width: 75%"></div>--}}
    {{--                                </div>--}}
    {{--                            </div>--}}

    {{--                            <!-- /.progress-group -->--}}
    {{--                            <div class="progress-group">--}}
    {{--                                <span class="progress-text">Totalt ej godkända egenkontroller</span>--}}
    {{--                                <span class="float-right"><b>480</b>/800</span>--}}
    {{--                                <div class="progress progress-sm">--}}
    {{--                                    <div class="progress-bar bg-danger" style="width: 60%"></div>--}}
    {{--                                </div>--}}
    {{--                            </div>--}}

    {{--                            <!-- /.progress-group -->--}}

    {{--                            <!-- /.progress-group -->--}}
    {{--                        </div>--}}
    {{--                        <!-- /.col -->--}}
    {{--                    </div>--}}
    {{--                    <!-- /.row -->--}}
    {{--                </div>--}}
    {{--                <!-- ./card-body -->--}}

    {{--                <!-- /.card-footer -->--}}
    {{--            </div>--}}
    {{--            <!-- /.card -->--}}
    {{--        </div>--}}
    {{--        <!-- /.col -->--}}
    {{--    </div>--}}
    <!-- Main row -->
    <div class="row">
        <!-- Left col -->
        <div class="col-md-8">
            <!-- MAP & BOX PANE -->

            <!-- /.card -->

            <!-- /.row -->

            <!-- TABLE: LATEST ORDERS -->

            <!-- /.card -->
        </div>
        <!-- /.col -->

        <!-- /.col -->
    </div>
    <!-- /.row -->
@endsection

@section('script')
    <script type="text/javascript">

        $(document).ready(function () {

            $('#createContact').submit(function(event) {
                event.preventDefault();
                var formData = new FormData(this);
                $.ajax({
                    type: 'POST',
                    url: '{{ route('contact.store') }}',
                    data: formData,
                    processData: false,
                    contentType: false,
                    success:function (data) {
                        console.log(data);
                        Swal.fire(
                            'Bra jobbat !',
                            'kontakta oss registreringen slutförd',
                            'success');
                        $('#contact').modal('hide');
                    },
                    error:function (err) {
                        console.log(err);
                    }
                });
            });

            $("#contact").on("show.bs.modal", function (e) {

                $.ajax({
                    type : 'get',
                    url : '{{ route('contact.getContactInformation') }}',
                    success:function (data) {
                        $('#contactAddress').text(data['address']);
                        $('#contactPhone').val(data['phone']);
                        $('#contactPost').val(data['post']);
                        $('#contactUsTime').val(data['contactUsTime']);
                    }
                });
            });
        });


    </script>
@endsection
