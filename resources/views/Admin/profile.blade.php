@extends('Admin.master')
@section('content-title', 'Profil')
@section('title', 'Profil')
@section('content')
    <div class="row">
        <div class="col-md-3">

            <!-- Profile Image -->
            <div class="card card-primary card-outline">
                <div class="card-body box-profile">
                    <div class="text-center">
                        <img class="profile-user-img img-fluid img-circle" src="{{ asset($account->avatar) }}" alt="User profile picture">
                    </div>

                    <h3 class="profile-username text-center">{{ $account->name }}</h3>
                    @php
                        $role =  \App\Role::find($account->role_id);
                    @endphp
                    <p class="text-muted text-center">{{ $role ? $role->title : '' }}</p>

                    <ul class="list-group list-group-unbordered mb-3">
                        <li class="list-group-item noborder">
                            <b>E-post</b><br><a>{{ $account->email }}</a>
                        </li>
                        <li class="list-group-item noborder">
                            <b>Telefon</b><br><a>{{ $account->phone }}</a>
                        </li>
                    </ul>

                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->

            <!-- About Me Box -->
            <!-- /.card -->
        </div>
        <!-- /.col -->
        <div class="col-md-9">
            <div class="card">
                <div class="card-body">
                    <div class="">

                        <!-- /.tab-pane -->
                        <!-- /.tab-pane -->

                        <div class="" id="settings">
                            <form class="form-horizontal" action="{{ route('ProfileUpdate' , $account->id) }}" method="post" enctype="multipart/form-data">
                                @csrf
                                <div class="form-group row">
                                    <label for="inputName" class="col-sm-2 col-form-label">Namn</label>
                                    <div class="col-sm-10">
                                        <input type="text" value="{{ $account->name }}" name="name" class="form-control" id="inputName" placeholder="namn">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputEmail" class="col-sm-2 col-form-label">E-post</label>
                                    <div class="col-sm-10">
                                        <input type="email" value="{{ $account->email }}" name="email" class="form-control" id="inputEmail" placeholder="E-post">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputName2" class="col-sm-2 col-form-label">Telefon</label>
                                    <div class="col-sm-10">
                                        <input type="text" value="{{ $account->phone }}" name="phone" class="form-control" id="inputName2" placeholder="Telefon">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputName2" class="col-sm-2 col-form-label">Lösenord</label>
                                    <div class="col-sm-10">
                                        <input type="password" value="" name="password" class="form-control" id="inputName2" placeholder="Lösenord">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputName2" class="col-sm-2 col-form-label">Lösenords bekräftelse</label>
                                    <div class="col-sm-10">
                                        <input type="password" value="" name="password_confirmation" class="form-control" id="inputName2" placeholder="Lösenords bekräftelse">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputName2" class="col-sm-2 col-form-label">Avatar</label>
                                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 col-upload-in-project col-upload-inproject">
                                        <input type="file" name="avatar" class="btn-upload-in-project">
                                        <i class="fa fa-upload icon-upload" aria-hidden="true" style="left: 98px;"></i>
                                        <span class="specialupload" style="left: 0">ladda upp</span>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="offset-sm-2 col-sm-10">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox"> Jag godkänner de <a href="#">allmänna villkoren</a>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="offset-sm-2 col-sm-10">
                                        <button type="submit" class="btn btn-danger btnprofile">Redigera</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <!-- /.tab-pane -->
                    </div>
                    <!-- /.tab-content -->
                </div><!-- /.card-body -->
            </div>
            <!-- /.nav-tabs-custom -->
        </div>
        <!-- /.col -->
    </div>
@endsection

@section('script')
    @if(Session::has('success'))
        <script>
            Swal.fire(
                'Okej!',
                'Profilen redigerades framgångsrikt',
                'success');
        </script>
    @endif
@endsection
