@extends('Admin.master')
@section('content-title', 'Projekts')
@section('title', 'Projekts')
@section('content')
    <form action="{{ route('advance.store') }}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="card card-warning boxformproject">
            <div class="card-body">
                @include('Admin.section.errors')
                <div class="row">
                    <div class="col-sm-6">
                        <label for="inputName2" class="col-sm-2 col-form-label">Import Laws</label>
                        <div class="form-group">

                            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 col-upload-in-project col-upload-inproject">
                                <input type="file" name="lawFile" class="form-control btn-upload-in-project">
                                <i class="fa fa-upload icon-upload" style="left: 100px" aria-hidden="true"></i>
                                <span class="specialupload" style="left: 0">ladda upp</span>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="btn-in-forminfoproject">
                <button type="button" class="back3 bg-danger">Back</button>
                <button type="submit" class="next3 bg-success">Save</button>
            </div>
        </div>

    </form>
@endsection

@section('script')

@endsection
