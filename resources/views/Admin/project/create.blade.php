@extends('Admin.master')
@section('content-title', 'Nytt Projekt')
@section('title', 'Nytt Projekt')
@section('content')

    <div class="row">
        <div class="col-12 col-sm-12 col-md-12">
            <p class="pad15">Välj ett alternativ nedan</p>
        </div>
        <div class="col-12 col-sm-12 col-md-12 box-modelproject">
            <div class="col-7 col-sm-7 col-md-6 mr-auto">
                <a href="{{ route('createProject') }}"><div class="col-12 col-sm-12 col-md-12">
                        <div class="info-box info-box2">
                            <span class="info-box-icon"><i class="fas fa-cog"></i></span>
                            <div class="info-box-content">
                                <span class="info-box-text spanmodel">Manuellt <span class="desc-span">Bygg din egen kontrollplan från grunden</span></span>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                    </div></a>
                <a href="{{ route('project.createMall') }}"><div class="col-12 col-sm-12 col-md-12">
                        <div class="info-box info-box2 mb-3">
                            <span class="info-box-icon"><i class="fa fa-clipboard" aria-hidden="true"></i></span>
                            <div class="info-box-content">
                                <span class="info-box-text spanmodel">Standardmallar <span class="desc-span">Förslag till färdiga kontrollplaner</span></span>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                    </div></a>
                <a href="{{ route('project.uploadifc') }}"><div class="col-12 col-sm-12 col-md-12">
                        <div class="info-box info-box2 mb-3">
                            <span class="info-box-icon"><i class="fas fa-upload"></i></span>
                            <div class="info-box-content">
                                <span class="info-box-text spanmodel">Teknisk beskrivning <span class="desc-span">skapa en kontrollplan från en teknisk beskrivning</span></span>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                    </div></a>
                <a href="{{ route('project.owntemplate') }}"><div class="col-12 col-sm-12 col-md-12">
                        <div class="info-box info-box2 mb-3">
                            <span class="info-box-icon"><i class="fa fa-floppy-o" aria-hidden="true"></i></span>
                            <div class="info-box-content">
                                <span class="info-box-text spanmodel">Egna mallar <span class="desc-span">återanvänd sparade mallar</span></span>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                    </div></a>
                <!--<div class="btn-in-forminfoproject">
                  <a href="infoproject-step1.html" class="bg-success">Next</a>
                </div>-->
            </div>
            <div class="col-5 col-sm-5 col-md-6 nopadding r-mr-auto">
                <img src="{{ asset('dist/img/newproject.jpg') }}" alt="">
            </div><!--r-mr-auto-->
        </div>
    </div>
    @endsection

@section('script')

    @endsection
