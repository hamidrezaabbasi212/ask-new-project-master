@extends('Admin.master')
@section('content-title', 'Egna Mallar')
@section('title', 'Egna Mallar')
@section('content')

            <div class="card">
                <div class="card-body p-0">
                    <table class="table table-striped projects tbl-owntemplate">
                        <thead>
                        <tr>
                            <th style="width: 30%">
                                Mallnamn
                            </th>
                            <th style="width: 20%">
                                Historik
                            </th>
                            <th style="width: 30%">
                                kategori
                            </th>
                            <th style="text-align:center;width: 20%">
                                verksamheten
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($templates as $template)
                            <tr>
                                <td>{{ $template->templateName }}</td>
                                <td>{{ $template->created_at }}</td>
                                <td>{{ \App\Definitions\TemplateCategory::whereId($template->templateCategory)->pluck('title')->first() }}</td>
                                <td class="centertxt greenicon"><a href="#" data-toggle="modal" data-target="#EditTemplate" data-target-id="{{ $template->id }}" data-target-tempname="{{ $template->templateName }}" data-target-templatecategory="{{ $template->templateCategory }}" title=""><i class="fa fa-edit" aria-hidden="true"></i></a>
                                    <button type="button" onclick="deleteConfirmation('{{ $template->id }}')" class="fa fa-trash-alt sa-remove"></button>
                                    <a href="{{ route('project.template',['id' => $template->id]) }}" title="tilldela projektet"><i class="fa fa-arrow-alt-circle-right" aria-hidden="true"></i></a>
                                </td>

                            </tr>
                         @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="modal fade" id="EditTemplate" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                            </div>
                            <div class="modal-body">
                                <div class="logomodal">
                                    <img src="{{ asset('dist/img/favicon.png') }}" alt="">
                                </div>
                                <div class="title-modal"><span>Add Template</span></div>
                                <form class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-left" method="" action="">

                                    <div class="col-sm-6 pull-left">
                                        <div class="form-group">
                                            <label>TemplateCategory</label>
                                            <select id="templateCategoryedit" class="form-control select2 select2-hidden-accessible" style="width: 100%;" data-select2-id="1" tabindex="-1" aria-hidden="true">
                                                <option value="0">Selected Option</option>
                                                @foreach(\App\Definitions\TemplateCategory::all() as $TemplateCategory)
                                                    <option value="{{ $TemplateCategory->id }}">{{ $TemplateCategory->title }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 pull-left">
                                        <div class="form-group">
                                            <label>TemplateName</label>
                                            <input type="text" id="templateNameedit" class="form-control" placeholder="Enter ...">
                                        </div>
                                    </div>
                                    <div class="col-sm-12 pull-left">
                                        <button type="button" id="btn-editTemplate" class="btnmodal btn-add-user">edit Template</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.card-body -->
            </div>

    @endsection

@section('script')
    <script type="text/javascript">

        function deleteConfirmation(id) {
            Swal.fire({
                type: 'warning',
                title: 'Är du säker?',
                text: "Du kommer inte att kunna återställa detta!",
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                cancelButtonText: 'annullera',
                confirmButtonText: 'Ja, radera det!'
            }).then(function (e) {
                if (e.value === true) {
                    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
                    $.ajax({
                        type: 'POST',
                        url: "/panel/template/delete/" + id,
                        data: {_token: '{!! csrf_token() !!}',},
                        dataType: 'JSON',
                        success: function (results) {
                            if (results.success === true) {

                                Swal.fire(
                                    'Raderade!',
                                    'Din fil har tagits bort.',
                                    'success'
                                );
                                location.reload();
                            } else {
                                Swal.fire({
                                    icon: 'fel',
                                    title: 'Hoppsan...',
                                    text: 'Något gick fel!',
                                })
                            }
                        }
                    });
                }
                else {
                    e.dismiss;
                }}, function (dismiss) {
                return false;
            })

        }

        $(document).ready(function(){

            $("#EditTemplate").on("show.bs.modal", function (e) {
                id = $(e.relatedTarget).data('target-id');

                var  templateNameedit = $(e.relatedTarget).data('target-tempname');

                var templateCategoryedit = $(e.relatedTarget).data('target-templatecategory');

                $("#templateNameedit").val(templateNameedit);

                $("#templateCategoryedit" ).val(templateCategoryedit).change();

            });

            $('#btn-editTemplate').click(function () {
                var templateNameupdate = $('#templateNameedit').val();
                var templateupdate = $("#templateCategoryedit option:selected").val();
                $.ajax({
                    type: 'POST',
                    url: '/panel/template/' + id,
                    data: {"_token": "{{ csrf_token() }}",'templateName': templateNameupdate,
                        'templateCategory':templateupdate},
                    success:function (data) {
                        $('#EditTemplate').modal('hide');
                        Swal.fire(
                            'Bra jobbat !',
                            'mall uppdatering slutförd',
                            'success');
                        location.reload();
                    },
                    error:function (err) {
                        console.log(err);
                    }
                });
            });

        });

    </script>
@endsection
