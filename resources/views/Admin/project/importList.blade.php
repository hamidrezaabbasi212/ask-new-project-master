@extends('Admin.master')
@section('content-title', 'Biljetter')
@section('title', 'Biljetter')
@section('content')
    <style>
        .ajax-loader {
            visibility: hidden;
            background-color: rgba(255,255,255,0.7);
            position: absolute;
            z-index: +100 !important;
            width: 100%;
            height:100%;
        }

        .ajax-loader img {
            position: relative;
            top: 97%;
            left:29%;
        }
    </style>
    <div class="row mb-3">
        <div class="ajax-loader">
            <img src="{{ asset('loader/Ajax-loader.gif') }}" class="img-responsive" />
        </div>

        <div class="col">
            <a  href="{{ route('supportTicket.create') }}" class="btn-createproject">Ny biljett</a>
        </div><!-- /.col -->
    </div>
    <div class="row mb-3" dir="rtl">
        <div class="form-check mr-5">
            <input type="checkbox" class="form-check-input" id="Aval">
            <label class="form-check-label mr-4" for="Aval">Välj alla</label>
        </div>
        <div class="form-check mr-3">
            <input type="checkbox" class="form-check-input" id="PBL">
            <label class="form-check-label mr-4" for="PBL">Välj alla</label>
        </div>
    </div>
    <form id="updateLawForm" action="" method="post">
        @csrf
        <div class="card">
        <div class="card-body p-0">
            <div id="table_data">
                <table class="table table-striped text-center projects">
                    <thead>
                    <tr>
                        <th>
                            #
                        </th>
                        <th>
                            Kod
                        </th>
                        <th>
                            hänvisning
                        </th>
                        <th>
                            text
                        </th>
                        <th>
                            Kontrollplan PBL
                        </th>
                        <th>
                            Kontrollplan Avtal
                        </th>
                    </tr>
                    </thead>
                    <tbody id="table_data">
                    @foreach($imports as $key => $import)
                        <tr>
                           <td>{{ $key + 1 }}</td>
                           <td>{{ $import->code }}</td>
                           <td>{{ $import->title }}</td>
                           <td></td>
                           <td>
                               <input id="check1{{ $key }}" class="checkSingle1" type="checkbox"  {{ $import->check1 == 1 ? 'checked' : '' }} onchange="check1(this , {{ $key }})" name="{{ $import->id }}[check1]"></td>
                           <td>
                               <input id="check2{{ $key }}" class="checkSingle2" type="checkbox" {{ $import->check2 == 1 ? 'checked' : '' }}  onchange="check2(this , {{ $key }})" name="{{ $import->id }}[check2]">
                           </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="btn-in-forminfoproject">
            <button type="submit" class="back1 bg-danger">spara</button>
        </div>
    </div>

    </form>
@endsection
@section('script')

    @if(Session::has('success'))
        <script>
            Swal.fire({
                position: 'top-end',
                type: 'success',
                title: 'Meddelande skickat',
                showConfirmButton: false,
                timer: 1500
            });
        </script>
    @endif

    <script type="text/javascript">
        $(document).ready(function (){

            $("#PBL").change(function() {
                if (this.checked) {
                    $(".checkSingle1").each(function() {
                        this.checked=true;
                    });
                    $('#Aval').checked=false;
                    $(".checkSingle2").each(function() {
                        this.checked=false;
                    });
                } else {
                    $(".checkSingle1").each(function() {
                        this.checked=false;
                    });
                }
            });

            $("#Aval").change(function() {
                if (this.checked) {
                    $(".checkSingle2").each(function() {
                        this.checked=true;
                        $('#PBL').checked=false;
                    });
                    $(".checkSingle1").each(function() {
                        this.checked=false;
                    });
                } else {
                    $(".checkSingle2").each(function() {
                        this.checked=false;
                    });
                }
            });


            $("#updateLawForm").submit(function(e) {
                event.preventDefault();
                let formData = new FormData(this);
                let token =  $('input[name="_token"]').val();
                formData.append('_token',token);
                $.ajax({
                    type: 'POST',
                    url: '{{ route('law.import-laws.update') }}',
                    data: formData,
                    processData: false,
                    contentType: false,
                    beforeSend: function() {
                        $('.ajax-loader').css("visibility", "visible");
                    },
                    success:function (data) {
                        if(data['message'] == 'success')
                        {
                            $('.ajax-loader').css("visibility", "hidden");
                            Swal.fire(
                                'Bra jobbat !',
                                'Regler uppdaterade.',
                                'success');
                        }
                        window.location = '{{ route('law.import-laws.infoProject') }}';
                    },
                    error:function (err) {
                        console.log(err);
                    }
                });
            })
        });


        function check1(tag , value) {
            if ($(tag).is(':checked')) {
                if($('#check2' + value).is(':checked'))
                {
                    $('#check2' + value).prop("checked", false);
                }
            }
        }


        function check2(tag , value) {
            if ($(tag).is(':checked')) {
                if($('#check1' + value).is(':checked'))
                {
                    $('#check1' + value).prop("checked", false);
                }
            }
        }



        function deleteConfirmation(id) {
            Swal.fire({
                type: 'warning',
                title: 'Är du säker?',
                text: "Raderade biljetter kan inte längre återvinnas!",
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                cancelButtonText: 'annullera',
                confirmButtonText: 'Ja, radera det!'
            }).then(function (e) {
                if (e.value === true) {
                    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
                    $.ajax({
                        type: 'POST',
                        url: "{{ route('supportTicket.delete') }}",
                        data: {_token: '{!! csrf_token() !!}',id: id},
                        dataType: 'JSON',
                        success: function (results) {
                            console.log(results);
                            if (results.success === true) {
                                Swal.fire(
                                    'Raderade!',
                                    'Biljetten i fråga har tagits bort.',
                                    'success'
                                );
                                location.reload();
                            } else {
                                Swal.fire({
                                    icon: 'fel',
                                    title: 'Hoppsan...',
                                    text: 'Något gick fel!',
                                })
                            }
                        }
                    });
                }
                else {
                    e.dismiss;
                }}, function (dismiss) {
                return false;
            })

        }
    </script>
@endsection

