@extends('Admin.master')
@section('content-title', 'Projekt')
@section('title', 'Projekt')
@section('style')
    <link rel="stylesheet" href="{{ asset('css/map.css') }}" />
    <link rel="stylesheet" href="{{ asset('css/leaflet.css') }}" />
    <link rel="stylesheet" href="{{ asset('css/leaflet-search.css') }}" />
    <style>
        body { margin: 0; padding: 0; }
        #map { position: absolute; top: 55px; left: 0; right: 0; bottom: 0; width: 100%; height: 770px; }

        .mapboxgl-popup {
            max-width: 401px;
            font: 12px/20px 'Helvetica Neue', Arial, Helvetica, sans-serif;
        }
    </style>
@endsection
@section('content')
    <form action="{{ route('project.saveBaseProject' , ['id' => $id]) }}" method="post" id="Projectinfo" enctype="multipart/form-data">
        @csrf
        <div id="form-step">
            <div class="card card-warning boxformproject" style="height: 900px;">
                <div class="card-header">
                    <h3 class="card-title cardHead">Projektets plats</h3>
                    <div class="levelform">
                        <ul>
                            <li class="active">1</li>
                            <li>2</li>
                            <li>3</li>
                            <li>4</li>
                        </ul>
                    </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    @include('Admin.section.errors')
                    <div id="infoproject-step1">
                        <div class="row">
                            <div id="map"></div>
                        </div>
                        <input type="hidden" name="location" id="location" value="59.325264776484666,18.071823321193428">
                    </div>

                </div>
                <div class="btn-in-forminfoproject">
                    <button type="button" class="next bg-success">Nästa</button>
                </div>
            </div>
            <!-- /.card-body -->

        </div>

        <div id="form-step1" style="display: none">
            <div class="card card-warning boxformproject">
                <div class="card-header">
                    <h3 class="card-title cardHead">Projekt Info</h3>
                    <div class="levelform">
                        <ul>
                            <li>1</li>
                            <li class="active">2</li>
                            <li>3</li>
                            <li>4</li>
                        </ul>
                    </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    @include('Admin.section.errors')
                    <div id="infoproject-step1">
                        <div class="row">
                            <div class="col-sm-6">
                                <!-- text input -->
                                <div class="form-group">
                                    <label>Projektnamn</label>
                                    <input type="text" name="Projektnamn" value="{{ old('Projektnamn') }}"
                                           class="form-control" placeholder="Text ...">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Fastighetsbeteckning</label>
                                    <input type="text" name="Fastighetsbeteckning" value="{{ old('Fastighetsbeteckning') }}"
                                           class="form-control" placeholder="Text ...">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>BN-Diarienummer</label>
                                    <input type="text" name="BNDiarienummer"
                                           value="{{ old('BNDiarienummer') }}" class="form-control"
                                           placeholder="Text ...">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label></label>
                                    <div class="checkbox mt-2">
                                        <label>
                                            <input type="checkbox" name="Bygglovbeviljat"> Bygglov beviljat
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Startbesked</label>
                                    <input type="text" name="Startbesked" class="form-control" value="{{ old('Startbesked') }}"
                                           placeholder="Text ...">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label></label>
                                    <div class="checkbox mt-2">
                                        <label>
                                            <input type="checkbox" name="Slutsamråd">Slutsamråd
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Slutbesked</label>
                                    <input type="text" name="Slutbesked" class="form-control" value="{{ old('Slutbesked') }}"
                                           placeholder="Text ...">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label></label>
                                    <div class="checkbox mt-2">
                                        <label>
                                            <input type="checkbox" name="Interimistiskslutbesked">Interimistiskt slutbesked
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <!-- input states -->

                            @if(auth()->user()->is_supervisingCompany == 1 or auth()->user()->is_supervisingEngineer == 1)
                                <div class="col-sm-12">
                                    <label>Byggarens ägare</label>
                                    <select id="typeCreateInfoUser" name="typeCreateInfoUser" onchange="SetUser(this.value)"
                                            class="form-control" data-select2-id="1" tabindex="-1" aria-hidden="true">
                                        <option value="0">Selected Option</option>
                                        <option value="1">Från listan över användare</option>
                                        <option value="2">Ange profilen</option>
                                    </select>
                                </div>
                            @endif

                            <div id="PlaceInfoOwner" class="col-12 mt-3">

                            </div>

                        </div>
                    </div>
                </div>
                <!-- /.card-body -->
                <div class="btn-in-forminfoproject">
                    <button type="button" class="back1 bg-danger">Tillbaka</button>
                    <button type="button" class="next1 bg-success">Nästa</button>
                </div>
            </div>
        </div>

        <div id="form-step2" style="display:none;">
            <div class="card card-warning boxformproject">
                <div class="card-header">
                    <h3 class="card-title cardHead">Project Info</h3>
                    <div class="levelform">
                        <ul>
                            <li>1</li>
                            <li>2</li>
                            <li class="active">3</li>
                            <li>4</li>
                        </ul>
                    </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <div id="infoproject-step2">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Byggnad</label>
                                    <select id="byggnad" name="byggnad" id="byggnad" class="form-control" tabindex="-1" aria-hidden="true">
                                        @foreach(\App\Building::all() as $building)
                                            <option value="{{ $building->id }}">{{ $building->typeCode .'-' . $building->description }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Grundkonstruktion</label>
                                    <input type="text" name="Grundkonstruktion" value="{{ old('Grundkonstruktion') }}" class="form-control" placeholder="Text ...">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    @php
                                        $params = array(['id' => 1 ,'title' => 'Fjärrvärme'],['id' => 2 ,'title' => 'El (direktverkande och elpanna) 1,0'],
                                        ['id' => 3 ,'title' => 'El, frånluftsvärmepump'] , ['id' => 4 ,'title' => 'El, El, uteluft-vattenvärmepump'] ,
                                        ['id' => 5 ,'title' => 'El, markvärmepump (berg, mark, sjö)'],['id' => 6 ,'title' => 'Biobränslepanna (pellets, ved, flis mm)'],
                                        ['id' => 7 ,'title' => 'Olja']);
                                    @endphp
                                    <label>Värmesystem</label>
                                    <select name="Värmesystem" id="Värmesystem" class="form-control" tabindex="-1" aria-hidden="true">
                                        @foreach($params as $param)
                                            <option value="{{ $param['id'] }}">{{ $param['title'] }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Ventilationssystem</label>
                                    <input type="text" name="Ventilationssystem" value="{{ old('Ventilationssystem') }}" class="form-control" placeholder="Text ...">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Tekniskt samråd</label>
                                    <input type="text" id="Teknisktsamråd" name="Teknisktsamråd" value="{{ old('Teknisktsamråd') }}"
                                           class="form-control" placeholder="Text ...">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Entreprenadform</label>
                                    <select id="Entreprenadform" name="Entreprenadform" class="form-control">
                                        <option value="0">Selected Option</option>
                                        <option value="1">Totalentreprenad</option>
                                        <option value="2">Utförandeentreprenad</option>
                                        <option value="3">Egen regi</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label>Ange upphandlingsform</label>
                                    <input type="text" name="Angeupphandlingsform" value="{{ old('Angeupphandlingsform') }}"
                                           class="form-control" placeholder="Text ...">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Övrig information</label>
                                    <input type="text" name="Övriginformation" value="{{ old('Övriginformation') }}" class="form-control" placeholder="Text ...">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Projektbild</label>
                                    <input type="file" name="projectImage"
                                           class="form-control">
                                </div>
                            </div>
                            @if(auth()->user()->is_owner == 1)
                                <div class="col-sm-12">
                                    <label>Kontrollansvarig Urval</label>
                                    <select id="typeCreateInfoKa" name="typeCreateInfoKa" onchange="SetUserKa(this.value)"
                                            class="form-control" data-select2-id="1" tabindex="-1" aria-hidden="true">
                                        <option value="0">Selected Option</option>
                                        <option value="1">Från listan över användare</option>
                                        <option value="2">Ange profilen</option>
                                    </select>
                                </div>
                            @endif

                            <div id="PlaceInfoKa" class="col-12 mt-3">

                            </div>

                            <div class="col-sm-12">
                                <label>Teknikerhandledare</label>
                                <select id="typeCreateInfoPlat" name="typeCreateInfoPlat" onchange="SetUserPlat(this.value)"
                                        class="form-control" data-select2-id="1" tabindex="-1" aria-hidden="true">
                                    <option value="">Välj alternativ</option>
                                    <option value="1">Från listan över användare</option>
                                    <option value="2">Ange profilen</option>
                                </select>
                            </div>

                            <div id="PlaceInfoPlat" class="col-12 mt-3">

                            </div>

                       </div>
                    </div>
                </div>
                <!-- /.card-body -->
                <div class="btn-in-forminfoproject">
                    <button type="button" class="back2 bg-danger">Tillbaka</button>
                    <button type="button" class="next2 bg-success">Nästa</button>
                </div>
            </div>
        </div>

        <div id="form-step3" style="display: none;">
            <div class="card card-warning boxformproject">
                <div class="card-header">
                    <h3 class="card-title cardHead">Ladda upp projektdokument</h3>
                    <div class="levelform">
                        <ul>
                            <li>1</li>
                            <li>2</li>
                            <li>3</li>
                            <li class="active">4</li>
                        </ul>
                    </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body">

                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 nopadding">

                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 nopadding upload-in-project-info">
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 col-upload-in-project">
                                <span class="fa fa-hand-o-right spannameupload">Brandskyddsbeskrivning</span>
                            </div>
                            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 col-upload-in-project col-upload-inproject">
                                <input name="Brandskyddsbeskrivning[]" multiple type="file" id="Brandskyddsbeskrivning" onchange="fileSelect('Brandskyddsbeskrivning')"
                                       class="btn-upload-in-project">
                                <i class="fa fa-upload icon-upload" aria-hidden="true"></i>
                                <span class="specialupload">Ladda up</span>
                            </div><!--itemupload-->
                            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 col-upload-in-project">
                                <span class="show-name-upload Brandskyddsbeskrivning">Filnamn</span>
                            </div>
                        </div><!--upload-in-project-info-->

                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 nopadding upload-in-project-info">
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 col-upload-in-project">
                                <span class="fa fa-hand-o-right spannameupload">Projektbeskrivning</span>
                            </div>
                            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 col-upload-in-project col-upload-inproject">
                                <input type="file" name="Projektbeskrivning[]" multiple class="btn-upload-in-project" id="Projektbeskrivning" onchange="fileSelect('Projektbeskrivning')">
                                <i class="fa fa-upload icon-upload" aria-hidden="true"></i>
                                <span class="specialupload">Ladda up</span>
                            </div><!--itemupload-->
                            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 col-upload-in-project">
                                <span class="show-name-upload Projektbeskrivning">Filnamn</span>
                            </div>
                        </div><!--upload-in-project-info-->

                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 nopadding upload-in-project-info">
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 col-upload-in-project">
                                <span class="fa fa-hand-o-right spannameupload">Geoteknisk undersökning</span>
                            </div>
                            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 col-upload-in-project col-upload-inproject">
                                <input type="file" name="Geotekniskundersökning[]" multiple
                                       class="btn-upload-in-project" id="Geotekniskundersökning" onchange="fileSelect('Geotekniskundersökning')">
                                <i class="fa fa-upload icon-upload" aria-hidden="true"></i>
                                <span class="specialupload">Ladda up</span>
                            </div><!--itemupload-->
                            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 col-upload-in-project">
                                <span class="show-name-upload Geotekniskundersökning">Filnamn</span>
                            </div>
                        </div><!--upload-in-project-info-->

                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 nopadding upload-in-project-info">
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 col-upload-in-project">
                                <span class="fa fa-hand-o-right spannameupload">Fuktsäkerhetsprojektering</span>
                            </div>
                            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 col-upload-in-project col-upload-inproject">
                                <input type="file" name="Fuktsäkerhetsprojektering[]" multiple
                                       class="btn-upload-in-project" id="Fuktsäkerhetsprojektering" onchange="fileSelect('Fuktsäkerhetsprojektering')">
                                <i class="fa fa-upload icon-upload" aria-hidden="true"></i>
                                <span class="specialupload">Ladda up</span>
                            </div><!--itemupload-->
                            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 col-upload-in-project">
                                <span class="show-name-upload Fuktsäkerhetsprojektering">Filnamn</span>
                            </div>
                        </div><!--upload-in-project-info-->

                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 nopadding upload-in-project-info">
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 col-upload-in-project">
                                <span class="fa fa-hand-o-right spannameupload">Energiberäkning</span>
                            </div>
                            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 col-upload-in-project col-upload-inproject">
                                <input type="file" name="Energiberäkning[]" multiple class="btn-upload-in-project" id="Energiberäkning" onchange="fileSelect('Energiberäkning')">
                                <i class="fa fa-upload icon-upload" aria-hidden="true"></i>
                                <span class="specialupload">Ladda up</span>
                            </div><!--itemupload-->
                            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 col-upload-in-project">
                                <span class="show-name-upload Energiberäkning">Filnamn</span>
                            </div>
                        </div><!--upload-in-project-info-->

                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 nopadding upload-in-project-info">
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 col-upload-in-project">
                                <span class="fa fa-hand-o-right spannameupload">APD plan</span>
                            </div>
                            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 col-upload-in-project col-upload-inproject">
                                <input type="file" name="APDplan[]" multiple class="btn-upload-in-project" id="APDplan" onchange="fileSelect('APDplan')">
                                <i class="fa fa-upload icon-upload" aria-hidden="true"></i>
                                <span class="specialupload">Ladda up</span>
                            </div><!--itemupload-->
                            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 col-upload-in-project">
                                <span class="show-name-upload APDplan">Filnamn</span>
                            </div>
                        </div><!--upload-in-project-info-->

                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 nopadding upload-in-project-info">
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 col-upload-in-project">
                                <span class="fa fa-hand-o-right spannameupload">Sprängplan</span>
                            </div>
                            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 col-upload-in-project col-upload-inproject">
                                <input type="file" name="Sprängplan[]" multiple class="btn-upload-in-project" id="Sprängplan" onchange="fileSelect('Sprängplan')">
                                <i class="fa fa-upload icon-upload" aria-hidden="true"></i>
                                <span class="specialupload">Ladda up</span>
                            </div><!--itemupload-->
                            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 col-upload-in-project">
                                <span class="show-name-upload Sprängplan">Filnamn</span>
                            </div>
                        </div><!--upload-in-project-info-->

                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 nopadding upload-in-project-info">
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 col-upload-in-project">
                                <span class="fa fa-hand-o-right spannameupload">Tillgänglighetsbeskrivning</span>
                            </div>
                            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 col-upload-in-project col-upload-inproject">
                                <input type="file" name="Tillgänglighetsbeskrivning[]" multiple
                                       class="btn-upload-in-project" id="Tillgänglighetsbeskrivning" onchange="fileSelect('Tillgänglighetsbeskrivning')">
                                <i class="fa fa-upload icon-upload" aria-hidden="true"></i>
                                <span class="specialupload">Ladda up</span>
                            </div><!--itemupload-->
                            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 col-upload-in-project">
                                <span class="show-name-upload Tillgänglighetsbeskrivning">Filnamn</span>
                            </div>
                        </div><!--upload-in-project-info-->

                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 nopadding upload-in-project-info">
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 col-upload-in-project">
                                <span
                                        class="fa fa-hand-o-right spannameupload">Konstruktionsdokumentation enl EKS</span>
                            </div>
                            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 col-upload-in-project col-upload-inproject">
                                <input type="file" name="Konstruktionsdokumentation[]" multiple
                                       class="btn-upload-in-project" id="Konstruktionsdokumentation" onchange="fileSelect('Konstruktionsdokumentation')">
                                <i class="fa fa-upload icon-upload" aria-hidden="true"></i>
                                <span class="specialupload">Ladda up</span>
                            </div><!--itemupload-->
                            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 col-upload-in-project">
                                <span class="show-name-upload Konstruktionsdokumentation">Filnamn</span>
                            </div>
                        </div><!--upload-in-project-info-->

                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 nopadding upload-in-project-info">
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 col-upload-in-project">
                                <span class="fa fa-hand-o-right spannameupload">Konstruktionsritningar</span>
                            </div>
                            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 col-upload-in-project col-upload-inproject">
                                <input type="file" name="Konstruktionsritningar[]" multiple
                                       class="btn-upload-in-project" id="Konstruktionsritningar" onchange="fileSelect('Konstruktionsritningar')">
                                <i class="fa fa-upload icon-upload" aria-hidden="true"></i>
                                <span class="specialupload">Ladda up</span>
                            </div><!--itemupload-->
                            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 col-upload-in-project">
                                <span class="show-name-upload Konstruktionsritningar">Filnamn</span>
                            </div>
                        </div><!--upload-in-project-info-->

                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 nopadding upload-in-project-info">
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 col-upload-in-project">
                                <span class="fa fa-hand-o-right spannameupload">VVS ritningar</span>
                            </div>
                            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 col-upload-in-project col-upload-inproject">
                                <input type="file" name="VVSritningar[]" multiple class="btn-upload-in-project" id="VVSritningar" onchange="fileSelect('VVSritningar')">
                                <i class="fa fa-upload icon-upload" aria-hidden="true"></i>
                                <span class="specialupload">Ladda up</span>
                            </div><!--itemupload-->
                            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 col-upload-in-project">
                                <span class="show-name-upload VVSritningar">Filnamn</span>
                            </div>
                        </div><!--upload-in-project-info-->

                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 nopadding upload-in-project-info">
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 col-upload-in-project">
                                <span class="fa fa-hand-o-right spannameupload">VA plan</span>
                            </div>
                            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 col-upload-in-project col-upload-inproject">
                                <input type="file" name="VAplan[]" multiple class="btn-upload-in-project" id="VAplan" onchange="fileSelect('VAplan')">
                                <i class="fa fa-upload icon-upload" aria-hidden="true"></i>
                                <span class="specialupload">Ladda up</span>
                            </div><!--itemupload-->
                            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 col-upload-in-project">
                                <span class="show-name-upload VAplan">Filnamn</span>
                            </div>
                        </div><!--upload-in-project-info-->

                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 nopadding upload-in-project-info">
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 col-upload-in-project">
                                <span class="fa fa-hand-o-right spannameupload">Bullerutredning</span>
                            </div>
                            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 col-upload-in-project col-upload-inproject">
                                <input type="file" name="Bullerutredning[]" multiple class="btn-upload-in-project" id="Bullerutredning" onchange="fileSelect('Bullerutredning')">
                                <i class="fa fa-upload icon-upload" aria-hidden="true"></i>
                                <span class="specialupload">Ladda up</span>
                            </div><!--itemupload-->
                            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 col-upload-in-project">
                                <span class="show-name-upload Bullerutredning">Filnamn</span>
                            </div>
                        </div><!--upload-in-project-info-->

                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 nopadding upload-in-project-info">
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 col-upload-in-project">
                                <span class="fa fa-hand-o-right spannameupload">Dagsljusberäkning</span>
                            </div>
                            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 col-upload-in-project col-upload-inproject">
                                <input type="file" name="Dagsljusberäkning[]" multiple class="btn-upload-in-project" id="Dagsljusberäkning" onchange="fileSelect('Dagsljusberäkning')">
                                <i class="fa fa-upload icon-upload" aria-hidden="true"></i>
                                <span class="specialupload">Ladda up</span>
                            </div><!--itemupload-->
                            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 col-upload-in-project">
                                <span class="show-name-upload Dagsljusberäkning">Filnamn</span>
                            </div>
                        </div><!--upload-in-project-info-->

                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 nopadding upload-in-project-info">
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 col-upload-in-project">
                                <span class="fa fa-hand-o-right spannameupload">Marksaneringsplan</span>
                            </div>
                            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 col-upload-in-project col-upload-inproject">
                                <input type="file" name="Marksaneringsplan[]" multiple class="btn-upload-in-project" id="Marksaneringsplan" onchange="fileSelect('Marksaneringsplan')">
                                <i class="fa fa-upload icon-upload" aria-hidden="true"></i>
                                <span class="specialupload">Ladda up</span>
                            </div><!--itemupload-->
                            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 col-upload-in-project">
                                <span class="show-name-upload Marksaneringsplan">Filnamn</span>
                            </div>
                        </div><!--upload-in-project-info-->

                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 nopadding upload-in-project-info">
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 col-upload-in-project">
                                <span class="fa fa-hand-o-right spannameupload">Dagvattenutredning</span>
                            </div>
                            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 col-upload-in-project col-upload-inproject">
                                <input type="file" name="Dagvattenutredning[]" multiple class="btn-upload-in-project" id="Dagvattenutredning" onchange="fileSelect('Dagvattenutredning')">
                                <i class="fa fa-upload icon-upload" aria-hidden="true"></i>
                                <span class="specialupload">Ladda up</span>
                            </div><!--itemupload-->
                            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 col-upload-in-project">
                                <span class="show-name-upload Dagvattenutredning">Filnamn</span>
                            </div>
                        </div><!--upload-in-project-info-->

                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 nopadding upload-in-project-info">
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 col-upload-in-project">
                                <span class="fa fa-hand-o-right spannameupload">OVK & Luftflödesprotokoll</span>
                            </div>
                            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 col-upload-in-project col-upload-inproject">
                                <input type="file" name="OVK&Luftflödesprotokoll[]" multiple
                                       class="btn-upload-in-project" id="Luftflödesprotokoll" onchange="fileSelect('Luftflödesprotokoll')">
                                <i class="fa fa-upload icon-upload" aria-hidden="true"></i>
                                <span class="specialupload">Ladda up</span>
                            </div><!--itemupload-->
                            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 col-upload-in-project">
                                <span class="show-name-upload Luftflödesprotokoll">Filnamn</span>
                            </div>
                        </div><!--upload-in-project-info-->

                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 nopadding upload-in-project-info">
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 col-upload-in-project">
                                <span class="fa fa-hand-o-right spannameupload">Intyg täthetsprovning</span>
                            </div>
                            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 col-upload-in-project col-upload-inproject">
                                <input type="file" name="Intygtäthetsprovning[]" multiple class="btn-upload-in-project" id="Intygtäthetsprovning" onchange="fileSelect('Intygtäthetsprovning')">
                                <i class="fa fa-upload icon-upload" aria-hidden="true"></i>
                                <span class="specialupload">Ladda up</span>
                            </div><!--itemupload-->
                            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 col-upload-in-project">
                                <span class="show-name-upload Intygtäthetsprovning">Filnamn</span>
                            </div>
                        </div><!--upload-in-project-info-->

                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 nopadding upload-in-project-info">
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 col-upload-in-project">
                                <span class="fa fa-hand-o-right spannameupload">Intyg Miljö & Hälsoskyddsnämnden</span>
                            </div>
                            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 col-upload-in-project col-upload-inproject">
                                <input type="file" name="IntygMiljö&Hälsoskyddsnämnden[]" multiple
                                       class="btn-upload-in-project" id="IntygMiljö&Hälsoskyddsnämnden" onchange="fileSelect('IntygMiljö&Hälsoskyddsnämnden')">
                                <i class="fa fa-upload icon-upload" aria-hidden="true"></i>
                                <span class="specialupload">Ladda up</span>
                            </div><!--itemupload-->
                            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 col-upload-in-project">
                                <span class="show-name-upload IntygMiljö&Hälsoskyddsnämnden">Filnamn</span>
                            </div>
                        </div>
                        <!--upload-in-project-info-->

                        <div id="sectionpic">

                        </div>

                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 nopadding upload-in-project-info">
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 col-upload-in-project">
                                <button type="button" class="form-control btn btn-success BtnPlus" data-toggle="modal" data-target="#addPicModel"><i class="fa fa-plus"></i></button>
                            </div>
                        </div>
                        <!--upload-in-project-info-->

                    </div>


                </div>
                <!-- /.card-body -->
                <div class="btn-in-forminfoproject">
                    <button type="button" class="back3 bg-danger">Tillbaka</button>
                    <button type="submit" class="next3 bg-success">Spara</button>
                </div>
            </div>
        </div>

    </form>

    <div class="modal fade show" id="addPicModel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-modal="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body">
                    <div class="title-modal"><span>namn</span></div>
                    <label>Lägg till fil</label>
                    <input type="text" name="namnFile" id="namnFile" class="form-control" required>
                    <button type="button" id="btn-addPicModel" onclick="addPic()" class="btnmodal btn-add-user">Lägg till</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="locationModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body">
                    <div class="logomodal">
                        <img src="{{ asset('dist/img/favicon.png') }}" alt="">
                    </div>
                    <div class="title-modal"><span>platsval</span></div>

                    <div>
                        <div id="map" style="position:relative; top:0; bottom:0; width:100%;height: 300px;">
                        </div>
                    </div>
                    <div class="col-sm-12 pull-left">
                        <button type="button" class="btnmodal" data-dismiss="modal" aria-label="Close">kommer tillbaka</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('script')
    <script src="{{ asset('js/leaflet.js') }}"></script>
    <script src="{{ asset('js/leaflet-search.js') }}"></script>
    <script src="{{ asset('js/locations.js') }}"></script>

    <script type="text/javascript">



        function SetUser(typeID)
        {
            var PlaceInfoOwner = $('#PlaceInfoOwner');
            PlaceInfoOwner.html("");
            var a= '<div class="row"><div class="col-sm-12">\n' +
                '                        <label>Att välja ägare till byggnaden</label>\n' +
                '                        <select name="userId"class="form-control select2" data-select2-id="1" tabindex="-1" aria-hidden="true">\n' +
                '                            <option value="0">Selected Option</option>\n' +
                '                            @foreach(\App\User::where('is_owner',1)->where('parent_id',auth()->user()->id)->get() as $user)\n' +
                '                                <option value="{{ $user->id }}">{{ $user->name }}</option>\n' +
                '                            @endforeach\n' +
                '                        </select>\n' +
                '                    </div></div>';

            var b = ' <div class="row">\n' +
                '                            <div class="col-sm-6">\n' +
                '                                <!-- text input -->\n' +
                '                                <div class="form-group">\n' +
                '                                    <label>namn</label>\n' +
                '                                    <input type="text" name="name" value="{{ old('name') }}" class="form-control" placeholder="Text ...">\n' +
                '                                </div>\n' +
                '                            </div>\n' +
                '                            <div class="col-sm-6">\n' +
                '                                <!-- text input -->\n' +
                '                                <div class="form-group">\n' +
                '                                    <label>E-post</label>\n' +
                '                                    <input type="text" name="email" value="{{ old('email') }}" class="form-control" placeholder="Text ...">\n' +
                '                                </div>\n' +
                '                            </div>\n' +
                '                        </div>\n' +
                '                        <div class="row">\n' +
                '                            <div class="col-sm-6">\n' +
                '                                <!-- text input -->\n' +
                '                                <div class="form-group">\n' +
                '                                    <label>Telefon</label>\n' +
                '                                    <input type="text" name="tel" value="{{ old('tel') }}" class="form-control" placeholder="Text ...">\n' +
                '                                </div>\n' +
                '                            </div>\n' +
                '                            <div class="col-sm-6">\n' +
                '                                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 col-upload-in-project col-upload-inproject UPloadbtn">\n' +
                '                                <label for="inputName2">Avatar</label>\n' +
                '                                    <input type="file"  name="avatar" class="form-control btn-upload-in-project">\n' +
                '                                    <i class="fa fa-upload icon-upload" style="left: 100px" aria-hidden="true"></i>\n' +
                '                                    <span class="specialupload" style="left: 0">ladda upp</span>\n' +
                '                                </div>\n' +
                '                            </div>\n' +
                '                        </div>'+
                '  <div class="form-group">\n' +
                '    <label for="exampleFormControlTextarea1">Byggplatsensgatuadress</label>\n' +
                '    <textarea name="address" class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>\n' +
                '  </div>'
            if(typeID == 1)
            {
                PlaceInfoOwner.append(a);
                $('.select2').select2({
                    theme: 'bootstrap4'
                });
            }
            else
            {
                PlaceInfoOwner.append(b);
            }

        }

        function SetUserKa(typeID) {
            var PlaceInfoOwner = $('#PlaceInfoKa');
            PlaceInfoOwner.html("");
            var a = '<div class="row"><div class="col-sm-12">\n' +
                '                        <label>Kontrollansvarig Urval</label>\n' +
                '                        <select name="userIdKa"class="form-control select2" data-select2-id="1" tabindex="-1" aria-hidden="true">\n' +
                '                            <option value="0">Selected Option</option>\n' +
                '                            @foreach(\App\User::where('is_supervisingCompany',1)->where('supervisingCompany_parent_owner',auth()->user()->id)->get() as $user)\n' +
                '                                <option value="{{ $user->id }}">{{ $user->name }}</option>\n' +
                '                            @endforeach\n' +
                '                        </select>\n' +
                '                    </div></div>';

            var b = '<div class="row"><div class="col-sm-6">\n' +
                '                                <div class="form-group">\n' +
                '                                    <label>Kontrollansvarig</label>\n' +
                '                                    <input type="text" name="Kontrollansvarig"' +
                '                                           class="form-control" placeholder="Text ...">\n' +
                '                                </div>\n' +
                '                            </div>\n' +
                '                            <div class="col-sm-6">\n' +
                '                                <div class="form-group">\n' +
                '                                    <label>KA Företag</label>\n' +
                '                                    <input type="text" name="KAFöretag"' +
                '                                           class="form-control" placeholder="Text ...">\n' +
                '                                </div>\n' +
                '                            </div>\n' +
                '                            <div class="col-sm-6">\n' +
                '                                <div class="form-group">\n' +
                '                                    <label>KA Telefon</label>\n' +
                '                                    <input type="text" name="KATelefon"' +
                '                                           class="form-control" placeholder="Text ...">\n' +
                '                                </div>\n' +
                '                            </div>\n' +
                '                            <div class="col-sm-6">\n' +
                '                                <div class="form-group">\n' +
                '                                    <label>KA Mail</label>\n' +
                '                                    <input type="text" name="email" class="form-control"' +
                '                                           placeholder="Text ...">\n' +
                '                                </div>\n' +
                '                            </div>\n' +
                '                            <div class="col-sm-12">\n' +
                '                                <div class="form-group">\n' +
                '                                    <label>KA Cert Nr</label>\n' +
                '                                    <input type="text" name="KACertNr"' +
                '                                           class="form-control" placeholder="Text ...">\n' +
                '                                </div>\n' +
                '                            </div></div>' +
                '  <div class="form-group">\n' +
                '    <label for="exampleFormControlTextarea1">KA Adress</label>\n' +
                '    <textarea name="KAAdress" class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>\n' +
                '  </div>'
            if (typeID == 1) {
                PlaceInfoOwner.append(a);
                $('.select2').select2({
                    theme: 'bootstrap4'
                });
            } else {
                PlaceInfoOwner.append(b);
            }

        }

        function SetUserPlat(typeID) {
            var PlaceInfoPlat = $('#PlaceInfoPlat');
            PlaceInfoPlat.html("");
            var b = '<div class="row"><div class="col-sm-6">\n' +
                '                                <div class="form-group">\n' +
                '                                    <label>namn</label>\n' +
                '                                    <input type="text" name="platName"' +
                '                                           class="form-control" placeholder="Text ...">\n' +
                '                                </div>\n' +
                '                            </div>\n' +
                '                            <div class="col-sm-6">\n' +
                '                                <div class="form-group">\n' +
                '                                    <label>Telefon</label>\n' +
                '                                    <input type="text" name="platPhone"' +
                '                                           class="form-control" placeholder="Text ...">\n' +
                '                                </div>\n' +
                '                            </div>\n' +
                '                            <div class="col-sm-12">\n' +
                '                                <div class="form-group">\n' +
                '                                    <label>EMail</label>\n' +
                '                                    <input type="text" name="platEmail" class="form-control"' +
                '                                           placeholder="Text ...">\n' +
                '                                </div>\n' +
                '                            </div></div>\n';
            if (typeID == 2) {
                PlaceInfoPlat.append(b);
            } else {
                PlaceInfoPlat.append("");
            }

        }


        function addPic() {
            let name = $('#namnFile').val();
            if(name !='')
            {
                let pic = '<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 nopadding upload-in-project-info">\n' +
                    '                                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 col-upload-in-project">\n' +
                    '                                    <span class="fa fa-hand-o-right spannameupload">'+name+'</span>\n' +
                    '                                </div>\n' +
                    '                                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 col-upload-in-project col-upload-inproject">\n' +
                    '                                    <input type="file" name="'+name+'[]" multiple\n' +
                    '                                           class="btn-upload-in-project" id="'+name+'" onchange="fileSelect(\''+name+'\')">\n' +
                    '                                    <i class="fa fa-upload icon-upload" aria-hidden="true"></i>\n' +
                    '                                    <span class="specialupload">Ladda up</span>\n' +
                    '                                </div><!--itemupload-->\n' +
                    '                                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 col-upload-in-project">\n' +
                    '                                    <span class="show-name-upload '+name+'">Filnamn</span>\n' +
                    '                                </div>\n' +
                    '                            </div>'

                $('#sectionpic').append(pic)
            }
            else
            {
                Swal.fire({
                    icon: 'fel',
                    title: 'Hoppsan...',
                    text: 'Ange namnet!',
                })
            }

        }

        $(document).ready(function () {
            $('.select2').select2({
                theme: 'bootstrap4'
            });

            $('#byggnad').select2({
                theme: 'bootstrap4'
            });

            $('#Värmesystem').select2({
                theme: 'bootstrap4'
            });

            $('#Teknisktsamråd').daterangepicker({
                singleDatePicker: true,
                showDropdowns: true
            });
        });



        $(".next").click(function() {
            $("#form-step").hide("fast");
            $("#form-step1").show("slow");
        });

        $(".next1").click(function() {
            $("#form-step1").hide("fast");
            $("#form-step2").show("slow");
        });

        $(".next2").click(function() {
            $("#form-step2").hide("fast");
            $("#form-step3").show("slow");
        });

        $(".back1").click(function() {
            $("#form-step1").hide("fast");
            $("#form-step").show("slow");
        });

        $(".back2").click(function() {
            $("#form-step2").hide("fast");
            $("#form-step1").show("slow");
        });

        $(".back3").click(function() {
            $("#form-step3").hide("fast");
            $("#form-step2").show("slow");
        });

        function fileSelect(id){

            var fileInput = document.getElementById(id);

            var files = fileInput.files;
            var file = [];
            $('.' + id).text("");
            for (var i = 0; i < files.length; i++) {
                file = files[i];
                if(i == 0)
                {
                    $('.' + id).text(file['name']);
                }
                else
                {
                    $('.' + id).text($('.' + id).text()+ '-' +file['name']);
                }
            }
        }

    </script>
@endsection
