@extends('Admin.master')
@section('content-title', 'Projekt')
@section('title', 'Template Type')
@section('content')
    <div class="row">
        <div class="col-12 col-sm-12 col-md-12 box-modelproject">
            <div class="col-7 col-sm-7 col-md-6 mr-auto">
                @foreach(\App\Definitions\TemplateCategory::all() as $templateCategory)
                <a href="{{ route('project.templatecategory',['id' => $templateCategory]) }}"><div class="col-12 col-sm-12 col-md-12">
                        <div class="info-box info-box2">
                            <span class="info-box-icon bg-info elevation-1"><img src="{{ asset($templateCategory->logo) }}" alt=""></span>
                            <div class="info-box-content">
                                <span class="info-box-text spanmodel">{{ $templateCategory->title }}</span>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                    </div></a>
                @endforeach
            </div>
            <div class="col-5 col-sm-5 col-md-6 nopadding r-mr-auto r-mr-auto2">
                <img src="{{ asset('dist/img/villa.jpg') }}" alt="">
            </div><!--r-mr-auto-->
        </div>
    </div>
    @endsection
@section('script')
    @endsection
