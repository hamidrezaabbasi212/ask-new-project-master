@extends('Admin.master')
@section('content-title', 'Egenkontroll')
@section('title', 'Egenkontroll')
@section('content')
    <style media="print" type="text/css">
        .no-print {display: none;}
        footer{display: none;}
        tr.orange td span{color:#dc9401;}
    </style>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 box-organization">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 top-tbl-EK tbl_new">
                <div class="table-responsive">
                    <table class="table tbl-top-EK" id="printpage">
                        <thead>
                        <tr class="nopadding">
                            <td class="nopadding" colspan="4">
                                <div class="top_print">
                                    <input class="btnPrint no-print" type="button" value="Print" onclick="javascript:window.print()">
                                </div><!--top_print-->
                                <div class="head_print">
                                    <div class="l_head_print">
                                        <h2>Projektnamn :  <strong>{{ $project['Projektnamn'] }}</strong></h2>
                                    </div><!--l_head_print-->
                                    <div class="r_head_print">
                                        <img src="http://www.askprojekt.se/dist/img/logo-ask.jpg" alt="">
                                    </div><!--r_head_print-->
                                </div><!--head_print-->
                            </td>
                        </tr>
                        </thead>
                        <tbody>
                        <tr class="empty_tr">
                            <td colspan="4"></td>
                        </tr>
                        <tr class="center_tr orange">
                            <td colspan="4"><span>Egenkontroll</span></td>
                        </tr>
                        <tr class="center_tr border_orange">
                            <td>PBL : {{ $controllPlan['PBLKategori']  ? $controllPlan['PBLKategori'] : '-'}}</td>
                            <td>Ansvarig : {{ $controllPlan['AnsvarigPart'] ? $controllPlan['AnsvarigPart'] : '-' }}</td>
                            <td>Skede : {{ $controllPlan['Kontrollskede'] ? $controllPlan['Kontrollskede'] : '-' }}</td>
                            <td>Risk : {{ $controllPlan['Risk'] ? $controllPlan['Risk'] : '-' }}</td>
                        </tr>
                        <tr class="center_tr border_orange tr_color">
                            <td>Intyg : {{ $controllPlan['ProduceratIntyg'] ? $controllPlan['ProduceratIntyg'] : '-' }}</td>
                            <td>Verifikation : {{ $controllPlan['Verifieringsmetod'] ? $controllPlan['Verifieringsmetod'] : '-' }}</td>
                            <td>Platsbesök : {{ $controllPlan['KraverPlatsbesok'] ? $controllPlan['KraverPlatsbesok'] : '-' }}</td>
                            <td>Funktionskrav : {{ array_key_exists('Funktionskrav',$controllPlan) ? $controllPlan['Funktionskrav'] : '-' }}</td>
                        </tr>
                        <tr class="empty_tr">
                            <td colspan="4"></td>
                        </tr>
                        @php
                            $count = $advance_law->count();
                        @endphp
                        @if($count > 0)
                        <tr class="center_tr orange">
                            <td colspan="4"><span>Verktyg</span></td>
                        </tr>

                        <tr class="center_tr border_orange">
                            @foreach($advance_law as $key => $row)

                                @if($key < 3)
                                    <td colspan="{{ $key == 1 || $key == 2 ? '' : '2'}}">{{ $row->templateInformation }}</td>
                                @endif
                            @endforeach
                        </tr>
                        <tr class="center_tr border_orange">
                            @foreach($advance_law as $key => $row)
                                @if($key >= 3)
                                 <td colspan="{{ $key == 4 || $key == 5 ? '' : '2'}}">{{ $row->templateInformation }}</td>
                                @endif
                            @endforeach
                        </tr>
                        @endif
                        <tr class="empty_tr">
                            <td colspan="4"></td>
                        </tr>
                        <tr>
                            <td class="gray_td" colspan="4">
                                <img src="{{ asset($controllPlan['image']) }}" alt="" >
                                <div>
                                    {!! $controllPlan['LawText'] !!}
                                </div>
                            </td>
                        {{--<td colspan="2"></td>--}}
                        </tr>
                        <tr class="empty_tr">
                            <td colspan="4"></td>
                        </tr>
                        <tr class="border_gray">
                            <td class="has_r_border"><i class="select_step {{ $controllPlan['status'] == 1 ? 'active' : '' }}"></i><strong>Tilldelad : </strong><span>{{ $data['tdate'] }}</span></td>
                            <td><i class="select_step"></i><strong>Funktion uppfylld : </strong><span>{{ $data['fdate'] }}</span></td>
                            <td colspan="2"><span>{{ $controllPlan['user'] !=null ? $controllPlan['user'] : 'Datum-Signerat av' }}</span></td>
                        </tr>
                        <tr class="border_gray">
                            <td class="has_r_border"><i class="select_step {{ $controllPlan['status'] == 2 ? 'active' : '' }}"></i><strong>Mottaget : </strong><span>{{ $data['mdate'] }}</span></td>
                            <td><i class="select_step"></i><strong>Egenkontroll Underkänt : </strong><span>{{ $data['edate'] }}</span></td>
                            <td colspan="2"><span>{{ $controllPlan['user'] !=null ? $controllPlan['user'] : 'Datum-Signerat av' }}</span></td>
                        </tr>
                        <tr class="border_gray">
                            <td class="has_r_border"><i class="select_step {{ $controllPlan['status'] == 3 ? 'active' : '' }}"></i><strong>Godkänt innehåll : </strong><span>{{ $data['gdate'] }}</span></td>
                            <td><i class="select_step"></i><strong>KA Utlåtande : </strong><span>{{ $data['kdate'] }}</span></td>
                            <td colspan="2"><span>{{ $controllPlan['user'] !=null ? $controllPlan['user'] : 'Datum-Signerat av' }}</span></td>
                        </tr>
                        <tr class="empty_tr">
                            <td colspan="4"></td>
                        </tr>
                        </tbody>
                        <tfoot>
                        <tr class="nopadding">
                            <td class="nopadding" colspan="4">
                                <div class="guid_print">
                                    <strong>GUID : </strong><span>{{ $controllPlan['guid'] }}</span>
                                </div><!--guid_print-->
                                <div class="footer_print">
                                    <a href="">www.askprojekt.se</a>
                                </div><!--footer_print-->
                            </td>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div><!--top-tbl-EK-->
        </div>
    </div>
    <script>
        function printdiv()
        {
            var printContents = document.getElementById('printpage').innerHTML;
            var originalContents = document.body.innerHTML;
            document.body.innerHTML = printContents;
            window.print();
            document.body.innerHTML = originalContents;
        }
    </script>
@endsection
