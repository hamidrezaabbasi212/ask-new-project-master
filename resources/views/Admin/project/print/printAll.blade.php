<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Print</title>
    <link rel="stylesheet" href="{{ asset('dist/css/adminlte.min.css') }}">
    <link href="{{ asset('dist/css/style.css') }}" rel="stylesheet">
    <style>
        body{overflow-x: hidden;}
        .row{display: unset;}
        .box-contentForm{padding: 30px;}
    </style>
    <style media="print" type="text/css">
        .no-print {display: none;}
        .box-contentForm{padding:0;}
        table.tbl-print-form tr.tblhead{position: relative;}
        table.tbl-print-form tr.tblhead td{color:#fff;}
        table.tbl-print-form tr.tblhead td:before{content:'';background:#dc9401;width: 100%;height:100%;position: absolute;top:0;left:0;}
        table.tbl-print-form tr.tblhead td span{position: relative;}
        .info-project-col {margin-bottom:0px;}
        .box-contentForm::after{display: none;}
        @page {
            size: A4;
        }
        html, body {
            width: 210mm;
            height: 282mm;
            font-size: 11px;
            background: #FFF;
            overflow:visible;
        }
    </style>
</head>
<body>
<div class="container-fluid contentForm">
    <div class="box-contentForm">
        <header class="row">
{{--            <div class="header-form">--}}
{{--                <img src="http://www.askprojekt.se/dist/img/favicon.png" alt="">--}}
{{--                <strong>ProjektAsk</strong>--}}
{{--            </div>--}}
            <div class="design-print">
                <div>
                    <img src="{{ asset('dist/img/favicon.png') }}" alt="">
                </div>
            </div><!--design-print-->
            <div class="box-title-print">
                <div class="title-print">
                    <h1>Villa / småhus</h1>
                </div><!--title-print-->
                <div class="title-Kontrollplan">
                    <strong>Kontrollplan</strong>
                    <input class="no-print btnPrint" type="button" value="Print" onclick="javascript:window.print()">
                </div><!--title-Kontrollplan-->
            </div>
        </header>
        <main class="row">
            <div class="sticky-table sticky-rtl-cells">
                <table class="col-xs-12 col-sm-12 col-md-12 col-lg-12 table tbl-print-form">
                    <tbody>
                    <tr class="tblhead sticky-header">
                        <td colspan="2"><span>BSAB/AMA</span></td>
                        <td colspan="2"><span>PBL</span></td>
                        <td><span>Ansvarig</span></td>
                        <td><span>Skede</span></td>
                        <td><span>Risk</span></td>
                        <td><span>Intyg</span></td>
                        <td><span>Verifikation</span></td>
                        <td colspan="2"><span>Platsbesök</span></td>
                        <td colspan="2"><span>Funktionskrav</span></td>
                        <td><span>Användare</span></td>
                    </tr>
                    @foreach($designsTemplate as $designTemplate)
                        <tr class="tblBorder">
                            <td colspan="2">{{ \Illuminate\Support\Str::limit($designTemplate['BSABAMA'],40) }}</td>
                            <td colspan="2">{{ \Illuminate\Support\Str::limit($designTemplate['PBLKategori'],40) }}</td>
                            <td>{{ \Illuminate\Support\Str::limit($designTemplate['AnsvarigPart'],40) }}</td>
                            <td>{{ \Illuminate\Support\Str::limit($designTemplate['Kontrollskede'],20) }}</td>
                            <td>{{ \Illuminate\Support\Str::limit($designTemplate['Risk'],20) }}</td>
                            <td>{{ \Illuminate\Support\Str::limit($designTemplate['ProduceratIntyg'],20) }}</td>
                            <td>{{ \Illuminate\Support\Str::limit($designTemplate['Verifieringsmetod'],20) }}</td>
                            <td colspan="2">{{ \Illuminate\Support\Str::limit($designTemplate['KraverPlatsbesok'],25) }}</td>
                            <td colspan="2">{!! \Illuminate\Support\Str::limit($designTemplate['LawText'],30) !!}</td>
                            @php
                                $image = '';
                                 if($designTemplate['assign_user'] !='')
                                 {
                                    $avatar = \App\OrganizationUsers::whereId($designTemplate['assign_user'])->pluck('avatar')->first();
                                    $image = '<img alt="Avatar" class="img-circle table-avatar" width="30" height="30" src="'.$avatar.'">';
                                 }
                                 else
                                 {
                                      $image = 'No Assign User';
                                 }
                            @endphp
                            <td colspan="2">{!! $image !!}</td>
                        </tr>
                    @endforeach
                    <tr>
                        <td colspan="14">
                            <div class="in-box-question">
                                @php
                                    unset($projectInfo['Projektnamn'],$projectInfo['BNÄrendenr'],$projectInfo['Entreprenadform'],$projectInfo['Byggherre'],$projectInfo['Strukturbeskrivning'],
                                    $projectInfo['Byggplatsensgatuadress'],$projectInfo['Byggnadsbeskrivning'],$projectInfo['Bygglov'],$projectInfo['Teknisktsamråd'],$projectInfo['Startbesked'],
                                    $projectInfo['Slutsamråd'],$projectInfo['Interimistisktslutbesked'],$projectInfo['Slutbesked'],$projectInfo['Projektstartslut'],
                                    $projectInfo['Kontrollansvarig'],$projectInfo['KAFöretag'],$projectInfo['KAAdress'],$projectInfo['KATelefon'],$projectInfo['KAMail'],$projectInfo['KACertNr'],$projectInfo['project_id']);

                                @endphp

                                <div class="box-question">
                                    <p class="question">AttachmentFile</p>
                                    <p class="answer">@foreach($projectInfo as $row=> $info)
                                            @if ($info !=null)
                                                @if($info === end($projectInfo))
                                                    @php echo $row;   @endphp
                                                @else
                                                    @php echo $row . ',' @endphp
                                                @endif
                                            @endif
                                            {{--TODO--}}
                                        @endforeach</p>
                                </div>


                                {{--                                <div class="box-question">--}}
                                {{--                                    <p class="question">TEKNINSKT SAMRÅD, VAD SKALL MAN SKRIVA HÄR?</p>--}}
                                {{--                                    <p class="answer">Avdelning A – Övergripande bestämmelser-Kontroll   -Mottagningskontroll av material och produkter   -26 §--Byggherren måste förvissa sig om att material och byggprodukter har sådana egenskaper att materialen   och produkterna korrekt använda i byggnadsverket gör att detta kan uppfylla egenskapskraven i denna författning   och i Boverkets byggregler (BFS 2011:6).   Med mottagningskontroll avses i denna författning byggherrens kontroll av att material och produkter har   förutsatta egenskaper när de tas emot på byggplatsen.   Har produkterna bedömda egenskaper enligt 18 § kan mottagningskontrollen inskränkas till identifiering,   kontroll av märkning och granskning av produktdeklarationen för att säkerställa att varorna har förutsatta   egenskaper.   Om byggprodukternas egenskaper inte är bedömda i den mening som avses i 18 § fordras verifiering genom   provning eller annan inom europeiska unionen vedertagen metod så att egenskaperna är kända och kan   värderas avseende lämplighet. (BFS 2015:6). ALLMÄNT RÅD   Byggprodukter vars egenskaper bedömts enligt alternativen a, c eller d i 18 § innebär inte att produkten   bedömts mot svenska krav på byggnadsverk i denna författning eller i Boverkets byggregler (BFS 2011:6).   Sådana bedömningar innebär endast att byggherren ska ha tilltro till den produkt- eller prestandadeklaration   av produktens egenskaper som medföljer. Med ledning av produkt- eller prestandadeklarationen kan   byggherren avgöra om byggprodukten är lämplig för aktuell användning.   För byggprodukter med bedömda egenskaper behöver byggherren inte göra någon egen provning av   dessa egenskaper. (BFS 2015:6). Avdelning A – Övergripande bestämmelser-Kontroll   -Mottagningskontroll av material och produkter   -26 §--Byggherren måste förvissa sig om att material och byggprodukter har sådana egenskaper att materialen   och produkterna korrekt använda i byggnadsverket gör att detta kan uppfylla egenskapskraven i denna författning   och i Boverkets byggregler (BFS 2011:6).   Med mottagningskontroll avses i denna författning byggherrens kontroll av att material och produkter har   förutsatta egenskaper när de tas emot på byggplatsen.   Har produkterna bedömda egenskaper enligt 18 § kan mottagningskontrollen inskränkas till identifiering,   kontroll av märkning och granskning av produktdeklarationen för att säkerställa att varorna har förutsatta   egenskaper.   Om byggprodukternas egenskaper inte är bedömda i den mening som avses i 18 § fordras verifiering genom   provning eller annan inom europeiska unionen vedertagen metod så att egenskaperna är kända och kan   värderas avseende lämplighet. (BFS 2015:6). ALLMÄNT RÅD   Byggprodukter vars egenskaper bedömts enligt alternativen a, c eller d i 18 § innebär inte att produkten   bedömts mot svenska krav på byggnadsverk i denna författning eller i Boverkets byggregler (BFS 2011:6).   Sådana bedömningar innebär endast att byggherren ska ha tilltro till den produkt- eller prestandadeklaration   av produktens egenskaper som medföljer. Med ledning av produkt- eller prestandadeklarationen kan   byggherren avgöra om byggprodukten är lämplig för aktuell användning.   För byggprodukter med bedömda egenskaper behöver byggherren inte göra någon egen provning av   dessa egenskaper. (BFS 2015:6). </p>--}}
                                {{--                                </div>--}}

                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>

        </main>
        <footer class="row">
        </footer>

        <hr>

        @foreach($designsTemplate as $designTemplate)
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 box-organization">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 top-tbl-EK">
                    <div class="table-responsive">
                        <table class="table tbl-top-EK" id="printpage">
                            <thead>
                            <tr>
                                <td style="width:6%;padding-right:0;padding-left:0;">Datum :</td>
                                <td style="width:20%;">{{ \Carbon\Carbon::now() }}</td>
                                <td style="width:4%;padding-right:0;padding-left:0;">GUID:{{ $designTemplate['guid'] }}</td>
                                <td style="width:20%;"></td>
                                <td style="width:35%;">{{ $projectInformations['Projektnamn'] }}</td>
                            </tr>
                            </thead>
                            <tbody>
                            <tr class="color-tr-print">
                                <td colspan="6">{{ $designTemplate['AnsvarigPart'] }}</td>
                            </tr>
                            <tr class="background-garay-print">
                                <td colspan="2">PBL Kategori : {{ $designTemplate['PBLKategori'] }}</td>
                                <td colspan="2">Producerat intyg : {{ $designTemplate['ProduceratIntyg'] }}</td>
                                <td colspan="2">Verifieringsmetod : {{ $designTemplate['Verifieringsmetod'] }}</td>
                            </tr>
                            <tr class="background-garay-print">
                                <td colspan="2">Kontrollskede : {{ $designTemplate['Kontrollskede'] }}</td>
                                <td colspan="2">Risk : {{ $designTemplate['Risk'] }}</td>
                                <td colspan="2">Kraver platsbesok : {{ $designTemplate['KraverPlatsbesok'] }}</td>
                            </tr>
                            <tr>
                                <td colspan="6">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 print-project">
                                        <div class="name-print-project">
                                            <h2>{{ $designTemplate['BSABAMA'] }}</h2>
                                        </div><!--name-print-project-->
                                        <div class="txt-print-project">
                                            <p>{!! $designTemplate['LawText'] !!}</p>
                                        </div><!--txt-print-project-->
                                    </div><!--print-project-->
                                </td>
                            </tr>
                            </tbody>
                            <tfoot>
                            <tr>
                                <td colspan="6">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bottom-print-project">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding" style="margin-bottom:45px;">
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 col-btmp-project">
                                                <input class="radiostyles" type="checkbox" name="" value="">FUNKTION UPPFYLLD
                                            </div><!--col-btmp-project-->
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 col-btmp-project">
                                                <input class="radiostyles" type="checkbox" name="" value="">FUNKTION UNDERKÄND
                                            </div><!--col-btmp-project-->
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col-btmp-project">
                                                <label>Funktionskrav kontrollerat verifieras/Ansvarig part</label>
                                                <input type="text" class="fullinput" placeholder="">
                                            </div><!--col-btmp-project-->
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col-btmp-project">
                                                <label>Noterad avvikelse:</label>
                                                <textarea class="fullinput textarea-print" placeholder=""></textarea>
                                            </div><!--col-btmp-project-->
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 col-btmp-project">
                                                <label>Utfall vidimeras, valideras och fastställs/(BH):</label>
                                                <input type="text" class="fullinput" placeholder="">
                                            </div><!--col-btmp-project-->
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 col-btmp-project">
                                                <label>DATUM: </label>
                                                <input type="text" class="fullinput" placeholder="">
                                            </div><!--col-btmp-project-->
                                        </div>
                                    </div><!--bottom-print-project-->
                                </td>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div><!--top-tbl-EK-->
            </div>
        </div>
        @endforeach
            <div class="box_printall no-print">
                <input class="btnPrint" type="button" value="Print" onclick="javascript:window.print()">
            </div>
        <hr>
    </div>
</div>
</body>
</html>

