@extends('Admin.master')
@section('content-title', 'Egenkontroll')
@section('title', 'Egenkontroll')
@section('content')
    <style media="print" type="text/css">
        .no-print {display: none;}
        footer{display: none;}
        tr.orange td span{color:#dc9401;}
    </style>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 box-organization">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 top-tbl-EK tbl_new">
                <div class="table-responsive">
                    <table class="table tbl-top-EK" id="printpage">
                        <thead>
                        <tr class="nopadding">
                            <td class="nopadding" colspan="4">
                                <div class="top_print">
                                    <input class="btnPrint no-print" type="button" value="Print" onclick="javascript:window.print()">
                                </div><!--top_print-->
                                <div class="head_print">
                                    <div class="l_head_print">
                                        <h2>Projektnamn :  <strong>{{ $project['Projektnamn'] }}</strong></h2>
                                    </div><!--l_head_print-->
                                    <div class="r_head_print">
                                        <img src="http://www.askprojekt.se/dist/img/logo-ask.jpg" alt="">
                                    </div><!--r_head_print-->
                                </div><!--head_print-->
                            </td>
                        </tr>
                        </thead>
                        <tbody>
                        <tr class="empty_tr">
                            <td colspan="4"></td>
                        </tr>
                        <tr class="empty_tr">
                            <td colspan="4"></td>
                        </tr>
                        @php
                            $count = $advance_law->count();
                        @endphp
                        @if($count > 0)
                            <tr class="center_tr orange">
                                <td colspan="4"><span>Verktyg</span></td>
                            </tr>

                            <tr class="center_tr border_orange">
                                @foreach($advance_law as $key => $row)

                                    @if($key < 3)
                                        <td colspan="{{ $key == 1 || $key == 2 ? '' : '2'}}">{{ $row->templateInformation }}</td>
                                    @endif
                                @endforeach
                            </tr>
                            <tr class="center_tr border_orange">
                                @foreach($advance_law as $key => $row)
                                    @if($key >= 3)
                                        <td colspan="{{ $key == 4 || $key == 5 ? '' : '2'}}">{{ $row->templateInformation }}</td>
                                    @endif
                                @endforeach
                            </tr>
                        @endif
                        <tr class="empty_tr">
                            <td colspan="4"></td>
                        </tr>
                        <tr class="empty_tr">
                            <td colspan="4"></td>
                        </tr>
                        <tr class="empty_tr">
                            <td colspan="4"></td>
                        </tr>
                        </tbody>
                        <tfoot>
                        <tr class="nopadding">
                            <td class="nopadding" colspan="4">
                                <div class="guid_print">
                                    <strong>GUID : </strong><span>{{ $controllPlan['guid'] }}</span>
                                </div><!--guid_print-->
                                <div class="footer_print">
                                    <a href="">www.askprojekt.se</a>
                                </div><!--footer_print-->
                            </td>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div><!--top-tbl-EK-->
        </div>
    </div>
    <script>
        function printdiv()
        {
            var printContents = document.getElementById('printpage').innerHTML;
            var originalContents = document.body.innerHTML;
            document.body.innerHTML = printContents;
            window.print();
            document.body.innerHTML = originalContents;
        }
    </script>
@endsection
