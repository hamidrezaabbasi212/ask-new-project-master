<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Print</title>
    <link rel="stylesheet" href="{{ asset('dist/css/adminlte.min.css') }}">
    <link href="{{ asset('dist/css/style.css') }}" rel="stylesheet">
    <style>
        body{overflow-x: hidden;}
        .row{display: unset;}
    </style>
    <style media="print" type="text/css">
        .no-print {display: none;}
        table.tbl-print-form tr.tblhead{position: relative;}
        table.tbl-print-form tr.tblhead td{color:#fff;}
        table.tbl-print-form tr.tblhead td:before{content:'';background:#405370;width: 100%;height:100%;position: absolute;top:0;left:0;}
        table.tbl-print-form tr.tblhead td span{position: relative;}
        .info-project-col {margin-bottom:0px;}
        .box-contentForm::after{display: none;}
        tr.trcolor td:before{content:'';width: 100%;height:100%;position: absolute;top:0;left:0;}
        tr.trcolor td span{position: relative;}
        @page {
            size: A4;
        }
        html, body {
            width: 210mm;
            height: 282mm;
            font-size: 11px;
            background: #FFF;
            overflow:visible;
        }
    </style>
</head>
<body>
<div class="container-fluid contentForm">
    <div class="box-contentForm">
        <header class="row"></header>
        <main class="row">
            <div class="sticky-table sticky-rtl-cells">
            <table class="col-xs-12 col-sm-12 col-md-12 col-lg-12 table tbl-print-form">
                <thead>
                <tr><td colspan="15">
                <div class="design-print">
                    <div>
                        <img src="{{ asset('dist/img/favicon.png') }}" alt="">
                    </div>
                </div><!--design-print-->
                <div class="box-title-print">
                    <div class="title-print">
                        <h1>Villa / småhus</h1>
                    </div><!--title-print-->
                    <div class="title-Kontrollplan">
                        <strong>Kontrollplan</strong>
                        <input class="no-print btnPrint" type="button" value="Print" onclick="javascript:window.print()">
                    </div><!--title-Kontrollplan-->
                </div>
                    </td></tr>
                </thead>
                    <tbody>
                    <tr>
                        <td colspan="5">
                            <div class="info-project-col info-project-col1">
                                <div><span class="t-col">Projektnamn : </span><span class="d-col">{{ $projectInformations['Projektnamn'] }}</span></div>
                                <div><span class="t-col">Fastighetsadress : </span><span class="d-col">{{ isset($projectInfo['Fastighetsadress']) ? $projectInfo['Fastighetsadress'] : '' }}</span></div>
                                <div><span class="t-col">Fastighetsbeteckning : </span><span class="d-col">{{ isset($projectInfo['Fastighetsbeteckning']) ? $projectInfo['Fastighetsbeteckning'] : '' }}</span></div>
                                <div><span class="t-col">BN-Diarienummer : </span><span class="d-col">{{ isset($projectInfo['BNDiarienummer']) ? $projectInfo['BNDiarienummer'] : '' }}</span></div>
                                <div><span class="t-col">Byggherre : </span><span class="d-col">{{ isset($projectInfo['Byggherre']) ? $projectInfo['Byggherre'] : '' }}</span></div>
                                <div><span class="t-col">Bygglov beviljat : </span><span class="d-col">{{ isset($projectInfo['Bygglovbeviljat']) ? $projectInfo['Bygglovbeviljat'] : '' }}</span></div>
                                <div><span class="t-col">Startbesked : </span><span class="d-col">{{ isset($projectInfo['Startbesked']) ? $projectInfo['Startbesked'] : '' }}</span></div>
                                <div><span class="t-col">Slutsamråd : </span><span class="d-col">{{ isset($projectInfo['Slutsamråd']) ? $projectInfo['Slutsamråd'] : '' }}</span></div>
                            </div>
                        </td>
                        <td colspan="5">
                            <div class="info-project-col">
                                <div><span class="t-col">Slutbesked : </span><span class="d-col">{{ isset($projectInfo['Slutbesked']) ? $projectInfo['Slutbesked'] : '' }}</span></div>
                                <div><span class="t-col">Interimistiskslutbesked : </span><span class="d-col">{{ isset($projectInfo['Interimistiskslutbesked'] ) ? $projectInfo['Interimistiskslutbesked']  : ''}}</span></div>
                                <div><span class="t-col">Hustyp : </span><span class="d-col">{{ isset($projectInfo['Hustyp']) ? $projectInfo['Hustyp'] : '' }}</span></div>
                                <div><span class="t-col">Grundkonstruktion : </span><span class="d-col">{{ isset($projectInfo['Grundkonstruktion']) ? : '' }}</span></div>
                                <div><span class="t-col">Värmesystem : </span><span class="d-col">{{ isset($projectInfo['Värmesystem']) ? : '' }}</span></div>
                                <div><span class="t-col">Ventilationssystem : </span><span class="d-col">{{ isset($projectInfo['Ventilationssystem']) ? $projectInfo['Ventilationssystem'] : '' }}</span></div>
                                <div><span class="t-col">Tekniskt samråddatum : </span><span class="d-col"{{ isset($projectInfo['Teknisktsamråddatum']) ? $projectInfo['Teknisktsamråddatum'] : ''}}</span></div>
                                <div><span class="t-col">Entreprenadform : </span><span class="d-col">{{ isset($projectInfo['Entreprenadform']) ? $projectInfo['Entreprenadform'] : '' }}</span></div>
                            </div>
                        </td>
                        <td colspan="5">
                            <div class="info-project-col">
                                <div><span class="t-col">Övrig information :  </span><span class="d-col">{{ isset($projectInfo['Övriginformation']) ? $projectInfo['Övriginformation'] : '' }}</span></div>
                                <div><span class="t-col">Kontrollansvarig  : </span><span class="d-col">{{ isset($kaInforation->name) ? $kaInforation->name : '' }}</span></div>
                                <div><span class="t-col">KA Företag : </span><span class="d-col">{{ isset($kaInforation->companyName) ? $kaInforation->companyName : '' }}</span></div>
                                <div><span class="t-col">KA Telefon : </span><span class="d-col">{{ isset($kaInforation->phone) ? : '' }}</span></div>
                                <div><span class="t-col">KA Mail : </span><span class="d-col">{{ isset($kaInforation->email) ? $kaInforation->email : '' }}</span></div>
                                <div><span class="t-col">KA Cert Nr : </span><span class="d-col">{{ isset($kaInforation->supervisingCompanyCertNr) ? $kaInforation->supervisingCompanyCertNr : '' }}</span></div>
                                <div><span class="t-col">KA Adress : </span><span class="d-col">{{ isset($kaInforation->supervisingCompanyAddress) ? $kaInforation->supervisingCompanyAddress : '' }}</span></div>
                            </div>
                        </td>
                    </tr>
                    <tr class="tblhead sticky-header">
                        <td colspan="2"><span>BSAB/AMA</span></td>
                        <td colspan="2"><span>PBL</span></td>
                        <td><span>Ansvarig</span></td>
                        <td><span>Skede</span></td>
                        <td><span>Risk</span></td>
                        <td><span>Intyg</span></td>
                        <td><span>Verifikation</span></td>
                        <td colspan="2"><span>Platsbesök</span></td>
                        <td colspan="2"><span>Funktionskrav</span></td>
                        <td><span>Användare</span></td>
                    </tr>
                    @foreach($designsTemplate as $designTemplate)
                    <tr class="tblBorder trcolor">
                        <td colspan="2"><span>{{ \Illuminate\Support\Str::limit($designTemplate['BSABAMA'],40) }}</span></td>
                        <td colspan="2"><span>{{ \Illuminate\Support\Str::limit($designTemplate['PBLKategori'],40) }}</span></td>
                        <td><span>{{ \Illuminate\Support\Str::limit($designTemplate['AnsvarigPart'],40) }}</span></td>
                        <td><span>{{ \Illuminate\Support\Str::limit($designTemplate['Kontrollskede'],20) }}</span></td>
                        <td><span>{{ \Illuminate\Support\Str::limit($designTemplate['Risk'],20) }}</span></td>
                        <td><span>{{ \Illuminate\Support\Str::limit($designTemplate['ProduceratIntyg'],20) }}</span></td>
                        <td><span>{{ \Illuminate\Support\Str::limit($designTemplate['Verifieringsmetod'],20) }}</span></td>
                        <td colspan="2"><span>{{ \Illuminate\Support\Str::limit($designTemplate['KraverPlatsbesok'],25) }}</span></td>
                        <td colspan="2"><span>{!! \Illuminate\Support\Str::limit($designTemplate['LawText'],30) !!}</span></td>
                        @php
                            $image = '';
                             if($designTemplate['assign_user'] !='')
                             {
                                $avatar = \App\OrganizationUsers::whereId($designTemplate['assign_user'])->pluck('avatar')->first();
                                $image = '<img alt="Avatar" class="img-circle table-avatar" width="30" height="30" src="'.$avatar.'">';
                             }
                             else
                             {
                                  $image = 'No Assign User';
                             }
                        @endphp
                        <td><span>{!! $image !!}</span></td>
                    </tr>
                    @endforeach
                    <tr>
                        <td colspan="15">
                            <div class="in-box-question">
                                @php
                                    unset($projectInfo['Projektnamn'],$projectInfo['BNÄrendenr'],$projectInfo['Entreprenadform'],$projectInfo['Byggherre'],$projectInfo['Strukturbeskrivning'],
                                    $projectInfo['Byggplatsensgatuadress'],$projectInfo['Byggnadsbeskrivning'],$projectInfo['Bygglov'],$projectInfo['Teknisktsamråd'],$projectInfo['Startbesked'],
                                    $projectInfo['Slutsamråd'],$projectInfo['Interimistisktslutbesked'],$projectInfo['Slutbesked'],$projectInfo['Projektstartslut'],
                                    $projectInfo['Kontrollansvarig'],$projectInfo['KAFöretag'],$projectInfo['KAAdress'],$projectInfo['KATelefon'],$projectInfo['KAMail'],$projectInfo['KACertNr']);

                                @endphp

                                <div class="box-question">
                                    <p class="question">AttachmentFile</p>
                                    <p class="answer">@foreach($projectInfo as $row=> $info)
                                            @if ($info !=null)
                                                @if($info === end($projectInfo))
                                                    @php echo $row;   @endphp
                                                @else
                                                    @php echo $row . ',' @endphp
                                                @endif
                                            @endif
                                            {{--TODO--}}
                                        @endforeach</p>
                                </div>


{{--                                <div class="box-question">--}}
{{--                                    <p class="question">TEKNINSKT SAMRÅD, VAD SKALL MAN SKRIVA HÄR?</p>--}}
{{--                                    <p class="answer">Avdelning A – Övergripande bestämmelser-Kontroll   -Mottagningskontroll av material och produkter   -26 §--Byggherren måste förvissa sig om att material och byggprodukter har sådana egenskaper att materialen   och produkterna korrekt använda i byggnadsverket gör att detta kan uppfylla egenskapskraven i denna författning   och i Boverkets byggregler (BFS 2011:6).   Med mottagningskontroll avses i denna författning byggherrens kontroll av att material och produkter har   förutsatta egenskaper när de tas emot på byggplatsen.   Har produkterna bedömda egenskaper enligt 18 § kan mottagningskontrollen inskränkas till identifiering,   kontroll av märkning och granskning av produktdeklarationen för att säkerställa att varorna har förutsatta   egenskaper.   Om byggprodukternas egenskaper inte är bedömda i den mening som avses i 18 § fordras verifiering genom   provning eller annan inom europeiska unionen vedertagen metod så att egenskaperna är kända och kan   värderas avseende lämplighet. (BFS 2015:6). ALLMÄNT RÅD   Byggprodukter vars egenskaper bedömts enligt alternativen a, c eller d i 18 § innebär inte att produkten   bedömts mot svenska krav på byggnadsverk i denna författning eller i Boverkets byggregler (BFS 2011:6).   Sådana bedömningar innebär endast att byggherren ska ha tilltro till den produkt- eller prestandadeklaration   av produktens egenskaper som medföljer. Med ledning av produkt- eller prestandadeklarationen kan   byggherren avgöra om byggprodukten är lämplig för aktuell användning.   För byggprodukter med bedömda egenskaper behöver byggherren inte göra någon egen provning av   dessa egenskaper. (BFS 2015:6). Avdelning A – Övergripande bestämmelser-Kontroll   -Mottagningskontroll av material och produkter   -26 §--Byggherren måste förvissa sig om att material och byggprodukter har sådana egenskaper att materialen   och produkterna korrekt använda i byggnadsverket gör att detta kan uppfylla egenskapskraven i denna författning   och i Boverkets byggregler (BFS 2011:6).   Med mottagningskontroll avses i denna författning byggherrens kontroll av att material och produkter har   förutsatta egenskaper när de tas emot på byggplatsen.   Har produkterna bedömda egenskaper enligt 18 § kan mottagningskontrollen inskränkas till identifiering,   kontroll av märkning och granskning av produktdeklarationen för att säkerställa att varorna har förutsatta   egenskaper.   Om byggprodukternas egenskaper inte är bedömda i den mening som avses i 18 § fordras verifiering genom   provning eller annan inom europeiska unionen vedertagen metod så att egenskaperna är kända och kan   värderas avseende lämplighet. (BFS 2015:6). ALLMÄNT RÅD   Byggprodukter vars egenskaper bedömts enligt alternativen a, c eller d i 18 § innebär inte att produkten   bedömts mot svenska krav på byggnadsverk i denna författning eller i Boverkets byggregler (BFS 2011:6).   Sådana bedömningar innebär endast att byggherren ska ha tilltro till den produkt- eller prestandadeklaration   av produktens egenskaper som medföljer. Med ledning av produkt- eller prestandadeklarationen kan   byggherren avgöra om byggprodukten är lämplig för aktuell användning.   För byggprodukter med bedömda egenskaper behöver byggherren inte göra någon egen provning av   dessa egenskaper. (BFS 2015:6). </p>--}}
{{--                                </div>--}}

                                <div class="no-print btn-printpage">
                                    <form action="{{ route('design.KontrollPlan.backControllPlan') }}" method="POST">
                                        @csrf
                                        <input name="project_id" type="hidden" value="{{ $projectInfo['project_id'] }}">
                                        <input name="tabindex" type="hidden" value="Kontrollplan">
                                        <button type="button" onclick="history.back()"  class="back">Return</button>
                                        <input class="btnPrint" type="button" value="Print" onclick="javascript:window.print()">
                                    </form>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="15">

                        </td>
                    </tr>
                    </tbody>
                    <tfoot>
                    <tr>
                        <td colspan="15">
                            <div class="footer-print">
                                <span class="foot-td2">Projekt Ask Team</span>
                            </div>
                        </td>
                    </tr>
                    </tfoot>
            </table>
            </div>

        </main>
        <footer class="row">
        </footer>
    </div>
</div>
</body>
</html>

