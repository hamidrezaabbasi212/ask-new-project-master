@extends('Admin.master')
@section('content-title', 'Ladda Excel fil')
@section('title', 'Ladda Excel fil')
@section('content')
    <style>
        .ajax-loader {
            visibility: hidden;
            background-color: rgba(255,255,255,0.7);
            position: absolute;
            z-index: +100 !important;
            width: 100%;
            height:100%;
        }

        .ajax-loader img {
            position: relative;
            top:4%;
            left:29%;
        }
    </style>
    <div class="row">
        <div class="ajax-loader">
            <img src="{{ asset('loader/Ajax-loader.gif') }}" class="img-responsive" />
        </div>
        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 uploadbox">
            <div class="title-uploadbox">
                <h3>Ladda Excel fil</h3>
            </div><!--title-uploadbox-->
            <form id="frmUploadExcelFile" action="" method="post"  enctype="multipart/form-data">
                @csrf
                <div class="inbox-upload">
                <div class="uploadbtn">
                    <input type="file" id="laws" name="laws"/>
                    <span class="specialupload">Välj Excel fil</span>
                </div>
                <div class="documentlist">
                    <strong>Uppladdad Excel fil</strong>
                    <div class="table-responsive">
                        <table class="table tbl-inbox-upload">
                            <thead>
                            <tr>
                                <td>File Name</td>
                                <td>Size</td>
                            </tr>
                            </thead>
                            <tbody >
                            <tr>
                                <td id="fileName">-</td>
                                <td id="fileSize">-</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>

                </div><!--documentlist-->
                <div class="btn-in-formupload">
                    <button type="submit" id="btnSave" class="bg-success">Ladda upp</button>
                    <button type="button" class="bg-danger">Avbryt</button>
                </div><!--btn-in-formupload-->
            </div>
                <!--inbox-upload-->
            </form>
        </div><!--uploadbox-->
    </div>
@endsection

@section('script')
    <script type="text/javascript">

        $(function() {
            $("#frmUploadExcelFile").submit(function(e) {
                event.preventDefault();
                var formData = new FormData(this);
                let token =  $('input[name="_token"]').val();
                formData.append('_token',token);
                $.ajax({
                    type: 'POST',
                    url: '{{ route('law.import-laws') }}',
                    data: formData,
                    processData: false,
                    contentType: false,
                    beforeSend: function() {
                        $('.ajax-loader').css("visibility", "visible");
                    },
                    success:function (data) {
                        if(data['message'] == 'success')
                        {
                            $('.ajax-loader').css("visibility", "hidden");
                            Swal.fire(
                                'Bra jobbat !',
                                'Filen har laddats upp.',
                                'success');
                        }
                        window.location = '{{ route('law.import-laws.laws') }}';
                    },
                    error:function (err) {
                        console.log(err);
                    }
                });

                });

            $('#laws').on('change', function () {
                var file = this.files[0];
                var name = file.name;
                var size = file.size;
                $('#fileName').text(name);
                $('#fileSize').text(size + ' KB');
            });
        });

    </script>
@endsection


