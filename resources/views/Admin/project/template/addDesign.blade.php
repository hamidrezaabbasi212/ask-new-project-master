@extends('Admin.master')
@section('content-title', 'kontrolplane')
@section('title', 'kontrolplane')
@section('content')

    @csrf
    <div class="row">
        <div id="tttterfr" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 box-organization">
            <div id="tabs-Kontrol" class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation">
                        <a class="active" href="#userform" aria-controls="userform" role="tab" data-toggle="tab">Design Fönster</a></li>
                </ul>

                <div class="tab-content col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding">

                    {{--StartUserForm--}}
                    <div role="tabpanel" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding tab-pane active" id="userform">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding">
                            <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 float-left">
                                <!-- Search 1 !-->
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-left nopadding">
                                    <form class="form-inline">
                                        <div class="input-group input-group-sm poscenter poscenter2">
                                            <input class="form-control FormControl  form-control-navbar" type="search" id="searchBesabkeyword" placeholder="Search" aria-label="Search">
                                            <div class="input-group-append search-top-Kontrollplan">
                                                <button class="btn btn-navbar" id="searhBesab" type="button">
                                                    <i class="fas fa-search"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <!--End Search 1 !-->
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-left nopadding">
                                    <label>Hänvisningar/Paragrafer</label>
                                    <div class="showrules" id="showrules" style="overflow-y: scroll">

                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7 float-left nopadding">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 r1-formuser">
                                    <h4>OBLIGATORISKT ENT PBL</h4>
                                    <form id="frmcate" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-rinfo pull-left">
                                        <div class="col-sm-12 pull-left">
                                            <div class="form-group">
                                                <label>BSAB/AMA</label>
                                                <input type="text" id="besabtext" name="BSABAMA" class="form-control" placeholder="Enter ...">
                                            </div>
                                        </div>
                                        <div class="col-sm-12 pull-left">
                                            <div class="form-group">
                                                <label style="width:17%;">PBL Kategori</label>
                                                <select style="width:82%;" class="form-control" name="PBLKategori" id="PBLKategori">
                                                    <option value="0">Selected Option</option>
                                                    @foreach(\App\Definitions\PBLCategory::latest()->get() as $pblCategory)
                                                        <option value="{{ $pblCategory->id }}">{{ $pblCategory->title }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-sm-12 pull-left">
                                            <div class="form-group">
                                                <label style="width:17%;">Ansvarig part</label>
                                                <input type="text" id="searchAnsvarig" class="form-control partsec" placeholder="Enter ...">
                                                <select class="form-control partsec" name="AnsvarigPart" id="selectAnsvarig">
                                                    <option value="0">Option 1</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 pull-left">
                                            <div class="form-group half-groupform">
                                                <label class="lbl1" style="width:30%;">Kontrollskede</label>
                                                <select style="width:68%;" class="form-control select1" name="Kontrollskede" id="Kontrollskede">
                                                    <option value="0">Selected Option</option>
                                                    @foreach(\App\Definitions\Kontrollskede::latest()->get() as $KontrollSkede)
                                                        <option value="{{ $KontrollSkede->id }}">{{ $KontrollSkede->title }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 pull-left">
                                            <div class="form-group half-groupform">
                                                <label style="width:17%;">Risk</label>
                                                <select onclick="showinput(this);" class="form-control" name="Risk" id="Risk">
                                                    <option value="0">Selected Option</option>
                                                    @foreach(\App\Definitions\Risk::latest()->get() as $risk)
                                                        <option value="{{ $risk->id }}">{{ $risk->title }}</option>
                                                    @endforeach
                                                    <option value="500">Övrig</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 input-Other pull-left">
                                            <input type="text" id="riskText" name="riskText" class="form-control" placeholder="Enter ...">
                                        </div>
                                        <div class="col-sm-12 pull-left" style="display: none;" id="riskdesdiv">
                                            <input type="text" id="riskdescription" name="riskdescription" class="form-control inputdescription" placeholder="beskrivning ...">
                                        </div>
                                        <div class="col-sm-12 pull-left">
                                            <div class="form-group">
                                                <label style="width:21%;">Producerat intyg</label>
                                                <select onclick="showinput2(this);" style="width:78%;" class="form-control" name="ProduceratIntyg" id="Producerat">
                                                    <option value="0">Selected Option</option>
                                                    @foreach(\App\Definitions\ProduceratIntyg::latest()->get() as $produceIntyg)
                                                        <option value="{{ $produceIntyg->id }}">{{ $produceIntyg->title }}</option>
                                                    @endforeach
                                                    <option value="500">Övrig</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 input-Other2 pull-left">
                                            <input type="text" id="ProduceratIntygText" name="ProduceratIntygText" class="form-control" placeholder="Enter ...">
                                        </div>
                                        <div class="col-sm-12 pull-left" style="display: none;" id="PIntydesdiv">
                                            <input type="text" id="PIntygdescription" name="PIntygdescription" class="form-control inputdescription" placeholder="beskrivning ...">
                                        </div>
                                        <div class="col-sm-12 pull-left">
                                            <div class="form-group">
                                                <label style="width:22%;">Verifieringsmetod</label>
                                                <select onclick="showinput3(this);" style="width:77%;" class="form-control" name="Verifieringsmetod" id="Verifieringsmetod">
                                                    <option value="0">Selected Option</option>
                                                    @foreach(\App\Definitions\Verifieringsmetod::latest()->get() as $verifieringsmetod)
                                                        <option value="{{ $verifieringsmetod->id }}">{{ $verifieringsmetod->title }}</option>
                                                    @endforeach
                                                    <option value="500">Övrig</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 input-Other3 pull-left">
                                            <input type="text" id="VerifieringsmetodText" name="VerifieringsmetodText" class="form-control" placeholder="Enter ...">
                                        </div>
                                        <div class="col-sm-12 pull-left" style="display: none;" id="Vmetoddesdiv">
                                            <input type="text" id="Vmetoddescription" name="Vmetoddescription" class="form-control inputdescription" placeholder="beskrivning ...">
                                        </div>
                                        <div class="col-sm-12 pull-left">
                                            <div class="form-group">
                                                <label style="width:22%;">Kraver platsbesok</label>
                                                <select onclick="showinput4(this);" style="width:77%;" class="form-control" name="KraverPlatsbesok" id="KraverPlatsbesok">
                                                    <option value="0">Selected Option</option>
                                                    @foreach(\App\Definitions\KraverPlatsbesok::latest()->get() as $KraverPlatsbesok)
                                                        <option value="{{ $KraverPlatsbesok->id }}">{{ $KraverPlatsbesok->title }}</option>
                                                    @endforeach
                                                    <option value="500">Övrig</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 input-Other4 pull-left">
                                            <input type="text" id="KraverPlatsbesokText" name="KraverPlatsbesokText" class="form-control" placeholder="Enter ...">
                                        </div>
                                        <div class="col-sm-12 pull-left" style="display: none;" id="KraverPdesdiv">
                                            <input type="text" id="KraverPdescription" name="KraverPdescription" class="form-control inputdescription" placeholder="beskrivning ...">
                                        </div>
                                </form>

                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding l-formuser">

                            <!-- Search 2 !-->
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 pull-left nopadding">
                                <div class="input-group input-group-sm poscenter poscenter-search">
                                    <input  name="keywords" id="searchAllLawskeyword" class="form-control FormControl  form-control-navbar" type="search" placeholder="Search" aria-label="Search">
                                    <div class="input-group-append search-top-Kontrollplan">
                                        <button class="btn btn-navbar" id="searhAllLaws" type="button">
                                            <i class="fas fa-search"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <!--End Search 2 !-->
                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 pull-left nopadding">
                                <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 rlogo-formuser btnFormUser">
                                    <button type="button" onclick="filter(1)" id="btnbbr" class="btnstyle">BBR<span id="bbr" class="ml-1 mb-1 badge badge bg-warning"></span></button>
                                    <button type="button" onclick="filter(2)" class="btnstyle">PBL<span id="pbl" class="ml-1 mb-1 badge bg-warning"></span></button>
                                    <button type="button" onclick="filter(3)" class="btnstyle">PBF<span id="pbf" class="ml-1 mb-1 badge bg-warning"></span></button>
                                    <button type="button" onclick="filter(4)" class="btnstyle">EKS10<span id="eks10" class="ml-1 mb-1 badge bg-warning"></span></button>
                                    <button type="button" onclick="filter(5)" class="btnstyle">AML<span id="aml" class="ml-1 mb-1 badge bg-warning"></span></button>
                                    <button type="button" onclick="filter(7)" class="btnstyle">MB<span id="mb" class="ml-1 mb-1 badge bg-warning"></span></button>
                                    <button type="button" onclick="filter(8)" class="btnstyle">JVL<span id="jvl" class="ml-1 mb-1 badge bg-warning"></span></button>
                                    <button type="button" onclick="filter(6)" class="btnstyle">BSAB<span id="bsab" class="ml-1 mb-1 badge bg-warning"></span></button>
                                </div><!--rlogo-formuser-->
                            </div>

                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-left nopadding">
                                <label>Hänvisningar/Paragrafer</label>
                                <div class="showrules" id="showrules1" style="overflow-y: scroll">

                                </div>
                            </div>
                            <form action="#" method="post">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-left nopadding">
                                    <p>Funktionskrav, hjälptexter råd och anvisningar. Använd denna ruta för hitta texter att kopiera till editorn. </p>
                                    <div class="showtext"></div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding thirdtab pull-left nopadding">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 additem nopadding pull-left">
                                        <button type="button" id="copyLaw" class="btnstyle btnstyle2"><i class="fa fa-plus" aria-hidden="true"></i>Lägg till hänvisning</button>
                                        <button type="button" id="copyText" class="btnstyle btnstyle2"><i class="fa fa-plus" aria-hidden="true"></i>Lägg till text</button>
                                        <button class="flagbtn"><img src="{{ asset('dist/img/flag-england.jpg') }}" alt=""></button>
                                        <button class="flagbtn"><img src="{{ asset('dist/img/flag-Sweden.jpg') }}" alt=""></button>
                                        <p>Funktionskrav EDITOR. Det du skriver här hamnar i egenkontrollprotokollet. Här kan du även komplettera dina krav med fritext.</p>
                                        <textarea class="textarea" id="law" placeholder="Place some text here"
                                                  style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                                    </div>
                                </div>
                            </form>
                        </div><!--l-formuser-->
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 r-formuser">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 r1-formuser">
                                <h4>KONTROLLPLAN OCH PROTOKOLL</h4>

                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-left nopadding">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 allbtn pull-left">
                                        <span class="show-all-btn collapsed" data-toggle="collapse" data-target="#readiqera"><i class="fa fa-angle-down" aria-hidden="true"></i>readiqera</span>
                                        <ul class="collapse show PROTOKOLL" id="readiqera">
                                            <li><button type="button" id="addControllPlan"><i class="fa fa-plus" aria-hidden="true"></i>Lägg till</button></li>
                                            <li><button type="button" class="btn-primary" id="editTemplate"><i class="fa fa-save" aria-hidden="true"></i>Spara ändringar</button></li>
                                            <li><button type="button" id="deleteTemplate"><i class="fa fa-close" aria-hidden="true"></i>Radera</button></li>
                                        </ul>
                                    </div>
                                </div>

                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-left nopadding">
                                    <label>Hänvisningar/Paragrafer</label>
                                    <div class="showrules">
                                        <ul id="LawComplete" style="min-height: 200px">

                                        </ul>

                                    </div>
                                    <div style="margin-left: 15px;" id="count"></div>
                                </div>
                            </div><!--r1-formuser-->
                        </div><!--r-formuser-->
                    </div>

                    {{--EndUserForm--}}

                </div><!--tabs-product-->
            </div><!--box-organization-->
        </div>
        @endsection

        @section('script')

            @if(Session::has('successAddProject'))
                <script>
                    Swal.fire(
                        'Bra jobbat !',
                        'projekt registreringen slutförd',
                        'success');
                </script>
            @endif

            @if(Session::has('successEditProject'))
                <script>
                    Swal.fire(
                        'Bra jobbat !',
                        'Färdig redigering av projektinformation',
                        'success');
                </script>
            @endif

            @if(Session::has('successBaseProject'))
                <script>
                    Swal.fire(
                        'Bra jobbat !',
                        'projekt registreringen slutförd',
                        'success');
                </script>
            @endif

            <script type="text/javascript">

                function eneter(e) {
                    var keycode = (event.keyCode ? event.keyCode : event.which);
                    if(keycode == '13'){
                        $(":focus").each(function() {
                            var focusElementId = this.id;
                            if(focusElementId === "searchBesabkeyword")
                            {
                                $('#searhBesab').click();
                            }
                            else if(focusElementId === "searchAllLawskeyword")
                            {
                                $('#searhAllLaws').click();
                            }
                        });
                        return false;
                    }
                }

                $(document).bind("keydown", eneter);

                function deleteConfirmation(id) {
                    Swal.fire({
                        type: 'warning',
                        title: 'Är du säker?',
                        text: "Du kommer inte att kunna återställa detta!",
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        cancelButtonText: 'annullera',
                        confirmButtonText: 'Ja, radera det!'
                    }).then(function (e) {
                        if (e.value === true) {
                            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
                            $.ajax({
                                type: 'POST',
                                url: "/panel/template/design/delete/" + id,
                                data: {_token: '{!! csrf_token() !!}',},
                                dataType: 'JSON',
                                success: function (results) {
                                    if (results.success === true) {
                                        getTemplate(publicid);
                                        Swal.fire(
                                            'Raderade!',
                                            'Din fil har tagits bort.',
                                            'success'
                                        );
                                    } else {
                                        Swal.fire({
                                            icon: 'fel',
                                            title: 'Hoppsan...',
                                            text: 'Något gick fel!',
                                        })
                                    }
                                }
                            });
                        }
                        else {
                            e.dismiss;
                        }}, function (dismiss) {
                        return false;
                    })

                }

                var keywordAllLaws;
                var keywordBesab;
                var keywordCategory;
                var keyCount = 0;
                var c = 0;
                var p = 0;
                var Laws = 0;
                var title = '';
                var FinalTextLaw ='';
                var design_id = '';
                var designs = [];
                var organizationidu = 0;
                var organizationidEdit = 0;
                var url = document.URL;
                var id = url.substring(url.lastIndexOf('/') + 1);
                var publicid = url.substring(url.lastIndexOf('/') + 1);
                $('#bbr').text("0");
                $('#pbl').text("0");
                $('#pbf').text("0");
                $('#eks10').text("0");
                $('#bsab').text("0");
                $('#aml').text("0");
                $('#mb').text("0");
                $('#jvl').text("0");

                $('#searchAnsvarig').on('keyup',function () {
                    keyword=$(this).val();
                    var KeyID = event.keyCode;
                    if(KeyID == 8)
                    {

                        if(keyCount>0)
                        {
                            keyCount--;
                        }

                    }
                    else
                    {
                        keyCount++;
                    }


                    if(keyCount >= 3)
                    {
                        keyword=$(this).val();

                        if(keyword == "")
                        {
                            $('#selectAnsvarig').html("");
                        }
                        $.ajax({
                            type : 'get',
                            url : '{{route('searchAnsvarig')}}',
                            data:{'keywords':keyword},
                            success:function(data){
                                $("#selectAnsvarig").html(data);
                            }
                        });
                        keyCount--;
                    }
                    else
                    {
                        $("#selectAnsvarig").html("");
                    }


                });

                $(document).ready(function () {
                    getTemplate(id);
                    $('#searchBesabkeyword').keyup(function () {
                        $(".showtext").html("");
                        $('#showrules').html("");
                        $('#searchAllLawskeyword').val("");
                        $('#bbr').text("0");
                        $('#pbl').text("0");
                        $('#pbf').text("0");
                        $('#eks10').text("0");
                        $('#bsab').text("0");
                        $('#aml').text("0");
                        $('#mb').text("0");
                        $('#jvl').text("0");

                    });

                    $("#searhBesab").click(function () {
                        $('#bsab').text("0");
                        $('#searchAllLawskeyword').val("");
                        keywordCategory = 0;
                        $('#showrules').html("");
                        $('#bbr').text("0");
                        $('#pbl').text("0");
                        $('#pbf').text("0");
                        $('#eks10').text("0");
                        $('#aml').text("0");
                        $('#mb').text("0");
                        $('#jvl').text("0");
                        let timerInterval;
                        Swal.fire({
                            title: 'nu Sök!',
                            html: 'Vänta, Automatisk Stängningsvarning.',
                            timer: 20000,
                            timerProgressBar: true,
                            allowOutsideClick: false,
                            onBeforeOpen: () => {
                                Swal.showLoading();
                                timerInterval = setInterval(() => {
                                    const content = Swal.getContent();
                                    if (content) {
                                        const b = content.querySelector('b');
                                        if (b) {
                                            b.textContent = Swal.getTimerLeft();
                                        }
                                    }
                                }, 100)
                            },
                            onClose: () => {
                                clearInterval(timerInterval);
                            }
                        }).then((result) => {
                            /* Read more about handling dismissals below */
                            if (result.dismiss === Swal.DismissReason.timer) {
                                console.log('I was closed by the timer');
                            }
                        });


                        keywordBesab=$('#searchBesabkeyword').val();
                        keywordCategory = keywordBesab;
                        var len = $('#searchBesabkeyword').val().length;
                        if(len < 3)
                        {
                            Swal.fire(
                                'Hoppsan...',
                                'Ange minst tre bokstäver!',
                                'error'
                            );
                        }
                        else
                        {
                            $.ajax({
                                type : 'get',
                                url : '{{route('searchBesab')}}',
                                data:{'keywords':keywordBesab},
                                success:function(data){
                                    swal.close();
                                    $('#showrules').html(data[0]);
                                    beseb = data[1];

                                    if (typeof beseb === "undefined")
                                    {
                                        $('#bsab').text("0");
                                    }
                                    else
                                    {
                                        $('#bsab').text(beseb);
                                    }

                                },
                                error:function (err) {
                                    console.log(err);
                                }
                            });
                        }
                    });

                    $('#searchAllLawskeyword').keyup(function () {
                        $(".showtext").html("");
                        $('#showrules').html("");
                        $('#searchBesabkeyword').val("");
                        $('#bbr').text("0");
                        $('#pbl').text("0");
                        $('#pbf').text("0");
                        $('#eks10').text("0");
                        $('#bsab').text("0");
                        $('#aml').text("0");
                        $('#mb').text("0");
                        $('#jvl').text("0");

                    });

                    $("#searhAllLaws").click(function () {
                        $('#bsab').text("0");
                        $('#showrules').html("");
                        $('#searchBesabkeyword').val("");
                        keywordCategory = 0;
                        let timerInterval;
                        Swal.fire({
                            title: 'nu Sök!',
                            html: 'Vänta, Automatisk Stängningsvarning.',
                            timer: 20000,
                            timerProgressBar: true,
                            allowOutsideClick: false,
                            onBeforeOpen: () => {
                                Swal.showLoading();
                                timerInterval = setInterval(() => {
                                    const content = Swal.getContent();
                                    if (content) {
                                        const b = content.querySelector('b');
                                        if (b) {
                                            b.textContent = Swal.getTimerLeft();
                                        }
                                    }
                                }, 100)
                            },
                            onClose: () => {
                                clearInterval(timerInterval);
                            }
                        }).then((result) => {
                            /* Read more about handling dismissals below */
                            if (result.dismiss === Swal.DismissReason.timer) {
                                console.log('I was closed by the timer');
                            }
                        });
                        keywordAllLaws=$('#searchAllLawskeyword').val();
                        keywordCategory =  keywordAllLaws;
                        var len = $('#searchAllLawskeyword').val().length;
                        if(len < 3)
                        {
                            Swal.fire(
                                'Hoppsan...',
                                'Ange minst tre bokstäver!',
                                'error'
                            );
                        }
                        else
                        {
                            $.ajax({
                                type : 'get',
                                url : '{{route('search')}}',
                                data:{'keywords':keywordAllLaws},
                                success:function(data){
                                    swal.close();
                                    $('#showrules1').html(data[0]);
                                    Laws = data[2];
                                    var bbr = data[1];
                                    if(bbr.length>0)
                                    {
                                        $('#bbr').text("0");
                                        $('#pbl').text("0");
                                        $('#pbf').text("0");
                                        $('#eks10').text("0");
                                        $('#bsab').text("0");
                                        $('#jvl').text("0");
                                        $('#mb').text("0");
                                        $('#aml').text("0");
                                        $.each(bbr,function(i,key){
                                            var value = key['category_id'];
                                            var count = key['count'];
                                            if(value == 1)
                                            {
                                                $('#bbr').text(count);
                                            }
                                            else if(value == 2)
                                            {
                                                $('#pbl').text(count);
                                            }
                                            else if(value == 3)
                                            {
                                                $('#pbf').text(count);
                                            }
                                            else if(value == 4)
                                            {
                                                $('#eks10').text(count);
                                            }
                                            else if(value == 5)
                                            {
                                                $('#aml').text(count);
                                            }
                                            else if(value == 6)
                                            {
                                                $('#bsab').text(count);
                                            }
                                            else if(value == 7)
                                            {
                                                $('#mb').text(count);
                                            }
                                            else if(value == 8)
                                            {
                                                $('#jvl').text(count);
                                            }
                                        });
                                    }

                                },
                                error:function (err) {
                                    console.log(err);
                                }
                            });
                        }
                    });

                    $("#copyLaw").click(function () {
                        if($('.showtext').text() == "")
                        {
                            Swal.fire(
                                'fel',
                                'Välj en regel från listan',
                                'error');
                        }
                        else
                        {
                            var text = Law['Text'];
                            var data = Law;
                            var arr = [];
                            data['updated_at'] = null;
                            data['created_at'] = null;
                            data['category_id'] = null;
                            data['Text'] = null;
                            data['Stycke5']=null;
                            data['Stycke4']=null;
                            data['Stycke3']=null;
                            data['Stycke2']=null;
                            data['Stycke1']=null;
                            data['Huvudparagraf']=null;
                            data['id']=null;
                            $.each(Object.keys(data),function(i,key){
                                var value = data[key];
                                if(value !=null)
                                {
                                    title = value;
                                    FinalTextLaw = $('#law').val();
                                    $("#law").summernote('code', '<p>' +$('#law').val()+'<b>'+title+'</b></p>');
                                    // return false; // breaks
                                }
                            });
                        }
                    });

                    var count = 0;

                    $("#addControllPlan").click(function () {

                        var besab = $('#besabtext').val();
                        var PBLKategori = $("#PBLKategori option:selected").val();
                        var AnsvarigPart = $("#selectAnsvarig option:selected").val();
                        var AnsvarigPartText = $("#selectAnsvarig option:selected").html();

                        var Kontrollskede = $("#Kontrollskede option:selected").val();

                        var Risk = $("#Risk option:selected").val();
                        var riskText = $("#riskText").val();
                        var riskDescription = $("#riskdescription").val();

                        var Producerat = $("#Producerat option:selected").val();
                        var ProduceratIntygText = $("#ProduceratIntygText").val();
                        var ProduceratIntygDescription = $("#PIntygdescription").val();

                        var Verifieringsmetod = $("#Verifieringsmetod option:selected").val();
                        var VerifieringsmetodText = $("#VerifieringsmetodText").val();
                        var VerifieringsmetodDescription = $("#Vmetoddescription").val();

                        var KraverPlatsbesok = $("#KraverPlatsbesok option:selected").val();
                        var KraverPlatsbesokText = $("#KraverPlatsbesokText").val();
                        var KraverPlatsbesokDescription = $("#KraverPdescription").val();

                        count++;
                        $('#count').html(count);

                        $.ajax({
                            type: 'POST',
                            url: '{{ route('template.saveDesign') }}',
                            data: {'_token': $('input[name="_token"]').val(),'title': $('#title').val(),
                                'BSABAMA':besab,'PBLKategori':PBLKategori,'AnsvarigPart':AnsvarigPart,'AnsvarigPartText': AnsvarigPartText ,'Kontrollskede':Kontrollskede,
                                'Risk':Risk, 'riskText': riskText,'riskDescription': riskDescription,
                                'ProduceratIntyg':Producerat,'ProduceratIntygText':ProduceratIntygText,'ProduceratIntygDescription':ProduceratIntygDescription,
                                'Verifieringsmetod':Verifieringsmetod,'VerifieringsmetodText':VerifieringsmetodText,'VerifieringsmetodDescription':VerifieringsmetodDescription,
                                'KraverPlatsbesok':KraverPlatsbesok,'KraverPlatsbesokText':KraverPlatsbesokText,'KraverPlatsbesokDescription':KraverPlatsbesokDescription,
                                'LawText':$(".textarea").val(),'id':id,},
                            success:function (data) {
                                getTemplate(id);
                                Swal.fire(
                                    'Bra jobbat !',
                                    'mall registreringen slutförd',
                                    'success');
                                $('.textarea').summernote('code','');
                                $('#searchAnsvarig').val('');
                                $("#PBLKategori").val("0").change();
                                $("#selectAnsvarig").val("0").change();
                                $("#Kontrollskede").val("0").change();
                                $("#Risk").val("0").change();
                                $("#Producerat").val("0").change();
                                $("#Verifieringsmetod").val("0").change();
                                $("#KraverPlatsbesok").val("0").change();

                                $('#searchAllLawskeyword').val("");
                                $('#besabtext').val("");
                                $('#showrules1').html("");
                                $('.showtext').html("");

                                $('#bbr').text("0");
                                $('#pbl').text("0");
                                $('#pbf').text("0");
                                $('#eks10').text("0");
                                $('#bsab').text("0");
                                $('#jvl').text("0");
                                $('#mb').text("0");
                                $('#aml').text("0");

                                $('#riskText').val("");
                                $('#riskText').parent().css({"display": "none"});
                                $("#riskdescription").val("");
                                $('#riskdescription').parent().css({"display": "none"});

                                $('#ProduceratIntygText').val("");
                                $('#ProduceratIntygText').parent().css({"display": "none"});
                                $("#PIntygdescription").val("");
                                $('#PIntygdescription').parent().css({"display": "none"});

                                $('#VerifieringsmetodText').val();
                                $('#VerifieringsmetodText').parent().css({"display": "none"});
                                $("#Vmetoddescription").val("");
                                $('#Vmetoddescription').parent().css({"display": "none"});

                                $('#KraverPlatsbesokText').val("");
                                $('#KraverPlatsbesokText').parent().css({"display": "none"});
                                $("#KraverPdescription").val("");
                                $('#KraverPdescription').parent().css({"display": "none"});
                            },
                            error:function (err) {

                            }
                        });
                    });

                    $('#editTemplate').click(function () {
                        var besab = $('#besabtext').val();
                        var PBLKategori = $("#PBLKategori option:selected").val();
                        var AnsvarigPart = $("#selectAnsvarig option:selected").val();
                        var AnsvarigPartText = $("#selectAnsvarig option:selected").html();

                        var Kontrollskede = $("#Kontrollskede option:selected").val();

                        var Risk = $("#Risk option:selected").val();
                        var riskText = $("#riskText").val();
                        var riskDescription = $("#riskdescription").val();
                        if(Risk == 500)
                        {
                            riskDescription = "";
                        }
                        else
                        {
                            riskText = "";
                        }

                        var Producerat = $("#Producerat option:selected").val();
                        var ProduceratIntygText = $("#ProduceratIntygText").val();
                        var ProduceratIntygDescription = $("#PIntygdescription").val();
                        if(Producerat == 500)
                        {
                            ProduceratIntygDescription = "";
                        }
                        else
                        {
                            ProduceratIntygText = "";
                        }

                        var Verifieringsmetod = $("#Verifieringsmetod option:selected").val();
                        var VerifieringsmetodText = $("#VerifieringsmetodText").val();
                        var VerifieringsmetodDescription = $("#Vmetoddescription").val();
                        if(Verifieringsmetod == 500)
                        {
                            VerifieringsmetodDescription = "";
                        }
                        else
                        {
                            VerifieringsmetodText = "";
                        }

                        var KraverPlatsbesok = $("#KraverPlatsbesok option:selected").val();
                        var KraverPlatsbesokText = $("#KraverPlatsbesokText").val();
                        var KraverPlatsbesokDescription = $("#KraverPdescription").val();
                        if(KraverPlatsbesok == 500)
                        {
                            KraverPlatsbesokDescription = "";
                        }
                        else
                        {
                            KraverPlatsbesokText = "";
                        }

                        $.ajax({
                            type: 'POST',
                            url: '/panel/template/design/' + design_id,
                            data: {'_token': $('input[name="_token"]').val(),'title': $('#title').val(),
                                'BSABAMA':besab,'PBLKategori':PBLKategori,'AnsvarigPart':AnsvarigPart,'AnsvarigPartText': AnsvarigPartText,'Kontrollskede':Kontrollskede,
                                'Risk':Risk, 'riskText': riskText,'riskDescription': riskDescription,
                                'ProduceratIntyg':Producerat,'ProduceratIntygText':ProduceratIntygText,'ProduceratIntygDescription':ProduceratIntygDescription,
                                'Verifieringsmetod':Verifieringsmetod,'VerifieringsmetodText':VerifieringsmetodText,'VerifieringsmetodDescription':VerifieringsmetodDescription,
                                'KraverPlatsbesok':KraverPlatsbesok,'KraverPlatsbesokText':KraverPlatsbesokText,'KraverPlatsbesokDescription':KraverPlatsbesokDescription,
                                'LawText':$(".textarea").val(),},
                            success:function (data) {
                                Swal.fire(
                                    'Bra jobbat !',
                                    'mall uppdatering slutförd',
                                    'success');
                                $('.textarea').summernote('code','');
                                getTemplate(id);

                                $('#searchAnsvarig').val("");
                                $("#PBLKategori").val("").change();
                                $("#selectAnsvarig").val("0").change();
                                $("#Kontrollskede").val("0").change();
                                $("#Risk").val("0").change();
                                $("#Producerat").val("0").change();
                                $("#Verifieringsmetod").val("0").change();
                                $("#KraverPlatsbesok").val("0").change();

                                $('#searchAllLawskeyword').val("");
                                $('#besabtext').val("");
                                $('#showrules1').html("");
                                $('.showtext').html("");

                                $('#bbr').text("0");
                                $('#pbl').text("0");
                                $('#pbf').text("0");
                                $('#eks10').text("0");
                                $('#bsab').text("0");
                                $('#jvl').text("0");
                                $('#mb').text("0");
                                $('#aml').text("0");

                                $('#riskText').val("");
                                $('#riskText').parent().css({"display": "none"});
                                $("#riskdescription").val("");
                                $('#riskdescription').parent().css({"display": "none"});

                                $('#ProduceratIntygText').val("");
                                $('#ProduceratIntygText').parent().css({"display": "none"});
                                $("#PIntygdescription").val("");
                                $('#PIntygdescription').parent().css({"display": "none"});

                                $('#VerifieringsmetodText').val();
                                $('#VerifieringsmetodText').parent().css({"display": "none"});
                                $("#Vmetoddescription").val("");
                                $('#Vmetoddescription').parent().css({"display": "none"});

                                $('#KraverPlatsbesokText').val("");
                                $('#KraverPlatsbesokText').parent().css({"display": "none"});
                                $("#KraverPdescription").val("");
                                $('#KraverPdescription').parent().css({"display": "none"});
                                //ok
                            },
                            error:function (err) {

                            }
                        });
                    });

                    $('#deleteTemplate').click(function () {
                        deleteConfirmation(design_id);
                        $('.textarea').summernote('code','');
                        $('.textarea').summernote('code','');
                        $('#besabtext').val('');
                        $('#searchAnsvarig').val('');
                        $("#PBLKategori").val("0").change();
                        $("#selectAnsvarig").val("0").change();
                        $("#Kontrollskede").val("0").change();
                        $("#Risk").val("0").change();
                        $("#Producerat").val("0").change();
                        $("#Verifieringsmetod").val("0").change();
                        $("#KraverPlatsbesok").val("0").change();

                        $('#riskText').val("");
                        $('#riskText').parent().css({"display": "none"});
                        $("#riskdescription").val("");
                        $('#riskdescription').parent().css({"display": "none"});

                        $('#ProduceratIntygText').val("");
                        $('#ProduceratIntygText').parent().css({"display": "none"});
                        $("#PIntygdescription").val("");
                        $('#PIntygdescription').parent().css({"display": "none"});

                        $('#VerifieringsmetodText').val();
                        $('#VerifieringsmetodText').parent().css({"display": "none"});
                        $("#Vmetoddescription").val("");
                        $('#Vmetoddescription').parent().css({"display": "none"});

                        $('#KraverPlatsbesokText').val("");
                        $('#KraverPlatsbesokText').parent().css({"display": "none"});
                        $("#KraverPdescription").val("");
                        $('#KraverPdescription').parent().css({"display": "none"});
                        getTemplate(id);
                    });

                    $('#copyText').click(function () {
                        console.log(LawText);
                        var text = '';
                        if(LawText!=null)
                        {
                            text = LawText;
                        }
                        FinalTextLaw = $('#law').val();
                        $("#law").summernote('code', '<p>' +FinalTextLaw+'<b></b><p>'+text+'</p></p>');

                    });

                });

                var Law;
                var TempID = '';
                var LawText = '';
                function getLaw(id) {
                    $.ajax({
                        type : 'get',
                        url : '/panel/law/getLaw/' + id,
                        success:function(data){
                            if(data['Text'] != null)
                            {
                                $('.showtext').text(data['Text']);
                                LawText = data['Text'];

                            }
                            else
                            {
                                $('.showtext').text(data['Hanvisning']);

                            }

                            Law = data;
                        }
                    });
                }

                function getBesab(id) {
                    const swalWithBootstrapButtons = Swal.mixin({
                        customClass: {
                            confirmButton: 'btn btn-success',
                            cancelButton: 'btn btn-danger'
                        },
                        buttonsStyling: false
                    });

                    swalWithBootstrapButtons.fire({
                        title: 'Är du säker?',
                        text: "Du är säker på att lägga till detta!",
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonText: 'Ja, Lagt till',
                        cancelButtonText: 'Nej, Avbryt',
                        reverseButtons: true
                    }).then((result) => {
                        if (result.value) {
                            $.ajax({
                                type : 'get',
                                url : '/panel/law/getBesabLaw/' + id,
                                success:function(data){
                                    $('.showtext').text(data['Hanvisning']);
                                    Law = data;
                                    var Hanvisning = '';
                                    var besabtext = "";
                                    if(typeof Law['Hanvisning'] === "undefined")
                                    {
                                        $('#besabtext').val("");
                                    }
                                    else
                                    {
                                        besabtext = $('#besabtext').val() + Law['Hanvisning'] ;
                                        if(besabtext !=null)
                                        {
                                            besabtext += '  ' + '|' + ' ';
                                        }
                                    }


                                    $('#besabtext').val(besabtext);
                                }
                            });

                        }});

                }


                function getTemplate() {
                    $.ajax({
                        type : 'get',
                        url : '/panel/template/getTemplate/' + id,
                        success:function (data) {
                            // console.log(data['design']);
                            designs = data['design'];
                            $('#LawComplete').html(data['output']);
                        },
                        error:function (err) {
                            console.log(err);
                        }
                    });
                }


                function getTemplateid(id) {
                    TempID = id;
                    design_id = id;
                    $.ajax({
                        type : 'get',
                        url : '/panel/template/getTemp/' + id,
                        success:function (data) {
                            $('#riskText').val("");
                            $('#ProduceratIntygText').val("");
                            $('#VerifieringsmetodText').val("");
                            $('#KraverPlatsbesokText').val("");

                            $("#riskdescription").val("");
                            $("#PIntygdescription").val("");
                            $("#Vmetoddescription").val("");
                            $("#KraverPdescription").val("");

                            $("#Kontrollskede").val(data['Kontrollskede']).trigger('change');
                            $('#besabtext').val(data['BSABAMA']);
                            $('#PBLKategori').val(data['PBLKategori']);
                            $('#PBLKategori').trigger('change');
                            $('#searchAnsvarig').val("");
                            $("#selectAnsvarig option:selected").html(data['AnsvarigPart']);
                            let keywordAnsvaring =  data['AnsvarigPartText'];
                            $.ajax({
                                type : 'get',
                                url : '{{route('searchAnsvarig')}}',
                                data:{'keywords':keywordAnsvaring},
                                success:function(data){
                                    $("#selectAnsvarig").html(data);
                                }
                            });

                            $('#Risk').val(data['Risk']);
                            if(data['Risk'] == 500)
                            {
                                $("#riskText").parent().css({"display": "block"});
                                $('#riskText').val(data['riskText']);
                            }
                            else {
                                $("#riskText").parent().css({"display": "none"});
                                $("#riskdescription").parent().css({"display": "block"});
                                $("#riskdescription").val(data['riskDescription']);
                            }

                            $('#Producerat').val(data['ProduceratIntyg']);
                            if(data['ProduceratIntyg'] == 500)
                            {
                                $("#ProduceratIntygText").parent().css({"display": "block"});
                                $('#ProduceratIntygText').val(data['ProduceratIntygText']);
                            }
                            else {
                                $("#ProduceratIntygText").parent().css({"display": "none"});
                                $("#PIntygdescription").parent().css({"display": "block"});
                                $("#PIntygdescription").val(data['ProduceratIntygDescription']);
                            }


                            $('#Verifieringsmetod').val(data['Verifieringsmetod']);
                            if(data['Verifieringsmetod'] == 500)
                            {
                                $("#VerifieringsmetodText").parent().css({"display": "block"});
                                $('#VerifieringsmetodText').val(data['VerifieringsmetodText']);
                            }
                            else {
                                $("#VerifieringsmetodText").parent().css({"display": "none"});
                                $("#Vmetoddescription").parent().css({"display": "block"});
                                $("#Vmetoddescription").val(data['VerifieringsmetodDescription']);
                            }

                            $('#KraverPlatsbesok').val(data['KraverPlatsbesok']);
                            if(data['KraverPlatsbesok'] == 500)
                            {
                                $("#KraverPlatsbesokText").parent().css({"display": "block"});
                                $('#KraverPlatsbesokText').val(data['KraverPlatsbesokText']);
                            }
                            else {
                                $("#KraverPlatsbesokText").parent().css({"display": "none"});
                                $("#KraverPdescription").parent().css({"display": "block"});
                                $("#KraverPdescription").val(data['KraverPlatsbesokDescription']);
                            }


                            $('.textarea').summernote('code',data['LawText']);
                        }
                    });
                }


                function filter(id) {

                    $.ajax({
                        type : 'get',
                        url : '{{route('searchCategory')}}',
                        data:{'keywords':keywordCategory,'id' :id},
                        success:function(data){
                            $('#showrules1').html("");
                            $('#showrules1').html(data);
                        }
                    });
                }


                $(function () {
                    // Summernote
                    $('.textarea').summernote();

                });

                function select(e){
                    if($('.liclick').hasClass('bluetext')){
                        $('.liclick').removeClass('bluetext')
                    }
                    $(e).addClass('bluetext');
                }

                function select2(e){
                    if($('.liclick2').hasClass('bluetext')){
                        $('.liclick2').removeClass('bluetext')
                    }
                    $(e).addClass('bluetext');
                }

                function select3(e){
                    $('.liclick').removeClass('greentext');
                    if($('#controlplan' + e).hasClass('greentext')){
                        $('#controlplan' + e).removeClass('greentext')
                    }
                    $('#controlplan' + e).addClass('greentext');
                }

                function showinput(e){
                    var item= $(e).val();
                    if(item == "500"){
                        $('#riskdesdiv').css('display','none');
                        $('.input-Other').show();
                    }
                    else if(item != "500" && item != "0"){
                        $('.input-Other').hide();
                        $('#riskdesdiv').css('display','block');
                    }
                    else if(item == 0)
                    {
                        $('.input-Other').hide();
                        $('#riskdesdiv').css('display','none');
                    }
                }

                function showinput2(e){
                    var item= $(e).val();
                    if(item == "500"){
                        $('#PIntydesdiv').css('display','none');
                        $('.input-Other2').show();
                    }
                    else if(item != "500" && item != "0"){
                        $('.input-Other2').hide();
                        $('#PIntydesdiv').css('display','block');
                    }
                    else if(item == 0)
                    {
                        $('.input-Other2').hide();
                        $('#PIntydesdiv').css('display','none');
                    }
                }

                function showinput3(e){
                    var item= $(e).val();
                    if(item == "500"){
                        $('#Vmetoddesdiv').css('display','none');
                        $('.input-Other3').show();
                    }
                    else if(item != "500" && item != "0"){
                        $('.input-Other3').hide();
                        $('#Vmetoddesdiv').css('display','block');
                    }
                    else if(item == 0)
                    {
                        $('.input-Other3').hide();
                        $('#Vmetoddesdiv').css('display','none');
                    }
                }

                function showinput4(e){
                    var item= $(e).val();
                    if(item == "500"){
                        $('#KraverPdesdiv').css('display','none');
                        $('.input-Other4').show();
                    }
                    else if(item != "500" && item != "0"){
                        $('.input-Other4').hide();
                        $('#KraverPdesdiv').css('display','block');
                    }
                    else if(item == 0)
                    {
                        $('.input-Other4').hide();
                        $('#KraverPdesdiv').css('display','none');
                    }
                }

            </script>
@endsection
