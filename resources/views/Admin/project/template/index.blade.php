@extends('Admin.master')
@section('content-title', 'Egna mallar')
@section('title', 'Egna mallar')
@section('content')
    <div class="row mb-3">
        <div class="col">
            <a  href="#" class="btn-createproject" data-toggle="modal" data-target="#saveTemplate">Ny mall</a>
        </div><!-- /.col -->
    </div>
    <div class="card">
        <div class="card-body p-0">
            <div id="table_data">
                @include('Admin.project.template.loadDataTemplate')
            </div>
        </div>
        <div class="modal fade" id="saveTemplate" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    </div>
                    <div class="modal-body">
                        <div class="logomodal">
                            <img src="{{ asset('dist/img/favicon.png') }}" alt="">
                        </div>
                        <div class="title-modal"><span>Add Template</span></div>
                        <form class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-left" method="" action="">
                            <div class="col-sm-6 pull-left">
                                <div class="form-group">
                                    <label>TemplateCategory</label>
                                    <select id="templateCategory" class="form-control select2" style="width: 100%;" data-select2-id="1" tabindex="-1" aria-hidden="true">
                                        <option value="0">Selected Option</option>
                                        @foreach(\App\Definitions\TemplateCategory::all() as $TemplateCategory)
                                            <option value="{{ $TemplateCategory->id }}">{{ $TemplateCategory->title }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6 pull-left">
                                <div class="form-group">
                                    <label>TemplateName</label>
                                    <input type="text" id="templateName" class="form-control" placeholder="Enter ...">
                                </div>
                            </div>
                            <div class="col-sm-12 pull-left">
                                <button type="button" id="btn-saveTemplate" class="btnmodal btn-add-user">Add Template</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="EditTemplate" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    </div>
                    <div class="modal-body">
                        <div class="logomodal">
                            <img src="{{ asset('dist/img/favicon.png') }}" alt="">
                        </div>
                        <div class="title-modal"><span>Add Template</span></div>
                        <form class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-left" method="" action="">

                            <div class="col-sm-6 pull-left">
                                <div class="form-group">
                                    <label>TemplateCategory</label>
                                    <select id="templateCategoryedit" class="form-control" style="width: 100%;" data-select2-id="1" tabindex="-1" aria-hidden="true">
                                        <option value="0">Selected Option</option>
                                        @foreach(\App\Definitions\TemplateCategory::all() as $TemplateCategory)
                                            <option value="{{ $TemplateCategory->id }}">{{ $TemplateCategory->title }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6 pull-left">
                                <div class="form-group">
                                    <label>TemplateName</label>
                                    <input type="text" id="templateNameedit" class="form-control" placeholder="Enter ...">
                                </div>
                            </div>
                            <div class="col-sm-12 pull-left">
                                <button type="button" id="btn-editTemplate" class="btnmodal btn-add-user">edit Template</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.card-body -->
    </div>
@endsection


@section('script')
    <script type="text/javascript">
        function getdata(page)
        {
            $.ajax({
                url:"/panel/template/getData?page="+page,
                success:function(data)
                {
                    $('#table_data').html(data);
                }
            });
        }
        var page;
        $(document).ready(function(){
            var id;

            $(document).on('click', '.pagination a', function(event){
                event.preventDefault();
                page = $(this).attr('href').split('page=')[1];
                getdata(page);
            });


            $('#btn-saveTemplate').click(function () {
                var templateName = $('#templateName').val();
                var templateCategory = $("#templateCategory option:selected").val();
                $.ajax({
                    type: 'POST',
                    url: '{{ route('template.addTemplate') }}',
                    data: {"_token": "{{ csrf_token() }}",'templateName': templateName,
                        'templateCategory':templateCategory},
                    success:function (data) {
                        getdata(page);
                        $('#saveTemplate').modal('hide');
                        Swal.fire(
                            'Bra jobbat !',
                            'mall registreringen slutförd',
                            'success');
                    },
                    error:function (err) {
                        console.log(err);
                    }
                });
            });


            $("#EditTemplate").on("show.bs.modal", function (e) {
                id = $(e.relatedTarget).data('target-id');

                var  templateNameedit = $(e.relatedTarget).data('target-tempname');

                var templateCategoryedit = $(e.relatedTarget).data('target-templatecategory');

                $("#templateNameedit").val(templateNameedit);

                $("#templateCategoryedit" ).val(templateCategoryedit).change();

            });

            $('#btn-editTemplate').click(function () {
                var templateNameupdate = $('#templateNameedit').val();
                var templateupdate = $("#templateCategoryedit option:selected").val();
                $.ajax({
                    type: 'POST',
                    url: '/panel/template/' + id,
                    data: {"_token": "{{ csrf_token() }}",'templateName': templateNameupdate,
                        'templateCategory':templateupdate},
                    success:function (data) {
                        getdata(page);
                        $('#EditTemplate').modal('hide');
                        Swal.fire(
                            'Bra jobbat !',
                            'mall uppdatering slutförd',
                            'success');
                    },
                    error:function (err) {
                        console.log(err);
                    }
                });
            });

        });

        function deleteConfirmation(id) {
            Swal.fire({
                type: 'warning',
                title: 'Är du säker?',
                text: "Du kommer inte att kunna återställa detta!",
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                cancelButtonText: 'annullera',
                confirmButtonText: 'Ja, radera det!'
            }).then(function (e) {
                if (e.value === true) {
                    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
                    $.ajax({
                        type: 'POST',
                        url: "/panel/template/delete/" + id,
                        data: {_token: '{!! csrf_token() !!}',},
                        dataType: 'JSON',
                        success: function (results) {
                            if (results.success === true) {
                                getdata(page);
                                Swal.fire(
                                    'Raderade!',
                                    'Din fil har tagits bort.',
                                    'success'
                                );
                            } else {
                                Swal.fire({
                                    icon: 'fel',
                                    title: 'Hoppsan...',
                                    text: 'Något gick fel!',
                                })
                            }
                        }
                    });
                }
                else {
                    e.dismiss;
                }}, function (dismiss) {
                return false;
            })

        }

        function addDesign(id)
        {
            var url = document.location.origin + "/panel/template/addDesign/" + id;
            location.href = url;


        }

    </script>
@endsection
