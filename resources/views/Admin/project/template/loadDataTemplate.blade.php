<table class="table table-striped projects">
    <thead>
    <tr>
        <th style="width: 1%">
            #
        </th>
        <th style="width: 20%">
            Namn
        </th>
        <th style="width: 30%">
            Title
        </th>
        <th>
            Skapad av
        </th>
        <th style="text-align:center;width: 20%">
            Drift
        </th>
    </tr>
    </thead>
    <tbody id="table_data">
    @foreach($templates as $key => $template)
    <tr>
        <td>{{ ($templates->currentpage()-1) * $templates->perpage() +  $key + 1 }}</td>
        <td>{{ $template->templateName }}</td>
        <td>{{ $template->templateCategory }}</td>
        <td>{{ $template->user_id }}</td>
        <td class="project-actions">
            <a href="#" data-toggle="modal" data-target="#EditTemplate" data-target-id="{{ $template->id }}" data-target-tempname="{{ $template->templateName }}" data-target-templatecategory="{{ $template->templateCategoryid }}" title=""><i class="fa fa-edit" aria-hidden="true"></i></a>
            <button type="button" onclick="deleteConfirmation({{ $template->id }})" class="fa fa-trash-alt sa-remove"></button>
            <a href="#" onclick="addDesign({{ $template->id }})" type="button" class="fa fa-list btn-save-template"></a>
        </td>
    </tr>
    @endforeach
    </tbody>
</table>
<div class="text-center" style="width:15%;  margin: auto; margin-top: 20px !important;">{!! $templates->links() !!}</div>
