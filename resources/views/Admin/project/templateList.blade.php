@extends('Admin.master')
@section('content-title', 'Mall lista')
@section('title', 'Mall lista')
@section('content')

    <div class="card">
        <div class="card-body p-0">
            <table class="table table-striped projects tbl-owntemplate">
                <thead>
                <tr>
                    <th style="width: 30%">
                        Mallnamn
                    </th>
                    <th style="width: 20%">
                        Historik
                    </th>

                    <th style="text-align:center;width: 20%">
                        Operationen
                    </th>
                </tr>
                </thead>
                <tbody>
                @foreach($templates as $template)
                <tr>
                    <td>{{ $template->templateName }}</td>
                    <td>{{ $template->created_at }}</td>
                    <td class="centertxt greenicon"><a href="{{ route('project.template',['id' => $template->id]) }}" title="tilldela projektet"><i class="fa fa-arrow-alt-circle-right" aria-hidden="true"></i></a></td>
                </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <!-- /.card-body -->
    </div>

@endsection
