<div class="wrapper">
    <!-- Navbar -->
    <nav class="main-header navbar navbar-expand navbar-white navbar-light">
        <!-- Left navbar links -->
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
            </li>
            <li class="nav-item d-none d-sm-inline-block">
                <a href="{{ route('Panel') }}" class="nav-link">Hem</a>
            </li>
            <li class="nav-item d-none d-sm-inline-block">
                <a href="#" data-toggle="modal" data-target="#contact" class="nav-link">Kontakt</a>
            </li>
        </ul>



{{--        <!-- SEARCH FORM -->--}}
{{--        <form class="form-inline ml-3" action="{{ route('project.search') }}" method="POST">--}}
{{--            @csrf--}}
{{--            <div class="input-group input-group-sm">--}}
{{--                <input class="form-control form-control-navbar" type="search" name="keyword" placeholder="Sök" aria-label="Search">--}}
{{--                <div class="input-group-append">--}}
{{--                    <button class="btn btn-navbar" type="submit">--}}
{{--                        <i class="fas fa-search"></i>--}}
{{--                    </button>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </form>--}}

        <!-- Right navbar links -->
        <ul class="navbar-nav ml-auto">
            <!-- Messages Dropdown Menu -->

            <!-- Notifications Dropdown Menu -->
            <li class="nav-item dropdown">
                <a class="nav-link" data-toggle="dropdown" href="#">
                    <i class="far fa-bell"></i>
                    <span class="badge badge-warning navbar-badge">15</span>
                </a>
                <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                    <span class="dropdown-item dropdown-header">15 Notifications</span>
                    <div class="dropdown-divider"></div>
                    <a href="#" class="dropdown-item">
                        <i class="fas fa-envelope mr-2"></i> 4 new messages
                        <span class="float-right text-muted text-sm">3 mins</span>
                    </a>
                    <div class="dropdown-divider"></div>
                    <a href="#" class="dropdown-item">
                        <i class="fas fa-users mr-2"></i> 8 friend requests
                        <span class="float-right text-muted text-sm">12 hours</span>
                    </a>
                    <div class="dropdown-divider"></div>
                    <a href="#" class="dropdown-item">
                        <i class="fas fa-file mr-2"></i> 3 new reports
                        <span class="float-right text-muted text-sm">2 days</span>
                    </a>
                    <div class="dropdown-divider"></div>
                    <a href="#" class="dropdown-item dropdown-footer">See All Notifications</a>
                </div>
            </li>
            <li class="nav-item">
                <form action="{{ route('logout') }}" method="post">
                    @csrf
                    <button type="submit" class="nav-link" style="background: url('{{ asset('dist/img/logout.png') }}') no-repeat;background-size: 20px; border: none; margin-top: 7px">
                    </button>
                </form>

            </li>

        </ul>
    </nav>
    <!-- /.navbar -->

    <!-- Main Sidebar Container -->
    <aside class="main-sidebar sidebar-dark-primary elevation-4">
        <!-- Brand Logo -->
        <div class="brand-link">
            <img src="{{ asset('dist/img/white-logo.png') }}" alt="ProjectAsk Logo" class="brand-image">
        </div>

        <!-- Sidebar -->
        <div class="sidebar">
            <!-- Sidebar user panel (optional) -->
            <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                <div class="image">
                    <img src="{{ asset(auth()->user()->avatar) }}" class="img-circle elevation-2" alt="User Image">
                </div>
                <div class="info">
                    <span class="d-block">{{ auth()->user()->name }}</span>
                </div>
            </div>

            <!-- Sidebar Menu -->
            <nav class="mt-2">
                <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                    <!-- Add icons to the links using the .nav-icon class
                         with font-awesome or any other icon font library -->
                    <li class="nav-item has-treeview menu-open">
                        <a href="{{ route('Panel') }}" class="nav-link">
                            <i class="fas fa-tachometer-alt"></i>
                            <p>
                                Panel
                            </p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('project.index') }}" class="nav-link">
                            <i class="fa fa-file-powerpoint-o" aria-hidden="true"></i>
                            <p>
                               Pågående Projekt
                            </p>
                        </a>
                    </li>
                    {{--                    <li class="nav-item">--}}
                    {{--                        <a href="{{ route('Organization.index') }}" class="nav-link">--}}
                    {{--                            <i class="fa fa-building" aria-hidden="true"></i>--}}
                    {{--                            <p>--}}
                    {{--                                Organisation--}}
                    {{--                            </p>--}}
                    {{--                        </a>--}}
                    {{--                    </li>--}}
                    {{--                    <li class="nav-item">--}}
                    {{--                        <a href="{{ route('Law.index') }}" class="nav-link">--}}
                    {{--                            <i class="fa fa-gavel" aria-hidden="true"></i>--}}
                    {{--                            <p>--}}
                    {{--                                Laws--}}
                    {{--                            </p>--}}
                    {{--                        </a>--}}
                    {{--                    </li>--}}
                    {{--                    <li class="nav-item has-treeview">--}}
                    {{--                        <a href="#" class="nav-link">--}}
                    {{--                            <i class="nav-icon fas fa-chart-pie"></i>--}}
                    {{--                            <p>--}}
                    {{--                                Definitions--}}
                    {{--                                <i class="right fas fa-angle-right"></i>--}}
                    {{--                            </p>--}}
                    {{--                        </a>--}}
                    {{--                        <ul class="nav nav-treeview">--}}
                    {{--                            <li class="nav-item">--}}
                    {{--                                <a href="{{ route('PBLCategory.index') }}" class="nav-link">--}}
                    {{--                                    <i class="fas fa-list nav-icon"></i>--}}
                    {{--                                    <p>PBL Kategori</p>--}}
                    {{--                                </a>--}}
                    {{--                            </li>--}}
                    {{--                            <li class="nav-item">--}}
                    {{--                                <a href="{{ route('ansvarigPart.index') }}" class="nav-link">--}}
                    {{--                                    <i class="fas fa-circle nav-icon"></i>--}}
                    {{--                                    <p>Ansvarig part</p>--}}
                    {{--                                </a>--}}
                    {{--                            </li>--}}
                    {{--                            <li class="nav-item">--}}
                    {{--                                <a href="{{ route('kontrollskede.index') }}" class="nav-link">--}}
                    {{--                                    <i class="fas fa-circle nav-icon"></i>--}}
                    {{--                                    <p>KontrollerasSkede</p>--}}
                    {{--                                </a>--}}
                    {{--                            </li>--}}
                    {{--                            <li class="nav-item">--}}
                    {{--                                <a href="{{ route('risk.index') }}" class="nav-link">--}}
                    {{--                                    <i class="fas fa-circle nav-icon"></i>--}}
                    {{--                                    <p>Risk</p>--}}
                    {{--                                </a>--}}
                    {{--                            </li>--}}
                    {{--                            <li class="nav-item">--}}
                    {{--                                <a href="{{ route('verifieringsmetod.index') }}" class="nav-link">--}}
                    {{--                                    <i class="fas fa-circle nav-icon"></i>--}}
                    {{--                                    <p>Verifieringsmetod</p>--}}
                    {{--                                </a>--}}
                    {{--                            </li>--}}
                    {{--                            <li class="nav-item">--}}
                    {{--                                <a href="{{ route('produceratIntyg.index') }}" class="nav-link">--}}
                    {{--                                    <i class="fas fa-circle nav-icon"></i>--}}
                    {{--                                    <p>Intyg</p>--}}
                    {{--                                </a>--}}
                    {{--                            </li>--}}
                    {{--                            <li class="nav-item">--}}
                    {{--                                <a href="{{ route('kraverPlatsbesok.index') }}" class="nav-link">--}}
                    {{--                                    <i class="fas fa-circle nav-icon"></i>--}}
                    {{--                                    <p>Kräver platsbesök</p>--}}
                    {{--                                </a>--}}
                    {{--                            </li>--}}
                    {{--                        </ul>--}}
                    {{--                    </li>--}}

                    <li class="nav-item">
                        <a href="{{ route('templateCategory.index') }}" class="nav-link">
                            <i class="fas fa-list-ol"></i>
                            <p>Mallar</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('template.index') }}" class="nav-link">
                            <i class="fa fa-folder"></i>
                            <p>Egna Mallar</p>
                        </a>
                    </li>
{{--                    <li class="nav-item">--}}
{{--                        <a href="{{ route('amaLaw.index') }}" class="nav-link">--}}
{{--                            <i class="fas fa-book"></i>--}}
{{--                            <p>Ama</p>--}}
{{--                        </a>--}}
{{--                    </li>--}}
                    <li class="nav-item">
                        <a href="{{ route('supportTicket.all') }}" class="nav-link">
                            <i class="fas fa-ticket-alt"></i>
                            <p>Meddelanden</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('Profile') }}" class="nav-link">
                            <i class="fa fa-user" aria-hidden="true"></i>
                            <p>
                                Konto
                            </p>
                        </a>
                    </li>

{{--                    <li class="nav-item">--}}
{{--                        <a href="{{ route('account.index') }}" class="nav-link">--}}
{{--                            <i class="fas fa-users" aria-hidden="true"></i>--}}
{{--                            <p>--}}
{{--                                Redigera Konto--}}
{{--                            </p>--}}
{{--                        </a>--}}
{{--                    </li>--}}
                    {{--                    <li class="nav-item">--}}
                    {{--                        <a href="{{ route('setting') }}" class="nav-link">--}}
                    {{--                            <i class="fa fa-tools" aria-hidden="true"></i>--}}
                    {{--                            <p>--}}
                    {{--                                Setting--}}
                    {{--                                <i class="right fas fa-angle-right"></i>--}}
                    {{--                            </p>--}}
                    {{--                        </a>--}}
                    {{--                        <ul class="nav nav-treeview">--}}
                    {{--                            <li class="nav-item">--}}
                    {{--                                <a href="{{ route('setting.contact') }}" class="nav-link">--}}
                    {{--                                    <i class="fa fa-phone-square" aria-hidden="true"></i>--}}
                    {{--                                    <p>Contact</p>--}}
                    {{--                                </a>--}}
                    {{--                            </li>--}}
                    {{--                        </ul>--}}
                    {{--                    </li>--}}
                </ul>
            </nav>
            <!-- /.sidebar-menu -->
        </div>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">@yield('content-title')</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ route('Panel') }}">Hem</a></li>
                            <li class="breadcrumb-item active">@yield('content-title')</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->
