@extends('Admin.master')
@section('content-title', 'Meddelanden')
@section('title', 'Meddelanden')
@section('content')
    <div class="row mb-3">
        <div class="col">
            <a  href="{{ route('supportTicket.create') }}" class="btn-createproject">Skicka meddelande</a>
        </div><!-- /.col -->
    </div>
    <div class="card">
        <div class="card-body p-0">
            <div id="table_data">
                <table class="table table-striped text-center projects">
                    <thead>
                    <tr>
                        <th style="width: 1%">
                            #
                        </th>
                        <th>
                            Ämne
                        </th>
                        <th>
                            Projektnamn
                        </th>
                        <th>
                            Egenkontroll
                        </th>
                        <th>
                            Ansvarig
                        </th>
                        <th>
                            Status
                        </th>
                        <th style="width:100px;">
                            Drift
                        </th>
                    </tr>
                    </thead>
                    <tbody id="table_data">
                    @foreach($tickets as $key => $ticket)
                        @php
                            $color = '';
                            if($ticket->status == 0)
                                {
                                     $color = '#dee2e6';
                                }
                            else
                                {
                                  $color = '#dee2e6';
                                }
                        @endphp
                        <tr style="background-color: {{ $color }}">
                            <td>{{ ($tickets->currentpage()-1) * $tickets->perpage() +  $key + 1 }}</td>
                            <td>{{ $ticket->subject }}</td>
                            @php
                                $projectInfo = unserialize(\App\Project\Project::where('id',$ticket->project_id)->pluck('projectInfo')->first());
                                $design = unserialize(\App\design::where('id',$ticket->design_id)->pluck('templateInformation')->first());
                            @endphp
                            <td>{{ isset($projectInfo['Projektnamn']) ? $projectInfo['Projektnamn'] : '' }}</td>
                            <td>@if($design) {{$design['BSABAMA']}} @else  -  @endif</td>

                            <td>{{ \App\User::where('id',$ticket->appUser_id)->pluck('name')->first() }}</td>
                            <td> @if($ticket->status == 0) Oläst @else svarade @endif</td>
                            <td class="project-actions">
                                <button type="button" onclick="deleteConfirmation({{ $ticket->id }})" class="fa fa-trash-alt sa-remove"></button>
                                <a href="{{ route('supportTicket.getTicket',$ticket->id) }}" type="button" class="fa fa-reply btn-save-template"></a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <div class="text-center" style="width:15%;  margin: auto; margin-top: 20px !important;">{!! $tickets->links() !!}</div>
            </div>
        </div>
    </div>
@endsection
@section('script')

    @if(Session::has('success'))
        <script>
            Swal.fire({
                position: 'top-end',
                type: 'success',
                title: 'Meddelande skickat',
                showConfirmButton: false,
                timer: 1500
            });
        </script>
    @endif

    <script type="text/javascript">
        function deleteConfirmation(id) {
            Swal.fire({
                type: 'warning',
                title: 'Är du säker?',
                text: "Raderade biljetter kan inte längre återvinnas!",
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                cancelButtonText: 'annullera',
                confirmButtonText: 'Ja, radera det!'
            }).then(function (e) {
                if (e.value === true) {
                    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
                    $.ajax({
                        type: 'POST',
                        url: "{{ route('supportTicket.delete') }}",
                        data: {_token: '{!! csrf_token() !!}',id: id},
                        dataType: 'JSON',
                        success: function (results) {
                            console.log(results);
                            if (results.success === true) {
                                Swal.fire(
                                    'Raderade!',
                                    'Biljetten i fråga har tagits bort.',
                                    'success'
                                );
                                location.reload();
                            } else {
                                Swal.fire({
                                    icon: 'fel',
                                    title: 'Hoppsan...',
                                    text: 'Något gick fel!',
                                })
                            }
                        }
                    });
                }
                else {
                    e.dismiss;
                }}, function (dismiss) {
                return false;
            })

        }
    </script>
@endsection

