@extends('Admin.master')
@section('content-title', 'Biljetter')
@section('title', 'Biljetter')
@section('content')
    <div class="card">
        <div class="card-body p-0">
            <!-- DIRECT CHAT -->
            <div class="card direct-chat direct-chat-primary">
                <div class="card-header">
                    <h3 class="card-title">Direct Chat</h3>
                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse">
                            <i class="fas fa-minus"></i>
                        </button>
                    </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <!-- Conversations are loaded here -->
                    <div class="direct-chat-messages">
                        <!-- Message. Default to the left -->
                        @foreach($tickets as $ticket)
                            @php
                                $user = \App\User::where('id',$ticket->projectEngineer_id)->first()
                            @endphp
                            @if($ticket->type == 1)
                                <div class="direct-chat-msg">
                            <div class="direct-chat-infos clearfix">
                                <span class="direct-chat-name float-left">{{ $user->name }}</span>
                                <span class="direct-chat-timestamp float-right">{{ $user->created_at }}</span>
                            </div>
                            <!-- /.direct-chat-infos -->
                            <img class="direct-chat-img" src="{{ asset( $user->avatar ) }}" alt="message user image">
                            <!-- /.direct-chat-img -->
                            <div class="direct-chat-text">
                               {{ $ticket->description }}
                            </div>
                            @if($ticket->attachment !=null)
                                <div class="show_file"><i class="fa fa-download"></i><a target="_blank" href="{{ asset($ticket->attachment) }}">show files</a></div>
                            @endif
                            <!-- /.direct-chat-text -->
                        </div>
                             @else
                            <!-- Message to the right -->
                                <div class="direct-chat-msg right">
                                    <div class="direct-chat-infos clearfix">
                                        <span class="direct-chat-name float-right">{{ $user->name }}</span>
                                        <span class="direct-chat-timestamp float-left">{{ $user->created_at }}</span>
                                    </div>
                                    <!-- /.direct-chat-infos -->
                                    <img class="direct-chat-img" src="{{ asset( $user->avatar ) }}" alt="message user image">
                                    <!-- /.direct-chat-img -->
                                    <div class="direct-chat-text">
                                        {{ $ticket->description }}
                                    </div>
                                    @if($ticket->attachment !=null)
                                        <div class="show_file"><i class="fa fa-download"></i><a target="_blank" href="{{ asset($ticket->attachment) }}">show files</a></div>
                                    @endif
                                    <!-- /.direct-chat-text -->
                                </div>
                                <!-- /.direct-chat-msg -->
                            @endif
                        @endforeach
                        <!-- /.direct-chat-msg -->

                    </div>
                    <!--/.direct-chat-messages-->
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                    <form action="{{ route('supportTicket.sendTicket') }}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="input-group">
                            <input type="hidden" name="chet_id" value="{{ $id }}">
                            <input type="hidden" name="appUser_id" value="{{ $appUser_id }}">
                            <input type="text" name="message" placeholder="Type Message ..." class="form-control">
                            <input type="file" name="attachment">
                            <span class="input-group-append">
                                <button type="submit" class="btn btn-primary">Send</button>
                    </span>
                        </div>
                    </form>
                </div>
                <!-- /.card-footer-->
            </div>
            <!--/.direct-chat -->
        </div>
    </div>
@endsection

@section('script')
    @if(Session::has('success'))
        <script>
            Swal.fire({
                position: 'top-end',
                type: 'success',
                title: 'Meddelande skickat',
                showConfirmButton: false,
                timer: 1500
            });
        </script>
    @endif
@endsection

