@extends('Admin.master')
@section('content-title', 'Skapa meddelanden ')
@section('title', 'Skapa meddelanden ')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    @include('Admin.section.errors')
                    <div class="">
                        <!-- /.tab-pane -->
                        <!-- /.tab-pane -->
                        <div class="" id="settings">
                            <form class="form-horizontal" action="{{ route('supportTicket.store') }}" method="post" enctype="multipart/form-data">
                                @csrf
                                <div class="form-group row">
                                    <label for="inputName" class="col-sm-2 col-form-label">Ämne</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="subject" class="form-control" id="inputName" placeholder="ämne">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputEmail" class="col-sm-2 col-form-label">Meddelande</label>
                                    <div class="col-sm-10">
                                        <textarea name="description" class="form-control" rows="10"></textarea>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputName2" class="col-sm-2 col-form-label">Projekt</label>
                                    <div class="col-sm-10">
                                        <select name="project_id" class="form-control" onchange="FilterDesign(this.value)">
                                            <option value="0">Gör ett val</option>
                                            @foreach(\App\Project\Project::where('engineer_id',auth()->user()->id)->get() as $Project)
                                                @php $projectInfo = unserialize($Project->projectInfo) @endphp
                                                <option value="{{ $Project->id }}">{{ $projectInfo['Projektnamn'] }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="inputName2" class="col-sm-2 col-form-label">Egenkontroll</label>
                                    <div class="col-sm-10">
                                        <select name="design_id" id="design_id" class="form-control">
                                            <option value="0">Gör ett val</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="inputName2" class="col-sm-2 col-form-label">Meddelande till </label>
                                    <div class="col-sm-10">
                                        <select name="appUser_id" id="appUser_id" class="form-control">
                                            <option value="0">Gör ett val</option>
                                            @foreach(\App\User::where('parent_organization_app',auth()->user()->parent_id)->where('is_app',1)->get() as $user)
                                                <option value="{{ $user->id }}">{{ $user->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="inputName2" class="col-sm-2 col-form-label">anknytning</label>
                                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 col-upload-in-project col-upload-inproject">
                                        <input type="file"  name="attachment" class="form-control btn-upload-in-project">
                                        <i class="fa fa-upload icon-upload" style="left: 100px" aria-hidden="true"></i>
                                        <span class="specialupload" style="left: 0">ladda upp</span>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="offset-sm-2 col-sm-10">
                                        <button type="submit" class="btn btn-danger btnprofile">Skicka</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <!-- /.tab-pane -->
                    </div>
                    <!-- /.tab-content -->
                </div><!-- /.card-body -->
            </div>
            <!-- /.nav-tabs-custom -->
        </div>
    </div>
@endsection

@php
    $array = \App\design::all();
    $designs = [];
    foreach ($array as $key => $row)
    {
        $templateInformation = unserialize($row['templateInformation']);
        $designs[$key]['id'] = $row['id'];
        $designs[$key]['project_id'] = $row['project_id'];
        $designs[$key]['templateName'] = $templateInformation['BSABAMA'];
    }
@endphp

@section('script')

    @if(Session::has('success'))
        <script>
            Swal.fire({
                position: 'top-end',
                type: 'success',
                title: 'Meddelande skickat',
                showConfirmButton: false,
                timer: 1500
            });
        </script>
    @endif

    <script type="text/javascript">
        function FilterDesign(id) {
            var design = $("#design_id");
            design.html('<option value="0">Select an Option</option>');
            $.each(<?= json_encode($designs) ?>,function (index,val) {
                if (val['project_id'] == id)
                    design.append('<option value="'+val['id']+'">'+val['templateName']+'</option>')
            })
        }
    </script>
@endsection
