@extends('Admin.master')
@section('content-title', 'Contact')
@section('title', 'Contact')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <form id="createContact" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-left" method="post" action="" enctype="multipart/form-data">
                        @csrf
                        <div class="col-sm-12 pull-left">
                            <!-- text input -->
                            <div class="form-group FormInline">
                                <label>Phone</label>
                                <input type="text" id="contactPhone" name="contactPhone" required class="form-control" placeholder="Enter ...">
                            </div>
                        </div>
                        <div class="col-sm-12 pull-left">
                            <!-- text input -->
                            <div class="form-group FormInline">
                                <label>Post</label>
                                <input type="text" id="contactPost" name="contactPost" required class="form-control" placeholder="Enter ...">
                            </div>
                        </div>
                        <div class="col-sm-12 pull-left">
                            <div class="form-group FormInline">
                                <label>ContactUsTime</label>
                                <input type="text" id="contactUsTime" name="contactUsTime" required class="form-control" placeholder="Enter ...">
                            </div>
                        </div>

                        <div class="col-sm-12 pull-left">
                            <div class="form-group FormInline FormInline-textarea">
                                <label>Address</label>
                                <textarea id="contactAddress" name="contactAddress" class="form-control"></textarea>
                            </div>
                        </div>
                        <div class="col-sm-12 pull-left">
                            <button id="btn-contact" type="submit" class="btnmodal btn-add-user btn-ContactSend">Set Contact us</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
