<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title>Print</title>
<link rel="stylesheet" href="dist/css/adminlte.min.css">
<link href="dist/css/style.css" rel="stylesheet">
<style media="print" type="text/css">
    .no-print {
        display: none;
    }
</style>
</head>
<body>
<div class="container-fluid contentForm">
    <div class="box-contentForm">
<header class="row"></header>
<main class="row">
<table class="col-xs-12 col-sm-12 col-md-12 col-lg-12 table tbl-print-form">
<thead>
<tr>
    <td colspan="15">
        <div class="design-print">
            <div>
            <img src="dist/img/favicon.png" alt="">
            </div>
        </div><!--design-print-->
        <div class="box-title-print">
        <div class="title-print">
        <h1>Villa / småhus</h1>
        </div><!--title-print-->
        </div>
    </td>
</tr>
</thead>
<tbody>
<tr>
    <td style="width:33.3333%;" colspan="5">
        <div class="info-project-col info-project-col1">
            <div><span class="t-col">Beskrivning : </span><span class="d-col">BYGGNADSNÄMNDENS ÄRDENDENR</span></div>
            <div><span class="t-col">BN-Ärendenr : </span><span class="d-col">BYGGNADSNÄMNDENS ÄRDENDENR</span></div>
            <div><span class="t-col">Gällande normer, version : </span><span class="d-col">BYGGNADSNÄMNDENS ÄRDENDENR</span></div>
            <div><span class="t-col">KA-Certifikat : </span><span class="d-col">BYGGNADSNÄMNDENS ÄRDENDENR</span></div>
            <div><span class="t-col">Mitt projektnr : </span><span class="d-col">BYGGNADSNÄMNDENS ÄRDENDENR</span></div>
            <div><span class="t-col">Bygglov : </span><span class="d-col">BYGGLOV , VAD MENAS ATT MAN SKA SKRIVA HÄR?</span></div>
            <div><span class="t-col">Bygglov start/slut : </span><span class="d-col">BYGGNADSNÄMNDENS ÄRDENDENR</span></div>
        </div>
    </td>
    <td style="width:33.3333%;" colspan="5">
        <div class="info-project-col">
            <div><span class="t-col">Publicerad och signerad : </span><span class="d-col">BYGGNADSNÄMNDENS ÄRDENDENR</span></div>
            <div><span class="t-col">Entreprenadform : </span><span class="d-col">ENTREPRENADFORM; HÄR BÖR MAN FÅ EN FÄRDIG DROPDOWN?</span></div>
            <div><span class="t-col">Byggnadsåtgärd : </span><span class="d-col">BYGGNADSNÄMNDENS ÄRDENDENR</span></div>
            <div><span class="t-col">Byggherre : </span><span class="d-col">BYGGHERRE, VAD MENAS ATT MAN SKA SKRIVA UNDER " BYGGHERRE"??</span></div>
        </div>
    </td>
    <td style="width:33.3333%;" colspan="5">
        <div class="info-project-col">
            <div><span class="t-col">Strukturbeskrivning :  </span><span class="d-col">STRUKTURBESKRIVNING, VAD ÄR DET?</span></div>
            <div><span class="t-col">Byggplatsens gatuadress : </span><span class="d-col">BYGGPLATSENS GATUADRESS</span></div>
            <div><span class="t-col">Rivningsplan : </span><span class="d-col">BYGGNADSNÄMNDENS ÄRDENDENR</span></div>
            <div><span class="t-col">Byggnadsbeskrivning : </span><span class="d-col">BYGGNADSBESKRIVNING, HUR SKA DEN SE UT??</span></div>
        </div>
    </td>
</tr>
<tr class="tblhead">
    <td colspan="2">BSAB/AMA</td>
    <td colspan="2">PBL</td>
    <td>Ansvarig</td>
    <td>Skede</td>
    <td>Risk</td>
    <td>Intyg</td>
    <td>Verifikation</td>
    <td colspan="2">Platsbesök</td>
    <td colspan="2">Funktionskrav</td>
    <td>Användare</td>
</tr>
<tr class="tblBorder">
    <td colspan="2">BJB.31 Utsättning för husunderbyggnad, grundkonstruktion o d |</td>
    <td colspan="2">7. Lämplighet för det avsedda ändamålet, </td>
    <td>3112 Utsättare</td>
    <td>Produktion</td>
    <td>Hög risk</td>
    <td></td>
    <td>Lägeskontroll</td>
    <td colspan="2">Sakkunnig skall göra platsbesök</td>
    <td colspan="2">10 kap. Genomförandet av bygg-, rivnings- och </td>
    <td>No Assign User To Point</td>
</tr>
<tr class="tblBorder">
    <td colspan="2">BJB.31 Utsättning för husunderbyggnad, grundkonstruktion o d |</td>
    <td colspan="2">7. Lämplighet för det avsedda ändamålet, </td>
    <td>3112 Utsättare</td>
    <td>Produktion</td>
    <td>Hög risk</td>
    <td></td>
    <td>Lägeskontroll</td>
    <td colspan="2">Sakkunnig skall göra platsbesök</td>
    <td colspan="2">10 kap. Genomförandet av bygg-, rivnings- och </td>
    <td>No Assign User To Point</td>
</tr>
<tr class="tblBorder">
    <td colspan="2">BJB.31 Utsättning för husunderbyggnad, grundkonstruktion o d |</td>
    <td colspan="2">7. Lämplighet för det avsedda ändamålet, </td>
    <td>3112 Utsättare</td>
    <td>Produktion</td>
    <td>Hög risk</td>
    <td></td>
    <td>Lägeskontroll</td>
    <td colspan="2">Sakkunnig skall göra platsbesök</td>
    <td colspan="2">10 kap. Genomförandet av bygg-, rivnings- och </td>
    <td>No Assign User To Point</td>
</tr>
<tr class="tblBorder">
    <td colspan="2">BJB.31 Utsättning för husunderbyggnad, grundkonstruktion o d |</td>
    <td colspan="2">7. Lämplighet för det avsedda ändamålet, </td>
    <td>3112 Utsättare</td>
    <td>Produktion</td>
    <td>Hög risk</td>
    <td></td>
    <td>Lägeskontroll</td>
    <td colspan="2">Sakkunnig skall göra platsbesök</td>
    <td colspan="2">10 kap. Genomförandet av bygg-, rivnings- och </td>
    <td>No Assign User To Point</td>
</tr>
<tr class="tblBorder">
    <td colspan="2">BJB.31 Utsättning för husunderbyggnad, grundkonstruktion o d |</td>
    <td colspan="2">7. Lämplighet för det avsedda ändamålet, </td>
    <td>3112 Utsättare</td>
    <td>Produktion</td>
    <td>Hög risk</td>
    <td></td>
    <td>Lägeskontroll</td>
    <td colspan="2">Sakkunnig skall göra platsbesök</td>
    <td colspan="2">10 kap. Genomförandet av bygg-, rivnings- och </td>
    <td>No Assign User To Point</td>
</tr>
<tr class="tblBorder">
    <td colspan="2">BJB.31 Utsättning för husunderbyggnad, grundkonstruktion o d |</td>
    <td colspan="2">7. Lämplighet för det avsedda ändamålet, </td>
    <td>3112 Utsättare</td>
    <td>Produktion</td>
    <td>Hög risk</td>
    <td></td>
    <td>Lägeskontroll</td>
    <td colspan="2">Sakkunnig skall göra platsbesök</td>
    <td colspan="2">10 kap. Genomförandet av bygg-, rivnings- och </td>
    <td>No Assign User To Point</td>
</tr>
<tr class="tblBorder">
    <td colspan="2">BJB.31 Utsättning för husunderbyggnad, grundkonstruktion o d |</td>
    <td colspan="2">7. Lämplighet för det avsedda ändamålet, </td>
    <td>3112 Utsättare</td>
    <td>Produktion</td>
    <td>Hög risk</td>
    <td></td>
    <td>Lägeskontroll</td>
    <td colspan="2">Sakkunnig skall göra platsbesök</td>
    <td colspan="2">10 kap. Genomförandet av bygg-, rivnings- och </td>
    <td>No Assign User To Point</td>
</tr>
<tr class="tblBorder">
    <td colspan="2">BJB.31 Utsättning för husunderbyggnad, grundkonstruktion o d |</td>
    <td colspan="2">7. Lämplighet för det avsedda ändamålet, </td>
    <td>3112 Utsättare</td>
    <td>Produktion</td>
    <td>Hög risk</td>
    <td></td>
    <td>Lägeskontroll</td>
    <td colspan="2">Sakkunnig skall göra platsbesök</td>
    <td colspan="2">10 kap. Genomförandet av bygg-, rivnings- och </td>
    <td>No Assign User To Point</td>
</tr>
<tr class="tblBorder">
    <td colspan="2">BJB.31 Utsättning för husunderbyggnad, grundkonstruktion o d |</td>
    <td colspan="2">7. Lämplighet för det avsedda ändamålet, </td>
    <td>3112 Utsättare</td>
    <td>Produktion</td>
    <td>Hög risk</td>
    <td></td>
    <td>Lägeskontroll</td>
    <td colspan="2">Sakkunnig skall göra platsbesök</td>
    <td colspan="2">10 kap. Genomförandet av bygg-, rivnings- och </td>
    <td>No Assign User To Point</td>
</tr>
<tr class="tblBorder">
    <td colspan="2">BJB.31 Utsättning för husunderbyggnad, grundkonstruktion o d |</td>
    <td colspan="2">7. Lämplighet för det avsedda ändamålet, </td>
    <td>3112 Utsättare</td>
    <td>Produktion</td>
    <td>Hög risk</td>
    <td></td>
    <td>Lägeskontroll</td>
    <td colspan="2">Sakkunnig skall göra platsbesök</td>
    <td colspan="2">10 kap. Genomförandet av bygg-, rivnings- och </td>
    <td>No Assign User To Point</td>
</tr>
<tr class="tblBorder">
    <td colspan="2">BJB.31 Utsättning för husunderbyggnad, grundkonstruktion o d |</td>
    <td colspan="2">7. Lämplighet för det avsedda ändamålet, </td>
    <td>3112 Utsättare</td>
    <td>Produktion</td>
    <td>Hög risk</td>
    <td></td>
    <td>Lägeskontroll</td>
    <td colspan="2">Sakkunnig skall göra platsbesök</td>
    <td colspan="2">10 kap. Genomförandet av bygg-, rivnings- och </td>
    <td>No Assign User To Point</td>
</tr>
<tr class="tblBorder">
    <td colspan="2">BJB.31 Utsättning för husunderbyggnad, grundkonstruktion o d |</td>
    <td colspan="2">7. Lämplighet för det avsedda ändamålet, </td>
    <td>3112 Utsättare</td>
    <td>Produktion</td>
    <td>Hög risk</td>
    <td></td>
    <td>Lägeskontroll</td>
    <td colspan="2">Sakkunnig skall göra platsbesök</td>
    <td colspan="2">10 kap. Genomförandet av bygg-, rivnings- och </td>
    <td>No Assign User To Point</td>
</tr>
<tr>
    <td colspan="15">
        <div class="box-question">
        <p class="question">TEKNINSKT SAMRÅD, VAD SKALL MAN SKRIVA HÄR?</p>
        <p class="answer">Avdelning A – Övergripande bestämmelser-Kontroll   -Mottagningskontroll av material och produkter   -26 §--Byggherren måste förvissa sig om att material och byggprodukter har sådana egenskaper att materialen   och produkterna korrekt använda i byggnadsverket gör att detta kan uppfylla egenskapskraven i denna författning   och i Boverkets byggregler (BFS 2011:6).   Med mottagningskontroll avses i denna författning byggherrens kontroll av att material och produkter har   förutsatta egenskaper när de tas emot på byggplatsen.   Har produkterna bedömda egenskaper enligt 18 § kan mottagningskontrollen inskränkas till identifiering,   kontroll av märkning och granskning av produktdeklarationen för att säkerställa att varorna har förutsatta   egenskaper.   Om byggprodukternas egenskaper inte är bedömda i den mening som avses i 18 § fordras verifiering genom   provning eller annan inom europeiska unionen vedertagen metod så att egenskaperna är kända och kan   värderas avseende lämplighet. (BFS 2015:6). ALLMÄNT RÅD   Byggprodukter vars egenskaper bedömts enligt alternativen a, c eller d i 18 § innebär inte att produkten   bedömts mot svenska krav på byggnadsverk i denna författning eller i Boverkets byggregler (BFS 2011:6).   Sådana bedömningar innebär endast att byggherren ska ha tilltro till den produkt- eller prestandadeklaration   av produktens egenskaper som medföljer. Med ledning av produkt- eller prestandadeklarationen kan   byggherren avgöra om byggprodukten är lämplig för aktuell användning.   För byggprodukter med bedömda egenskaper behöver byggherren inte göra någon egen provning av   dessa egenskaper. (BFS 2015:6). </p>
        </div>
    </td>
</tr>
<tr>
    <td colspan="15">
        <div class="no-print btn-printpage">
            <input class="btnPrint" type="button" value="Print" onclick="javascript:window.print()">
        </div>
    </td>
</tr>
</tbody>
<tfoot>
<tr class="tr-foot">
    <td class="foot-td2" colspan="5"><span>Projekt Ask Team</span></td>
</tr>
</tfoot>
</table>





</main>
<footer class="row"></footer>
</div>
</div>
</body>
</html>

