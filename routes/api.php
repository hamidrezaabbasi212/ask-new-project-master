<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::group(['namespace' => 'api'], function () {
    Route::post('/login', 'UserController@login');
});


Route::group(['middleware' => 'auth:api', 'namespace' => 'api'], function(){
    Route::post('/details', 'UserController@details');
    Route::post('/resetPassword', 'UserController@resetPassword');
    Route::get('/projectList','ProjectController@index')->name('project.list');
    Route::get('/projectDetails/{id}','ProjectController@projectDetails')->name('project.projectDetails');
    Route::get('/project/agentControls/{id}','ProjectController@agentControl')->name('project.agentControls');
    Route::get('/project/agentControls/assigned/{id}','ProjectController@assignedAgentControl')->name('project.assignedAgentControl');
    Route::get('/project/agentControls/details/{id}','ProjectController@agentControlDetails')->name('project.agentControlDetails');
    Route::get('/project/agentControls/goToVerify/{id}','ProjectController@agentControllerGoToVerify')->name('project.agentControllerGoToVerify');
    Route::get('/project/agentControls/extraLaw/{id}','ProjectController@extraLaw')->name('project.agentControlExtraLaw');
    Route::get('/project/attachment/files/{id}','ProjectController@attachmentFiles')->name('project.attachmentFiles');
    Route::get('/project/agentControls/history/{id}','ProjectController@agentHistory')->name('project.agentControlHistory');
    Route::post('/project/agentControls/verify','ProjectController@agentControllVerify')->name('project.agentControllVerify');
    Route::get('/project/agentControls/attachment/{id}','ProjectController@agentControlAttachment')->name('project.agentControlAttachment');


    Route::get('/project/definition/projectList','DefinitionController@getProjectList')->name('project.definition.projectList');
    Route::get('/project/definition/projectAgentControllers/{id}','DefinitionController@getAgentControllersProject')->name('project.definition.getAgentControllersProject');

    Route::get('/ticket/allTickets','ProjectController@allTickets')->name('project.allTickets');
    Route::post('/ticket/createNewTicket','ProjectController@createNewTicket')->name('project.createNewTicket');
    Route::get('/ticket/getParentTicket/{id}','ProjectController@getParentTicket')->name('project.getParentTicket');
    Route::get('/ticket/replayTicket/{id}','ProjectController@replayTicketById')->name('project.replayTicket');
    Route::post('/ticket/replayTicket','ProjectController@replayTicket')->name('project.replayTicket');

});
