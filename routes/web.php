<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/print', function () {
//    return view('print.print1');
//});

Route::get('/', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('/', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

//Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('register', 'Auth\RegisterController@register');

Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset')->name('password.update');
Route::get('password/confirm', 'Auth\ConfirmPasswordController@showConfirmForm')->name('password.confirm');
Route::post('password/confirm', 'Auth\ConfirmPasswordController@confirm');

Route::get('email/verify', 'Auth\VerificationController@show')->name('verification.notice');
Route::get('email/verify/{id}/{hash}', 'Auth\VerificationController@verify')->name('verification.verify');
Route::post('email/resend', 'Auth\VerificationController@resend')->name('verification.resend');


Route::group(['prefix' => 'panel' , 'namespace' => 'Admin' ,'middleware' => ['auth']], function () {
    Route::get('/', 'PanelController@index')->name('Panel');
    Route::get('/settings', 'PanelController@setting')->name('setting');
    Route::post('/contact', 'ContactUsController@store')->name('contact.store');
    Route::get('/contactInformation', 'ContactUsController@getContactInformation')->name('contact.getContactInformation');
    Route::get('/settings/contact', 'PanelController@contact')->name('setting.contact');

    Route::get('/project', 'Project\ProjectController@index')->name('project.index');
    Route::get('/project/create', 'Project\ProjectController@create')->name('project.create');
    Route::get('/project/createMall', 'Project\ProjectController@createMall')->name('project.createMall');
    Route::get('/project/uploadifc', 'Project\ProjectController@uploadifc')->name('project.uploadifc');
    Route::get('/project/owntemplate', 'Project\ProjectController@owntemplate')->name('project.owntemplate');
    Route::get('/project/createProject', 'Project\ProjectController@createProject')->name('createProject');
    Route::post('/project', 'Project\ProjectController@store')->name('project.store');
    Route::get('/project/{id}' , 'Project\ProjectController@edit')->name('project.edit');
    Route::get('/project/controllPlan/{id}' , 'Project\ProjectController@controllPlan')->name('project.controllPlan');
    Route::post('/project/delete/{id}' , 'Project\ProjectController@delete')->name('project.delete');
    Route::post('/project/{id}' , 'Project\ProjectController@update')->name('project.update');
    Route::post('/project/owner/delete' , 'Project\ProjectController@ownerDelete')->name('project.ownerDelete');
    //ItsTrueDelete notChange
    Route::post('/project/proj/project/delete' , 'Project\ProjectController@delete')->name('project.delete');
    Route::post('/searchProject' , 'Project\ProjectController@search')->name('project.search');
    Route::get('/project/information/getInformation/{id}','Project\ProjectController@getInformation')->name('project.getInformation');
    Route::post('/project/step/changeStep' , 'Project\ProjectController@step')->name('project.step');
    Route::get('/project/step/getStep','Project\ProjectController@getStep')->name('project.getStep');
    Route::get('/project/get/file','Project\ProjectController@getFile')->name('project.getFile');
    Route::get('/project/remove/file','Project\ProjectController@removeFile')->name('project.removeFile');
    Route::get('/project/organization/getOrganizations', 'Project\ProjectController@getOrganizations')->name('getOrganizations');
    Route::get('/project/organization/getOrga', 'Project\ProjectController@getOrga')->name('getOrga');
    Route::post('/project/organization/addOrganizations', 'Project\ProjectController@addOrganizations')->name('addOrganizations');
    Route::post('/project/organization/addUser/{id}', 'Project\ProjectController@addUser')->name('addUser');
    Route::post('/project/organization/delete/{id}', 'Project\ProjectController@deleteOrganization')->name('deleteOrganization');
    Route::post('/project/organization/update/{id}', 'Project\ProjectController@updateOrganization')->name('updateOrganization');
    //create Project base Template
    Route::get('/project/template/category/{id}','Project\ProjectController@templatecategory')->name('project.templatecategory');
    Route::get('/project/template/{id}','Project\ProjectController@template')->name('project.template');
    Route::post('/project/template/creatProject/{id}' , 'Project\ProjectController@saveBaseProject')->name('project.saveBaseProject');

    //Confirm AgentControlls Projetc
    Route::post('/project/template/kontrolansvaring/confirm' , 'Project\ProjectController@kaConfirm')->name('project.kaConfirm');
    Route::post('/project/template/bigherrConfirm/confirm' , 'Project\ProjectController@bigherrConfirm')->name('project.bigherrConfirm');
    Route::post('/project/template/bignadConfirm/confirm' , 'Project\ProjectController@bignadConfirm')->name('project.bignadConfirm');
    Route::post('/project/template/supervisingEngineer/confirm' , 'Project\ProjectController@supervisingEngineerConfirm')->name('project.supervisingEngineerConfirm');
    Route::post('/project/template/show/history' , 'Project\ProjectController@showHistory')->name('project.showHistory');




    Route::get('/getallLaws/sdfr/sdfsdf/dfgg/' , 'Project\ControllePlanController@getallLaws')->name('getallLaws');


    //KontrollPlan
    Route::get('/controllePlan' , 'Project\ControllePlanController@index')->name('controllePlan.index');
//    Route::post('/search' , 'Project\ControllePlanController@search')->name('search');
    Route::get('/search' , 'Project\ControllePlanController@search')->name('search');
    Route::get('/searchBesab' , 'Project\ControllePlanController@searchBesab')->name('searchBesab');
    Route::get('/searchAnsvarig' , 'Project\ControllePlanController@searchAnsvarig')->name('searchAnsvarig');
    Route::get('/searchAnsvarig/user' , 'Project\ControllePlanController@searchAnsvarigUser')->name('searchAnsvarigUser');
    Route::get('/searchByCategory' , 'Project\ControllePlanController@searchCategory')->name('searchCategory');

    Route::get('/organisation','OrganizationController@index')->name('Organization.index');
    Route::post('/organisation','OrganizationController@store')->name('Organization.store');
    Route::get('/organisation/{id}' , 'OrganizationController@edit')->name('Organization.edit');
    Route::post('/organisation/{id}' , 'OrganizationController@update')->name('Organization.update');
    Route::post('/organisation/delete/{id}' , 'OrganizationController@delete')->name('Organization.delete');

    Route::get('/organisation/users/{id}','OrganizationController@users')->name('Organization.users');
    Route::get('/organisation/user/get','OrganizationController@getUser')->name('Organization.getUser');
    Route::get('/organisation/user/getInfo/edit','OrganizationController@getInfoUser')->name('Organization.getInfoUser');
    Route::get('/organisation/getExperts/user','OrganizationController@getExperts')->name('Organization.getExperts');
    Route::get('/organisation/delete/expert/user','OrganizationController@deleteExperts')->name('Organization.deleteExperts');
    Route::post('/organisation/createUser/{id}','OrganizationController@createUser')->name('Organization.createUser');
    Route::post('/organisation/user/{id}','OrganizationController@userUpdate')->name('Organization.userUpdate');
    Route::post('/organisation/deleteUser/{id}','OrganizationController@deleteUser')->name('Organization.deleteUser');

    Route::get('/profile', 'PanelController@profile')->name('Profile');
    Route::post('/profile/{id}' , 'ProfileController@update')->name('ProfileUpdate');

    Route::get('/account', 'UserController@index')->name('account.index');
    Route::get('/account/create', 'UserController@create')->name('account.create');
    Route::post('/account','UserController@store')->name('account.store');
    Route::get('/account/{id}' , 'UserController@edit')->name('account.edit');
    Route::post('/account/delete/{id}' , 'UserController@delete')->name('account.delete');
    Route::post('/account/{id}' , 'UserController@update')->name('account.update');

    Route::get('/law', 'LawController@index')->name('Law.index');
    Route::get('/law/create', 'LawController@create')->name('Law.create');
    Route::post('/law', 'LawController@store')->name('Law.store');
    Route::get('/law/getLaw/{id}','LawController@getLaw')->name('Law.getLaw');
    Route::get('/law/getBesabLaw/{id}','LawController@getBesabLaw')->name('Law.getBesabLaw');


    Route::get('/definitions/PBLCategory', 'Definition\PBLCategoryController@index')->name('PBLCategory.index');
    Route::get('/definitions/PBLCategory/getData', 'Definition\PBLCategoryController@getData')->name('PBLCategory.getData');
    Route::post('/definitions/PBLCategory', 'Definition\PBLCategoryController@store')->name('PBLCategory.store');
    Route::post('/definitions/PBLCategory/{id}', 'Definition\PBLCategoryController@update')->name('PBLCategory.update');
    Route::post('/definitions/PBLCategory/delete/{id}' , 'Definition\PBLCategoryController@delete')->name('PBLCategory.delete');

    Route::get('/definitions/kontrollskede', 'Definition\KontrollskedeController@index')->name('kontrollskede.index');
    Route::get('/definitions/kontrollskede/getData', 'Definition\KontrollskedeController@getData')->name('kontrollskede.getData');
    Route::post('/definitions/kontrollskede', 'Definition\KontrollskedeController@store')->name('kontrollskede.store');
    Route::post('/definitions/kontrollskede/{id}', 'Definition\KontrollskedeController@update')->name('kontrollskede.update');
    Route::post('/definitions/kontrollskede/delete/{id}' , 'Definition\KontrollskedeController@delete')->name('kontrollskede.delete');

    Route::get('/definitions/risk', 'Definition\RiskController@index')->name('risk.index');
    Route::get('/definitions/risk/getData', 'Definition\RiskController@getData')->name('risk.getData');
    Route::post('/definitions/risk', 'Definition\RiskController@store')->name('risk.store');
    Route::post('/definitions/risk/{id}', 'Definition\RiskController@update')->name('risk.update');
    Route::post('/definitions/risk/delete/{id}' , 'Definition\RiskController@delete')->name('risk.delete');

    Route::get('/definitions/templateCategory', 'Definition\RiskController@index')->name('risk.index');
    Route::get('/definitions/risk/getData', 'Definition\RiskController@getData')->name('risk.getData');
    Route::post('/definitions/risk', 'Definition\RiskController@store')->name('risk.store');
    Route::post('/definitions/risk/{id}', 'Definition\RiskController@update')->name('risk.update');
    Route::post('/definitions/risk/delete/{id}' , 'Definition\RiskController@delete')->name('risk.delete');



    Route::get('/definitions/produceratIntyg', 'Definition\ProduceratIntygController@index')->name('produceratIntyg.index');
    Route::get('/definitions/produceratIntyg/getData', 'Definition\ProduceratIntygController@getData')->name('produceratIntyg.getData');
    Route::post('/definitions/produceratIntyg', 'Definition\ProduceratIntygController@store')->name('produceratIntyg.store');
    Route::post('/definitions/produceratIntyg/{id}', 'Definition\ProduceratIntygController@update')->name('produceratIntyg.update');
    Route::post('/definitions/produceratIntyg/delete/{id}' , 'Definition\ProduceratIntygController@delete')->name('produceratIntyg.delete');

    Route::get('/definitions/verifieringsmetod', 'Definition\VerifieringsmetodController@index')->name('verifieringsmetod.index');
    Route::get('/definitions/verifieringsmetod/getData', 'Definition\VerifieringsmetodController@getData')->name('verifieringsmetod.getData');
    Route::post('/definitions/verifieringsmetod', 'Definition\VerifieringsmetodController@store')->name('verifieringsmetod.store');
    Route::post('/definitions/verifieringsmetod/{id}', 'Definition\VerifieringsmetodController@update')->name('verifieringsmetod.update');
    Route::post('/definitions/verifieringsmetod/delete/{id}' , 'Definition\VerifieringsmetodController@delete')->name('verifieringsmetod.delete');

    Route::get('/definitions/kraverPlatsbesok', 'Definition\KraverPlatsbesokController@index')->name('kraverPlatsbesok.index');
    Route::get('/definitions/kraverPlatsbesok/getData', 'Definition\KraverPlatsbesokController@getData')->name('kraverPlatsbesok.getData');
    Route::post('/definitions/kraverPlatsbesok', 'Definition\KraverPlatsbesokController@store')->name('kraverPlatsbesok.store');
    Route::post('/definitions/kraverPlatsbesok/{id}', 'Definition\KraverPlatsbesokController@update')->name('kraverPlatsbesok.update');
    Route::post('/definitions/kraverPlatsbesok/delete/{id}', 'Definition\KraverPlatsbesokController@delete')->name('kraverPlatsbesok.delete');

    Route::get('/definitions/ansvarigPart', 'Definition\AnsvarigPartController@index')->name('ansvarigPart.index');
    Route::get('/definitions/ansvarigPart/getData', 'Definition\AnsvarigPartController@getData')->name('ansvarigPart.getData');
    Route::post('/definitions/ansvarigPart', 'Definition\AnsvarigPartController@store')->name('ansvarigPart.store');
    Route::post('/definitions/ansvarigPart/{id}', 'Definition\AnsvarigPartController@update')->name('ansvarigPart.update');
    Route::post('/definitions/ansvarigPart/delete/{id}', 'Definition\AnsvarigPartController@delete')->name('ansvarigPart.delete');
    Route::post('/definitions/import/ansvarigPart', 'Definition\AnsvarigPartController@import')->name('ansvarigPart.import');


    Route::get('/definitions/templateCategory', 'Definition\TemplateCategoryController@index')->name('templateCategory.index');

    Route::get('/definitions/templateCategory/getData', 'Definition\TemplateCategoryController@getData')->name('templateCategory.getData');

    Route::post('/definitions/templateCategory', 'Definition\TemplateCategoryController@store')->name('templateCategory.store');
    Route::post('/definitions/templateCategory/{id}', 'Definition\TemplateCategoryController@update')->name('templateCategory.update');
    Route::post('/definitions/templateCategory/delete/{id}' , 'Definition\TemplateCategoryController@delete')->name('templateCategory.delete');

    Route::post('/design','DesignController@store')->name('design.store');
    Route::get('/design/getTemplate/{id}','DesignController@getTemplate')->name('design.getTemplate');
    Route::get('/design/getTemp/{id}','DesignController@getTemp')->name('design.getTemp');
    Route::post('/design/{id}','DesignController@update')->name('design.update');
    Route::post('/design/delete/{id}', 'DesignController@delete')->name('design.delete');
    Route::get('/design/getTemplateKontrollPlan/{id}','DesignController@getTemplateKontrollPlan')->name('design.getTemplateKontrollPlan');

    Route::get('/design/KontrollPlan/print/{id}','DesignController@printKontrollPlan')->name('design.KontrollPlan.print');
    Route::post('/design/KontrollPlan/print/back','DesignController@backControllPlan')->name('design.KontrollPlan.backControllPlan');


    Route::get('/design/getUserOrganization/{id}','DesignController@getUserOrganization')->name('design.getUserOrganization');
    Route::post('/design/user/assignUser/{id}','DesignController@assignUser')->name('design.assignUser');
    Route::post('/design/search/ControllePlan','DesignController@searchControllePlan')->name('design.searchControllePlan');
    Route::get('/design/KontrollPlan/print/all/{id}','DesignController@printAllKontrollPlan')->name('design.KontrollPlan.printAll');
    Route::get('/design/KontrollPlan/attachment/print/{id}','DesignController@printAttachmentKontrollPlan')->name('design.KontrollPlan.printAttachment');
    Route::post('/design/KontrollPlan/uploadAttachmentFileUser', 'DesignController@uploadAttachmentFileUser')->name('template.uploadAttachmentFileUser');
    Route::post('/design/KontrollPlan/getAttachmentFile', 'DesignController@getAttachment')->name('template.getAttachment');
    Route::post('/design/KontrollPlan/AttachmentFile/delete/{id}','DesignController@deleteFileAttachment')->name('template.deleteFileAttachment');
    Route::post('/design/KontrollPlan/uploadAttachmentFileUser/edit', 'DesignController@uploadAttachmentFileUserEdit')->name('template.uploadAttachmentFileUserEdit');
    Route::post('/design/KontrollPlan/history/getHistory', 'DesignController@getHistory')->name('template.getHistory');
    Route::post('/design/KontrollPlan/status/change' , 'DesignController@changeStatus')->name('template.changeStatus');

    //filter design
    Route::get('/design/filter/KontrollplanAvtal/{id}' , 'DesignController@KontrollplanAvtal')->name('template.KontrollplanAvtal');
    Route::get('/design/filter/KontrollplanPBL/{id}' , 'DesignController@KontrollplanPBL')->name('template.KontrollplanPBL');
    Route::get('/design/filter/refresh/{id}' , 'DesignController@refresh')->name('template.refresh');

    Route::get('/template','TemplateController@index')->name('template.index');
    Route::get('/template/getData', 'TemplateController@getData')->name('template.getData');
    Route::post('/template','TemplateController@store')->name('template.store');
    Route::post('/template/addTemplate','TemplateController@addTemplate')->name('template.addTemplate');
    Route::post('/template/{id}','TemplateController@update')->name('template.update');
    Route::post('/template/delete/{id}', 'TemplateController@delete')->name('template.delete');

    Route::get('/template/addDesign/{id}', 'TemplateController@addDesign')->name('template.addDesign');
    Route::get('/template/getTemplate/{id}','TemplateController@getTemplate')->name('template.getTemplate');
    Route::get('/template/getTemp/{id}','TemplateController@getTemp')->name('template.getTemp');
    Route::post('/template/design/saveDesign', 'TemplateController@saveDesign')->name('template.saveDesign');
    Route::post('/template/design/{id}','TemplateController@designUpdate')->name('template.designUpdate');
    Route::post('/template/design/delete/{id}', 'TemplateController@designDelete')->name('template.designDelete');



    Route::get('/advance/import-law', 'Project\AdvanceLawController@import')->name('advance.import');
    Route::post('/advance/import-law', 'Project\AdvanceLawController@store')->name('advance.store');
    Route::get('/advance/law', 'AmaDesignController@search')->name('advance.search');
    Route::get('/advance/get-law/{id}', 'AmaDesignController@getLaw')->name('advance.getLaw');
    Route::post('/advance/law', 'AmaDesignController@store')->name('advance.store');
    Route::get('/advance/get-template/{id}', 'AmaDesignController@getTemplate')->name('advance.getTemplate');
    Route::get('/advance/get-design-template/{id}', 'AmaDesignController@getDesignTemplate')->name('advance.getDesignTemplate');
    Route::post('/advance/design-template/{id}', 'AmaDesignController@update')->name('advance.DesignTemplateUpdate');
    Route::post('/advance/design-template/delete/{id}', 'AmaDesignController@delete')->name('advance.DesignTemplateDelete');



    Route::get('/supportTicket','TicketController@index')->name('supportTicket.all');
    Route::get('/supportTicket/tickets/{id}','TicketController@getTicket')->name('supportTicket.getTicket');
    Route::post('/supportTicket/tickets/sendTicket','TicketController@sendTicket')->name('supportTicket.sendTicket');
    Route::post('/supportTicket/tickets/delete','TicketController@delete')->name('supportTicket.delete');
    Route::get('/supportTicket/newTicket','TicketController@create')->name('supportTicket.create');
    Route::post('/supportTicket/tickets/send','TicketController@store')->name('supportTicket.store');


    Route::get('/amaLaw','AmaLawController@index')->name('amaLaw.index');
    Route::get('/amaLaw/create','AmaLawController@create')->name('amaLaw.create');
    Route::post('/amaLaw/store','AmaLawController@store')->name('amaLaw.store');
    Route::get('/amaLaw/edit/{id}','AmaLawController@edit')->name('amaLaw.edit');
    Route::post('/amaLaw/delete','AmaLawController@delete')->name('amaLaw.delete');

    //for agentkontroll
    Route::get('/amaLaw/agentControll/show/{id}','AmaLawController@agentControllAmaLaw')->name('amaLaw.agentControllAmaLaw');


    Route::resource('ka' , 'KaController')->except(['show']);
    Route::post('/ka/store/title' ,'KaController@storeTitle')->name('ka.storeTitle');
    Route::get('/ka/search' ,'KaController@kaSearch')->name('ka.kaSearch');
    Route::get('/ka/searchAll' ,'KaController@searchAll')->name('ka.searchAll');
    Route::post('/ka/design','KaController@design')->name('ka.design');
    Route::get('/ka/design/getTemplate/{id}','KaController@getTemplate')->name('ka.getTemplate');
    Route::get('/ka/getLaw/{id}','KaController@getLaw')->name('ka.getLaw');
    Route::get('/ka/getBesabLaw/{id}','KaController@getBesabLaw')->name('ka.getBesabLaw');
    Route::post('/ka/getRobrikLaw','KaController@getRobrikLaw')->name('ka.getRobrikLaw');
    Route::get('/ka/getTemp/{id}','KaController@getTemp')->name('ka.getTemp');
    Route::post('/ka/{id}','KaController@update')->name('ka.update');
    Route::post('/ka/delete/{id}', 'KaController@delete')->name('ka.delete');

    Route::get('/ka/getAllTemplate/{id}','KaController@getAllTemplate')->name('ka.getAllTemplate');

    Route::get('/ka/searchByCategory' , 'KaController@searchCategory')->name('ka.searchCategory');
});

Route::post('/import-laws' ,'ImportController@import')->name('law.import-laws');
Route::post('/import-laws/update' ,'ImportController@update')->name('law.import-laws.update');
Route::get('/import-laws/laws' ,'ImportController@laws')->name('law.import-laws.laws');
Route::get('/import-laws/infoProject' ,'ImportController@infoProject')->name('law.import-laws.infoProject');

Route::get('/home', 'HomeController@index')->name('home');


Route::get('/search-in-map','Admin\Project\ProjectController@searchInMap')->name('searchInMap');
