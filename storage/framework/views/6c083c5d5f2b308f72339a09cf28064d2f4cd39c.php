<?php $__env->startSection('content-title', 'Panel'); ?>
<?php $__env->startSection('title', 'Panel'); ?>
<?php $__env->startSection('content'); ?>
    <div class="row mb-3">
        <div class="col">
            <a class="btn-createproject" href="<?php echo e(route('project.create')); ?>">Nytt Projekt</a>
        </div><!-- /.col -->
    </div>

    <div class="row">
        <div class="col-12 col-sm-6 col-md-3">
            <a href="<?php echo e(route('project.index')); ?>"><div class="info-box">
                    <span class="info-box-icon"><i class="fas fa-tasks"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Pågående Projekt</span>
                        <span class="info-box-number"><?php echo e(number_format($projectCount)); ?></span>
                    </div>
                    <!-- /.info-box-content -->
                </div></a>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-12 col-sm-6 col-md-3">
            <a href="<?php echo e(route('project.index')); ?>"><div class="info-box">
                    <span class="info-box-icon"><i class="fas fa-thumbs-up"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Avslutade Projekt</span>
                        <span class="info-box-number"><?php echo e(number_format($projectCompleteCount)); ?></span>
                    </div>
                    <!-- /.info-box-content -->
                </div></a>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->

        <!-- fix for small devices only -->
        <div class="clearfix hidden-md-up"></div>

        <div class="col-12 col-sm-6 col-md-3">
            <a href="<?php echo e(route('account.index')); ?>"><div class="info-box mb-3">
                    <span class="info-box-icon"><i class="fas fa-users"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text">Aktiva Användare</span>
                        <span class="info-box-number"><?php echo e(number_format($usersCount)); ?></span>
                    </div>
                    <!-- /.info-box-content -->
                </div></a>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-12 col-sm-6 col-md-3">
            <a href="<?php echo e(route('Organization.index')); ?>"><div class="info-box mb-3">
                    <span class="info-box-icon"><i class="fas fa-building"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text">Aktiva Organisationer</span>
                        <span class="info-box-number"><?php echo e(number_format($organizationCount)); ?></span>
                    </div>
                    <!-- /.info-box-content -->
                </div></a>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->
    </div>



    <div class="row">
        <div class="modal fade" id="contact" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    </div>
                    <div class="modal-body">
                        <div class="logomodal">
                            <img src="<?php echo e(asset('dist/img/favicon.png')); ?>" alt="">
                        </div>
                        <div class="title-modal"><span>Support</span></div>
                        <form id="createContact" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-left" method="post" action="" enctype="multipart/form-data">
                            <?php echo csrf_field(); ?>
                            <div class="col-sm-6 pull-left">
                                <!-- text input -->
                                <div class="form-group">
                                    <label>Telefon</label>
                                    <input type="text" id="contactPhone" name="contactPhone" required class="form-control" placeholder="Enter ...">
                                </div>
                            </div>
                            <div class="col-sm-6 pull-left">
                                <!-- text input -->
                                <div class="form-group">
                                    <label>Email</label>
                                    <input type="text" id="contactPost" name="contactPost" required class="form-control" placeholder="Enter ...">
                                </div>
                            </div>
                            <div class="col-sm-12 pull-left">
                                <div class="form-group">
                                    <label>Öppettider</label>
                                    <input type="text" id="contactUsTime" name="contactUsTime" required class="form-control" placeholder="Enter ...">
                                </div>
                            </div>

                            <div class="col-sm-12 pull-left">
                                <div class="form-group">
                                    <label>Address</label>
                                    <textarea id="contactAddress" name="contactAddress" class="form-control"></textarea>
                                </div>
                            </div>



                        </form>
                    </div>
                </div>
            </div>
        </div>


        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding sec-projects">
            <?php $__currentLoopData = $projects; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $project): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <?php $prjectInformation = unserialize($project->projectInfo)  ?>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 col-project">
                    <div class="col-md-12 box-project">
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 l-box-project">
                            <div class="name-project">
                                <h3><a style="color: #1b1e21" href="<?php echo e(route('project.controllPlan' , ['id'=>$project['id']])); ?>"><?php echo e($prjectInformation['Projektnamn'] !=null ? $prjectInformation['Projektnamn'] : 'inget namn'); ?></a></h3>
                            </div>
                            <span class="span-boxproject"><?php echo e(step($project->step)); ?></span>
                            <ul>
                                <li><i class="fa fa-check-square" aria-hidden="true"></i><?php echo e(percent($project->step)); ?> Pågående egenkontroller</li>
                                <li><i class="fa fa-clock-o" aria-hidden="true"></i>15 Avslutade egenkontroller</li>
                                <li><i class="fa fa-info-circle" aria-hidden="true"></i>7 Ej godkända egenkontroller</li>
                            </ul>
                        </div><!--l-box-project-->
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 r-box-project">
                            <div class="img-box-project">
                                <img src="<?php echo e(asset($prjectInformation['projectImage'] ? $prjectInformation['projectImage'] : 'dist/img/project.jpg')); ?>" alt="">
                            </div>
                            
                            
                            
                        </div><!--r-box-project-->
                    </div><!--box-project-->
                </div><!--col-project-->
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            <?php
                function percent($value)
               {
                   switch ($value) {
                       case "1":
                           echo "15%";
                           break;
                       case "2":
                           echo "30%";
                           break;
                       case "3":
                           echo "45%";
                           break;
                       case "4":
                           echo "60%";
                           break;
                       case "5":
                           echo "75%";
                           break;
                       case "6":
                           echo "85%";
                           break;
                       case "7":
                           echo "100%";
                           break;

                       default:
                           echo "Your favorite color is neither red, blue, nor green!";
                   }
               }

           function step($value)
        {
                           switch ($value) {
                    case "1":
                        echo "Pågående projekt";
                        break;
                    case "2":
                        echo "Tekniskt samråd";
                        break;
                    case "3":
                        echo "Startbesked";
                        break;
                    case "4":
                        echo "Slutsamråd";
                        break;
                    case "5":
                        echo "Interimistiskt slutbesked";
                        break;
                    case "6":
                        echo "Slutbesked";
                        break;
                    case "7":
                        echo "Projekt slut";
                        break;

                    default:
                        echo "Your favorite color is neither red, blue, nor green!";
                }
        }
            ?>
        </div><!--sec-projects-->
    </div>

    
    
    
    
    

    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    

    
    
    
    
    
    
    
    
    
    
    

    
    
    
    
    
    
    
    

    
    
    
    
    
    
    

    
    
    
    
    
    
    
    

    

    
    
    
    
    
    
    

    
    
    
    
    
    
    <!-- Main row -->
    <div class="row">
        <!-- Left col -->
        <div class="col-md-8">
            <!-- MAP & BOX PANE -->

            <!-- /.card -->

            <!-- /.row -->

            <!-- TABLE: LATEST ORDERS -->

            <!-- /.card -->
        </div>
        <!-- /.col -->

        <!-- /.col -->
    </div>
    <!-- /.row -->
<?php $__env->stopSection(); ?>

<?php $__env->startSection('script'); ?>
    <script type="text/javascript">

        $(document).ready(function () {

            $('#createContact').submit(function(event) {
                event.preventDefault();
                var formData = new FormData(this);
                $.ajax({
                    type: 'POST',
                    url: '<?php echo e(route('contact.store')); ?>',
                    data: formData,
                    processData: false,
                    contentType: false,
                    success:function (data) {
                        console.log(data);
                        Swal.fire(
                            'Bra jobbat !',
                            'kontakta oss registreringen slutförd',
                            'success');
                        $('#contact').modal('hide');
                    },
                    error:function (err) {
                        console.log(err);
                    }
                });
            });

            $("#contact").on("show.bs.modal", function (e) {

                $.ajax({
                    type : 'get',
                    url : '<?php echo e(route('contact.getContactInformation')); ?>',
                    success:function (data) {
                        $('#contactAddress').text(data['address']);
                        $('#contactPhone').val(data['phone']);
                        $('#contactPost').val(data['post']);
                        $('#contactUsTime').val(data['contactUsTime']);
                    }
                });
            });
        });


    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('Admin.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\xampp\htdocs\askprojectNew\resources\views/Admin/panel.blade.php ENDPATH**/ ?>