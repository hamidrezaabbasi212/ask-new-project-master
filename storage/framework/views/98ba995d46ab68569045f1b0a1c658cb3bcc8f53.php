<?php $__env->startSection('content-title', $projectName); ?>
<?php $__env->startSection('title', $projectName); ?>
<?php $__env->startSection('content'); ?>
    <?php echo csrf_field(); ?>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 box-organization">
            <div id="tabs-Kontrol" class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation">
                        <a class="active" href="#desingka" aria-controls="desingka" role="tab" data-toggle="tab">Design
                            KA</a></li>
                    <li role="presentation">
                        <a href="#userform" aria-controls="userform" role="tab" data-toggle="tab">Design EK</a>
                    </li>
                    <li role="presentation">
                        <a href="#Kontrollplan" aria-controls="Kontrollplan" role="tab"
                           data-toggle="tab">Kontrollplan</a></li>
                    
                    
                    <li role="presentation">
                        <a href="#Organisation" aria-controls="Organisation" role="tab"
                           data-toggle="tab">Organisation</a></li>
                    <li role="presentation">
                        <a href="#Information" aria-controls="Information" role="tab" data-toggle="tab">Projekt Info</a>
                    </li>
                </ul>

                <div class="tab-content col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding">

                    <div role="tabpanel" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding tab-pane"
                         id="Organisation">
                        <div class="btnadd-organization">
                            <a href="#" class="" data-toggle="modal" data-target="#neworganization">+ Lägg till ny
                                organisation</a>

                            <div class="modal fade" id="neworganization" tabindex="-1" role="dialog"
                                 aria-labelledby="myModalLabel">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">×</span></button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="logomodal">
                                                <img src="<?php echo e(asset('dist/img/favicon.png')); ?>" alt="">
                                            </div>
                                            <div class="title-modal"><span>Lägg till organisation</span></div>
                                            <form method="post" id="organizationForm"
                                                  class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-left">
                                                <div class="col-sm-6 pull-left">
                                                    <!-- text input -->
                                                    <div class="form-group">
                                                        <label>Namn</label>
                                                        <input type="text" id="organizationName" name="organizationName"
                                                               required class="form-control" placeholder="Enter ...">
                                                    </div>
                                                </div>
                                                <div class="col-sm-6 pull-left">
                                                    <!-- text input -->
                                                    <div class="form-group">
                                                        <label>Ansvarig</label>
                                                        <input type="text" id="organizationDescription"
                                                               name="organizationDescription" required
                                                               class="form-control" placeholder="Enter ...">
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 pull-left">
                                                    <!-- text input -->
                                                    <div class="form-group">
                                                        <label>Kontaktperson</label>
                                                        <input type="text" id="organizationContact"
                                                               name="organizationContact" required class="form-control"
                                                               placeholder="Enter ...">
                                                    </div>
                                                </div>

                                                <div class="col-sm-6 pull-left">
                                                    <!-- text input -->
                                                    <div class="form-group">
                                                        <label>Mailadress</label>
                                                        <input type="text" id="organizationMailadress"
                                                               name="organizationMailadress" required
                                                               class="form-control" placeholder="Enter ...">
                                                    </div>
                                                </div>

                                                <div class="col-sm-6 pull-left">
                                                    <!-- text input -->
                                                    <div class="form-group">
                                                        <label>Mobil tel</label>
                                                        <input type="text" id="organizationMobiltel"
                                                               name="organizationMobiltel" required class="form-control"
                                                               placeholder="Enter ...">
                                                    </div>
                                                </div>

                                                <div class="col-sm-12 pull-left">
                                                    <label>Ladda upp logotyp</label>
                                                    <input type="file" onchange="fileSelect('organizationLogo')"
                                                           id="organizationLogo" name="organizationLogo"
                                                           class="hideinput">
                                                    <span class="special-upload">Ladda upp</span>
                                                    <span class="shownameupload">show File Name upload</span>
                                                </div>
                                                <div class="col-sm-12 pull-left">
                                                    <button id="saveOrganization" type="submit" class="btnmodal">Lägg
                                                        till organisation
                                                    </button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="modal fade" id="editorganization" tabindex="-1" role="dialog"
                                 aria-labelledby="myModalLabel">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">×</span></button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="logomodal">
                                                <img src="<?php echo e(asset('dist/img/favicon.png')); ?>" alt="">
                                            </div>
                                            <div class="title-modal"><span>Edit Organisation</span></div>
                                            <form id="editOrganizationForm" method="post"
                                                  class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-left">
                                                <div class="col-sm-6 pull-left">
                                                    <!-- text input -->
                                                    <div class="form-group">
                                                        <label>namn</label>
                                                        <input type="text" id="organizationNameEdit"
                                                               name="organizationNameEdit" required class="form-control"
                                                               placeholder="Enter ...">
                                                    </div>
                                                </div>
                                                <div class="col-sm-6 pull-left">
                                                    <!-- text input -->
                                                    <div class="form-group">
                                                        <label>Beskrivning</label>
                                                        <input type="text" id="organizationDescriptionEdit"
                                                               name="organizationDescriptionEdit" required
                                                               class="form-control" placeholder="Enter ...">
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 pull-left">
                                                    <!-- text input -->
                                                    <div class="form-group">
                                                        <label>Kontakt</label>
                                                        <input type="text" id="organizationContactEdit"
                                                               name="organizationContact" required class="form-control"
                                                               placeholder="Enter ...">
                                                    </div>
                                                </div>
                                                <div class="col-sm-6 pull-left">
                                                    <!-- text input -->
                                                    <div class="form-group">
                                                        <label>Mailadress</label>
                                                        <input type="text" id="organizationMailadressEdit"
                                                               name="organizationMailadressEdit" required
                                                               class="form-control" placeholder="Enter ...">
                                                    </div>
                                                </div>

                                                <div class="col-sm-6 pull-left">
                                                    <!-- text input -->
                                                    <div class="form-group">
                                                        <label>Mobil tel</label>
                                                        <input type="text" id="organizationMobiltelEdit"
                                                               name="organizationMobiltelEdit" required
                                                               class="form-control" placeholder="Enter ...">
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 pull-left">
                                                    <label>Upload Your Logo</label>
                                                    <input type="file" id="organizationLogo" name="organizationLogo"
                                                           class="hideinput">
                                                    <span class="special-upload">Upload</span>
                                                    <span class="shownameupload">show File Name upload</span>
                                                </div>
                                                <div class="col-sm-12 pull-left">
                                                    <button type="submit" class="btnmodal">uppdatera organisationen
                                                    </button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="modal fade" id="CreatUser" tabindex="-1" role="dialog"
                                 aria-labelledby="myModalLabel">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">×</span></button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="logomodal">
                                                <img src="<?php echo e(asset('dist/img/favicon.png')); ?>" alt="">
                                            </div>
                                            <div class="title-modal"><span>Skapa användare</span></div>
                                            <form id="createUserForm"
                                                  class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-left"
                                                  method="post" action="" enctype="multipart/form-data">
                                                <?php echo csrf_field(); ?>
                                                <div class="col-sm-6 pull-left">
                                                    <!-- text input -->
                                                    <div class="form-group">
                                                        <label>Name</label>
                                                        <input type="text" id="nameUser" name="nameUser" required
                                                               class="form-control" placeholder="Enter ...">
                                                    </div>
                                                </div>
                                                <div class="col-sm-6 pull-left">
                                                    <!-- text input -->
                                                    <div class="form-group">
                                                        <label>Tel</label>
                                                        <input type="text" id="phoneUser" name="phoneUser" required
                                                               class="form-control" placeholder="Enter ...">
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 pull-left">
                                                    <div class="form-group">
                                                        <label>E-Post</label>
                                                        <input type="text" id="emailUser" name="emailUser" required
                                                               class="form-control" placeholder="Enter ...">
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 pull-left">
                                                    <div class="form-group">
                                                        <label>Legitimation Card</label>
                                                        <input type="text" id="legitimationCard" name="legitimationCard"
                                                               required class="form-control" placeholder="Enter ...">
                                                    </div>
                                                </div>

                                                
                                                
                                                
                                                
                                                
                                                
                                                
                                                
                                                
                                                
                                                

                                                <div class="col-sm-12 pull-left">
                                                    <div class="form-group">
                                                        <label>Search Ansvarig part</label>
                                                        <input type="text" id="searchAnsvarigUser"
                                                               class="form-control mb-3" placeholder="Enter ...">
                                                        <select class="form-control" name="AnsvarigPartUser[]"
                                                                id="AnsvarigPartUser" multiple>
                                                            <option value="0">Option 1</option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-sm-12 pull-left">
                                                    <label>Lägg till foto/Avatar</label>
                                                    <input type="file" onchange="fileSelect('avatarUser')"
                                                           id="avatarUser" name="avatarUser" class="hideinput">
                                                    <span class="special-upload">Upload</span>
                                                    <span class="shownameupload">show File Name upload</span>
                                                </div>
                                                <div class="col-sm-12 pull-left">
                                                    <button id="btnAddUserToOrganization" type="submit"
                                                            class="btnmodal btn-add-user">Lägg till ansvarig part
                                                    </button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>


                        </div>
                        <div class="table-responsive">
                            <table id="tbl_organization" style="width: 100%"
                                   class="table table-bordered table-striped mt-3 tbl-Kontrollplan">
                                <thead>
                                <tr>
                                    <th>Namn</th>
                                    <th>Ansvarig</th>
                                    <th>Mailadress</th>
                                    <th>Kontaktperson</th>
                                    <th>Foto</th>
                                    <th>Drift</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>

                    <div role="tabpanel" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding tab-pane active"
                         id="desingka">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding">
                            <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 float-left">
                                <!-- stage 1 !-->
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-left nopadding">
                                    <div class="steg-div">
                                        <span>Steg 1 </span>
                                    </div>
                                    <form class="form-inline">
                                        <div class="input-group input-group-sm poscenter poscenter2">
                                            <select style="width:79%;"
                                                    class="form-control FormControl  form-control-navbar select22"
                                                    name="robrik"
                                                    id="robrik">
                                                <option value="0">Välj ett alternativ</option>
                                                <option value="NEW">Lägg till rubrik</option>
                                                <?php $__currentLoopData = \App\Robrik::all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $robrik): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <option value="<?php echo e($robrik->id); ?>"><?php echo e($robrik->title); ?></option>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </select>
                                        </div>
                                    </form>
                                </div>
                                <!-- end stage 1 !-->

                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 pull-left nopadding">
                                    <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 rlogo-formuser btnFormUser">
                                        <button type="button" onclick="filter11(1);btnactive1(this)" id="btnbbr11"
                                                class="btnstyle">BBR<span id="bbr1"
                                                                          class="ml-1 mb-1 badge badge bg-warning"></span>
                                        </button>
                                        <button type="button" onclick="filter11(6);btnactive1(this)" class="btnstyle">
                                            BSAB<span
                                                id="bsab11" class="ml-1 mb-1 badge bg-warning"></span></button>
                                    </div><!--rlogo-formuser-->
                                </div>

                                <!-- Search 1 !-->
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-left nopadding">
                                    <div class="steg-div">
                                        <span>Steg 2 </span>
                                    </div>
                                    <form class="form-inline">
                                        <div class="input-group input-group-sm poscenter poscenter2">
                                            <input class="form-control FormControl  form-control-navbar" type="search"
                                                   id="searchKAInput"
                                                   placeholder="Namnge Kontroll / Sok Kontroll" aria-label="Search">
                                            <div class="input-group-append search-top-Kontrollplan">
                                                <button class="btn btn-navbar" type="button" id="searchButtonInput">
                                                    <i class="fas fa-search"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <!--End Search 1 !-->

                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-left nopadding">
                                    <label>Kontrollpunkt/Egenkontroll</label>
                                    <div class="showrulesKa" id="showrulesKa" style="overflow-y: scroll">

                                    </div>
                                </div>

                            </div>

                            <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7 float-left nopadding">
                                <div class="steg-div">
                                    <span>Steg 3 </span>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 r1-formuser">
                                    <h4>Egenskap</h4>
                                    <form id="frmcate"
                                          class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-rinfo pull-left">
                                        <div class="col-sm-12 pull-left">
                                            <div class="form-group">
                                                <label>Kontrollpunkt</label>
                                                <input type="text" id="besabtext1" name="BSABAMA" class="form-control"
                                                       placeholder="Enter ...">
                                            </div>
                                        </div>
                                        <div class="col-sm-12 pull-left">
                                            <div class="form-group">
                                                <label style="width:20%;">PBL Kategori</label>
                                                <select style="width:79%;" class="form-control" name="PBLKategori"
                                                        id="PBLKategori1">
                                                    <option value="0">Välj ett alternativ</option>
                                                    <?php $__currentLoopData = \App\Definitions\PBLCategory::latest()->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $pblCategory): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <option
                                                            value="<?php echo e($pblCategory->id); ?>"><?php echo e($pblCategory->title); ?></option>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                </select>
                                            </div>
                                        </div>
                                        <span class="table-addItem float-left" onclick="addAnsvaringPart()"><a href="#"
                                                                                                               class="text-success"><i
                                                    class="fa fa-plus fa-2x"
                                                    aria-hidden="true"></i></a></span>

                                        <div class="col-sm-12 pull-left" id="AnsvarigPartParent1">









                                        </div>

                                        <div id="placeAnsvarigPart">

                                        </div>

                                        <div class="col-sm-12 pull-left" style="padding-right:0;">
                                            <div class="form-group half-groupform">
                                                <label class="lbl1" style="width:36%;">Kontrollskede</label>
                                                <select onchange="showinput5ka(this);" style="width:62%;"
                                                        class="form-control select1" name="Kontrollskede"
                                                        id="Kontrollskede1">
                                                    <option value="0">Välj ett alternativ</option>
                                                    <?php $__currentLoopData = \App\Definitions\Kontrollskede::latest()->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $KontrollSkede): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <option
                                                            value="<?php echo e($KontrollSkede->id); ?>"><?php echo e($KontrollSkede->title); ?></option>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    <option value="500">Övrig</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-12  input-Other5ka pull-left" id="kontrollskedeTextParent1"
                                             style="padding-right:0; display: none">
                                            <input type="text" id="kontrollskedeText1" name="kontrollskedeText"
                                                   class="form-control" placeholder="Enter ...">
                                        </div>
                                        <div class="col-sm-12 pull-left mt-3">
                                            <div class="form-group half-groupform">
                                                <label style="width:17%;">Risk</label>
                                                <select onchange="showinputka(this);" class="form-control select2"
                                                        name="Risk" id="Risk1">
                                                    <option value="0">Välj ett alternativ</option>
                                                    <?php $__currentLoopData = \App\Definitions\Risk::latest()->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $risk): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <option value="<?php echo e($risk->id); ?>"><?php echo e($risk->title); ?></option>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    <option value="500">Övrig</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 input-Otherka pull-left">
                                            <input type="text" id="riskText1" name="riskText" class="form-control"
                                                   placeholder="Enter ...">
                                        </div>
                                        <div class="col-sm-12 pull-left" style="display: none;" id="riskdesdiv1">
                                            <input type="text" id="riskdescription1" name="riskdescription"
                                                   class="form-control inputdescription" placeholder="beskrivning ...">
                                        </div>
                                        <div class="col-sm-12 pull-left">
                                            <div class="form-group">
                                                <label style="width:25%;">Producerat intyg</label>
                                                <select onchange="showinput2ka(this);" style="width:74%;"
                                                        class="form-control" name="ProduceratIntyg" id="Producerat1">
                                                    <option value="0">Välj ett alternativ</option>
                                                    <?php $__currentLoopData = \App\Definitions\ProduceratIntyg::latest()->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $produceIntyg): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <option
                                                            value="<?php echo e($produceIntyg->id); ?>"><?php echo e($produceIntyg->title); ?></option>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    <option value="500">Övrig</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 input-Other2ka pull-left">
                                            <input type="text" id="ProduceratIntygText1" name="ProduceratIntygText"
                                                   class="form-control" placeholder="Enter ...">
                                        </div>
                                        <div class="col-sm-12 pull-left" style="display: none;" id="PIntydesdiv1">
                                            <input type="text" id="PIntygdescription1" name="PIntygdescription"
                                                   class="form-control inputdescription" placeholder="beskrivning ...">
                                        </div>
                                        <div class="col-sm-12 pull-left">
                                            <div class="form-group">
                                                <label style="width:26%;">Verifieringsmetod</label>
                                                <select onchange="showinput3ka(this);" style="width:73%;"
                                                        class="form-control" name="Verifieringsmetod"
                                                        id="Verifieringsmetod1">
                                                    <option value="0">Välj ett alternativ</option>
                                                    <?php $__currentLoopData = \App\Definitions\Verifieringsmetod::latest()->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $verifieringsmetod): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <option
                                                            value="<?php echo e($verifieringsmetod->id); ?>"><?php echo e($verifieringsmetod->title); ?></option>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    <option value="500">Övrig</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 input-Other3ka pull-left">
                                            <input type="text" id="VerifieringsmetodText1" name="VerifieringsmetodText"
                                                   class="form-control" placeholder="Enter ...">
                                        </div>
                                        <div class="col-sm-12 pull-left" style="display: none;" id="Vmetoddesdiv1">
                                            <input type="text" id="Vmetoddescription1" name="Vmetoddescription"
                                                   class="form-control inputdescription" placeholder="beskrivning ...">
                                        </div>
                                        <div class="col-sm-12 pull-left">
                                            <div class="form-group">
                                                <label style="width:27%;">Platsbesök</label>
                                                <select onchange="showinput4(this);" style="width:72%;"
                                                        class="form-control" name="KraverPlatsbesok"
                                                        id="KraverPlatsbesok1">
                                                    <option value="0">Välj ett alternativ</option>
                                                    <?php $__currentLoopData = \App\Definitions\KraverPlatsbesok::latest()->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $KraverPlatsbesok): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <option
                                                            value="<?php echo e($KraverPlatsbesok->id); ?>"><?php echo e($KraverPlatsbesok->title); ?></option>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    <option value="500">Övrig</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 input-Other4ka pull-left">
                                            <input type="text" id="KraverPlatsbesokText1" name="KraverPlatsbesokText"
                                                   class="form-control" placeholder="Enter ...">
                                        </div>
                                        <div class="col-sm-12 pull-left" style="display: none;" id="KraverPdesdiv">
                                            <input type="text" id="KraverPdescription1" name="KraverPdescription"
                                                   class="form-control inputdescription" placeholder="beskrivning ...">
                                        </div>

                                        <div class="col-sm-12 pull-left">
                                            <div class="form-group">
                                                <label style="width:27%;">Funktionskrav</label>
                                                <input type="text" id="Funktionskrav1" name="Funktionskrav"
                                                       style="width: 70%" class="form-control inputdescription"
                                                       placeholder="Funktionskrav ...">
                                            </div>
                                        </div>

                                        <div class="col-sm-12 pull-left">
                                            <div class="form-group form_ask">
                                                <label style="width:27%;">document</label>
                                                <div class="uploader_img">
                                                    <input type="file" name="image" id="image1"
                                                           onchange="fileSelect('image1')" class="hideinput">
                                                    <span class="special-upload">Upload</span>
                                                </div><!--uploader_img-->
                                                <span class="shownameupload" style="width:30%;">Filnamn</span>
                                                <a href="#" id="showImageDesign1" style="display: none"
                                                   data-toggle="modal"
                                                   data-target-src="http://localhost:8000/dist/img/project.jpg"
                                                   data-target="#showIMAGE"><img alt="Avatar" id="showImageDesignImg"
                                                                                 class="img-circle table-avatar"
                                                                                 width="30" height="30"
                                                                                 src="http://localhost:8000/dist/img/project.jpg"></a>
                                            </div>
                                        </div>

                                        <div class="col-sm-12 pull-left">
                                            <div class="form-group">
                                                <label style="width:20%;">EndastSignering</label>
                                                <select style="width:79%;" class="form-control" name="EndastSignering" id="EndastSignering">
                                                    <option value="0">Välj ett alternativ</option>
                                                    <option value="End">End</option>
                                                    <option value="Fok">Fok</option>
                                                </select>
                                            </div>
                                        </div>

                                    </form>
                                </div>
                            </div>

                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding l-formuser">
                            <div class="steg-div steg-div2">
                                <span>Steg 4 </span>
                            </div>
                            <!-- Search 2 !-->
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 pull-left nopadding">
                                <div class="input-group input-group-sm poscenter poscenter-search">
                                    <input name="keywordsKa" id="keywordsKa"
                                           class="form-control FormControl  form-control-navbar"
                                           type="search" placeholder="Sök inom författningar och lagtexter, fritext"
                                           aria-label="Search">
                                    <div class="input-group-append search-top-Kontrollplan">
                                        <button class="btn btn-navbar" type="button" id="btnSearchKa">
                                            <i class="fas fa-search"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <!--End Search 2 !-->
                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 pull-left nopadding">
                                <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 rlogo-formuser btnFormUser">
                                    <button type="button" onclick="filter1(1);btnactive1(this)" id="btnbbr1"
                                            class="btnstyle">BBR<span id="bbr1"
                                                                      class="ml-1 mb-1 badge badge bg-warning"></span>
                                    </button>
                                    <button type="button" onclick="filter1(2);btnactive1(this)" class="btnstyle">PBL<span
                                            id="pbl1" class="ml-1 mb-1 badge bg-warning"></span></button>
                                    <button type="button" onclick="filter1(3);btnactive1(this)" class="btnstyle">PBF<span
                                            id="pbf1" class="ml-1 mb-1 badge bg-warning"></span></button>
                                    <button type="button" onclick="filter1(4);btnactive1(this)" class="btnstyle">
                                        EKS10<span id="eks101" class="ml-1 mb-1 badge bg-warning"></span></button>
                                    <button type="button" onclick="filter1(5);btnactive1(this)" class="btnstyle">AML<span
                                            id="aml1" class="ml-1 mb-1 badge bg-warning"></span></button>
                                    <button type="button" onclick="filter1(7);btnactive1(this)" class="btnstyle">MB<span
                                            id="mb1" class="ml-1 mb-1 badge bg-warning"></span></button>
                                    <button type="button" onclick="filter1(8);btnactive1(this)" class="btnstyle">JVL<span
                                            id="jvl1" class="ml-1 mb-1 badge bg-warning"></span></button>
                                    <button type="button" onclick="filter1(6);btnactive1(this)" class="btnstyle">
                                        BSAB<span
                                            id="bsab1" class="ml-1 mb-1 badge bg-warning"></span></button>
                                </div><!--rlogo-formuser-->
                            </div>

                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-left nopadding">
                                <label>Hänvisningar/Paragrafer</label>
                                <div class="showrulesKa" id="showrulesKaAll" style="overflow-y: scroll">

                                </div>
                            </div>
                            <form action="#" method="post">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-left nopadding">
                                    <p>Funktionskrav, hjälptexter råd och anvisningar. Använd denna ruta för hitta
                                        texter att kopiera till editorn. </p>
                                    <div class="showtextKa"></div>
                                </div>
                                <div
                                    class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding thirdtab pull-left nopadding">
                                    <div class="steg-div steg-div2">
                                        <span>Steg 5 </span>
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 additem nopadding pull-left">
                                        <button type="button" id="copyLawKa" class="btnstyle btnstyle2"><i
                                                class="fa fa-plus" aria-hidden="true"></i>Lägg till hänvisning
                                        </button>
                                        <button type="button" id="copyTextKa" class="btnstyle btnstyle2"><i
                                                class="fa fa-plus" aria-hidden="true"></i>Lägg till text
                                        </button>
                                        
                                        
                                        <p>Funktionskrav, hjälptexter råd och anvisningar. Använd denna ruta för hitta
                                            texter att kopiera till editorn.</p>
                                        <textarea class="textarea" id="lawKa" placeholder="Place some text here"
                                                  style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                                    </div>
                                </div>
                            </form>
                        </div><!--l-formuser-->

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 r-formuser">
                            <div class="steg-div">
                                <span>Steg 6 </span>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 r1-formuser">
                                <h4>Skapa en kontrollpunkt</h4>

                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-left nopadding">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 allbtn pull-left">
                                        <span class="show-all-btn collapsed" data-toggle="collapse"
                                              data-target="#readiqera"><i class="fa fa-angle-down"
                                                                          aria-hidden="true"></i>Redigera</span>
                                        <ul class="collapse show PROTOKOLL" id="readiqera">
                                            <li>
                                                <button type="button" id="addControllPlanKa"><i class="fa fa-plus"
                                                                                                aria-hidden="true"></i>Lägg
                                                    till
                                                </button>
                                            </li>
                                            <li>
                                                <button type="button" class="btn-primary" id="editTemplateKa"><i
                                                        class="fa fa-save" aria-hidden="true"></i>Spara ändringar
                                                </button>
                                            </li>
                                            <li>
                                                <button type="button" id="deleteTemplateKa"><i class="fa fa-close"
                                                                                               aria-hidden="true"></i>Radera
                                                </button>
                                            </li>
                                        </ul>
                                        <div class="btn-save-template btn-save-template2">
                                            <a href="#" data-toggle="modal" data-target="#saveTemplateKa">Spara som egen
                                                mall</a>
                                        </div><!--btn-save-template-->
                                    </div>
                                </div>

                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-left nopadding">
                                    <label></label>
                                    <div class="showrules">
                                        <ul id="LawCompleteKa" style="min-height: 200px">

                                        </ul>

                                    </div>
                                    <div style="margin-left: 15px;" id="count"></div>
                                </div>
                            </div><!--r1-formuser-->
                        </div><!--r-formuser-->

                    </div>

                    
                    <div role="tabpanel" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding tab-pane"
                         id="userform">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding">
                            <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 float-left">
                                <!-- Search 1 !-->
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-left nopadding">
                                    <div class="steg-div">
                                        <span>Steg 1 </span>
                                    </div>

                                    <form class="form-inline">
                                        <div class="input-group input-group-sm poscenter poscenter2">
                                            <select style="width:79%;"
                                                    class="form-control FormControl  form-control-navbar select22"
                                                    name="robrik_id1"
                                                    id="robrik_id1" on onchange="selectRobrik(this.value)">
                                                <option value="0">Välj ett alternativ</option>
                                                <option value="NEW">Lägg till ny typ</option>
                                                <?php $__currentLoopData = \App\Robrik::all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $robrik): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <option value="<?php echo e($robrik->id); ?>"><?php echo e($robrik->title); ?></option>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </select>
                                        </div>
                                    </form>

                                    <form class="form-inline">
                                        <div class="input-group input-group-sm poscenter poscenter2">
                                            <select style="width:79%;"
                                                    class="form-control FormControl  form-control-navbar select22"
                                                    name="sub_robrik_id"
                                                    id="sub_robrik_id">
                                                <option value="0">Välj ett alternativ</option>
                                            </select>
                                        </div>
                                    </form>

                                    <form class="form-inline">
                                        <div class="input-group input-group-sm poscenter poscenter2">
                                            <input class="form-control FormControl  form-control-navbar" type="search"
                                                   id="searchBesabkeyword"
                                                   placeholder="Vad ska kontrolleras, sök med valfri text eller BASAB/CoClass"
                                                   aria-label="Search">
                                            <div class="input-group-append search-top-Kontrollplan">
                                                <button class="btn btn-navbar" id="searhBesab" type="button">
                                                    <i class="fas fa-search"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>


                                <!--End Search 1 !-->
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-left nopadding">
                                    <label>Kontrollpunkt/Egenkontroll</label>
                                    <div class="showrules" id="showrules" style="overflow-y: scroll">

                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7 float-left nopadding">
                                <div class="steg-div">
                                    <span>Steg 2 </span>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 r1-formuser">
                                    <h4>Obligatoriskt</h4>
                                    <form id="frmcate"
                                          class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-rinfo pull-left">
                                        <div class="col-sm-12 pull-left">
                                            <div class="form-group">
                                                <label>BSAB/AMA</label>
                                                <input type="text" id="besabtext" name="BSABAMA" class="form-control"
                                                       placeholder="Enter ...">
                                            </div>
                                        </div>
                                        <div class="col-sm-12 pull-left">
                                            <div class="form-group">
                                                <label style="width:20%;">PBL Kategori</label>
                                                <select style="width:79%;" class="form-control" name="PBLKategori"
                                                        id="PBLKategori">
                                                    <option value="0">Välj ett alternativ</option>
                                                    <?php $__currentLoopData = \App\Definitions\PBLCategory::latest()->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $pblCategory): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <option
                                                            value="<?php echo e($pblCategory->id); ?>"><?php echo e($pblCategory->title); ?></option>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 pull-left">
                                            <div class="form-group">
                                                <label style="width:21%;">Ansvarig part</label>
                                                <input type="text" id="searchAnsvarig" class="form-control partsec"
                                                       placeholder="Enter ...">
                                                <select class="form-control partsec" name="AnsvarigPart"
                                                        id="selectAnsvarig">
                                                    <option value="0">Välj ett alternativ</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 pull-left" style="padding-right:0;">
                                            <div class="form-group half-groupform">
                                                <label class="lbl1" style="width:36%;">Kontrollskede</label>
                                                <select onchange="showinput5(this);" style="width:62%;"
                                                        class="form-control select1" name="Kontrollskede"
                                                        id="Kontrollskede">
                                                    <option value="0">Välj ett alternativ</option>
                                                    <?php $__currentLoopData = \App\Definitions\Kontrollskede::latest()->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $KontrollSkede): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <option
                                                            value="<?php echo e($KontrollSkede->id); ?>"><?php echo e($KontrollSkede->title); ?></option>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    <option value="500">Övrig</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-12  input-Other5 pull-left" id="kontrollskedeTextParent"
                                             style="padding-right:0; display: none">
                                            <input type="text" id="kontrollskedeText" name="kontrollskedeText"
                                                   class="form-control" placeholder="Enter ...">
                                        </div>
                                        <div class="col-sm-12 pull-left mt-3">
                                            <div class="form-group half-groupform">
                                                <label style="width:17%;">Risk</label>
                                                <select onchange="showinput(this);" class="form-control select2"
                                                        name="Risk" id="Risk">
                                                    <option value="0">Välj ett alternativ</option>
                                                    <?php $__currentLoopData = \App\Definitions\Risk::latest()->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $risk): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <option value="<?php echo e($risk->id); ?>"><?php echo e($risk->title); ?></option>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    <option value="500">Övrig</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 input-Other pull-left">
                                            <input type="text" id="riskText" name="riskText" class="form-control"
                                                   placeholder="Enter ...">
                                        </div>
                                        <div class="col-sm-12 pull-left" style="display: none;" id="riskdesdiv">
                                            <input type="text" id="riskdescription" name="riskdescription"
                                                   class="form-control inputdescription" placeholder="beskrivning ...">
                                        </div>
                                        <div class="col-sm-12 pull-left">
                                            <div class="form-group">
                                                <label style="width:25%;">Producerat intyg</label>
                                                <select onchange="showinput2(this);" style="width:74%;"
                                                        class="form-control" name="ProduceratIntyg" id="Producerat">
                                                    <option value="0">Välj ett alternativ</option>
                                                    <?php $__currentLoopData = \App\Definitions\ProduceratIntyg::latest()->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $produceIntyg): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <option
                                                            value="<?php echo e($produceIntyg->id); ?>"><?php echo e($produceIntyg->title); ?></option>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    <option value="500">Övrig</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 input-Other2 pull-left">
                                            <input type="text" id="ProduceratIntygText" name="ProduceratIntygText"
                                                   class="form-control" placeholder="Enter ...">
                                        </div>
                                        <div class="col-sm-12 pull-left" style="display: none;" id="PIntydesdiv">
                                            <input type="text" id="PIntygdescription" name="PIntygdescription"
                                                   class="form-control inputdescription" placeholder="beskrivning ...">
                                        </div>
                                        <div class="col-sm-12 pull-left">
                                            <div class="form-group">
                                                <label style="width:26%;">Verifieringsmetod</label>
                                                <select onchange="showinput3(this);" style="width:73%;"
                                                        class="form-control" name="Verifieringsmetod"
                                                        id="Verifieringsmetod">
                                                    <option value="0">Välj ett alternativ</option>
                                                    <?php $__currentLoopData = \App\Definitions\Verifieringsmetod::latest()->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $verifieringsmetod): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <option
                                                            value="<?php echo e($verifieringsmetod->id); ?>"><?php echo e($verifieringsmetod->title); ?></option>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    <option value="500">Övrig</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 input-Other3 pull-left">
                                            <input type="text" id="VerifieringsmetodText" name="VerifieringsmetodText"
                                                   class="form-control" placeholder="Enter ...">
                                        </div>
                                        <div class="col-sm-12 pull-left" style="display: none;" id="Vmetoddesdiv">
                                            <input type="text" id="Vmetoddescription" name="Vmetoddescription"
                                                   class="form-control inputdescription" placeholder="beskrivning ...">
                                        </div>
                                        <div class="col-sm-12 pull-left">
                                            <div class="form-group">
                                                <label style="width:27%;">Platsbesök</label>
                                                <select onchange="showinput4(this);" style="width:72%;"
                                                        class="form-control" name="KraverPlatsbesok"
                                                        id="KraverPlatsbesok">
                                                    <option value="0">Välj ett alternativ</option>
                                                    <?php $__currentLoopData = \App\Definitions\KraverPlatsbesok::latest()->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $KraverPlatsbesok): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <option
                                                            value="<?php echo e($KraverPlatsbesok->id); ?>"><?php echo e($KraverPlatsbesok->title); ?></option>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    <option value="500">Övrig</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 input-Other4 pull-left">
                                            <input type="text" id="KraverPlatsbesokText" name="KraverPlatsbesokText"
                                                   class="form-control" placeholder="Enter ...">
                                        </div>
                                        <div class="col-sm-12 pull-left" style="display: none;" id="KraverPdesdiv">
                                            <input type="text" id="KraverPdescription" name="KraverPdescription"
                                                   class="form-control inputdescription" placeholder="beskrivning ...">
                                        </div>

                                        <div class="col-sm-12 pull-left">
                                            <div class="form-group">
                                                <label style="width:27%;">Funktionskrav</label>
                                                <input type="text" id="Funktionskrav" name="Funktionskrav"
                                                       style="width: 70%" class="form-control inputdescription"
                                                       placeholder="Funktionskrav ...">
                                            </div>
                                        </div>

                                        <div class="col-sm-12 pull-left">
                                            <div class="form-group">
                                                <label style="width:27%;">Kontrollplan Typ</label>
                                                <select style="width:72%;" class="form-control" name="importSelect"
                                                        id="importSelect">
                                                    <option value="0">Välj ett alternativ</option>
                                                    <option value="KontrollplanPBL">Kontrollplan PBL</option>
                                                    <option value="KontrollplanAvtal">Kontrollplan Avtal</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-sm-12 pull-left">
                                            <div class="form-group form_ask">
                                                <label style="width:27%;">bild</label>
                                                <div class="uploader_img">
                                                    <input type="file" name="image" id="image"
                                                           onchange="fileSelect('image')" class="hideinput">
                                                    <span class="special-upload">Upload</span>
                                                </div><!--uploader_img-->
                                                <span class="shownameupload" style="width:30%;">Filnamn</span>
                                                <a href="#" id="showImageDesign" style="display: none"
                                                   data-toggle="modal"
                                                   data-target-src="http://localhost:8000/dist/img/project.jpg"
                                                   data-target="#showIMAGE"><img alt="Avatar" id="showImageDesignImg"
                                                                                 class="img-circle table-avatar"
                                                                                 width="30" height="30"
                                                                                 src="http://localhost:8000/dist/img/project.jpg"></a>
                                            </div>
                                        </div>


                                        
                                        
                                        
                                        
                                        
                                        
                                        
                                        

                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding l-formuser">
                            <div class="steg-div steg-div2">
                                <span>Steg 3 </span>
                            </div>
                            <!-- Search 2 !-->
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 pull-left nopadding">
                                <div class="input-group input-group-sm poscenter poscenter-search">
                                    <input name="keywords" id="searchAllLawskeyword"
                                           class="form-control FormControl  form-control-navbar" type="search"
                                           placeholder="Sök inom författningar och lagtexter, fritext "
                                           aria-label="Search">
                                    <div class="input-group-append search-top-Kontrollplan">
                                        <button class="btn btn-navbar" id="searhAllLaws" type="button">
                                            <i class="fas fa-search"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <!--End Search 2 !-->
                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 pull-left nopadding">
                                <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 rlogo-formuser btnFormUser">
                                    <button type="button" onclick="filter(1);btnactive(this)" id="btnbbr"
                                            class="btnstyle">BBR<span id="bbr"
                                                                      class="ml-1 mb-1 badge badge bg-warning"></span>
                                    </button>
                                    <button type="button" onclick="filter(2);btnactive(this)" class="btnstyle">PBL<span
                                            id="pbl" class="ml-1 mb-1 badge bg-warning"></span></button>
                                    <button type="button" onclick="filter(3);btnactive(this)" class="btnstyle">PBF<span
                                            id="pbf" class="ml-1 mb-1 badge bg-warning"></span></button>
                                    <button type="button" onclick="filter(4);btnactive(this)" class="btnstyle">
                                        EKS10<span id="eks10" class="ml-1 mb-1 badge bg-warning"></span></button>
                                    <button type="button" onclick="filter(5);btnactive(this)" class="btnstyle">AML<span
                                            id="aml" class="ml-1 mb-1 badge bg-warning"></span></button>
                                    <button type="button" onclick="filter(7);btnactive(this)" class="btnstyle">MB<span
                                            id="mb" class="ml-1 mb-1 badge bg-warning"></span></button>
                                    <button type="button" onclick="filter(8);btnactive(this)" class="btnstyle">JVL<span
                                            id="jvl" class="ml-1 mb-1 badge bg-warning"></span></button>
                                    <button type="button" onclick="filter(6);btnactive(this)" class="btnstyle">BSAB<span
                                            id="bsab" class="ml-1 mb-1 badge bg-warning"></span></button>
                                </div><!--rlogo-formuser-->
                            </div>

                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-left nopadding">
                                <label>Hänvisningar/Paragrafer</label>
                                <div class="showrules" id="showrules1" style="overflow-y: scroll">

                                </div>
                            </div>
                            <form action="#" method="post">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-left nopadding">
                                    <p>Funktionskrav, hjälptexter råd och anvisningar. Använd denna ruta för hitta
                                        texter att kopiera till editorn. </p>
                                    <div class="showtext"></div>
                                </div>
                                <div
                                    class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding thirdtab pull-left nopadding">
                                    <div class="steg-div steg-div2">
                                        <span>Steg 4 </span>
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 additem nopadding pull-left">
                                        <button type="button" id="copyLaw" class="btnstyle btnstyle2"><i
                                                class="fa fa-plus" aria-hidden="true"></i>Lägg till hänvisning
                                        </button>
                                        <button type="button" id="copyText" class="btnstyle btnstyle2"><i
                                                class="fa fa-plus" aria-hidden="true"></i>Lägg till text
                                        </button>
                                        
                                        
                                        <p>Funktionskrav, hjälptexter råd och anvisningar. Använd denna ruta för hitta
                                            texter att kopiera till editorn.</p>
                                        <textarea class="textarea" id="law" placeholder="Place some text here"
                                                  style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                                    </div>
                                </div>
                            </form>
                        </div><!--l-formuser-->

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 r-formuser">
                            <div class="steg-div">
                                <span>Steg 5 </span>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 r1-formuser">
                                <h4>Skapa en kontrollpunkt/egenkontroll</h4>

                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-left nopadding">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 allbtn pull-left">
                                        <span class="show-all-btn collapsed" data-toggle="collapse"
                                              data-target="#readiqera"><i class="fa fa-angle-down"
                                                                          aria-hidden="true"></i>Redigera</span>
                                        <ul class="collapse show PROTOKOLL" id="readiqera">
                                            <li>
                                                <button type="button" id="addControllPlan"><i class="fa fa-plus"
                                                                                              aria-hidden="true"></i>Lägg
                                                    till
                                                </button>
                                            </li>
                                            <li>
                                                <button type="button" class="btn-primary" id="editTemplate"><i
                                                        class="fa fa-save" aria-hidden="true"></i>Spara ändringar
                                                </button>
                                            </li>
                                            <li>
                                                <button type="button" id="deleteTemplate"><i class="fa fa-close"
                                                                                             aria-hidden="true"></i>Radera
                                                </button>
                                            </li>
                                        </ul>
                                        <div class="btn-save-template btn-save-template2">
                                            <a href="#" data-toggle="modal" data-target="#saveTemplate">Spara som egen
                                                mall</a>
                                        </div><!--btn-save-template-->
                                    </div>
                                </div>

                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-left nopadding">
                                    <label></label>
                                    <div class="showrules">
                                        <ul id="LawComplete" style="min-height: 200px">

                                        </ul>

                                    </div>
                                    <div style="margin-left: 15px;" id="count"></div>
                                </div>
                            </div><!--r1-formuser-->
                        </div><!--r-formuser-->

                    </div>

                    

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding tab-pane" id="Kontrollplan">
                        <div class="btns-kontrol">

                            <div class="new-buttons">

                                <button data-toggle="modal" data-target="#Bekräftelseshistorik"
                                        class="history-ask"></button>
                                <button data-toggle="modal" onclick="supervisingEngineerConfirm()">Klar för utlåtande
                                </button>
                                <button data-toggle="modal" data-target="#Kontrollansvarig">Kontrollansvarig</button>
                                <button data-toggle="modal" data-target="#Byggherren">Byggherren</button>
                                <button data-toggle="modal" data-target="#Byggnadsnämnden">Byggnadsnämnden</button>
                                <button><img src="http://www.askprojekt.se/dist/img/color.png" alt="">
                                    <div class="tips-color">
                                        <div class="box-tips-color">
                                            <span style="background:#f7f7f7"></span>
                                            <span>Ej tilldelad</span>
                                        </div><!--box-tips-color-->

                                        <div class="box-tips-color">
                                            <span style="background:#ccf1fd"></span>
                                            <span>Tilldelad</span>
                                        </div><!--box-tips-color-->

                                        <div class="box-tips-color">
                                            <span style="background:#f2e599"></span>
                                            <span>Mottaget</span>
                                        </div><!--box-tips-color-->

                                        <div class="box-tips-color">
                                            <span style="background:#fae5c0"></span>
                                            <span>Godkänt innehåll</span>
                                        </div><!--box-tips-color-->

                                        <div class="box-tips-color">
                                            <span style="background:#d9fdd9"></span>
                                            <span>Funktion kontrollerad</span>
                                        </div><!--box-tips-color-->

                                        <div class="box-tips-color">
                                            <span style="background:#c1f0c1"></span>
                                            <span>Admin verifierar</span>
                                        </div><!--box-tips-color-->

                                        <div class="box-tips-color">
                                            <span style="background:#fad8fa"></span>
                                            <span>KA Utlåtande</span>
                                        </div><!--box-tips-color-->

                                        <div class="box-tips-color">
                                            <span style="background:#eebcee"></span>
                                            <span>Byggherren signerat</span>
                                        </div><!--box-tips-color-->

                                        <div class="box-tips-color">
                                            <span style="background:#fafaad"></span>
                                            <span>Slutbesked ges</span>
                                        </div><!--box-tips-color-->

                                        <div class="box-tips-color">
                                            <span style="background:#f0b6b6"></span>
                                            <span>Egenkontroll Underkänt</span>
                                        </div><!--box-tips-color-->

                                        
                                        
                                        
                                        

                                    </div><!--tips-color-->
                                </button>
                            </div><!--new-buttons-->
                            <div class="leftbtn-kontroll">
                                <button class="PBL_BTN" onclick="KontrollplanPBL()" type="button">Kontrollplan PBL
                                </button>
                                <button class="Avtal_btn" onclick="KontrollplanAvtal()" type="button">Kontrollplan
                                    Avtal
                                </button>
                                <button class="reload_btn" onclick="refresh()" type="button"><span></span></button>
                                <a href="#" data-toggle="modal" data-target-id="<?php echo e($project_id); ?>"
                                   data-target="#printModal" class="btn btn-warning btnPrint" type="button" onclick="">Skriv
                                    ut</a>
                            </div><!--leftbtn-kontroll-->
                        </div>

                        <div class="modal fade show" id="Byggherren" tabindex="-1" role="dialog"
                             aria-labelledby="myModalLabel" aria-modal="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">×</span></button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="logomodal">
                                            <img src="http://www.askprojekt.se/dist/img/favicon.png" alt="">
                                        </div>
                                        <div class="title-modal"><span>Byggherren</span></div>
                                        <form id="ByggherrenConfirm">
                                            <?php echo csrf_field(); ?>
                                            <select id="ByggherrenConfirmSelected" name="ByggherrenConfirmSelected"
                                                    class="form-control" aria-hidden="true">
                                                <option value="">Välj alternativ</option>
                                                <option value="1">Tar ansvar</option>
                                                <option value="2">Underkänner kontrollplanen</option>
                                            </select>
                                            <textarea name="ByggherrenDescription" id="ByggherrenDescription"
                                                      class="form-control mt-3" rows="5"
                                                      style="display: none"></textarea>
                                            <button type="button" id="btn-saveTemplateByggherren"
                                                    class="btnmodal btn-add-user">Skicka
                                            </button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div><!--end-modal-->

                        <div class="modal fade show" id="Byggnadsnämnden" tabindex="-1" role="dialog"
                             aria-labelledby="myModalLabel" aria-modal="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">×</span></button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="logomodal">
                                            <img src="http://www.askprojekt.se/dist/img/favicon.png" alt="">
                                        </div>
                                        <div class="title-modal"><span>Byggnadsnämnden</span></div>
                                        <form id="ByggnadsnConfirm">
                                            <?php echo csrf_field(); ?>
                                            <select id="ByggnadsnConfirmSelected" name="ByggnadsnConfirmSelected"
                                                    class="form-control" aria-hidden="true">
                                                <option value="">Välj alternativ</option>
                                                <option value="1">Slutbesked ges</option>
                                                <option value="2">Intremistiskt slutbesked ges</option>
                                                <option value="3">Slutbesked nekas</option>
                                            </select>
                                            <textarea name="ByggnadsnDescription" id="ByggnadsnDescription"
                                                      class="form-control mt-3" style="display: none"
                                                      rows="5"></textarea>
                                            <button type="button" id="btn-saveTemplateByggnadsn"
                                                    class="btnmodal btn-add-user">Skicka
                                            </button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div><!--end-modal-->

                        <div class="modal fade show" id="Kontrollansvarig" tabindex="-1" role="dialog"
                             aria-labelledby="myModalLabel" aria-modal="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">×</span></button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="logomodal">
                                            <img src="http://www.askprojekt.se/dist/img/favicon.png" alt="">
                                        </div>
                                        <div class="title-modal"><span>Kontrollansvarig</span></div>
                                        <form id="KontrollansvarigConfirm">
                                            <select id="koConfirmSelected" name="koConfirmSelected" class="form-control"
                                                    aria-hidden="true" required>
                                                <option value="">Välj alternativ</option>
                                                <option value="1">Ge utlåtande</option>
                                                <option value="2">Underkänner kontrollplan</option>
                                            </select>
                                            <textarea name="koDescription" id="koDescription" class="form-control mt-3"
                                                      style="display: none" rows="5"></textarea>
                                            <button type="button" id="btn-saveTemplateKo" class="btnmodal btn-add-user">
                                                Skicka
                                            </button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div><!--end-modal-->

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding top-Kontrollplan">

                        </div><!--top-Kontrollplan-->

                        <div class="table-responsive Limited-high">
                            <table id="user_table" class="table table-bordered table-striped mt-3 tbl-Kontrollplan">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>BSAB/AMA</th>
                                    <th>PBL</th>
                                    <th>Ansvarig</th>
                                    <th>Skede</th>
                                    <th>Risk</th>
                                    <th>Intyg</th>
                                    <th>Verifikation</th>
                                    <th>Platsbesök</th>
                                    <th>Funktionskrav</th>
                                    <th>Status</th>
                                    <th>Användare</th>
                                    <th>Alternativ</th>
                                </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-left">
                            <ul id="progressbar" class="text-center ">
                                <li class="step" id="step1">
                                    <div class="d-none d-md-block namestep">Projekt startat</div>
                                </li>
                                <li class="step" id="step2">
                                    <div class="d-none d-md-block namestep">Tekniskt samråd</div>
                                </li>
                                <li class="step" id="step3">
                                    <div class="d-none d-md-block namestep">Startbesked</div>
                                </li>
                                <li class="step" id="step4">
                                    <div class="d-none d-md-block namestep">Slutsamråd</div>
                                </li>
                                <li class="step" id="step5">
                                    <div class="d-none d-md-block namestep">Interimistiskt slutbesked</div>
                                </li>
                                <li class="step" id="step6">
                                    <div class="d-none d-md-block namestep">Slutbesked</div>
                                </li>
                                <li class="step" id="step7">
                                    <div class="d-none d-md-block namestep">Projekt slut</div>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div class="modal fade" id="asignUser" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                            aria-hidden="true">×</span></button>
                                </div>
                                <div class="modal-body">
                                    <div class="logomodal">
                                        <img src="<?php echo e(asset('dist/img/favicon.png')); ?>" alt="">
                                    </div>
                                    <div class="title-modal"><span>Lägg till användare</span></div>
                                    <form class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-left" method="" action="">
                                        <input type="hidden" name="_token" id="token" value="<?php echo e(csrf_token()); ?>">

                                        <div class="col-sm-6 pull-left">
                                            <div class="form-group">
                                                <label>Organisation</label>
                                                <select id="selectOrganization" class="form-control"
                                                        style="width: 100%;" data-select2-id="1" tabindex="-1"
                                                        aria-hidden="true">
                                                    <option value="0">Välj ett alternativ</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 pull-left">
                                            <div class="form-group">
                                                <label>Användare</label>
                                                <select id="selectUser" class="form-control" style="width: 100%;"
                                                        data-select2-id="1" tabindex="-1" aria-hidden="true">
                                                    <option value="0">Välj ett alternativ</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 pull-left">
                                            <button type="button" id="btn-add-user" class="btnmodal btn-add-user">
                                                Delegera kontrollpunkt
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="modal fade" id="saveTemplate" tabindex="-1" role="dialog"
                         aria-labelledby="myModalLabel">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                            aria-hidden="true">×</span></button>
                                </div>
                                <div class="modal-body">
                                    <div class="logomodal">
                                        <img src="<?php echo e(asset('dist/img/favicon.png')); ?>" alt="">
                                    </div>
                                    <div class="title-modal"><span>Lägg till i mall</span></div>
                                    <form class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-left" method="" action="">

                                        <div class="col-sm-6 pull-left">
                                            <div class="form-group">
                                                <label>Mall-kategori</label>
                                                <select id="templateCategory" class="form-control" style="width: 100%;"
                                                        data-select2-id="1" tabindex="-1" aria-hidden="true">
                                                    <option value="0">Välj ett alternativ</option>
                                                    <?php $__currentLoopData = \App\Definitions\TemplateCategory::all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $TemplateCategory): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <option
                                                            value="<?php echo e($TemplateCategory->id); ?>"><?php echo e($TemplateCategory->title); ?></option>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 pull-left">
                                            <div class="form-group">
                                                <label>Hitta Mall</label>
                                                <input type="text" id="templateName" class="form-control"
                                                       placeholder="Enter ...">
                                            </div>
                                        </div>
                                        <div class="col-sm-12 pull-left">
                                            <button type="button" id="btn-saveTemplate" class="btnmodal btn-add-user">
                                                Lägg till i mall
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div role="tabpanel" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding tab-pane"
                         id="Information">
                        <div id="tabs-Kontrol" class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding">

                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding tab-pane">

                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col-Information pull-left">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 titlestep pull-left">
                                            <strong>Tidslinjen</strong>
                                            <a class="btn-createproject" href="" data-toggle="modal"
                                               data-target="#Stepmodal">Ändra tidslinjen</a>
                                            <a class="btn-createproject mr-2"
                                               href="<?php echo e(route('project.edit',['id' => $project_id])); ?>">Redigera
                                                projektinfo</a>
                                        </div>

                                        <div class="modal fade" id="Stepmodal" tabindex="-1" role="dialog"
                                             aria-labelledby="myModalLabel">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal"
                                                                aria-label="Close"><span aria-hidden="true">×</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="logomodal">
                                                            <img src="<?php echo e(asset('dist/img/favicon.png')); ?>" alt="">
                                                        </div>
                                                        <div class="title-modal"><span>Ändra tidslinjen</span></div>
                                                        <form action="" method="">
                                                            <p>I vilket skede är projektet?</p>
                                                            <div class="form-group">
                                                                <div class="form-check">
                                                                    <input class="step form-check-input" value="1"
                                                                           type="radio" name="stepproject">
                                                                    <label class="form-check-label">Projekt
                                                                        startat</label>
                                                                </div>
                                                                <div class="form-check">
                                                                    <input class="step form-check-input" value="2"
                                                                           type="radio" name="stepproject" checked="">
                                                                    <label class="form-check-label">Tekniskt
                                                                        samråd</label>
                                                                </div>
                                                                <div class="form-check">
                                                                    <input class="step form-check-input" value="3"
                                                                           type="radio" name="stepproject">
                                                                    <label class="form-check-label">Startbesked</label>
                                                                </div>
                                                                <div class="form-check">
                                                                    <input class="step form-check-input" value="4"
                                                                           type="radio" name="stepproject">
                                                                    <label class="form-check-label">Slutsamråd</label>
                                                                </div>
                                                                <div class="form-check">
                                                                    <input class="step form-check-input" value="5"
                                                                           type="radio" name="stepproject">
                                                                    <label class="form-check-label">Interimistiskt
                                                                        slutbesked</label>
                                                                </div>
                                                                <div class="form-check">
                                                                    <input class="step form-check-input" value="6"
                                                                           type="radio" name="stepproject">
                                                                    <label class="form-check-label">Slutbesked</label>
                                                                </div>
                                                                <div class="form-check">
                                                                    <input class="step form-check-input" value="7"
                                                                           type="radio" name="stepproject">
                                                                    <label class="form-check-label">Projekt slut</label>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-left">
                                            <ul id="progressbar" class="text-center ">
                                                <li class="step" id="stepK1">
                                                    <div class="d-none d-md-block namestep">Projekt startat</div>
                                                </li>
                                                <li class="step" id="stepK2">
                                                    <div class="d-none d-md-block namestep">Tekniskt samråd</div>
                                                </li>
                                                <li class="step" id="stepK3">
                                                    <div class="d-none d-md-block namestep">Startbesked</div>
                                                </li>
                                                <li class="step" id="stepK4">
                                                    <div class="d-none d-md-block namestep">Slutsamråd</div>
                                                </li>
                                                <li class="step" id="stepK5">
                                                    <div class="d-none d-md-block namestep">Interimistiskt slutbesked
                                                    </div>
                                                </li>
                                                <li class="step" id="stepK6">
                                                    <div class="d-none d-md-block namestep">Slutbesked</div>
                                                </li>
                                                <li class="step" id="stepK7">
                                                    <div class="d-none d-md-block namestep">Projekt slut</div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div><!--col-Information-->
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col-Information">
                                        <div class="tab-content tab-content2">
                                            <div role="tabpanel" class="tab-pane active" id="Tekniskt">
                                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 col-box-in-information">
                                                    <div class="box-in-information">
                                                        <div class="title-col-Information"><strong>Projekt
                                                                Information</strong></div>
                                                        <div class="txt-col-Information">
                                                            <ul>
                                                                <li id="li1">Projektnamn</li>
                                                                <li id="li2">Fastighetsbeteckning</li>
                                                                <li id="li3">BNDiarienummer</li>
                                                                <li id="li4">Startbesked</li>
                                                                <li id="li5">Slutbesked</li>
                                                                <li id="li6">byggnad</li>
                                                                <li id="li7">Grundkonstruktion</li>
                                                                <li id="li8">Värmesystem</li>
                                                                <li>Ventilationssystem</li>
                                                                <li>Entreprenadform</li>
                                                                <li>Teknisktsamråd</li>
                                                                <li>Angeupphandlingsform</li>
                                                            </ul>
                                                        </div><!--txt-col-Information-->
                                                    </div><!--box-in-information-->
                                                </div><!--col-box-in-information-->
                                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 col-box-in-information">
                                                    <div class="box-in-information">
                                                        <div class="title-col-Information"><strong>Byggnad / Struktur
                                                                beskrivning</strong></div>
                                                        <div class="txt-col-Information">
                                                            <ul>
                                                                <li id="li9">Övriginformation</li>
                                                                
                                                                
                                                                
                                                                
                                                                
                                                                
                                                                
                                                                
                                                                
                                                                
                                                                
                                                            </ul>
                                                        </div><!--txt-col-Information-->
                                                    </div><!--box-in-information-->
                                                </div><!--col-box-in-information-->
                                            </div>
                                            <div role="tabpanel" class="tab-pane" id="Startbesked">
                                                2
                                            </div>
                                            <div role="tabpanel" class="tab-pane" id="Slutsamråd">
                                                3
                                            </div>
                                            <div role="tabpanel" class="tab-pane" id="Interimistiskt">
                                                4
                                            </div>
                                            <div role="tabpanel" class="tab-pane" id="Slutbesked">
                                                5
                                            </div>
                                        </div><!--tab-content-->
                                    </div><!--col-Information-->
                                </div>
                            </div><!--tab-content-->
                        </div><!--tabs-product-->
                    </div>

                    <div role="tabpanel" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding tab-pane"
                         id="Attachment">

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding l-formuser">
                            <div class="col-sm-12 pull-left">
                                <div class="form-group">
                                    <label style="width:20%;">Hänvisningar/Paragrafer</label>
                                    <select class="form-control" name="controllPoint" id="controllPoint"
                                            onchange="getAmaTemplate(this.value)">

                                    </select>
                                </div>
                            </div>
                            <!-- Search 2 !-->
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 pull-left nopadding">
                                <div class="input-group input-group-sm poscenter poscenter-search">
                                    <input name="keywords" id="searchAmakeyword"
                                           class="form-control FormControl  form-control-navbar" type="search"
                                           placeholder="Sök med valfri text eller BSAB/CoClass kod" aria-label="Search">
                                    <div class="input-group-append search-top-Kontrollplan">
                                        <button class="btn btn-navbar" id="searhAma" type="button">
                                            <i class="fas fa-search"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <!--End Search 2 !-->
                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 pull-left nopadding">
                                <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 rlogo-formuser btnFormUser">
                                    <li style="width: 10px;display: inline-table;" type="button" class="btnstyle3">
                                        AMA<span id="AmaVer" class="ml-1 mb-1 badge badge bg-warning">0</span></li>
                                    <li style="width: 10px;display: inline-table;" type="button"
                                        onClick="appendConfirmationAmaLaw('https://www.byggai.se');select2(this)"
                                        value="578316" class="btnstyle3">Byggai
                                    </li>
                                    <li style="width: 10px;display: inline-table;" type="button" href="#"
                                        onClick="appendConfirmationAmaLaw('https://www.sbuf.se/');select2(this)"
                                        value="578317" class="btnstyle3">SBUF
                                    </li>
                                    <li style="width: 10px;display: inline-table;" type="button" href="#"
                                        onClick="appendConfirmationAmaLaw('http://byggal.se/');select2(this)"
                                        value="578318" class="btnstyle3">ByggaL
                                    </li>
                                    <li style="width: 10px;display: inline-table;" type="button" href="#"
                                        onClick="appendConfirmationAmaLaw('http://byggae.se/');select2(this)"
                                        value="578319" class="btnstyle3">ByggaE
                                    </li>
                                    <li style="width: 10px;display: inline-table;" type="button" href="#"
                                        onClick="appendConfirmationAmaLaw('http://www.fuktcentrum.lth.se/verktyg-och-hjaelpmedel/fuktsaekert-byggande/byggaf-metoden/');select2(this)"
                                        value="5783120" class="btnstyle3">ByggaF
                                    </li>
                                </div><!--rlogo-formuser-->
                            </div>

                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-left nopadding">
                                <label>Hänvisningar/Paragrafer/Länk</label>
                                <div class="showrules" id="showrulesAma" style="height:200px;overflow-y: scroll">

                                </div>
                            </div>
                            <form action="#" method="post">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-left nopadding">
                                    
                                    
                                    
                                    
                                </div>
                                <div
                                    class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding thirdtab pull-left nopadding">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 additem nopadding pull-left">
                                        
                                        
                                        
                                        <p>De val du gör här hamnar i egenkontrollprotokollet.</p>
                                        <div class="table-responsive">
                                            <table id="ama_law_table" style="width: 100%;"
                                                   class="table table-bordered table-striped mt-3 tbl-Kontrollplan">
                                                <thead>
                                                <tr>
                                                    <th>Länk</th>
                                                    <th style="width: 1%">Alternativ</th>
                                                </tr>
                                                </thead>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div><!--l-formuser-->
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                    </div>

                    <div class="modal fade" id="showIMAGE" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                         aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                            aria-hidden="true">×</span></button>
                                </div>
                                <div class="modal-body">
                                    <div class="img-in-modal text-center">
                                        <a id="download" href=""><img id="viewImage" src="" alt="" width="400"
                                                                      height="300"></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="modal fade" id="printModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                            aria-hidden="true">×</span></button>
                                </div>
                                <div class="modal-body">
                                    <div class="logomodal">
                                        <img src="<?php echo e(asset('dist/img/favicon.png')); ?>" alt="">
                                    </div>
                                    <div class="title-modal"><span>Skriv ut</span></div>
                                    <form id="printAllForm" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-left"
                                          method="get" action="">
                                        <div class="col-sm-12 pull-left">
                                            <div class="form-group">
                                                <label>Utskriftsval</label>
                                                <select id="selectPrint" class="form-control" name="printOption"
                                                        tabindex="-1" aria-hidden="true">
                                                    <option value="0">Välj ett alternativ</option>
                                                    <option value="1">Skriv ut Kontrollplan ink. alla Egenkontroller
                                                    </option>
                                                    <option value="2">Skriv ut endast Kontrollplan</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 pull-left">
                                            <button type="submit" class="btnmodal btn-add-user">Skriv ut</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="modal fade" id="attachmentModal" tabindex="-1" role="dialog"
                         aria-labelledby="myModalLabel">
                        <div class="modal-dialog" style="max-width: 700px" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                            aria-hidden="true">×</span></button>
                                </div>
                                <div class="modal-body">
                                    <div class="logomodal">
                                        <img src="<?php echo e(asset('dist/img/favicon.png')); ?>" alt="">
                                    </div>
                                    <div class="title-modal"><span>Bifogade filer</span></div>
                                    <button type="button" data-toggle="modal" data-target="#FileAttachmentModal"
                                            data-target-id="" class="btn btn-primary" style="margin-bottom: 64px;">Lägg
                                        till
                                    </button>
                                    <div class="table-responsive Limited-high">
                                        <table class="table table-bordered table-striped text-center">
                                            <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>titel</th>
                                                <th>Alternativ</th>
                                            </tr>
                                            </thead>
                                            <tbody id="tbl-tbodyAttachment">

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="modal fade" id="FileAttachmentModal" tabindex="-1" role="dialog"
                         aria-labelledby="myModalLabel">
                        <div class="modal-dialog" style="max-width: 500px" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                            aria-hidden="true">×</span></button>
                                </div>
                                <div class="modal-body">
                                    <div class="logomodal">
                                        <img src="<?php echo e(asset('dist/img/favicon.png')); ?>" alt="">
                                    </div>
                                    <form id="attachmentFileUserForm" action="" method="post"
                                          enctype="multipart/form-data">
                                        <?php echo csrf_field(); ?>
                                        <input type="hidden" name="design_idForFile" id="design_idForFile" value="">
                                        <div class="title-modal"><span>Bifogade filer</span></div>
                                        <div class="col-sm-12 pull-left">
                                            <!-- text input -->
                                            <div class="form-group">
                                                <label>titel</label>
                                                <input type="text" name="title" required="" class="form-control"
                                                       placeholder="Enter ...">
                                            </div>
                                        </div>
                                        <div class="col-sm-12 pull-left">
                                            <label>Upload Your File</label>
                                            <input type="file" id="attachmentFileToUserU" name="attachmentFileToUser[]"
                                                   multiple class="hideinput">
                                            <span class="special-upload">Ladda upp</span>
                                            <span class="shownameupload">show File Name upload</span>
                                        </div>
                                        <div class="col-sm-12 pull-left">
                                            <button type="submit" class="btnmodal">ladda upp</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="modal fade" id="FileAttachmentModalEdit" tabindex="-1" role="dialog"
                         aria-labelledby="myModalLabel">
                        <div class="modal-dialog" style="max-width: 500px" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                            aria-hidden="true">×</span></button>
                                </div>
                                <div class="modal-body">
                                    <div class="logomodal">
                                        <img src="<?php echo e(asset('dist/img/favicon.png')); ?>" alt="">
                                    </div>
                                    <form id="attachmentFileUserFormEdit" action="" method="post"
                                          enctype="multipart/form-data">
                                        <?php echo csrf_field(); ?>
                                        <input type="hidden" name="design_idForFile" id="design_idForFileEdit" value="">
                                        <input type="hidden" name="filesIdUploadEdit" id="filesIdUploadEdit" value="">
                                        <div class="title-modal"><span>Redigera Bifogade filer</span></div>
                                        <div class="col-sm-12 pull-left">
                                            <!-- text input -->
                                            <div class="form-group">
                                                <label>titel</label>
                                                <input type="text" id="titleAttachmentEdit" name="title" required=""
                                                       class="form-control" placeholder="Enter ...">
                                            </div>
                                        </div>

                                        <div class="col-sm-12 pull-left mb-3">
                                            <div class="img-in-modal text-center">
                                                <a id="downloadFormEditAttachmnetFile" href="" target="_blank"><img
                                                        id="viewImageFormEditAttachmnetFile" src="" alt="" width="150"
                                                        height="150"></a>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 pull-left">
                                            <label>Upload Your File</label>
                                            <input type="file" id="attachmentFileToUserE" name="attachmentFileToUser"
                                                   class="hideinput">
                                            <span class="special-upload">Upload</span>
                                            <span class="shownameupload">show File Name upload</span>
                                        </div>
                                        <div class="col-sm-12 pull-left">
                                            <button type="submit" class="btnmodal">ladda upp</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="modal fade" id="showHisttoryModal" tabindex="-1" role="dialog"
                         aria-labelledby="myModalLabel">
                        <div class="modal-dialog" style="max-width: 700px" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                            aria-hidden="true">×</span></button>
                                </div>
                                <div class="modal-body">
                                    <div class="logomodal">
                                        <img src="<?php echo e(asset('dist/img/favicon.png')); ?>" alt="">
                                    </div>
                                    <div class="title-modal"><span>Historia</span></div>
                                    <div class="table-responsive tblhistory Limited-high">
                                        <table class="table table-bordered table-striped text-center">
                                            <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>status</th>
                                                <th>tid</th>
                                                <th>drift</th>
                                            </tr>
                                            </thead>
                                            <tbody id="tbl-history">

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="modal fade" id="changeStatusModal" tabindex="-1" role="dialog"
                         aria-labelledby="myModalLabel">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                            aria-hidden="true">×</span></button>
                                </div>
                                <div class="modal-body">
                                    <div class="logomodal">
                                        <img src="<?php echo e(asset('dist/img/favicon.png')); ?>" alt="">
                                    </div>
                                    <div class="title-modal"><span>Ange status</span></div>
                                    <form id="changeStatusForm"
                                          class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-left">
                                        <div class="col-sm-12 pull-left">
                                            <div class="form-group">
                                                <label>Välj en status</label>
                                                <select id="selectStatus" class="form-control" name="selectStatus"
                                                        tabindex="-1" aria-hidden="true">
                                                    <option value="">Välj ett alternativ</option>
                                                    <option value="1">Tilldelad</option>
                                                    <option value="2">Mottaget</option>
                                                    <option value="3">Godkänt innehåll</option>
                                                    <option value="4">Funktion uppfylld</option>
                                                    <option value="5">Admin Vidimera</option>
                                                    <option value="6">KA Utlåtande</option>
                                                    <option value="7">Byggherren fastställer</option>
                                                    <option value="8">Slutbesked ges</option>
                                                    <option value="9">Egenkontroll Underkänt</option>
                                                    <option value="10">Avvisa</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 pull-left">
                                            <div class="form-group">
                                                <label for="exampleFormControlTextarea1">Beskrivning</label>
                                                <textarea class="form-control" name="description" rows="3"></textarea>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 pull-left">
                                            <label>Upload Fil</label>
                                            <input type="file" name="file" onchange="fileSelect('fileStatus')"
                                                   class="hideinput" id="fileStatus">
                                            <span class="special-upload">Upload</span>
                                            <span class="shownameupload">show File Name upload</span>
                                        </div>
                                        <div class="col-sm-12 pull-left">
                                            <button type="button" onclick="changeStatus()"
                                                    class="btnmodal btn-add-user">Ändra status
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="modal fade show" id="Bekräftelseshistorik" tabindex="-1" role="dialog"
                         aria-labelledby="myModalLabel" aria-modal="true">
                        <div class="modal-dialog" style="max-width: 700px" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                            aria-hidden="true">×</span></button>
                                </div>
                                <div class="modal-body">
                                    <div class="logomodal">
                                        <img src="http://www.askprojekt.se/dist/img/favicon.png" alt="">
                                    </div>
                                    <div class="title-modal"><span>Bekräftelseshistorik</span></div>
                                    <div class="table-responsive">
                                        <table class="table table-striped tbl-history">
                                            <thead>
                                            <tr>
                                                <th>Rad</th>
                                                <th>Användare</th>
                                                <th>Roll</th>
                                                <th>Status</th>
                                                <th>Tid</th>
                                                <th>Beskrivning</th>
                                            </tr>
                                            </thead>
                                            <tbody id="tblHistory">
                                            </tbody>
                                        </table><!--tbl-ticket-->
                                    </div><!--table-responsive-->
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="modal fade show" id="showDescriptionHistoryModal" tabindex="-1" role="dialog"
                         aria-labelledby="myModalLabel" aria-modal="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                            aria-hidden="true">×</span></button>
                                </div>
                                <div class="modal-body">
                                    <div class="logomodal">
                                        <img src="http://www.askprojekt.se/dist/img/favicon.png" alt="">
                                    </div>
                                    <div class="title-modal"><span>Beskrivning</span></div>
                                    <textarea id="descriptionHistory" class="form-control" rows="6"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>


                </div><!--tab-content-->

            </div><!--tabs-product-->
        </div><!--box-organization-->
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('script'); ?>

    <?php if(Session::has('successAddProject')): ?>
        <script>
            Swal.fire({
                position: 'top-end',
                type: 'success',
                title: 'projekt registreringen slutförd',
                showConfirmButton: false,
                timer: 1500
            });

        </script>
    <?php endif; ?>

    <?php if(Session::has('successEditProject')): ?>
        <script>
            Swal.fire({
                position: 'top-end',
                type: 'success',
                title: 'Färdig redigering av projektinformation',
                showConfirmButton: false,
                timer: 1500
            });
        </script>
    <?php endif; ?>

    <?php if(Session::has('successBaseProject')): ?>
        <script>
            Swal.fire({
                position: 'top-end',
                type: 'success',
                title: 'projekt registreringen slutförd',
                showConfirmButton: false,
                timer: 1500
            });
        </script>
    <?php endif; ?>

    <?php if(Session::has('notingAttachment')): ?>
        <script>
            Swal.fire({
                position: 'top-end',
                type: 'error',
                title: 'Den är inte bifogad !',
                showConfirmButton: false,
                timer: 1500
            });
        </script>
    <?php endif; ?>

    <script type="text/javascript">



        $('#showIMAGE').on('show.bs.modal', function (e) {
            var src = $(e.relatedTarget).data('target-src');
            $('#download').prop('href', src);
            $('#viewImage').prop("src", src);
        });

        $('#printModal').on('show.bs.modal', function (e) {
            var id = $(e.relatedTarget).data('target-id');
            var origin = window.location.origin;
            var address = '/panel/design/KontrollPlan/print/all/' + id
            $('#printAllForm').attr('action', origin + address);
        });

        var design_idForFile = 0;
        $('#attachmentModal').on('show.bs.modal', function (e) {
            var id = $(e.relatedTarget).data('target-id');
            showModalAttachFile(id)
            design_idForFile = id;
        });

        $('#showHisttoryModal').on('show.bs.modal', function (e) {
            var id = $(e.relatedTarget).data('target-id');
            showHistory(id);
        });


        $('#FileAttachmentModal').on('show.bs.modal', function (e) {
            $('#design_idForFile').val(design_idForFile);

            
            
            
            
            
            
            
            

            
            

        });


        $('#showDescriptionHistoryModal').on('show.bs.modal', function (e) {
            var descriptionHistory = $(e.relatedTarget).data('target-description');
            $('#descriptionHistory').val(descriptionHistory);
        });

        function showDiscriptionHistory(description) {
            // if(description !='')
            // {
            //     $('#descriptionHistory').val(description);
            //     $('#showDiscriptionHistoryModal').modal('show');
            // }
            // else
            // {
            //     Swal.fire({
            //         position: 'top-end',
            //         type: 'error',
            //         title: 'Det finns ingen historia !',
            //         showConfirmButton: false,
            //         timer: 1500
            //     });
            // }
        }

        $('#FileAttachmentModalEdit').on('show.bs.modal', function (e) {
            var id = $(e.relatedTarget).data('target-id');
            var designid = $(e.relatedTarget).data('target-designid');
            var title = $(e.relatedTarget).data('target-title');
            var fileid = $(e.relatedTarget).data('target-fileid');
            $('#titleAttachmentEdit').val(title);
            $('#filesIdUploadEdit').val(id);
            $('#design_idForFileEdit').val(designid);
            $('#downloadFormEditAttachmnetFile').prop('href', fileid);
            $('#viewImageFormEditAttachmnetFile').prop("src", fileid);
        });


        $('#Bekräftelseshistorik').on('show.bs.modal', function (e) {
            var formData = new FormData();
            formData.append('projectId', project_id);
            formData.append('_token', $('input[name="_token"]').val());
            var tblHistory = $('#tblHistory');
            var output = '';
            $.ajax({
                type: 'POST',
                contentType: false,
                processData: false,
                url: '<?php echo e(route('project.showHistory')); ?>',
                data: formData,
                success: function (data) {
                    if (data != 'NotingHistory') {
                        var count = 1;
                        $.each(data, function (index, value) {
                            output += '<tr><td>' + count + '</td><td>' + value['user'] + '</td><td>' + value['role'] + '</td><td>' + value['status'] + '</td><td>' + value['date'] + '</td><td><span class="show-history" onclick="showDiscriptionHistory(\'' + value['description'] + '\')"></span></td></tr>';
                            count = count + 1;
                        });
                        tblHistory.html("");
                        tblHistory.append(output);
                    } else {
                        Swal.fire({
                            position: 'top-end',
                            type: 'error',
                            title: 'Det finns ingen historia !',
                            showConfirmButton: false,
                            timer: 1500
                        });
                    }
                },
                error: function (err) {

                }
            });
        });

        var designidByStatus = '';
        $('#changeStatusModal').on('show.bs.modal', function (e) {
            designidByStatus = $(e.relatedTarget).data('target-id');
            $('.shownameupload').text("");
            document.forms["changeStatusForm"].reset();
        });


        function addAnsvaringPart() {
            event.preventDefault();
            let lengthAnsvarigPart = document.querySelectorAll('.AnsvarigPart').length;
            lengthAnsvarigPart = lengthAnsvarigPart + 1
            let placeAnsvarigPart = $('#placeAnsvarigPart')
            let oldPlaceAnsvarigPart = placeAnsvarigPart.html()
            let ansvaringPart = `<div class="col-sm-12 pull-left" id="AnsvarigPartParent${lengthAnsvarigPart}">
                                    <div class="form-group">
                                        <label style="width:21%;">Ansvarig part</label>
                                        <input type="text" id="searchAnsvarig${lengthAnsvarigPart}" style="width: 35.8%;" class="form-control AnsvarigPart"
                                               placeholder="Enter ...">
                                        <select class="form-control" style="width: 35.8%;" name="AnsvarigPart[]"
                                                id="selectAnsvarig${lengthAnsvarigPart}">
                                            <option value="0">Välj ett alternativ</option>
                                        </select>
                                        <span class="table-addItem float-right" onclick="romoveAnsvaringPart(${lengthAnsvarigPart})"><a href="#" class="text-success"><i class="fa fa-minus fa-2x" aria-hidden="true"></i></a></span>
                                    </div>
                                 </div>`;
            oldPlaceAnsvarigPart += ansvaringPart
            placeAnsvarigPart.html("")
            placeAnsvarigPart.append(oldPlaceAnsvarigPart)

            $('#searchAnsvarig' + lengthAnsvarigPart).on('keyup', function () {
                keyword = $(this).val();
                var KeyID = event.keyCode;
                if (KeyID == 8) {
                    if (keyCount > 0) {
                        keyCount--;
                    }

                } else {
                    keyCount++;
                }


                if (keyCount >= 3) {
                    keyword = $(this).val();

                    if (keyword == "") {
                        $('#selectAnsvarig' + lengthAnsvarigPart).html("");
                    }
                    $.ajax({
                        type: 'get',
                        url: '<?php echo e(route('searchAnsvarig')); ?>',
                        data: {'keywords': keyword},
                        success: function (data) {
                            $("#selectAnsvarig" + lengthAnsvarigPart).html(data);
                        }
                    });
                    keyCount--;
                } else {
                    $("#selectAnsvarig" + lengthAnsvarigPart).html("");
                }


            });
        }

        function romoveAnsvaringPart(rowId) {
            console.log(rowId)
            event.preventDefault()
            $('#AnsvarigPartParent' + rowId).remove();
        }


        function changeStatus() {
            $('.shownameupload').text("");
            var status = $('#selectStatus').val();
            var form = document.getElementById("changeStatusForm");
            var formData = new FormData(form);
            formData.append('designidByStatus', designidByStatus);
            formData.append('status', status);
            formData.append('_token', $('input[name="_token"]').val());
            $.ajax({
                type: 'POST',
                contentType: false,
                processData: false,
                url: '<?php echo e(route('template.changeStatus')); ?>',
                data: formData,
                success: function (data) {
                    if (data == 'success') {
                        Swal.fire({
                            position: 'top-end',
                            type: 'success',
                            title: 'registrerade',
                            showConfirmButton: false,
                            timer: 1500
                        });
                        $('#user_table').DataTable().ajax.reload();
                        $('#changeStatusModal').modal('hide');
                    } else {
                        Swal.fire({
                            position: 'top-end',
                            type: 'error',
                            title: 'Välj alternativ !',
                            showConfirmButton: false,
                            timer: 1500
                        });
                    }
                },
                error: function (err) {

                }
            });
        }


        function eneter(e) {
            var keycode = (event.keyCode ? event.keyCode : event.which);
            if (keycode == '13') {
                $(":focus").each(function () {
                    var focusElementId = this.id;
                    if (focusElementId === "searchBesabkeyword") {
                        $('#searhBesab').click();
                    } else if (focusElementId === "searchAllLawskeyword") {
                        $('#searhAllLaws').click();
                    } else if (focusElementId === "searchAmakeyword") {
                        $('#searhAma').click();
                    } else if (focusElementId === "searchKAInput") {
                        $('#searchButtonInput').click()
                    } else if (focusElementId === "keywordsKa") {
                        $('#btnSearchKa').click()
                    }

                });
                return false;
            }
        }

        $(document).bind("keydown", eneter);

        var project_id = <?php echo json_encode($project_id); ?>;

        var tabindex = <?php echo json_encode($tabindex); ?>;

        function selectRobrik(value) {
            $('#sub_robrik_id').html("")
            $.ajax({
                type: 'POST',
                url: '<?php echo e(route('ka.getRobrikLaw')); ?>',
                data: {'_token': $('input[name="_token"]').val(), 'project_id': project_id, 'robrik_id': value},
                success: function (response) {
                    let data = response['data'];
                    let content = `<option value="0">Välj ett alternativ</option>`;
                    data.forEach(function (item, index) {
                        console.log(item);
                        content += `<option value="${item['id']}">${item['title']}</option>`
                    })
                    $('#sub_robrik_id').html(content)
                },
                error: function (err) {
                    //err message
                }
            });

        }

        function deleteConfirmation(id) {
            Swal.fire({
                type: 'warning',
                title: 'Är du säker?',
                text: "Du kommer inte att kunna återställa detta!",
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                cancelButtonText: 'Avbryt',
                confirmButtonText: 'Ja, Radera'
            }).then(function (e) {
                if (e.value === true) {
                    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
                    $.ajax({
                        type: 'POST',
                        url: "/panel/design/delete/" + id,
                        data: {_token: '<?php echo csrf_token(); ?>',},
                        dataType: 'JSON',
                        success: function (results) {
                            getTemplate(project_id);
                            getTemplateKontrollPlan(project_id);
                            if (results.success === true) {
                                getTemplate(project_id);
                                getTemplateKontrollPlan(project_id);
                                Swal.fire(
                                    'Raderad!',
                                    'Din kontrollpunkt har tagits bort',
                                    'success'
                                );
                            } else {
                                Swal.fire({
                                    icon: 'fel',
                                    title: 'Hoppsan...',
                                    text: 'Något gick fel!',
                                })
                            }
                        }
                    });
                } else {
                    e.dismiss;
                }
            }, function (dismiss) {
                return false;
            })

        }

        function deleteConfirmationKa(id) {

            Swal.fire({
                type: 'warning',
                title: 'Är du säker?',
                text: "Du kommer inte att kunna återställa detta!",
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                cancelButtonText: 'Avbryt',
                confirmButtonText: 'Ja, Radera'
            }).then(function (e) {
                if (e.value === true) {
                    $.ajax({
                        type: 'POST',
                        url: "/panel/ka/delete/" + id,
                        data: {'_token': $('input[name="_token"]').val()},
                        success: function (results) {
                            getTemplateKa(project_id);
                            getAllTemplate(project_id)
                            if (results.success === true) {
                                getAllTemplate(project_id)
                                getTemplateKa(project_id);
                                Swal.fire(
                                    'Raderad!',
                                    'Din kontrollpunkt har tagits bort',
                                    'success'
                                );
                            } else {
                                Swal.fire({
                                    icon: 'fel',
                                    title: 'Hoppsan...',
                                    text: 'Något gick fel!',
                                })
                            }
                        }
                    });
                } else {
                    e.dismiss;
                }
            }, function (dismiss) {
                return false;
            })

        }


        function deleteConfirmationOrganization(id) {
            Swal.fire({
                type: 'warning',
                title: 'Är du säker?',
                text: "Du kommer inte att kunna återställa detta!",
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                cancelButtonText: 'Avbryt',
                confirmButtonText: 'Ja, Radera'
            }).then(function (e) {
                if (e.value === true) {
                    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
                    $.ajax({
                        type: 'POST',
                        url: "/panel/project/organization/delete/" + id,
                        data: {_token: '<?php echo csrf_token(); ?>',},
                        dataType: 'JSON',
                        success: function (results) {
                            console.log(results);
                            if (results.success === true) {
                                Swal.fire(
                                    'Raderad!',
                                    'Länken har tagits bort.',
                                    'success'
                                );
                                $('#tbl_organization').DataTable().ajax.reload();
                            } else {
                                Swal.fire({
                                    icon: 'fel',
                                    title: 'Hoppsan...',
                                    text: 'Något gick fel!',
                                })
                            }
                        }
                    });
                } else {
                    e.dismiss;
                }
            }, function (dismiss) {
                return false;
            })

        }

        function showModalAttachFile(id) {
            $.ajax({
                type: 'POST',
                url: '<?php echo e(route('template.getAttachment')); ?>',
                data: {'_token': $('input[name="_token"]').val(), 'id': id,},
                success: function (data) {
                    $('#tbl-tbodyAttachment').html("");
                    $('#tbl-tbodyAttachment').html(data['output']);
                },
                error: function (err) {
                    $('#tbl-tbodyAttachment').html("");
                }
            });
        }

        function showHistory(id) {
            $.ajax({
                type: 'POST',
                url: '<?php echo e(route('template.getHistory')); ?>',
                data: {'_token': $('input[name="_token"]').val(), 'id': id,},
                success: function (data) {
                    $('#tbl-history').html("");
                    $('#tbl-history').html(data['output']);
                },
                error: function (err) {
                    $('#tbl-history').html("");
                }
            });
        }

        function deleteConfirmationAmaLaw(id) {
            Swal.fire({
                type: 'warning',
                title: 'Är du säker?',
                text: "Du kommer inte att kunna återställa detta!",
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                cancelButtonText: 'Avbryt',
                confirmButtonText: 'Ja, Radera'
            }).then(function (e) {
                if (e.value === true) {
                    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
                    $.ajax({
                        type: 'POST',
                        url: "/panel/advance/design-template/delete/" + id,
                        data: {_token: '<?php echo csrf_token(); ?>',},
                        dataType: 'JSON',
                        success: function (results) {
                            if (results.success === true) {
                                $('#showrulesAma').html("");
                                $('.showtextama').text("");
                                $('#lawAMA').summernote('code', '');
                                getTemplateKontrollPlan(project_id);
                                Swal.fire(
                                    'Raderad!',
                                    'Länken har tagits bort.',
                                    'success'
                                );
                            } else {
                                Swal.fire({
                                    icon: 'fel',
                                    title: 'Hoppsan...',
                                    text: 'Något gick fel!',
                                })
                            }
                        }
                    });
                } else {
                    e.dismiss;
                }
            }, function (dismiss) {
                return false;
            })

        }


        function ConfirmDeleteFileAttachRow(designID, fileID) {

            Swal.fire({
                type: 'warning',
                title: 'Är du säker?',
                text: "Du kommer inte att kunna återställa detta!",
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                cancelButtonText: 'Avbryt',
                confirmButtonText: 'Ja, Radera'
            }).then(function (e) {
                if (e.value === true) {
                    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
                    $.ajax({
                        type: 'POST',
                        url: "/panel/design/KontrollPlan/AttachmentFile/delete/" + fileID,
                        data: {_token: '<?php echo csrf_token(); ?>', design_id: designID},
                        dataType: 'JSON',
                        success: function (results) {
                            console.log(results);
                            if (results.success === true) {
                                Swal.fire(
                                    'Raderad!',
                                    'Länken har tagits bort.',
                                    'success'
                                );
                                showModalAttachFile(designID);
                            } else {
                                Swal.fire({
                                    icon: 'fel',
                                    title: 'Hoppsan...',
                                    text: 'Något gick fel!',
                                })
                            }
                        }
                    });
                } else {
                    e.dismiss;
                }
            }, function (dismiss) {
                return false;
            })
        }


        function supervisingEngineerConfirm() {
            Swal.fire({
                type: 'warning',
                title: 'Vill du skicka för utlåtande ?',
                text: "Kontrollplan enligt PBL är uppfylld.",
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                cancelButtonText: 'Nej',
                confirmButtonText: 'Ja'
            }).then(function (e) {
                if (e.value === true) {
                    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
                    $.ajax({
                        type: 'POST',
                        url: "<?php echo e(route('project.supervisingEngineerConfirm')); ?>",
                        data: {_token: '<?php echo csrf_token(); ?>', 'projectId': project_id},
                        dataType: 'JSON',
                        success: function (results) {
                            Swal.fire({
                                position: 'top-end',
                                type: 'success',
                                title: 'Situationen förändrades framgångsrikt',
                                showConfirmButton: false,
                                timer: 1500
                            });
                        }
                    });
                } else {
                    e.dismiss;
                }
            }, function (dismiss) {
                return false;
            })

        }

        var keywordAllLaws;
        var keywordBesab;
        var keywordCategory;
        var keyCount = 0;
        var keyCountUser = 0;
        var c = 0;
        var p = 0;
        var Laws = 0;
        var Laws1 = 0;
        var title = '';
        var FinalTextLaw = '';
        var FinalTextLawKa = '';
        var design_id = '';
        var ka_id = '';
        var designs = [];
        var organizationidu = 0;
        var organizationidEdit = 0;
        var LawAma = 0;
        var AmaDesignId = '';
        var keywordCategoryKa = '';
        $('#bbr').text("0");
        $('#pbl').text("0");
        $('#pbf').text("0");
        $('#eks10').text("0");
        $('#bsab').text("0");
        $('#aml').text("0");
        $('#mb').text("0");
        $('#jvl').text("0");

        $('#bbr1').text("0");
        $('#pbl1').text("0");
        $('#pbf1').text("0");
        $('#eks101').text("0");
        $('#bsab1').text("0");
        $('#aml1').text("0");
        $('#mb1').text("0");
        $('#jvl1').text("0");

        $('#searchAnsvarig').on('keyup', function () {
            keyword = $(this).val();
            var KeyID = event.keyCode;
            if (KeyID == 8) {
                if (keyCount > 0) {
                    keyCount--;
                }

            } else {
                keyCount++;
            }


            if (keyCount >= 3) {
                keyword = $(this).val();

                if (keyword == "") {
                    $('#selectAnsvarig').html("");
                }
                $.ajax({
                    type: 'get',
                    url: '<?php echo e(route('searchAnsvarig')); ?>',
                    data: {'keywords': keyword},
                    success: function (data) {
                        $("#selectAnsvarig").html(data);
                    }
                });
                keyCount--;
            } else {
                $("#selectAnsvarig").html("");
            }


        });

        $('#searchAnsvarig1').on('keyup', function () {
            keyword = $(this).val();
            var KeyID = event.keyCode;
            if (KeyID == 8) {
                if (keyCount > 0) {
                    keyCount--;
                }

            } else {
                keyCount++;
            }


            if (keyCount >= 3) {
                keyword = $(this).val();

                if (keyword == "") {
                    $('#selectAnsvarig1').html("");
                }
                $.ajax({
                    type: 'get',
                    url: '<?php echo e(route('searchAnsvarig')); ?>',
                    data: {'keywords': keyword},
                    success: function (data) {
                        $("#selectAnsvarig1").html(data);
                    }
                });
                keyCount--;
            } else {
                $("#selectAnsvarig1").html("");
            }


        });

        $('#searchAnsvarigUser').on('keyup', function () {
            let keyword = $(this).val();
            var KeyID = event.keyCode;

            if (KeyID == 8) {
                if (keyCountUser > 0) {
                    keyCountUser--;
                }

            } else {
                keyCountUser++;
            }

            if (keyCountUser >= 3) {
                keyword = $(this).val();

                if (keyword == "") {
                    $('#AnsvarigPartUser').html("");
                }
                $.ajax({
                    type: 'get',
                    url: '<?php echo e(route('searchAnsvarigUser')); ?>',
                    data: {'keywords': keyword},
                    success: function (data) {
                        $("#AnsvarigPartUser").html(data);
                    }
                });
                keyCountUser--;
            } else {
                $("#AnsvarigPartUser").html("");
            }


        });

        $(document).ready(function () {


            getAllTemplate(project_id)


            $('#controllPoint').select2({
                theme: 'bootstrap4'
            });


            $(".select22")
                .select2({
                    theme: 'bootstrap4',
                    placeholder: 'Select type',
                    width: '50%',
                    minimumResultsForSearch: Infinity
                })
                .on('select2:close', function () {
                    var el = $(this);
                    if (el.val() === "NEW") {
                        var newval = prompt("Ange nytt värde: ");
                        if (newval !== null) {
                            var formData = new FormData();
                            formData.append('title', newval);
                            formData.append('_token', $('input[name="_token"]').val());
                            $.ajax({
                                type: 'POST',
                                contentType: false,
                                processData: false,
                                url: '<?php echo e(route('ka.storeTitle')); ?>',
                                data: formData,
                                success: function (response) {
                                    let data = response['data']
                                    if (response['message'] == 'success') {
                                        el.append('<option value="' + data['id'] + '">' + data['title'] + '</option>')
                                            .val(newval);

                                        var previousOption;
                                        $('.select22 > option').each(function () {
                                            if (this.text == previousOption) $(this).remove();
                                            previousOption = this.text;
                                        });

                                    } else {
                                    }
                                },
                                error: function (err) {

                                }
                            });

                        }
                    }
                });

            $('#searchButtonInput').click(function () {
                var searchKakeyword = $('#searchKAInput').val();
                keywordCategoryKa = searchKakeyword;
                var len = $('#searchKAInput').val().length;
                if (len < 3) {
                    Swal.fire(
                        'Hoppsan...',
                        'Ange minst tre bokstäver!',
                        'error'
                    );
                } else {
                    let timerInterval;
                    Swal.fire({
                        title: 'nu Sök!',
                        html: 'Vänta, Automatisk Stängningsvarning.',
                        timer: 20000,
                        timerProgressBar: true,
                        allowOutsideClick: false,
                        onBeforeOpen: () => {
                            Swal.showLoading();
                            timerInterval = setInterval(() => {
                                const content = Swal.getContent();
                                if (content) {
                                    const b = content.querySelector('b');
                                    if (b) {
                                        b.textContent = Swal.getTimerLeft();
                                    }
                                }
                            }, 100)
                        },
                        onClose: () => {
                            clearInterval(timerInterval);
                        }
                    }).then((result) => {
                        /* Read more about handling dismissals below */
                        if (result.dismiss === Swal.DismissReason.timer) {
                            console.log('I was closed by the timer');
                        }
                    });

                    $.ajax({
                        type: 'get',
                        url: '<?php echo e(route('ka.kaSearch')); ?>',
                        data: {'keywords': searchKakeyword},
                        success: function (data) {
                            $('#showrulesKa').html(data['output']);
                            // $('#AmaVer').text(data['count']);
                            swal.close();
                        },
                        error: function (err) {
                            console.log(err);
                        }
                    });
                }
            });

            $('#ByggherrenConfirmSelected').change(function () {
                var selectedValue = $(this).val();
                if (selectedValue == 2) {
                    $('#ByggherrenDescription').show();
                } else {
                    $('#ByggherrenDescription').hide();
                }
            });

            $('#ByggnadsnConfirmSelected').change(function () {
                // var selectedValue = $(this).val();
                // if(selectedValue == 2)
                // {
                //     $('#ByggnadsnDescription').show();
                // }
                // else
                // {
                //     $('#ByggnadsnDescription').hide();
                // }
            });

            $('#koConfirmSelected').change(function () {
                var selectedValue = $(this).val();
                if (selectedValue == 2) {
                    $('#koDescription').show();
                } else {
                    $('#koDescription').hide();
                }
            });

            $('#supervisingEngineerConfirmSelected').change(function () {
                var selectedValue = $(this).val();
                if (selectedValue == 2) {
                    $('#supervisingEngineerDescription').show();
                } else {
                    $('#supervisingEngineerDescription').hide();
                }
            });


            $('#btn-saveTemplateKo').on('click', function () {
                var formData = new FormData($('#KontrollansvarigConfirm')[0]);
                formData.append('projectId', project_id);
                formData.append('_token', $('input[name="_token"]').val());
                $.ajax({
                    type: 'POST',
                    contentType: false,
                    processData: false,
                    url: '<?php echo e(route('project.kaConfirm')); ?>',
                    data: formData,
                    success: function (data) {
                        if (data == 'success') {
                            Swal.fire({
                                position: 'top-end',
                                type: 'success',
                                title: 'registrerade',
                                showConfirmButton: false,
                                timer: 1500
                            });
                            $('#Kontrollansvarig').modal('hide');
                        } else {
                            Swal.fire({
                                position: 'top-end',
                                type: 'error',
                                title: 'Välj alternativ !',
                                showConfirmButton: false,
                                timer: 1500
                            });
                        }
                    },
                    error: function (err) {

                    }
                });
            })

            $('#btn-saveTemplateByggherren').on('click', function () {
                var formData = new FormData($('#ByggherrenConfirm')[0]);
                formData.append('projectId', project_id);
                formData.append('_token', $('input[name="_token"]').val());
                $.ajax({
                    type: 'POST',
                    contentType: false,
                    processData: false,
                    url: '<?php echo e(route('project.bigherrConfirm')); ?>',
                    data: formData,
                    success: function (data) {
                        if (data == 'success') {
                            Swal.fire({
                                position: 'top-end',
                                type: 'success',
                                title: 'registrerade',
                                showConfirmButton: false,
                                timer: 1500
                            });
                            $('#Byggherren').modal('hide');
                        } else {
                            Swal.fire({
                                position: 'top-end',
                                type: 'error',
                                title: 'Välj alternativ !',
                                showConfirmButton: false,
                                timer: 1500
                            });
                        }
                    },
                    error: function (err) {

                    }
                });
            })


            $('#btn-saveTemplateByggnadsn').on('click', function () {
                var formData = new FormData($('#ByggnadsnConfirm')[0]);
                formData.append('projectId', project_id);
                formData.append('_token', $('input[name="_token"]').val());
                $.ajax({
                    type: 'POST',
                    contentType: false,
                    processData: false,
                    url: '<?php echo e(route('project.bignadConfirm')); ?>',
                    data: formData,
                    success: function (data) {
                        if (data == 'success') {
                            Swal.fire({
                                position: 'top-end',
                                type: 'success',
                                title: 'registrerade',
                                showConfirmButton: false,
                                timer: 1500
                            });
                            $('#Byggnadsnämnden').modal('hide');
                        } else {
                            Swal.fire({
                                position: 'top-end',
                                type: 'error',
                                title: 'Välj alternativ !',
                                showConfirmButton: false,
                                timer: 1500
                            });
                        }
                    },
                    error: function (err) {

                    }
                });
            })


            $('#attachmentFileUserForm').submit(function (event) {
                event.preventDefault();
                var formData = new FormData($(this)[0]);
                var file_data = $('#attachmentFileToUserU').prop('files')[0];
                formData.append('file', file_data);
                $.ajax({
                    url: '<?php echo e(route('template.uploadAttachmentFileUser')); ?>',
                    type: 'POST',
                    data: formData,
                    enctype: 'multipart/form-data',
                    processData: false,
                    contentType: false,
                    success: function (result) {
                        console.log(result);
                        if (result == 'success') {
                            $("#FileAttachmentModal").modal('hide');
                            Swal.fire({
                                position: 'top-end',
                                type: 'success',
                                title: 'filuppladdning',
                                showConfirmButton: false,
                                timer: 1500
                            });
                            showModalAttachFile(design_idForFile);


                        }
                    },
                    error: function (data) {
                        console.log(data);
                    }
                });

            });

            $('#attachmentFileUserFormEdit').submit(function (event) {
                event.preventDefault();
                var formData = new FormData($(this)[0]);
                var file_data = $('#attachmentFileToUserE').prop('files')[0];
                formData.append('file', file_data);
                $.ajax({
                    url: '<?php echo e(route('template.uploadAttachmentFileUserEdit')); ?>',
                    type: 'POST',
                    data: formData,
                    enctype: 'multipart/form-data',
                    processData: false,
                    contentType: false,
                    success: function (result) {
                        if (result == 'success') {
                            $("#FileAttachmentModalEdit").modal('hide');
                            Swal.fire({
                                position: 'top-end',
                                type: 'success',
                                title: 'filuppladdning',
                                showConfirmButton: false,
                                timer: 1500
                            });
                            showModalAttachFile(design_idForFile);
                        }
                    },
                    error: function (data) {
                        console.log(data);
                    }
                });

            });


            if (tabindex != null) {
                $('.nav-tabs a[href="#' + tabindex + '"]').tab('show');
            }
            $('#AnsvarigPartUser').select2({
                'placeholder': 'Välj ett alternativ'
            });
            getTemplate(project_id);
            getTemplateKa(project_id);
            // getTemplateKontrollPlan(project_id);
            getTemplateForAdvance(project_id);
            getOrganization();
            getInformationProject();
            getStep();
            selectOrganizations();
            $.fn.dataTable.ext.errMode = 'none';


            $('#tbl_organization').DataTable({
                "fnDrawCallback": function (oSettings) {
                    if (oSettings._iDisplayLength > oSettings.fnRecordsDisplay()) {
                        $(oSettings.nTableWrapper).find('.dataTables_paginate').hide();
                    }
                },
                language: {
                    "emptyTable": "Data saknas"
                },
                processing: true,
                serverSide: true,
                ajax: {
                    url: '<?php echo e(route('getOrganizations')); ?>',
                },
                language: {
                    "sInfoEmpty": "Visar 0 To 0 From 0 kontrollpunkter",
                    "sInfo": "Visar _START_ To _END_  From _TOTAL_ kontrollpunkter",
                    "sInfoPostFix": "",
                    "sInfoThousands": ",",
                    "sLengthMenu": "Visa _MENU_ Kontrollpunkter",
                    "sSearch": "Sök",
                },
                columns: [
                    {
                        data: 'Name',
                        name: 'Name'
                    },
                    {
                        data: 'Description',
                        name: 'Description'
                    },
                    {
                        data: 'Address',
                        name: 'Address'
                    },
                    {
                        data: 'Contact',
                        name: 'Contact'
                    },
                    {
                        data: 'Logo',
                        name: 'Logo'
                    },
                    {
                        data: 'Action',
                        name: 'Action',
                        orderable: false

                    }
                ]

            });

            $('#searchBesabkeyword').keyup(function () {
                $(".showtext").html("");
                $('#showrules').html("");
                $('#searchAllLawskeyword').val("");
                $('#bbr').text("0");
                $('#pbl').text("0");
                $('#pbf').text("0");
                $('#eks10').text("0");
                $('#bsab').text("0");
                $('#aml').text("0");
                $('#mb').text("0");
                $('#jvl').text("0");

            });

            $('#searchBesabkeyword').keyup(function () {
                $(".showtextKa").html("");
                $('#showrules1').html("");
                $('#searchKAInput').val("");
                $('#bbr1').text("0");
                $('#pbl1').text("0");
                $('#pbf1').text("0");
                $('#eks101').text("0");
                $('#bsab1').text("0");
                $('#aml1').text("0");
                $('#mb1').text("0");
                $('#jvl1').text("0");

            });

            $("#searhBesab").click(function () {
                $('#bsab').text("0");
                $('#searchAllLawskeyword').val("");
                keywordCategory = 0;
                $('#showrules').html("");
                $('#bbr').text("0");
                $('#pbl').text("0");
                $('#pbf').text("0");
                $('#eks10').text("0");
                $('#aml').text("0");
                $('#mb').text("0");
                $('#jvl').text("0");
                let timerInterval;
                Swal.fire({
                    title: 'nu Sök!',
                    html: 'Vänta, Automatisk Stängningsvarning.',
                    timer: 20000,
                    timerProgressBar: true,
                    allowOutsideClick: false,
                    onBeforeOpen: () => {
                        Swal.showLoading();
                        timerInterval = setInterval(() => {
                            const content = Swal.getContent();
                            if (content) {
                                const b = content.querySelector('b');
                                if (b) {
                                    b.textContent = Swal.getTimerLeft();
                                }
                            }
                        }, 100)
                    },
                    onClose: () => {
                        clearInterval(timerInterval);
                    }
                }).then((result) => {
                    /* Read more about handling dismissals below */
                    if (result.dismiss === Swal.DismissReason.timer) {
                        console.log('I was closed by the timer');
                    }
                });


                keywordBesab = $('#searchBesabkeyword').val();
                keywordCategory = keywordBesab;
                var len = $('#searchBesabkeyword').val().length;
                if (len < 3) {
                    Swal.fire(
                        'Hoppsan...',
                        'Ange minst tre bokstäver!',
                        'error'
                    );
                } else {
                    $.ajax({
                        type: 'get',
                        url: '<?php echo e(route('searchBesab')); ?>',
                        data: {'keywords': keywordBesab},
                        success: function (data) {
                            swal.close();
                            $('#showrules').html(data[0]);
                            beseb = data[1];

                            if (typeof beseb === "undefined") {
                                $('#bsab').text("0");
                            } else {
                                $('#bsab').text(beseb);
                            }

                        },
                        error: function (err) {
                            console.log(err);
                        }
                    });
                }
            });

            $('#searchAllLawskeyword').keyup(function () {
                $(".showtext").html("");
                $('#showrules').html("");
                $('#searchBesabkeyword').val("");
                $('#bbr').text("0");
                $('#pbl').text("0");
                $('#pbf').text("0");
                $('#eks10').text("0");
                $('#bsab').text("0");
                $('#aml').text("0");
                $('#mb').text("0");
                $('#jvl').text("0");

            });

            $("#searhAllLaws").click(function () {
                $('#bsab').text("0");
                $('#showrules').html("");
                $('#searchBesabkeyword').val("");
                keywordCategory = 0;
                let timerInterval;
                Swal.fire({
                    title: 'nu Sök!',
                    html: 'Vänta, Automatisk Stängningsvarning.',
                    timer: 20000,
                    timerProgressBar: true,
                    allowOutsideClick: false,
                    onBeforeOpen: () => {
                        Swal.showLoading();
                        timerInterval = setInterval(() => {
                            const content = Swal.getContent();
                            if (content) {
                                const b = content.querySelector('b');
                                if (b) {
                                    b.textContent = Swal.getTimerLeft();
                                }
                            }
                        }, 100)
                    },
                    onClose: () => {
                        clearInterval(timerInterval);
                    }
                }).then((result) => {
                    /* Read more about handling dismissals below */
                    if (result.dismiss === Swal.DismissReason.timer) {
                        console.log('I was closed by the timer');
                    }
                });
                keywordAllLaws = $('#searchAllLawskeyword').val();
                keywordCategory = keywordAllLaws;
                var len = $('#searchAllLawskeyword').val().length;
                if (len < 3) {
                    Swal.fire(
                        'Hoppsan...',
                        'Ange minst tre bokstäver!',
                        'error'
                    );
                } else {
                    $.ajax({
                        type: 'get',
                        url: '<?php echo e(route('search')); ?>',
                        data: {'keywords': keywordAllLaws},
                        success: function (data) {
                            swal.close();
                            $('#showrules1').html(data[0]);
                            Laws = data[2];
                            var bbr = data[1];
                            if (bbr.length > 0) {
                                $('#bbr').text("0");
                                $('#pbl').text("0");
                                $('#pbf').text("0");
                                $('#eks10').text("0");
                                $('#bsab').text("0");
                                $('#jvl').text("0");
                                $('#mb').text("0");
                                $('#aml').text("0");
                                $.each(bbr, function (i, key) {
                                    var value = key['category_id'];
                                    var count = key['count'];

                                    if (value == 1) {
                                        $('#bbr').text(count);

                                    } else if (value == 2) {
                                        $('#pbl').text(count);
                                    } else if (value == 3) {
                                        $('#pbf').text(count);
                                    } else if (value == 4) {
                                        $('#eks10').text(count);
                                    } else if (value == 5) {
                                        $('#aml').text(count);
                                    } else if (value == 6) {
                                        $('#bsab').text(count);
                                    } else if (value == 7) {
                                        $('#mb').text(count);
                                    } else if (value == 8) {
                                        $('#jvl').text(count);
                                    }

                                });
                            }

                        },
                        error: function (err) {
                            console.log(err);
                        }
                    });
                }
            });

            $('#searchAmakeyword').keyup(function () {
                $('#showrulesAma').html("");
                $('.showtextama').text("");
                // $('#lawAMA').summernote('code','');
            });

            $('#searhAma').click(function () {
                var searchAmakeyword = $('#searchAmakeyword').val();
                var len = $('#searchAmakeyword').val().length;
                if (len < 3) {
                    Swal.fire(
                        'Hoppsan...',
                        'Ange minst tre bokstäver!',
                        'error'
                    );
                } else {
                    let timerInterval;
                    Swal.fire({
                        title: 'nu Sök!',
                        html: 'Vänta, Automatisk Stängningsvarning.',
                        timer: 20000,
                        timerProgressBar: true,
                        allowOutsideClick: false,
                        onBeforeOpen: () => {
                            Swal.showLoading();
                            timerInterval = setInterval(() => {
                                const content = Swal.getContent();
                                if (content) {
                                    const b = content.querySelector('b');
                                    if (b) {
                                        b.textContent = Swal.getTimerLeft();
                                    }
                                }
                            }, 100)
                        },
                        onClose: () => {
                            clearInterval(timerInterval);
                        }
                    }).then((result) => {
                        /* Read more about handling dismissals below */
                        if (result.dismiss === Swal.DismissReason.timer) {
                            console.log('I was closed by the timer');
                        }
                    });

                    $.ajax({
                        type: 'get',
                        url: '<?php echo e(route('advance.search')); ?>',
                        data: {'keywords': searchAmakeyword},
                        success: function (data) {
                            $('#showrulesAma').html(data['output']);
                            $('#AmaVer').text(data['count']);
                            swal.close();
                        },
                        error: function (err) {
                            console.log(err);
                        }
                    });
                }
            });

            $('#searchControllPlanButton').click(function () {
                var searchControllPlan = $('#searchControllPlan').val();
                $.ajax({
                    type: 'POST',
                    url: '<?php echo e(route('design.searchControllePlan')); ?>',
                    data: {
                        '_token': $('input[name="_token"]').val(), 'keyword': searchControllPlan,
                        'project_id': project_id
                    },
                    success: function (data) {
                        console.log(data);
                        //TODO
                    },
                    error: function (err) {

                    }
                });
            })

            $('#addAmatoControllPoint').click(function () {
                var controllPoint = $('#controllPoint').val();
                var lawAMA = $('#lawAMA').val();
                if (controllPoint != 0) {
                    if (lawAMA != null) {
                        $.ajax({
                            type: 'POST',
                            url: '<?php echo e(route('advance.store')); ?>',
                            data: {
                                '_token': $('input[name="_token"]').val(),
                                'LawAmaText': $("#lawAMA").val(),
                                'controllPoint': controllPoint,
                                'project_id': project_id,
                            },
                            success: function (data) {
                                // console.log(data);
                                Swal.fire(
                                    'Bra jobbat !',
                                    'mall registreringen slutförd',
                                    'success');
                                getTemplateKontrollPlan(project_id);
                                $('#showrulesAma').html("");
                                $('.showtextama').text("");
                                $('#lawAMA').summernote('code', '');
                            },
                            error: function (err) {

                            }
                        });
                    } else {
                        Swal.fire(
                            'fel',
                            'Texten i lagen är tom',
                            'error');
                    }

                } else {
                    Swal.fire(
                        'fel',
                        'Vald Design Fönster',
                        'error');
                }

            })

            $("#copyLaw").click(function () {
                if ($('.showtext').text() == "") {
                    Swal.fire(
                        'fel',
                        'Välj en regel från listan',
                        'error');
                } else {
                    var text = '';
                    if (Law['Text'] != null) {
                        text = Law['Text'];
                    }
                    var data = Law;
                    var arr = [];
                    var catLaw = '';
                    if (data['category_id'] == 1) {
                        catLaw = 'BBR : ';
                    } else if (data['category_id'] == 2) {
                        catLaw = 'PBL : ';
                    } else if (data['category_id'] == 3) {
                        catLaw = 'PBF : ';
                    } else if (data['category_id'] == 4) {
                        catLaw = 'EKS10 : ';
                    } else if (data['category_id'] == 5) {
                        catLaw = 'AML : ';
                    } else if (data['category_id'] == 6) {
                        catLaw = 'BSAB : ';
                    } else if (data['category_id'] == 7) {
                        catLaw = 'MB : ';
                    } else if (data['category_id'] == 8) {
                        catLaw = 'JVL : ';
                    }

                    data['updated_at'] = null;
                    data['created_at'] = null;
                    data['category_id'] = null;
                    data['Text'] = null;
                    data['Stycke5'] = null;
                    data['Stycke4'] = null;
                    data['Stycke3'] = null;
                    data['Stycke2'] = null;
                    data['Stycke1'] = null;
                    data['Huvudparagraf'] = null;
                    data['id'] = null;
                    $.each(Object.keys(data), function (i, key) {
                        var value = data[key];
                        if (value != null) {
                            title = catLaw + value;
                            FinalTextLaw = $('#law').val();
                            // $("#law").summernote('code', '<p>' +FinalTextLaw+'<b>'+title+'</b><p>'+text+'</p></p>');
                            FinalTextLaw = $('#law').val();
                            $("#law").summernote('code', '<p>' + FinalTextLaw + '<b>' + title + '</b></p>');
                            // return false; // breaks
                        }
                    });
                    // $('.showtext').html("");
                    // $('#myid').html("");
                    // $('#search').val("");
                    // $('#bbr').text(0);
                    // $('#pbl').text(0);
                    // $('#pbf').text(0);
                    // $('#eks10').text(0);
                    // $('#bsab').text(0);
                }
            });

            $("#copyLawKa").click(function () {
                if ($('.showtextKa').text() == "") {
                    Swal.fire(
                        'fel',
                        'Välj en regel från listan',
                        'error');
                } else {
                    var text = '';
                    console.log(LawKa)
                    if (LawKa['Text'] != null) {
                        text = LawKa['Text'];
                    }
                    var data = LawKa;
                    var arr = [];
                    var catLaw = '';
                    if (data['category_id'] == 1) {
                        catLaw = 'BBR : ';
                    } else if (data['category_id'] == 2) {
                        catLaw = 'PBL : ';
                    } else if (data['category_id'] == 3) {
                        catLaw = 'PBF : ';
                    } else if (data['category_id'] == 4) {
                        catLaw = 'EKS10 : ';
                    } else if (data['category_id'] == 5) {
                        catLaw = 'AML : ';
                    } else if (data['category_id'] == 6) {
                        catLaw = 'BSAB : ';
                    } else if (data['category_id'] == 7) {
                        catLaw = 'MB : ';
                    } else if (data['category_id'] == 8) {
                        catLaw = 'JVL : ';
                    }

                    data['updated_at'] = null;
                    data['created_at'] = null;
                    data['category_id'] = null;
                    data['Text'] = null;
                    data['Stycke5'] = null;
                    data['Stycke4'] = null;
                    data['Stycke3'] = null;
                    data['Stycke2'] = null;
                    data['Stycke1'] = null;
                    data['Huvudparagraf'] = null;
                    data['id'] = null;
                    $.each(Object.keys(data), function (i, key) {
                        var value = data[key];
                        if (value != null) {
                            title = catLaw + value;
                            FinalTextLawKa = $('#lawKa').val();
                            // $("#lawKa").summernote('code', '<p>' +FinalTextLawKa+'<b>'+title+'</b><p>'+text+'</p></p>');
                            FinalTextLawKa = $('#lawKa').val();
                            console.log(title)
                            console.log(FinalTextLawKa)

                            $("#lawKa").summernote('code', '<p>' + FinalTextLawKa + '<b>' + title + '</b></p>');
                            // return false; // breaks
                        }
                    });
                    // $('.showtext').html("");
                    // $('#myid').html("");
                    // $('#search').val("");
                    // $('#bbr').text(0);
                    // $('#pbl').text(0);
                    // $('#pbf').text(0);
                    // $('#eks10').text(0);
                    // $('#bsab').text(0);
                }
            });

            $('#copyText').click(function () {
                var text = '';
                if (LawText != null) {
                    text = LawText;
                }
                FinalTextLaw = $('#law').val();
                $("#law").summernote('code', '<p>' + FinalTextLaw + '<b></b><p>' + text + '</p></p>');

            })

            $('#copyTextKa').click(function () {
                var text = '';
                if (LawText != null) {
                    text = LawTextKa;
                }
                FinalTextLawKa = $('#lawKa').val();
                $("#lawKa").summernote('code', '<p>' + FinalTextLawKa + '<b></b><p>' + text + '</p></p>');

            })

            var count = 0;
            $("#addControllPlan").click(function () {
                var formData = new FormData();
                var robrik_id = $('#robrik_id1').val();
                var ka_id = $('#sub_robrik_id').val();
                var BSABAMA = $('#besabtext').val();
                var PBLKategori = $("#PBLKategori option:selected").val();
                var AnsvarigPart = $("#selectAnsvarig option:selected").val();
                var AnsvarigPartText = $("#selectAnsvarig option:selected").html();
                var importSelect = $("#importSelect").val();

                var Kontrollskede = $("#Kontrollskede option:selected").val();
                var kontrollskedeText = $("#kontrollskedeText").val();

                var Risk = $("#Risk option:selected").val();
                var riskText = $("#riskText").val();
                var riskDescription = $("#riskdescription").val();

                var ProduceratIntyg = $("#Producerat option:selected").val();
                var ProduceratIntygText = $("#ProduceratIntygText").val();
                var ProduceratIntygDescription = $("#PIntygdescription").val();

                var Verifieringsmetod = $("#Verifieringsmetod option:selected").val();
                var VerifieringsmetodText = $("#VerifieringsmetodText").val();
                var VerifieringsmetodDescription = $("#Vmetoddescription").val();

                var KraverPlatsbesok = $("#KraverPlatsbesok option:selected").val();
                var KraverPlatsbesokText = $("#KraverPlatsbesokText").val();
                var KraverPlatsbesokDescription = $("#KraverPdescription").val();

                formData.append('project_id', project_id);
                formData.append('robrik_id', robrik_id);
                formData.append('ka_id', ka_id);
                formData.append('BSABAMA', BSABAMA);
                formData.append('PBLKategori', PBLKategori);
                formData.append('AnsvarigPart', AnsvarigPart);
                formData.append('AnsvarigPartText', AnsvarigPartText);
                formData.append('Kontrollskede', Kontrollskede);
                formData.append('kontrollskedeText', kontrollskedeText);
                formData.append('Risk', Risk);
                formData.append('riskText', riskText);
                formData.append('riskDescription', riskDescription);
                formData.append('ProduceratIntyg', ProduceratIntyg);
                formData.append('ProduceratIntygText', ProduceratIntygText);
                formData.append('ProduceratIntygDescription', ProduceratIntygDescription);
                formData.append('Verifieringsmetod', Verifieringsmetod);
                formData.append('VerifieringsmetodText', VerifieringsmetodText);
                formData.append('VerifieringsmetodDescription', VerifieringsmetodDescription);
                formData.append('KraverPlatsbesok', KraverPlatsbesok);
                formData.append('KraverPlatsbesokText', KraverPlatsbesokText);
                formData.append('KraverPlatsbesokDescription', KraverPlatsbesokDescription);
                formData.append('importSelect', importSelect);
                formData.append('LawText', $(".textarea").val());
                formData.append('image', $("#image")[0].files[0]);
                formData.append('_token', $('input[name="_token"]').val());
                formData.append('Funktionskrav', $('#Funktionskrav').val());
                count++;
                $('#count').html(count);

                $.ajax({
                    type: 'POST',
                    url: '<?php echo e(route('design.store')); ?>',
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function (data) {
                        getTemplate(project_id);
                        Swal.fire(
                            'Bra jobbat !',
                            'Kontrollpunkten är registrerad',
                            'success');
                        $('.textarea').summernote('code', '');
                        getTemplateKontrollPlan(project_id);
                        getTemplateForAdvance(project_id);
                        $('#searchAnsvarig').val("");
                        $("#PBLKategori").val("").change();
                        $("#selectAnsvarig").val("0").change();
                        $("#Kontrollskede").val("0").change();
                        $("#Risk").val("0").change();
                        $("#Producerat").val("0").change();
                        $("#Verifieringsmetod").val("0").change();
                        $("#KraverPlatsbesok").val("0").change();

                        $('#searchAllLawskeyword').val("");
                        $('#besabtext').val("");
                        $('#showrules1').html("");
                        $('.showtext').html("");

                        $('#bbr').text("0");
                        $('#pbl').text("0");
                        $('#pbf').text("0");
                        $('#eks10').text("0");
                        $('#bsab').text("0");
                        $('#jvl').text("0");
                        $('#mb').text("0");
                        $('#aml').text("0");

                        $('#riskText').val("");
                        $('#riskText').parent().css({"display": "none"});
                        $("#riskdescription").val("");

                        $('#ProduceratIntygText').val("");
                        $('#ProduceratIntygText').parent().css({"display": "none"});
                        $("#PIntygdescription").val("");

                        $('#VerifieringsmetodText').val();
                        $('#VerifieringsmetodText').parent().css({"display": "none"});
                        $("#Vmetoddescription").val("");

                        $('#KraverPlatsbesokText').val("");
                        $('#KraverPlatsbesokText').parent().css({"display": "none"});
                        $("#KraverPdescription").val("");
                        $('#Funktionskrav').val("");
                        $('.shownameupload').text("");

                    },
                    error: function (err) {

                    }
                });
            });

            $("#addControllPlanKa").click(function () {
                let AnsvarigPart = []
                let AnsvarigPartText = []
                $('select[name="AnsvarigPart[]"] option:selected').each(function () {
                    AnsvarigPart.push($(this).val())
                    AnsvarigPartText.push($(this).val())
                })

                var formData = new FormData();
                var robrik_id = $("#robrik").val();
                var BSABAMA = $('#besabtext1').val();
                var PBLKategori = $("#PBLKategori1 option:selected").val();
                var importSelect = $("#importSelect1").val();

                var Kontrollskede = $("#Kontrollskede1 option:selected").val();
                var kontrollskedeText = $("#kontrollskedeText1").val();

                var Risk = $("#Risk1 option:selected").val();
                var riskText = $("#riskText1").val();
                var riskDescription = $("#riskdescription1").val();

                var ProduceratIntyg = $("#Producerat1 option:selected").val();
                var ProduceratIntygText = $("#ProduceratIntygText1").val();
                var ProduceratIntygDescription = $("#PIntygdescription1").val();

                var Verifieringsmetod = $("#Verifieringsmetod1 option:selected").val();
                var VerifieringsmetodText = $("#VerifieringsmetodText1").val();
                var VerifieringsmetodDescription = $("#Vmetoddescription1").val();

                var KraverPlatsbesok = $("#KraverPlatsbesok1 option:selected").val();
                var KraverPlatsbesokText = $("#KraverPlatsbesokText1").val();
                var KraverPlatsbesokDescription = $("#KraverPdescription1").val();
                var Funktionskrav = $('#Funktionskrav1').val()
                var EndastSignering = $("#EndastSignering option:selected").val()

                formData.append('project_id', project_id);
                formData.append('robrik_id', robrik_id);
                formData.append('BSABAMA', BSABAMA);
                formData.append('PBLKategori', PBLKategori);
                formData.append('AnsvarigPart', AnsvarigPart);
                formData.append('AnsvarigPartText', AnsvarigPartText);
                formData.append('Kontrollskede', Kontrollskede);
                formData.append('kontrollskedeText', kontrollskedeText);
                formData.append('Risk', Risk);
                formData.append('riskText', riskText);
                formData.append('riskDescription', riskDescription);
                formData.append('ProduceratIntyg', ProduceratIntyg);
                formData.append('ProduceratIntygText', ProduceratIntygText);
                formData.append('ProduceratIntygDescription', ProduceratIntygDescription);
                formData.append('Verifieringsmetod', Verifieringsmetod);
                formData.append('VerifieringsmetodText', VerifieringsmetodText);
                formData.append('VerifieringsmetodDescription', VerifieringsmetodDescription);
                formData.append('KraverPlatsbesok', KraverPlatsbesok);
                formData.append('KraverPlatsbesokText', KraverPlatsbesokText);
                formData.append('KraverPlatsbesokDescription', KraverPlatsbesokDescription);
                formData.append('importSelect', importSelect);
                formData.append('LawText', $(".textarea").val());
                formData.append('image', $("#image1")[0].files[0]);
                formData.append('_token', $('input[name="_token"]').val());
                formData.append('Funktionskrav', Funktionskrav);
                formData.append('EndastSignering', EndastSignering);
                count++;
                $('#count').html(count);

                $.ajax({
                    type: 'POST',
                    url: '<?php echo e(route('ka.design')); ?>',
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function (data) {
                        getTemplate(project_id);
                        getAllTemplate(project_id)
                        Swal.fire(
                            'Bra jobbat !',
                            'Kontrollpunkten är registrerad',
                            'success');
                        $('#lawKa').summernote('code', '');
                        getTemplateKa(project_id);
                        $('#searchAnsvarig1').val("");
                        $("#PBLKategori1").val("").change();
                        $("#selectAnsvarig1").val("0").change();
                        $("#Kontrollskede1").val("0").change();
                        $("#Risk1").val("0").change();
                        $("#Producerat1").val("0").change();
                        $("#Verifieringsmetod1").val("0").change();
                        $("#KraverPlatsbesok1").val("0").change();

                        $('#searchAllLawskeyword1').val("");
                        $('#besabtext1').val("");
                        $('#showrules1').html("");
                        $('.showtextKa').html("");

                        $('#bbr1').text("0");
                        $('#pbl1').text("0");
                        $('#pbf1').text("0");
                        $('#eks101').text("0");
                        $('#bsab1').text("0");
                        $('#jvl1').text("0");
                        $('#mb1').text("0");
                        $('#aml1').text("0");

                        $('#riskText1').val("");
                        $('#riskText1').parent().css({"display": "none"});
                        $("#riskdescription1").val("");

                        $('#ProduceratIntygText1').val("");
                        $('#ProduceratIntygText1').parent().css({"display": "none"});
                        $("#PIntygdescription1").val("");

                        $('#VerifieringsmetodText1').val();
                        $('#VerifieringsmetodText1').parent().css({"display": "none"});
                        $("#Vmetoddescription1").val("");

                        $('#KraverPlatsbesokText1').val("");
                        $('#KraverPlatsbesokText1').parent().css({"display": "none"});
                        $("#KraverPdescription1").val("");
                        $('#Funktionskrav1').val("");
                        $('#EndastSignering1').val("");
                        $('.shownameupload').text("");


                    },
                    error: function (err) {

                    }
                });
            });



            var desginid = '';
            $("#asignUser").on("show.bs.modal", function (e) {
                desginid = $(e.relatedTarget).data('target-id');
            });

            $("#btn-add-user").click(function () {
                var organization_id = $("#selectOrganization option:selected").val();
                var user_id = $("#selectUser option:selected").val();

                $.ajax({
                    type: 'POST',
                    url: '/panel/design/user/assignUser/' + desginid,
                    data: {
                        "_token": "<?php echo e(csrf_token()); ?>", 'title': $('#title').val(),
                        'organization_id': organization_id, 'user_id': user_id
                    },
                    success: function (data) {
                        Swal.fire(
                            'Bra jobbat !',
                            'tilldelad användare slutförd',
                            'success');
                        getTemplateKontrollPlan(project_id);
                        $('#asignUser').modal('hide');
                    },
                    error: function (err) {
                        console.log(err);
                    }
                });
            });

            $('#editTemplate').click(function () {
                var formData = new FormData();
                var robrik_id = $('#robrik_id1').val();
                var ka_id = $('#sub_robrik_id').val();
                var BSABAMA = $('#besabtext').val();
                var PBLKategori = $("#PBLKategori option:selected").val();
                var AnsvarigPart = $("#selectAnsvarig option:selected").val();
                var AnsvarigPartText = $("#selectAnsvarig option:selected").html();
                var importSelect = $("#importSelect").val();

                var Kontrollskede = $("#Kontrollskede option:selected").val();
                var kontrollskedeText = $("#kontrollskedeText").val();
                if (Kontrollskede != 500) {
                    kontrollskedeText = "";
                }


                var Risk = $("#Risk option:selected").val();
                var riskText = $("#riskText").val();
                var riskDescription = $("#riskdescription").val();
                if (Risk == 500) {
                    riskDescription = "";
                } else {
                    riskText = "";
                }

                var Producerat = $("#Producerat option:selected").val();
                var ProduceratIntygText = $("#ProduceratIntygText").val();
                var ProduceratIntygDescription = $("#PIntygdescription").val();
                if (Producerat == 500) {
                    ProduceratIntygDescription = "";
                } else {
                    ProduceratIntygText = "";
                }

                var Verifieringsmetod = $("#Verifieringsmetod option:selected").val();
                var VerifieringsmetodText = $("#VerifieringsmetodText").val();
                var VerifieringsmetodDescription = $("#Vmetoddescription").val();
                if (Verifieringsmetod == 500) {
                    VerifieringsmetodDescription = "";
                } else {
                    VerifieringsmetodText = "";
                }

                var KraverPlatsbesok = $("#KraverPlatsbesok option:selected").val();
                var KraverPlatsbesokText = $("#KraverPlatsbesokText").val();
                var KraverPlatsbesokDescription = $("#KraverPdescription").val();
                if (KraverPlatsbesok == 500) {
                    KraverPlatsbesokDescription = "";
                } else {
                    KraverPlatsbesokText = "";
                }

                formData.append('robrik_id', robrik_id);
                formData.append('ka_id', ka_id);
                formData.append('BSABAMA', BSABAMA);
                formData.append('PBLKategori', PBLKategori);
                formData.append('AnsvarigPart', AnsvarigPart);
                formData.append('AnsvarigPartText', AnsvarigPartText);

                formData.append('Kontrollskede', Kontrollskede);
                formData.append('kontrollskedeText', kontrollskedeText);
                formData.append('Risk', Risk);
                formData.append('riskText', riskText);
                formData.append('riskDescription', riskDescription);
                formData.append('ProduceratIntyg', Producerat);
                formData.append('ProduceratIntygText', ProduceratIntygText);
                formData.append('ProduceratIntygDescription', ProduceratIntygDescription);
                formData.append('Verifieringsmetod', Verifieringsmetod);
                formData.append('VerifieringsmetodText', VerifieringsmetodText);
                formData.append('VerifieringsmetodDescription', VerifieringsmetodDescription);
                formData.append('KraverPlatsbesok', KraverPlatsbesok);
                formData.append('KraverPlatsbesokText', KraverPlatsbesokText);
                formData.append('KraverPlatsbesokDescription', KraverPlatsbesokDescription);
                formData.append('importSelect', importSelect);
                formData.append('LawText', $(".textarea").val());
                formData.append('image', $("#image")[0].files[0]);
                formData.append('_token', $('input[name="_token"]').val());
                formData.append('project_id', project_id);
                formData.append('Funktionskrav', $('#Funktionskrav').val());

                $.ajax({
                    type: 'POST',
                    url: '/panel/design/' + design_id,
                    processData: false,
                    contentType: false,
                    data: formData,
                    success: function (data) {
                        Swal.fire(
                            'Bra jobbat !',
                            'mall uppdatering slutförd',
                            'success');
                        $('.textarea').summernote('code', '');
                        getTemplate(project_id);
                        getTemplateKontrollPlan(project_id);
                        getTemplateForAdvance(project_id);
                        $('#searchAnsvarig').val("");
                        $("#PBLKategori").val("").change();
                        $("#selectAnsvarig").val("0").change();
                        $("#Kontrollskede").val("0").change();
                        $("#Risk").val("0").change();
                        $("#Producerat").val("0").change();
                        $("#Verifieringsmetod").val("0").change();
                        $("#KraverPlatsbesok").val("0").change();

                        $('#searchAllLawskeyword').val("");
                        $('#besabtext').val("");
                        $('#showrules1').html("");
                        $('.showtext').html("");

                        $('#bbr').text("0");
                        $('#pbl').text("0");
                        $('#pbf').text("0");
                        $('#eks10').text("0");
                        $('#bsab').text("0");
                        $('#jvl').text("0");
                        $('#mb').text("0");
                        $('#aml').text("0");

                        $('#riskText').val("");
                        $('#riskText').parent().css({"display": "none"});
                        $("#riskdescription").val("");

                        $('#ProduceratIntygText').val("");
                        $('#ProduceratIntygText').parent().css({"display": "none"});
                        $("#PIntygdescription").val("");

                        $('#VerifieringsmetodText').val();
                        $('#VerifieringsmetodText').parent().css({"display": "none"});
                        $("#Vmetoddescription").val("");

                        $('#KraverPlatsbesokText').val("");
                        $('#KraverPlatsbesokText').parent().css({"display": "none"});
                        $("#KraverPdescription").val("");
                        $('#showImageDesign').hide();
                        $('.shownameupload').text("");

                    },
                    error: function (err) {

                    }
                });
                $('#addControllPlan').show();
            });

            $('#editTemplateKa').click(function () {
                let AnsvarigPart = []
                let AnsvarigPartText = []
                $('select[name="AnsvarigPart[]"] option:selected').each(function () {
                    AnsvarigPart.push($(this).val())
                    AnsvarigPartText.push($(this).val())
                })
                var formData = new FormData();
                var robrik_id = $('#robrik').val();
                var project_id = project_id;
                var BSABAMA = $('#besabtext1').val();
                var PBLKategori = $("#PBLKategori1 option:selected").val();
                var importSelect = $("#importSelect").val();
                var Funktionskrav = $('#Funktionskrav1').val()
                var EndastSignering = $("#EndastSignering option:selected").val()

                var Kontrollskede = $("#Kontrollskede1 option:selected").val();
                var kontrollskedeText = $("#kontrollskedeText1").val();
                if (Kontrollskede != 500) {
                    kontrollskedeText = "";
                }


                var Risk = $("#Risk1 option:selected").val();
                var riskText = $("#riskText1").val();
                var riskDescription = $("#riskdescription1").val();
                if (Risk == 500) {
                    riskDescription = "";
                } else {
                    riskText = "";
                }

                var Producerat = $("#Producerat1 option:selected").val();
                var ProduceratIntygText = $("#ProduceratIntygText1").val();
                var ProduceratIntygDescription = $("#PIntygdescription1").val();
                if (Producerat == 500) {
                    ProduceratIntygDescription = "";
                } else {
                    ProduceratIntygText = "";
                }

                var Verifieringsmetod = $("#Verifieringsmetod1 option:selected").val();
                var VerifieringsmetodText = $("#VerifieringsmetodText1").val();
                var VerifieringsmetodDescription = $("#Vmetoddescription1").val();
                if (Verifieringsmetod == 500) {
                    VerifieringsmetodDescription = "";
                } else {
                    VerifieringsmetodText = "";
                }

                var KraverPlatsbesok = $("#KraverPlatsbesok1 option:selected").val();
                var KraverPlatsbesokText = $("#KraverPlatsbesokText1").val();
                var KraverPlatsbesokDescription = $("#KraverPdescription1").val();
                if (KraverPlatsbesok == 500) {
                    KraverPlatsbesokDescription = "";
                } else {
                    KraverPlatsbesokText = "";
                }

                formData.append('robrik_id', robrik_id);
                formData.append('BSABAMA', BSABAMA);
                formData.append('PBLKategori', PBLKategori);
                formData.append('AnsvarigPart', AnsvarigPart);
                formData.append('AnsvarigPartText', AnsvarigPartText);

                formData.append('Kontrollskede', Kontrollskede);
                formData.append('kontrollskedeText', kontrollskedeText);
                formData.append('Risk', Risk);
                formData.append('riskText', riskText);
                formData.append('riskDescription', riskDescription);
                formData.append('ProduceratIntyg', Producerat);
                formData.append('ProduceratIntygText', ProduceratIntygText);
                formData.append('ProduceratIntygDescription', ProduceratIntygDescription);
                formData.append('Verifieringsmetod', Verifieringsmetod);
                formData.append('VerifieringsmetodText', VerifieringsmetodText);
                formData.append('VerifieringsmetodDescription', VerifieringsmetodDescription);
                formData.append('KraverPlatsbesok', KraverPlatsbesok);
                formData.append('KraverPlatsbesokText', KraverPlatsbesokText);
                formData.append('KraverPlatsbesokDescription', KraverPlatsbesokDescription);
                formData.append('importSelect', importSelect);
                formData.append('LawText', $("#lawKa").val());
                formData.append('image', $("#image")[0].files[0]);
                formData.append('_token', $('input[name="_token"]').val());
                formData.append('project_id', project_id);
                formData.append('Funktionskrav', Funktionskrav);
                formData.append('EndastSignering', EndastSignering);

                $.ajax({
                    type: 'POST',
                    url: '/panel/ka/' + ka_id,
                    processData: false,
                    contentType: false,
                    data: formData,
                    success: function (data) {
                        getTemplate(project_id);
                        getAllTemplate(project_id)
                        Swal.fire(
                            'Bra jobbat !',
                            'mall uppdatering slutförd',
                            'success');
                        $('#lawKa').summernote('code', '');
                        getTemplateKa(data['data']);
                        $('#searchAnsvarig1').val("");
                        $("#PBLKategori1").val("").change();
                        $("#selectAnsvarig1").val("0").change();
                        $("#Kontrollskede1").val("0").change();
                        $("#Risk1").val("0").change();
                        $("#Producerat1").val("0").change();
                        $("#Verifieringsmetod1").val("0").change();
                        $("#KraverPlatsbesok1").val("0").change();
                        $("#KraverPlatsbesok1").val("0").change();
                        $("#EndastSignering").val("0").change();

                        $('#keywordsKa').val("");
                        $('#besabtext1').val("");
                        $('#showrulesKaAll').html("");
                        $('.showtextKa').html("");

                        $('#bbr1').text("0");
                        $('#pbl1').text("0");
                        $('#pbf1').text("0");
                        $('#eks101').text("0");
                        $('#bsab1').text("0");
                        $('#jvl1').text("0");
                        $('#mb1').text("0");
                        $('#aml1').text("0");

                        $('#riskText1').val("");
                        $('#riskText1').parent().css({"display": "none"});
                        $("#riskdescription1").val("");

                        $('#ProduceratIntygText1').val("");
                        $('#ProduceratIntygText1').parent().css({"display": "none"});
                        $("#PIntygdescription1").val("");

                        $('#VerifieringsmetodText1').val();
                        $('#VerifieringsmetodText1').parent().css({"display": "none"});
                        $("#Vmetoddescription1").val("");

                        $('#KraverPlatsbesokText1').val("");
                        $('#KraverPlatsbesokText1').parent().css({"display": "none"});
                        $("#KraverPdescription1").val("");
                        $("#Funktionskrav1").val("");
                        $("#EndastSignering1").val("");
                        $('#showImageDesign1').hide();
                        $('.shownameupload').text("");

                    },
                    error: function (err) {

                    }
                });
                $('#addControllPlanKa').show();
            });

            $('#deleteTemplate').click(function () {
                deleteConfirmation(design_id);
                $('#addControllPlan').show();
                $('.textarea').summernote('code', '');
                $('#besabtext').val('');
                $('#searchAnsvarig').val('');
                $("#PBLKategori").val("0").change();
                $("#selectAnsvarig").val("0").change();
                $("#Kontrollskede").val("0").change();
                $("#Risk").val("0").change();
                $("#Producerat").val("0").change();
                $("#Verifieringsmetod").val("0").change();
                $("#KraverPlatsbesok").val("0").change();

                $('#riskText').val("");
                $('#riskText').parent().css({"display": "none"});
                $("#riskdescription").val("");

                $('#ProduceratIntygText').val("");
                $('#ProduceratIntygText').parent().css({"display": "none"});
                $("#PIntygdescription").val("");

                $('#VerifieringsmetodText').val();
                $('#VerifieringsmetodText').parent().css({"display": "none"});
                $("#Vmetoddescription").val("");

                $('#KraverPlatsbesokText').val("");
                $('#KraverPlatsbesokText').parent().css({"display": "none"});
                $("#KraverPdescription").val("");
                $("#Funktionskrav").val("");
                $('.shownameupload').text("");
                getTemplate(project_id);
                getTemplateKontrollPlan(project_id)
                getTemplateForAdvance(project_id);
            });

            $('#deleteTemplateKa').click(function () {
                deleteConfirmationKa(ka_id)
                $('#addControllPlanKa').show();
                $('#lawKa').summernote('code', '');
                $('#besabtext1').val('');
                $('#searchAnsvarig1').val('');
                $("#PBLKategori1").val("0").change();
                $("#selectAnsvarig1").val("0").change();
                $("#Kontrollskede1").val("0").change();
                $("#Risk1").val("0").change();
                $("#Producerat1").val("0").change();
                $("#Verifieringsmetod1").val("0").change();
                $("#KraverPlatsbesok1").val("0").change();

                $('#riskText1').val("");
                $('#riskText1').parent().css({"display": "none"});
                $("#riskdescription1").val("");

                $('#ProduceratIntygText1').val("");
                $('#ProduceratIntygText1').parent().css({"display": "none"});
                $("#PIntygdescription1").val("");

                $('#VerifieringsmetodText1').val();
                $('#VerifieringsmetodText1').parent().css({"display": "none"});
                $("#Vmetoddescription1").val("");

                $('#KraverPlatsbesokText1').val("");
                $('#KraverPlatsbesokText1').parent().css({"display": "none"});
                $("#KraverPdescription1").val("");
                $("#Funktionskrav1").val("");
                $("#EndastSignering1").val("");
                $('.shownameupload').text("");
                getTemplateKa(project_id);
            });


            $('#btn-saveTemplate').click(function () {
                var templateName = $('#templateName').val();
                var templateCategory = $("#templateCategory option:selected").val();

                $.ajax({
                    type: 'POST',
                    url: '<?php echo e(route('template.store')); ?>',
                    data: {
                        '_token': $('input[name="_token"]').val(), 'templateName': templateName,
                        'templateCategory': templateCategory, 'designs': designs, 'is_supper_user': 0
                    },
                    success: function (data) {
                        $('#saveTemplate').modal('hide');
                        Swal.fire(
                            'Bra jobbat !',
                            'mall registreringen slutförd',
                            'success');
                    },
                    error: function (err) {
                        console.log(err);
                    }
                });

            });


            $('#selectOrganization').change(function () {
                var organizationId = $(this).children("option:selected").val();
                $.ajax({
                    type: 'get',
                    url: '/panel/design/getUserOrganization/' + organizationId,
                    success: function (data) {
                        $('#selectUser').html(data);
                    }
                });
            });


            $('#organizationForm').submit(function (event) {
                event.preventDefault();
                var formData = new FormData(this);
                let token = $('input[name="_token"]').val();
                formData.append('_token', token);
                $.ajax({
                    type: 'POST',
                    url: '<?php echo e(route('addOrganizations')); ?>',
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function (data) {
                        Swal.fire(
                            'Bra jobbat !',
                            'Organisation registreringen slutförd',
                            'success');
                        $('#tbl_organization').DataTable().ajax.reload();
                        selectOrganizations();
                        $('#organizationForm')[0].reset();
                        $('.shownameupload').text("");
                        $('#neworganization').modal('hide');
                        $('.shownameupload').text("");
                    },
                    error: function (err) {
                        console.log(err);
                    }
                });
            });

            $('#editOrganizationForm').submit(function (event) {
                event.preventDefault();
                var formData = new FormData(this);
                let token = $('input[name="_token"]').val();
                formData.append('_token', token);
                $.ajax({
                    type: 'POST',
                    url: '/panel/project/organization/update/' + organizationidEdit,
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function (data) {
                        $('#tbl_organization').DataTable().ajax.reload();
                        Swal.fire(
                            'Bra jobbat !',
                            'organisation uppdatering slutförd',
                            'success');

                        $('.shownameupload').text("");
                        $('#editOrganizationForm')[0].reset();
                        $('#editorganization').modal('hide');
                        getOrganization();
                        selectOrganizations();
                        $('.shownameupload').text("");
                    },
                    error: function (err) {
                        console.log(err);
                    }
                });
            });


            $("#CreatUser").on("show.bs.modal", function (e) {
                organizationidu = $(e.relatedTarget).data('target-organizationid');
            });


            $('#createUserForm').submit(function (event) {
                event.preventDefault();
                var formData = new FormData(this);
                let token = $('input[name="_token"]').val();
                formData.append('_token', token);
                $.ajax({
                    type: 'POST',
                    url: '/panel/project/organization/addUser/' + organizationidu,
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function (data) {
                        Swal.fire(
                            'Bra jobbat !',
                            'användare registreringen slutförd',
                            'success');
                        $('#createUserForm')[0].reset();
                        $('.shownameupload').text("");
                        $('#CreatUser').modal('hide');
                        $('.shownameupload').text("");
                        document.forms["createUserForm"].reset();
                    },
                    error: function (err) {
                        console.log(err);
                    }
                });

            });


            $("#editorganization").on("show.bs.modal", function (e) {
                organizationidEdit = $(e.relatedTarget).data('target-id');
                var name = $(e.relatedTarget).data('target-name');
                var description = $(e.relatedTarget).data('target-description');
                var address = $(e.relatedTarget).data('target-address');
                var contact = $(e.relatedTarget).data('target-contact');
                var mobile = $(e.relatedTarget).data('target-mobile');
                var email = $(e.relatedTarget).data('target-email');

                $('#organizationNameEdit').val(name);
                $('#organizationDescriptionEdit').val(description);
                $('#organizationAddressEdit').val(address);
                $('#organizationContactEdit').val(contact);

                $('#organizationMobiltelEdit').val(mobile);
                $('#organizationMailadressEdit').val(email);


            });


            $('.step').click(function () {
                var step = $('input[type="radio"]:checked').val();

                $.ajax({
                    type: 'POST',
                    url: '<?php echo e(route('project.step')); ?>',
                    data: {'_token': $('input[name="_token"]').val(), 'project_id': project_id, 'step': step},
                    success: function (data) {
                        Swal.fire(
                            'Bra jobbat !',
                            'Nytt skede är vald',
                            'success');
                        $('#Stepmodal').modal('hide');
                        getStep();
                    },
                    error: function (err) {
                        console.log(err);
                    }
                });

            });


            $("#copyLawAma").click(function () {

                if ($('.showtextama').summernote('code') == "") {
                    Swal.fire(
                        'fel',
                        'Välj en regel från listan',
                        'error');
                } else {
                    var text = '';
                    if (LawAma['Text'] != null) {
                        text = LawAma['Text'];
                    }
                    var data = LawAma;
                    var arr = [];
                    data['updated_at'] = null;
                    data['created_at'] = null;
                    data['category_id'] = null;
                    data['Text'] = null;
                    data['Stycke5'] = null;
                    data['Stycke4'] = null;
                    data['Stycke3'] = null;
                    data['Stycke2'] = null;
                    data['Stycke1'] = null;
                    data['Huvudparagraf'] = null;
                    data['id'] = null;
                    $.each(Object.keys(data), function (i, key) {
                        var value = data[key];
                        if (value != null) {
                            title = value;
                            FinalTextLaw = $('#lawAMA').val();
                            $("#lawAMA").summernote('code', '<p>' + FinalTextLaw + '<b>' + title + '</b><p>' + text + '</p></p>');
                        }
                    });
                }
            });

            $('#editAma').click(function () {
                $.ajax({
                    type: 'POST',
                    url: '/panel/advance/design-template/' + AmaDesignId,
                    data: {'_token': $('input[name="_token"]').val(), 'LawAmaText': $("#lawAMA").val(),},
                    success: function (data) {
                        Swal.fire(
                            'Bra jobbat !',
                            'mall uppdatering slutförd',
                            'success');
                        getTemplateKontrollPlan(project_id);
                        $('#controllPoint option[value=0]').attr('selected', true);
                        $('#searchAmakeyword').val("");
                        $('#showrulesAma').html("");
                        $('.showtextama').text("");
                        $('#lawAMA').summernote('code', '');
                    },
                    error: function (err) {

                    }
                });
            })

            $('#deleteAma').click(function () {
                controllPoint = $('#controllPoint').val();
                if (controllPoint != 0) {
                    deleteConfirmationAmaLaw(AmaDesignId);
                } else {
                    Swal.fire(
                        'fel',
                        'Vald Design Fönster',
                        'error');
                }

            });


            $("#btnSearchKa").click(function () {
                $('#bsab1').text("0");
                $('#showrules1').html("");
                keywordCategory = 0;
                let timerInterval;
                Swal.fire({
                    title: 'nu Sök!',
                    html: 'Vänta, Automatisk Stängningsvarning.',
                    timer: 20000,
                    timerProgressBar: true,
                    allowOutsideClick: false,
                    onBeforeOpen: () => {
                        Swal.showLoading();
                        timerInterval = setInterval(() => {
                            const content = Swal.getContent();
                            if (content) {
                                const b = content.querySelector('b');
                                if (b) {
                                    b.textContent = Swal.getTimerLeft();
                                }
                            }
                        }, 100)
                    },
                    onClose: () => {
                        clearInterval(timerInterval);
                    }
                }).then((result) => {
                    /* Read more about handling dismissals below */
                    if (result.dismiss === Swal.DismissReason.timer) {
                        console.log('I was closed by the timer');
                    }
                });
                keywordAllLaws = $('#keywordsKa').val();
                keywordCategory = keywordAllLaws;
                var len = $('#keywordsKa').val().length;
                console.log(len)
                if (len < 3) {
                    Swal.fire(
                        'Hoppsan...',
                        'Ange minst tre bokstäver!',
                        'error'
                    );
                } else {
                    $.ajax({
                        type: 'get',
                        url: '<?php echo e(route('ka.searchAll')); ?>',
                        data: {'keywords': keywordAllLaws},
                        success: function (data) {
                            swal.close();
                            $('#showrulesKaAll').html(data[0]);
                            Laws1 = data[2];
                            var bbr1 = data[1];
                            if (bbr1.length > 0) {
                                $('#bbr1').text("0");
                                $('#pbl1').text("0");
                                $('#pbf1').text("0");
                                $('#eks101').text("0");
                                $('#bsab1').text("0");
                                $('#jvl1').text("0");
                                $('#mb1').text("0");
                                $('#aml1').text("0");
                                $.each(bbr1, function (i, key) {
                                    var value = key['category_id'];
                                    var count = key['count'];

                                    if (value == 1) {
                                        $('#bbr1').text(count);

                                    } else if (value == 2) {
                                        $('#pbl1').text(count);
                                    } else if (value == 3) {
                                        $('#pbf1').text(count);
                                    } else if (value == 4) {
                                        $('#eks101').text(count);
                                    } else if (value == 5) {
                                        $('#aml1').text(count);
                                    } else if (value == 6) {
                                        $('#bsab1').text(count);
                                    } else if (value == 7) {
                                        $('#mb1').text(count);
                                    } else if (value == 8) {
                                        $('#jvl1').text(count);
                                    }

                                });
                            }

                        },
                        error: function (err) {
                            console.log(err);
                        }
                    });
                }
            });

        });


        var Law;
        var LawKa;
        var TempID = '';
        var LawText = '';
        var LawTextKa = '';

        // getBesab();


        function getLaw(id) {
            $.ajax({
                type: 'get',
                url: '/panel/law/getLaw/' + id,
                success: function (data) {
                    if (data['Text'] != null) {
                        $('.showtext').text(data['Text']);
                        LawText = data['Text'];
                    } else {
                        $('.showtext').text(data['Hanvisning']);

                    }

                    Law = data;
                    var search = $('#searchAllLawskeyword').val();
                    var context = $('.showtext').text();
                    $("div:contains(search)").each(function () {
                        $('.showtext').html($('.showtext').html().replace(search, '<span style="background:#fdf900">' + search + '</span>'));
                    });


                    // $(".showtext").html(context.replace(search,'<span style="background:#fdf900">'+search+'</span>'));
                }
            });
        }

        function getLawKa(id) {
            $.ajax({
                type: 'get',
                url: '/panel/ka/getLaw/' + id,
                success: function (data) {
                    if (data['Text'] != null) {
                        $('.showtextKa').text(data['Text']);
                        LawTextKa = data['Text'];
                    } else {
                        // $('.showtextKa').text(data['Hanvisning']);

                    }

                    LawKa = data;
                    // var search = $('#searchKAInput').val();
                    // var context = $('.showtextKa').text();
                    // $("div:contains(search)").each(function () {
                    //     // $('.showtextKa').html($('.showtextKa').html().replace(search, '<span style="background:#fdf900">' + search + '</span>'));
                    // });


                    // $(".showtext").html(context.replace(search,'<span style="background:#fdf900">'+search+'</span>'));
                }
            });
        }

        function getBesab(id) {
            const swalWithBootstrapButtons = Swal.mixin({
                customClass: {
                    confirmButton: 'btn btn-success',
                    cancelButton: 'btn btn-danger'
                },
                buttonsStyling: false
            });

            swalWithBootstrapButtons.fire({
                title: 'Är du säker?',
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Ja, Skapa kontrollpunkt',
                cancelButtonText: 'Nej, Avbryt',
                reverseButtons: true
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        type: 'get',
                        url: '/panel/law/getBesabLaw/' + id,
                        success: function (data) {
                            $('.showtext').text(data['Hanvisning']);
                            Law = data;
                            var Hanvisning = '';
                            var besabtext = "";
                            if (typeof Law['Hanvisning'] === "undefined") {
                                $('#besabtext').val("");
                            } else {
                                var checkValBesab = $('#besabtext').val();
                                if (checkValBesab == '') {
                                    besabtext = Law['Hanvisning'];
                                } else {
                                    besabtext = $('#besabtext').val() + '  ' + '|' + ' ' + Law['Hanvisning'];
                                }
                            }


                            $('#besabtext').val(besabtext);
                        }
                    });

                }
            });

        }

        function getBesabKa(id) {
            const swalWithBootstrapButtons = Swal.mixin({
                customClass: {
                    confirmButton: 'btn btn-success',
                    cancelButton: 'btn btn-danger'
                },
                buttonsStyling: false
            });

            swalWithBootstrapButtons.fire({
                title: 'Är du säker?',
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Ja, Skapa kontrollpunkt',
                cancelButtonText: 'Nej, Avbryt',
                reverseButtons: true
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        type: 'get',
                        url: '/panel/ka/getBesabLaw/' + id,
                        success: function (data) {
                            // $('.showtextKa').text(data['Hanvisning']);
                            Law = data;
                            var Hanvisning = '';
                            var besabtext = "";
                            if (typeof Law['Hanvisning'] === "undefined") {
                                $('#besabtext1').val("");
                            } else {
                                var checkValBesab = $('#besabtext1').val();
                                if (checkValBesab == '') {
                                    besabtext = Law['Hanvisning'];
                                } else {
                                    besabtext = $('#besabtext').val() + '  ' + '|' + ' ' + Law['Hanvisning'];
                                }
                            }


                            $('#besabtext1').val(besabtext);
                        }
                    });

                }
            });

        }

        function getTemplate(id) {
            $.ajax({
                type: 'get',
                url: '/panel/design/getTemplate/' + id,
                success: function (data) {
                    designs = data['design'];
                    $('#LawComplete').html(data['output']);
                }
            });
        }

        function getTemplateKa(id) {
            $.ajax({
                type: 'get',
                url: '/panel/ka/design/getTemplate/' + id,
                success: function (data) {
                    designs = data['design'];
                    $('#LawCompleteKa').html(data['output']);
                }
            });
        }


        function getTemplateid(id) {
            $('#addControllPlan').hide()
            TempID = id;
            design_id = id;
            $.ajax({
                type: 'get',
                url: '/panel/design/getTemp/' + id,
                success: function (data) {
                    $('#kontrollskedeText').val("");
                    $('#riskText').val("");
                    $('#ProduceratIntygText').val("");
                    $('#VerifieringsmetodText').val("");
                    $('#KraverPlatsbesokText').val("");
                    $("#riskdescription").val("");
                    $("#PIntygdescription").val("");
                    $("#Vmetoddescription").val("");
                    $("#KraverPdescription").val("");

                    // $("#Kontrollskede").val(data['Kontrollskede']).trigger('change');
                    $('#robrik_id1').val(data['robrik_id']);
                    $('#robrik_id1').trigger('change');

                    $('#robrik_id1').val(data['ka_id']);
                    $('#sub_robrik_id').trigger('change');

                    $('#besabtext').val(data['BSABAMA']);
                    $('#PBLKategori').val(data['PBLKategori']);
                    $('#PBLKategori').trigger('change');
                    $('#searchAnsvarig').val("");
                    $("#selectAnsvarig option:selected").html(data['AnsvarigPart']);
                    $('#Funktionskrav').val(data['Funktionskrav']);
                    var importSelect = '';
                    if (data['KontrollplanPBL'] != '') {
                        importSelect = data['KontrollplanPBL'];
                    } else if (data['KontrollplanAvtal'] != '') {
                        importSelect = data['KontrollplanAvtal'];
                    }

                    $('#importSelect').val(importSelect);
                    $('#importSelect').trigger('change');

                    if (data['image'] != '') {
                        $('#showImageDesign').attr('data-target-src', data['image']);
                        $('#showImageDesignImg').attr('src', data['image']);
                        $('#showImageDesign').show();
                    } else {
                        $('#showImageDesign').hide();
                    }


                    let keywordAnsvaring = data['AnsvarigPartText'];
                    $.ajax({
                        type: 'get',
                        url: '<?php echo e(route('searchAnsvarig')); ?>',
                        data: {'keywords': keywordAnsvaring},
                        success: function (data) {
                            $("#selectAnsvarig").html(data);
                        }
                    });

                    $('#Kontrollskede').val(data['Kontrollskede']);
                    if (data['Kontrollskede'] == 500) {
                        $("#kontrollskedeTextParent").css({"display": "block"});
                        $('#kontrollskedeText').val(data['kontrollskedeText']);
                    }

                    $('#Risk').val(data['Risk']);
                    if (data['Risk'] == 500) {
                        $("#riskText").parent().css({"display": "block"});
                        $('#riskText').val(data['riskText']);
                    } else {
                        $("#riskText").parent().css({"display": "none"});
                        $("#riskdescription").parent().css({"display": "block"});
                        $("#riskdescription").val(data['riskDescription']);
                    }

                    $('#Producerat').val(data['ProduceratIntyg']);
                    if (data['ProduceratIntyg'] == 500) {
                        $("#ProduceratIntygText").parent().css({"display": "block"});
                        $('#ProduceratIntygText').val(data['ProduceratIntygText']);
                    } else {
                        $("#ProduceratIntygText").parent().css({"display": "none"});
                        $("#PIntygdescription").parent().css({"display": "block"});
                        $("#PIntygdescription").val(data['ProduceratIntygDescription']);
                    }


                    $('#Verifieringsmetod').val(data['Verifieringsmetod']);
                    if (data['Verifieringsmetod'] == 500) {
                        $("#VerifieringsmetodText").parent().css({"display": "block"});
                        $('#VerifieringsmetodText').val(data['VerifieringsmetodText']);
                    } else {
                        $("#VerifieringsmetodText").parent().css({"display": "none"});
                        $("#Vmetoddescription").parent().css({"display": "block"});
                        $("#Vmetoddescription").val(data['VerifieringsmetodDescription']);
                    }

                    $('#KraverPlatsbesok').val(data['KraverPlatsbesok']);
                    if (data['KraverPlatsbesok'] == 500) {
                        $("#KraverPlatsbesokText").parent().css({"display": "block"});
                        $('#KraverPlatsbesokText').val(data['KraverPlatsbesokText']);
                    } else {
                        $("#KraverPlatsbesokText").parent().css({"display": "none"});
                        $("#KraverPdescription").parent().css({"display": "block"});
                        $("#KraverPdescription").val(data['KraverPlatsbesokDescription']);
                    }

                    if (data['KontrollplanPBL'] == 1) {
                        $('#importSelect').val('KontrollplanPBL').trigger('change');
                    } else if (data['KontrollplanAvtal'] == 1) {
                        $('#importSelect').val('KontrollplanAvtal').trigger('change');
                    }


                    $('.textarea').summernote('code', data['LawText']);
                }
            });
        }

        function getTemplateKaid(id) {
            $('#addControllPlanKa').hide()
            TempID = id;
            design_id = id;
            $.ajax({
                type: 'get',
                url: '/panel/ka/getTemp/' + id,
                success: function (response) {
                    let data = response['data']
                    let ansvarigpartSelected  = response['ansvarigSelected']
                    let ansvarigPart  = response['ansvarigPart']
                    ka_id = id
                    $('#kontrollskedeText1').val("");
                    $('#riskText1').val("");
                    $('#ProduceratIntygText1').val("");
                    $('#VerifieringsmetodText1').val("");
                    $('#KraverPlatsbesokText1').val("");
                    $("#riskdescription1").val("");
                    $("#PIntygdescription1").val("");
                    $("#Vmetoddescription1").val("");
                    $("#KraverPdescription1").val("");

                    let placeAnsvarigPart = $('#placeAnsvarigPart')
                    let ansvaringPartContent = ``;
                    let allAnsvarig = response['ansvarigPart']
                    for (let i = 0; i <= (allAnsvarig.length - 1); i++) {
                            ansvaringPartContent += `<div class="col-sm-12 pull-left" id="AnsvarigPartParent${i + 1}">
                                    <div class="form-group">
                                        <label style="width:21%;">Ansvarig part</label>
                                        <input type="text" id="searchAnsvarig${i + 1}" style="width: 35.8%;" class="form-control AnsvarigPart"
                                               placeholder="Enter ...">
                                        <select class="form-control" style="width: 35.8%;" name="AnsvarigPart[]"
                                                id="selectAnsvarig${i + 1}">
                                            <option value="0">Välj ett alternativ</option>
                                            <option value="${allAnsvarig[i]['id']}" selected>${allAnsvarig[i]['title']}</option>
                                        </select>
                                        <span class="table-addItem float-right" onclick="romoveAnsvaringPart(${i + 1})"><a href="#" class="text-success"><i class="fa fa-minus fa-2x" aria-hidden="true"></i></a></span>
                                    </div>
                                 </div>`;
                    }


                    placeAnsvarigPart.html(ansvaringPartContent)



                    for (let i = 1; i <= (allAnsvarig.length); i++) {
                        $('#searchAnsvarig' + i).on('keyup', function () {
                            keyword = $(this).val();
                            var KeyID = event.keyCode;
                            if (KeyID == 8) {
                                if (keyCount > 0) {
                                    keyCount--;
                                }

                            } else {
                                keyCount++;
                            }


                            if (keyCount >= 3) {
                                keyword = $(this).val();

                                if (keyword == "") {
                                    $('#selectAnsvarig' + i).html("");
                                }
                                $.ajax({
                                    type: 'get',
                                    url: '<?php echo e(route('searchAnsvarig')); ?>',
                                    data: {'keywords': keyword},
                                    success: function (data) {
                                        $("#selectAnsvarig" + i).html(data);
                                    }
                                });
                                keyCount--;
                            } else {
                                $("#selectAnsvarig" + i).html("");
                            }


                        });
                    }


                    // $("#Kontrollskede").val(data['Kontrollskede']).trigger('change');
                    $('#besabtext1').val(data['BSABAMA']);
                    $('#PBLKategori1').val(data['PBLKategori']);
                    $('#PBLKategori1').trigger('change');

                    $('#Funktionskrav1').val(data['Funktionskrav']);
                    var importSelect = '';
                    if (data['KontrollplanPBL'] != '') {
                        importSelect = data['KontrollplanPBL'];
                    } else if (data['KontrollplanAvtal'] != '') {
                        importSelect = data['KontrollplanAvtal'];
                    }

                    $('#importSelect').val(importSelect);
                    $('#importSelect').trigger('change');

                    if (data['image'] != '') {
                        $('#showImageDesign').attr('data-target-src', data['image']);
                        $('#showImageDesignImg').attr('src', data['image']);
                        $('#showImageDesign').show();
                    } else {
                        $('#showImageDesign').hide();
                    }

                    $('#Kontrollskede1').val(data['Kontrollskede']);
                    if (data['Kontrollskede'] == 500) {
                        $("#kontrollskedeTextParent1").css({"display": "block"});
                        $('#kontrollskedeText1').val(data['kontrollskedeText']);
                    }

                    $('#Risk').val(data['Risk']);
                    if (data['Risk'] == 500) {
                        $("#riskText1").parent().css({"display": "block"});
                        $('#riskText1').val(data['riskText']);
                    } else {
                        $("#riskText1").parent().css({"display": "none"});
                        $("#riskdescription1").parent().css({"display": "block"});
                        $("#riskdescription1").val(data['riskDescription']);
                    }

                    $('#Producerat1').val(data['ProduceratIntyg']);
                    if (data['ProduceratIntyg'] == 500) {
                        $("#ProduceratIntygText1").parent().css({"display": "block"});
                        $('#ProduceratIntygText1').val(data['ProduceratIntygText']);
                    } else {
                        $("#ProduceratIntygText1").parent().css({"display": "none"});
                        $("#PIntygdescription1").parent().css({"display": "block"});
                        $("#PIntygdescription1").val(data['ProduceratIntygDescription']);
                    }


                    $('#Verifieringsmetod1').val(data['Verifieringsmetod']);
                    if (data['Verifieringsmetod'] == 500) {
                        $("#VerifieringsmetodText1").parent().css({"display": "block"});
                        $('#VerifieringsmetodText1').val(data['VerifieringsmetodText']);
                    } else {
                        $("#VerifieringsmetodText1").parent().css({"display": "none"});
                        $("#Vmetoddescription1").parent().css({"display": "block"});
                        $("#Vmetoddescription1").val(data['VerifieringsmetodDescription']);
                    }

                    $('#KraverPlatsbesok1').val(data['KraverPlatsbesok']);
                    if (data['KraverPlatsbesok'] == 500) {
                        $("#KraverPlatsbesokText1").parent().css({"display": "block"});
                        $('#KraverPlatsbesokText1').val(data['KraverPlatsbesokText']);
                    } else {
                        $("#KraverPlatsbesokText1").parent().css({"display": "none"});
                        $("#KraverPdescription1").parent().css({"display": "block"});
                        $("#KraverPdescription1").val(data['KraverPlatsbesokDescription']);
                    }

                    if (data['KontrollplanPBL'] == 1) {
                        $('#importSelect').val('KontrollplanPBL').trigger('change');
                    } else if (data['KontrollplanAvtal'] == 1) {
                        $('#importSelect').val('KontrollplanAvtal').trigger('change');
                    }

                    $('#EndastSignering').val(data['EndastSignering']).trigger('change');

                    $('#lawKa').summernote('code', data['LawText']);
                }
            });
        }

        function btnactive(e) {
            if ($('.btnstyle').hasClass('active')) {
                $('.btnstyle').removeClass('active');
            }
            $(e).addClass('active');

        }

        function btnactive1(e) {
            if ($('.btnstyle').hasClass('active')) {
                $('.btnstyle').removeClass('active');
            }
            $(e).addClass('active');
        }

        function filter(id) {
            $.ajax({
                type: 'get',
                url: '<?php echo e(route('searchCategory')); ?>',
                data: {'keywords': keywordCategory, 'id': id},
                success: function (data) {
                    $('#showrules1').html("");
                    $('#showrules1').html(data);
                }
            });
        }
        function filter1(id) {
            $.ajax({
                type: 'get',
                url: '<?php echo e(route('searchCategory')); ?>',
                data: {'keywords': keywordCategory, 'id': id},
                success: function (data) {
                    $('#showrulesKaAll').html("");
                    $('#showrulesKaAll').html(data);
                }
            });
        }

        function filter11(id) {
            $.ajax({
                type: 'get',
                url: '<?php echo e(route('ka.searchCategory')); ?>',
                data: {'keywords': keywordCategoryKa, 'id': id},
                success: function (data) {
                    $('#showrulesKa').html("");
                    $('#showrulesKa').html(data);
                }
            });
        }


        function getTemplateKontrollPlan(id) {
            getAllTemplate(project_id)
            // $.ajax({
            //     type: 'get',
            //     url: '/panel/design/getTemplateKontrollPlan/' + id,
            //     success: function (data) {
            //         // console.log(data)
            //         $('#user_table').DataTable().ajax.reload();
            //     }
            // });
        }


        function activaTab(tab) {
            $('.nav-tabs a[href="#' + tab + '"]').tab('show');
        };


        $(function () {
            // Summernote
            $('.textarea').summernote({
                height: 400,
            });


        });


        function getOrganization() {

        }

        function selectOrganizations() {
            $.ajax({
                type: 'get',
                url: '<?php echo e(route('getOrga')); ?>',
                success: function (data) {
                    $('#selectOrganization').html(data);
                }
            });
        }

        //TODO Complete Last
        function getInformationProject() {

            $.ajax({
                type: 'get',
                url: '/panel/project/information/getInformation/' + project_id,
                success: function (data) {
                    $('#li1').text('Projektnamn : ' + data['Projektnamn']);
                    $('#li2').text('Fastighetsbeteckning : ' + data['BNÄrendenr']);
                    $('#li3').text('BN-Diarienummer : ' + data['BNDiarienummer']);
                    $('#li4').text('Startbesked : ' + data['Startbesked']);
                    $('#li5').text('Slutbesked : ' + data['Slutbesked']);
                    $('#li6').text('byggnad : ' + data['byggnad']);
                    $('#li7').text('Grundkonstruktion : ' + data['Grundkonstruktion']);
                    $('#li8').text('Värmesystem : ' + data['Värmesystem']);
                    $('#li9').text('Ventilationssystem : ' + data['Ventilationssystem']);
                    $('#li10').text('Ventilationssystem : ' + data['BNÄrendenr']);
                    $('#li11').text('Entreprenadform : ' + data['Entreprenadform']);
                    // $('#li12').text('Strukturbeskrivning : '  + data['Strukturbeskrivning']);
                    $('#li13').text('Teknisktsamråd : ' + data['Teknisktsamråd']);
                    $('#li14').text('Angeupphandlingsform : ' + data['Angeupphandlingsform']);
                    $('#li15').text('Övriginformation : ' + data['Övriginformation']);
                }
            });
        }

        function getStep() {
            $.ajax({
                type: 'get',
                url: '<?php echo e(route('project.getStep')); ?>',
                data: {'project_id': project_id},
                success: function (data) {
                    var countActive = data;
                    $("input[name=stepproject][value=" + data + "]").prop('checked', true);
                    $(".step").removeClass("active");
                    var i = 1;
                    for (i; i <= countActive; i++) {
                        $("#step" + i).addClass("active");
                        $("#stepK" + i).addClass("active");
                    }

                }
            });
        }

        function select(e) {
            if ($('.liclick').hasClass('bluetext')) {
                $('.liclick').removeClass('bluetext')
            }
            $(e).addClass('bluetext');
        }

        function select2(e) {
            if ($('.liclick2').hasClass('bluetext')) {
                $('.liclick2').removeClass('bluetext')
            }
            $(e).addClass('bluetext');
        }

        function select3(e) {
            $('.liclick').removeClass('greentext');
            if ($('#controlplan' + e).hasClass('greentext')) {
                $('#controlplan' + e).removeClass('greentext')
            }
            $('#controlplan' + e).addClass('greentext');
        }

        function showinput(e) {
            var item = $(e).val();
            if (item == "500") {
                $('#riskdesdiv').css('display', 'none');
                $('.input-Other').show();
            } else if (item != "500" && item != "0") {
                $('.input-Other').hide();
                $('#riskdesdiv').css('display', 'block');
            } else if (item == 0) {
                $('.input-Other').hide();
                $('#riskdesdiv').css('display', 'none');
            }
        }

        function showinput2(e) {
            var item = $(e).val();
            if (item == "500") {
                $('#PIntydesdiv1').css('display', 'none');
                $('.input-Other2ka').show();
            } else if (item != "500" && item != "0") {
                $('.input-Other2ka').hide();
                $('#PIntydesdiv1').css('display', 'block');
            } else if (item == 0) {
                $('.input-Other2ka').hide();
                $('#PIntydesdiv1').css('display', 'none');
            }
        }

        function showinput3(e) {
            var item = $(e).val();
            if (item == "500") {
                $('#Vmetoddesdiv1').css('display', 'none');
                $('.input-Other3ka').show();
            } else if (item != "500" && item != "0") {
                $('.input-Other3ka').hide();
                $('#Vmetoddesdiv1').css('display', 'block');
            } else if (item == 0) {
                $('.input-Other3ka').hide();
                $('#Vmetoddesdiv1').css('display', 'none');
            }
        }

        function showinput4(e) {
            var item = $(e).val();
            if (item == "500") {
                $('#KraverPdesdiv').css('display', 'none');
                $('.input-Other4').show();
            } else if (item != "500" && item != "0") {
                $('.input-Other4').hide();
                $('#KraverPdesdiv').css('display', 'block');
            } else if (item == 0) {
                $('.input-Other4').hide();
                $('#KraverPdesdiv').css('display', 'none');
            }
        }

        function showinput5(e) {
            var item = $(e).val();
            if (item == "500") {
                $('.input-Other5').show();
            } else if (item != "500" && item != "0") {
                $('.input-Other5').hide();
            } else if (item == 0) {
                $('.input-Other5').hide();
            }
        }


        function showinputka(e) {
            var item = $(e).val();
            if (item == "500") {
                $('#riskdesdiv1').css('display', 'none');
                $('.input-Otherka').show();
            } else if (item != "500" && item != "0") {
                $('.input-Otherka').hide();
                $('#riskdesdiv1').css('display', 'block');
            } else if (item == 0) {
                $('.input-Otherka').hide();
                $('#riskdesdiv1').css('display', 'none');
            }
        }

        function showinput2ka(e) {
            var item = $(e).val();
            if (item == "500") {
                $('#PIntydesdiv1').css('display', 'none');
                $('.input-Other2ka').show();
            } else if (item != "500" && item != "0") {
                $('.input-Other2ka').hide();
                $('#PIntydesdiv1').css('display', 'block');
            } else if (item == 0) {
                $('.input-Other2ka').hide();
                $('#PIntydesdiv1').css('display', 'none');
            }
        }

        function showinput3ka(e) {
            var item = $(e).val();
            if (item == "500") {
                $('#Vmetoddesdiv1').css('display', 'none');
                $('.input-Other3ka').show();
            } else if (item != "500" && item != "0") {
                $('.input-Other3ka').hide();
                $('#Vmetoddesdiv1').css('display', 'block');
            } else if (item == 0) {
                $('.input-Other3ka').hide();
                $('#Vmetoddesdiv1').css('display', 'none');
            }
        }

        function showinput4ka(e) {
            var item = $(e).val();
            if (item == "500") {
                $('#KraverPdesdiv').css('display', 'none');
                $('.input-Other4ka').show();
            } else if (item != "500" && item != "0") {
                $('.input-Other4').hide();
                $('#KraverPdesdiv').css('display', 'block');
            } else if (item == 0) {
                $('.input-Other4').hide();
                $('#KraverPdesdiv').css('display', 'none');
            }
        }

        function showinput5ka(e) {
            var item = $(e).val();
            if (item == "500") {
                $('.input-Other5ka').show();
            } else if (item != "500" && item != "0") {
                $('.input-Other5ka').hide();
            } else if (item == 0) {
                $('.input-Other5ka').hide();
            }
        }


        function getAmaLaw(id) {
            $.ajax({
                type: 'get',
                url: '/panel/advance/get-law/' + id,
                success: function (data) {
                    console.log(data);
                    // $('#lawAMA').summernote('code',data['link']);
                    var FinalTextLaw = $('#lawAMA').val();
                    $("#lawAMA").summernote('code', '<p>' + FinalTextLaw + '<b>' + data['link'] + '</b></p>');
                    LawAma = data;
                    var search = $('#searchAmakeyword').val();
                    // var  context = $('.showtextama').text();
                    // $("div:contains(search)").each(function () {
                    //     $('.showtextama').html($('.showtextama').html().replace(search, '<span style="background:#fdf900">'+search+'</span>'));
                    // });
                    //
                    //
                    $(".showtextama").html(context.replace(search, '<span style="background:#fdf900">' + search + '</span>'));
                }
            });
        }

        function getTemplateForAdvance(id) {
            $.ajax({
                type: 'get',
                url: '/panel/advance/get-template/' + id,
                success: function (data) {
                    var controllPoint = $('#controllPoint');
                    controllPoint.html("");
                    controllPoint.append(data);
                }
            });
        }

        function agentControllAmaLaw(id) {
            $('#ama_law_table').DataTable({
                "fnDrawCallback": function (oSettings) {
                    if (oSettings._iDisplayLength > oSettings.fnRecordsDisplay()) {
                        $(oSettings.nTableWrapper).find('.dataTables_paginate').hide();
                    }
                },
                language: {
                    "sInfoEmpty": "Visar 0 To 0 From 0 kontrollpunkter",
                    "sInfoPostFix": "",
                    "sInfoThousands": ",",
                    "sLengthMenu": "Visa _MENU_ Kontrollpunkter",
                    "sSearch": "Sök",
                    "emptyTable": "Data saknas"
                },
                processing: true,
                serverSide: true,
                ajax: {
                    url: '/panel/amaLaw/agentControll/show/' + id,
                },
                columns: [
                    {
                        data: 'link',
                        name: 'länk'
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false
                    }
                ]
            });
        }

        function getAmaTemplate(id) {
            if (id != 0) {
                agentControllAmaLaw(id);
            } else {
                $('#ama_law_table').DataTable().clear().destroy();
            }

        }

        function deleteConfirmationAmaLaw(id) {
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            $.ajax({
                type: 'POST',
                url: "/panel/advance/design-template/delete/" + id,
                data: {_token: '<?php echo csrf_token(); ?>',},
                dataType: 'JSON',
                success: function (results) {
                    if (results.success === true) {
                        $('#showrulesAma').html("");
                        getTemplateKontrollPlan(project_id);
                        $('#ama_law_table').DataTable().ajax.reload();
                        Swal.fire(
                            'Raderad!',
                            'Länken har tagits bort.',
                            'success'
                        );
                    } else {
                        Swal.fire({
                            icon: 'fel',
                            title: 'Hoppsan...',
                            text: 'Något gick fel!',
                        })
                    }
                }
            });

            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            

        }

        function appendConfirmationAmaLaw(link) {
            var controllPoint = $('#controllPoint').val();
            if (controllPoint != 0) {
                $.ajax({
                    type: 'POST',
                    url: '<?php echo e(route('advance.store')); ?>',
                    data: {
                        '_token': $('input[name="_token"]').val(),
                        'amaLink': link,
                        'controllPoint': controllPoint,
                        'project_id': project_id,
                    },
                    success: function (data) {
                        if (data == 'success') {
                            Swal.fire(
                                'Bra jobbat !',
                                'En,Länk her laggts till vald egenkontroll',
                                'success');
                            getTemplateKontrollPlan(project_id);
                            $('#showrulesAma').html("");
                            $('#ama_law_table').DataTable().ajax.reload();
                        } else {
                            Swal.fire(
                                'Fel !',
                                'Du får lägga till 6 länkar.',
                                'error');
                        }

                    },
                    error: function (err) {

                    }
                });
            } else {
                Swal.fire(
                    'fel',
                    'Vald Design Fönster',
                    'error');
            }
        }

        function showMessageError() {
            Swal.fire({
                type: "warning",
                title: 'Hoppsan...',
                text: 'Funktionen är inte möjlig!',
            })
        }

        // function KontrollplanAvtal() {
        //     var table = $('#user_table').DataTable();
        //     table.ajax.url('/panel/design/filter/KontrollplanAvtal/' + project_id).load();
        //     table.draw();
        //     table.ajax.reload();
        // }

        // function KontrollplanPBL() {
        //     var table = $('#user_table').DataTable();
        //     table.ajax.url('/panel/design/filter/KontrollplanPBL/' + project_id).load();
        //     table.draw();
        //     table.ajax.reload();
        // }

        // function refresh() {
        //     var table = $('#user_table').DataTable();
        //     table.ajax.url('/panel/design/filter/refresh/' + project_id).load();
        //     table.draw();
        //     table.ajax.reload();
        // }


        function fileSelect(id) {
            var filename = $('#' + id).val().match(/[^\\/]*$/)[0];
            $('.shownameupload').text("");
            $('.shownameupload').text(filename);
        }



        function getAllTemplate(project_id)
        {
            $.ajax({
                type: 'get',
                url: '/panel/ka/getAllTemplate/' + project_id,
                success: function (response) {
                    $('#user_table tbody').html(response['data'])

                },
                error: function (err) {
                    console.log(err);
                }
            });
        }

    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('Admin.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\xampp\htdocs\askprojectNew\resources\views/Admin/project/controllePlan/index.blade.php ENDPATH**/ ?>