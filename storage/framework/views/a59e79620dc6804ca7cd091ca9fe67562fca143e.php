<div class="wrapper">
    <!-- Navbar -->
    <nav class="main-header navbar navbar-expand navbar-white navbar-light">
        <!-- Left navbar links -->
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
            </li>
            <li class="nav-item d-none d-sm-inline-block">
                <a href="<?php echo e(route('Panel')); ?>" class="nav-link">Hem</a>
            </li>
            <li class="nav-item d-none d-sm-inline-block">
                <a href="#" data-toggle="modal" data-target="#contact" class="nav-link">Kontakt</a>
            </li>
        </ul>
















        <!-- Right navbar links -->
        <ul class="navbar-nav ml-auto">
            <!-- Messages Dropdown Menu -->

            <!-- Notifications Dropdown Menu -->
            <li class="nav-item dropdown">
                <a class="nav-link" data-toggle="dropdown" href="#">
                    <i class="far fa-bell"></i>
                    <span class="badge badge-warning navbar-badge">15</span>
                </a>
                <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                    <span class="dropdown-item dropdown-header">15 Notifications</span>
                    <div class="dropdown-divider"></div>
                    <a href="#" class="dropdown-item">
                        <i class="fas fa-envelope mr-2"></i> 4 new messages
                        <span class="float-right text-muted text-sm">3 mins</span>
                    </a>
                    <div class="dropdown-divider"></div>
                    <a href="#" class="dropdown-item">
                        <i class="fas fa-users mr-2"></i> 8 friend requests
                        <span class="float-right text-muted text-sm">12 hours</span>
                    </a>
                    <div class="dropdown-divider"></div>
                    <a href="#" class="dropdown-item">
                        <i class="fas fa-file mr-2"></i> 3 new reports
                        <span class="float-right text-muted text-sm">2 days</span>
                    </a>
                    <div class="dropdown-divider"></div>
                    <a href="#" class="dropdown-item dropdown-footer">See All Notifications</a>
                </div>
            </li>
            <li class="nav-item">
                <form action="<?php echo e(route('logout')); ?>" method="post">
                    <?php echo csrf_field(); ?>
                    <button type="submit" class="nav-link" style="background: url('<?php echo e(asset('dist/img/logout.png')); ?>') no-repeat;background-size: 20px; border: none; margin-top: 7px">
                    </button>
                </form>

            </li>

        </ul>
    </nav>
    <!-- /.navbar -->

    <!-- Main Sidebar Container -->
    <aside class="main-sidebar sidebar-dark-primary elevation-4">
        <!-- Brand Logo -->
        <div class="brand-link">
            <img src="<?php echo e(asset('dist/img/white-logo.png')); ?>" alt="ProjectAsk Logo" class="brand-image">
        </div>

        <!-- Sidebar -->
        <div class="sidebar">
            <!-- Sidebar user panel (optional) -->
            <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                <div class="image">
                    <img src="<?php echo e(asset(auth()->user()->avatar)); ?>" class="img-circle elevation-2" alt="User Image">
                </div>
                <div class="info">
                    <span class="d-block"><?php echo e(auth()->user()->name); ?></span>
                </div>
            </div>

            <!-- Sidebar Menu -->
            <nav class="mt-2">
                <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                    <!-- Add icons to the links using the .nav-icon class
                         with font-awesome or any other icon font library -->
                    <li class="nav-item has-treeview menu-open">
                        <a href="<?php echo e(route('Panel')); ?>" class="nav-link">
                            <i class="fas fa-tachometer-alt"></i>
                            <p>
                                Panel
                            </p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="<?php echo e(route('project.index')); ?>" class="nav-link">
                            <i class="fa fa-file-powerpoint-o" aria-hidden="true"></i>
                            <p>
                               Pågående Projekt
                            </p>
                        </a>
                    </li>
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    

                    <li class="nav-item">
                        <a href="<?php echo e(route('templateCategory.index')); ?>" class="nav-link">
                            <i class="fas fa-list-ol"></i>
                            <p>Mallar</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="<?php echo e(route('template.index')); ?>" class="nav-link">
                            <i class="fa fa-folder"></i>
                            <p>Egna Mallar</p>
                        </a>
                    </li>






                    <li class="nav-item">
                        <a href="<?php echo e(route('supportTicket.all')); ?>" class="nav-link">
                            <i class="fas fa-ticket-alt"></i>
                            <p>Meddelanden</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="<?php echo e(route('Profile')); ?>" class="nav-link">
                            <i class="fa fa-user" aria-hidden="true"></i>
                            <p>
                                Konto
                            </p>
                        </a>
                    </li>









                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                </ul>
            </nav>
            <!-- /.sidebar-menu -->
        </div>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark"><?php echo $__env->yieldContent('content-title'); ?></h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="<?php echo e(route('Panel')); ?>">Hem</a></li>
                            <li class="breadcrumb-item active"><?php echo $__env->yieldContent('content-title'); ?></li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->
<?php /**PATH D:\xampp\htdocs\askprojectNew\resources\views/Admin/section/header.blade.php ENDPATH**/ ?>