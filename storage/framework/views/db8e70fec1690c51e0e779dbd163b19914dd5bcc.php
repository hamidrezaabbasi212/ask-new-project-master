<?php $__env->startSection('content-title', 'Pågående Projekt'); ?>
<?php $__env->startSection('title', 'Pågående Projekt'); ?>
<?php $__env->startSection('content'); ?>
    <div class="row mb-3">
        <div class="col">
            <a class="btn-createproject" href="<?php echo e(route('project.create')); ?>">Nytt Projekt</a>
        </div><!-- /.col -->
    </div>
    <div class="card">
        <div class="card-body p-0">
            <table class="table table-striped projects">
                <thead>
                <tr>
                    <th style="width: 1%">
                        #
                    </th>
                    <th style="width: 20%">
                        Projektnamn
                    </th>
                    <th style="width: 30%">
                        Organisation
                    </th>
                    <th>
                        Framsteg
                    </th>
                    <th style="width: 8%" class="text-center">
                        Status
                    </th>
                    <th style="text-align:center;width: 20%">
                        Drift
                    </th>
                </tr>
                </thead>
                <tbody>
                <?php $__currentLoopData = $projects; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row =>$project): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <?php $projectInfo = unserialize($project->projectInfo) ?>
                    <tr>
                        <td>
                            <?php echo e($row + 1); ?>

                        </td>
                        <td>
                            <a>
                                <?php echo e($projectInfo['Projektnamn']); ?>

                            </a>
                            <br>
                            <small>
                                <?php echo e($project['created_at']); ?>

                            </small>
                        </td>
                        <td>
                            <ul class="list-inline">
                                <li class="list-inline-item">
                                    <?php $__currentLoopData = $project->designs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $k =>$item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <?php if($item->assign_user != null): ?>
                                            <?php
                                                $user = \App\User::whereId($item->assign_user)->first();
                                                $organizationInfo = '';
                                                if($user)
                                                {
                                                    $avatar = $user->avatar;
                                                    $name = $user->name;
                                                    $organizationName = \App\User::where('id',$user->parent_id)->pluck('organizationName')->first();
                                                    $organizationInfo = $organizationName;
                                                }
                                            ?>
                                            <img alt="Avatar" class="table-avatar" src="<?php echo e(asset(isset($avatar) ? $avatar : '')); ?>" data-toggle="tooltip" data-placement="top" title="<?php echo e(isset($name) ? $name : '' .' '.'From as'.' '. (isset($organizationInfo) ? $organizationInfo : '')); ?>">
                                        <?php else: ?>
                                            <?php break; ?>
                                        <?php endif; ?>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </li>
                            </ul>
                        </td>
                        <td class="project_progress">
                            <div class="progress progress-sm">
                                <div class="progress-bar bg-green" role="progressbar"  aria-volumemin="0" aria-volumemax="100" style="width : <?php echo e(percent($project->step)); ?>">
                                </div>
                            </div>
                            <small>
                                <?php echo e(percent($project->step)); ?> Avklarat
                            </small>
                        </td>
                        <td class="project-state">
                            <span class="badge badge-success"><?php echo e(step($project->step)); ?></span>
                        </td>
                        <td class="project-actions">
                            <div class="box_action">
                                <a href="<?php echo e(route('project.controllPlan' , ['id'=>$project['id']])); ?>" title=""><i class="fa fa-edit" aria-hidden="true"></i></a>
                                <a href="#" onclick="deleteConfirmation(<?php echo e($project['id']); ?>)" title=""><i class="fa fa-times" aria-hidden="true"></i></a>
                                <?php if($project->owner_id !=null and $project->owner_id !=0 and Auth::user()->is_owner !=1): ?>
                                    <a href="#" onclick="deleteOwner(<?php echo e($project['id']); ?>)" title=""><i class="fa fa-user-alt-slash" aria-hidden="true"></i></a>
                                <?php endif; ?>
                            </div><!--box_action-->
                        </td>
                    </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </tbody>
            </table>
            <div class="text-center" style="width:15%;  margin: auto; margin-top: 20px !important;"><?php echo $projects->links(); ?></div>
        </div>
        <!-- /.card-body -->

        <?php
            function percent($value)
           {
               switch ($value) {
                   case "1":
                       echo "15%";
                       break;
                   case "2":
                       echo "30%";
                       break;
                   case "3":
                       echo "45%";
                       break;
                   case "4":
                       echo "60%";
                       break;
                   case "5":
                       echo "75%";
                       break;
                   case "6":
                       echo "85%";
                       break;
                   case "7":
                       echo "100%";
                       break;

                   default:
                       echo "Your favorite color is neither red, blue, nor green!";
               }
           }

       function step($value)
       {
                          switch ($value) {
                   case "1":
                       echo "Pågående projekt";
                       break;
                   case "2":
                       echo "Tekniskt samråd";
                       break;
                   case "3":
                       echo "Startbesked";
                       break;
                   case "4":
                       echo "Slutsamråd";
                       break;
                   case "5":
                       echo "Interimistiskt slutbesked";
                       break;
                   case "6":
                       echo "Slutbesked";
                       break;
                   case "7":
                       echo "Projekt slut";
                       break;

                   default:
                       echo "Your favorite color is neither red, blue, nor green!";
               }
       }
        ?>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('script'); ?>
    <script type="text/javascript">
        function deleteConfirmation(id) {
            Swal.fire({
                type: 'warning',
                title: 'Är du säker?',
                text: "Du kommer inte att kunna återställa detta!",
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                cancelButtonText: 'annullera',
                confirmButtonText: 'Ja, radera det!'
            }).then(function (e) {
                if (e.value === true) {
                    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

                    $.ajax({
                        type: 'POST',
                        url: "<?php echo e(route('project.delete')); ?>",
                        data: {_token: '<?php echo csrf_token(); ?>', id: id},
                        dataType: 'JSON',
                        success: function (results) {
                            if (results.success === true) {
                                Swal.fire(
                                    'Raderade!',
                                    'Projekt fil har tagits bort.',
                                    'success'
                                );
                            } else {
                                Swal.fire({
                                    icon: 'fel',
                                    title: 'Hoppsan...',
                                    text: 'Något gick fel!',
                                })
                            }
                            location.reload();
                        }
                    });
                }
                else {
                    e.dismiss;
                }}, function (dismiss) {
                return false;
            })

        }

        function deleteOwner(id) {
            Swal.fire({
                type: 'warning',
                title: 'Är du säker?',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                cancelButtonText: 'annullera',
                confirmButtonText: 'Ja, radera det!'
            }).then(function (e) {
                if (e.value === true) {
                    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

                    $.ajax({
                        type: 'POST',
                        url: "<?php echo e(route('project.ownerDelete')); ?>",
                        data: {_token: '<?php echo csrf_token(); ?>', id: id},
                        dataType: 'JSON',
                        success: function (results) {
                            if (results.success === true) {
                                Swal.fire(
                                    'Raderade!',
                                    'Kompanjonerna till byggnaden togs bort.',
                                    'success'
                                );
                            } else {
                                Swal.fire({
                                    icon: 'fel',
                                    title: 'Hoppsan...',
                                    text: 'Något gick fel!',
                                })
                            }
                            location.reload();
                        }
                    });
                }
                else {
                    e.dismiss;
                }}, function (dismiss) {
                return false;
            })

        }
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('Admin.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\xampp\htdocs\askprojectNew\resources\views/Admin/project/index.blade.php ENDPATH**/ ?>