<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <link rel="icon" type="image/png" href="<?php echo e(asset('dist/favicon.png')); ?>" />

    <title><?php echo $__env->yieldContent('title'); ?></title>

    <!-- Font Awesome Icons -->
    <link href="<?php echo e(asset('plugins/fontawesome-free/css/all.min.css')); ?>" rel="stylesheet" type="text/css">
    <!-- overlayScrollbars -->
    <link href="<?php echo e(asset('plugins/overlayScrollbars/css/OverlayScrollbars.min.css')); ?>" rel="stylesheet" type="text/css">
    <!-- Theme style -->
    <link href="<?php echo e(asset('dist/css/adminlte.min.css')); ?>" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="<?php echo e(asset('plugins/select2/css/select2.css')); ?>">

    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <link href="<?php echo e(asset('dist/css/style.css')); ?>" rel="stylesheet" type="text/css">
    <link href="<?php echo e(asset('css/sweetalert2.min.css')); ?>" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="<?php echo e(asset('plugins/summernote/summernote-bs4.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('plugins/datatables-bs4/css/dataTables.bootstrap4.css')); ?>">

    <!-- Select2 -->
    <link rel="stylesheet" href="<?php echo e(asset('plugins/select2/css/select2.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('plugins/datepicker/daterangepicker.css')); ?>">
    <?php echo $__env->yieldContent('style'); ?>
</head>
<body class="hold-transition sidebar-mini layout-fixed layout-navbar-fixed layout-footer-fixed">
    <?php echo $__env->make('Admin.section.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <section class="content">
        <div class="container-fluid">
             <?php echo $__env->yieldContent('content'); ?>
        </div>
    </section>
    <?php echo $__env->make('Admin.section.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
</body>
</html>
<?php /**PATH D:\xampp\htdocs\askprojectNew\resources\views/Admin/master.blade.php ENDPATH**/ ?>